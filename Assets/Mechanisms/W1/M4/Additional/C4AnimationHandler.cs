using JetBrains.Annotations;
using UnityEngine;

namespace Mechanisms.W1.M4.Additional
{
  public class C4AnimationHandler : MonoBehaviour
  {
    [SerializeField] private GameObject _wholeSphere;
    [SerializeField] private GameObject[] _slices;
    
    [UsedImplicitly]
    public void ToCut()
    {
      _wholeSphere.SetActive(false);
      /*foreach (GameObject slice in _slices)
        slice.SetActive(true);*/
    }

    [UsedImplicitly]
    public void ToWhole()
    {
      _wholeSphere.SetActive(true);
      /*foreach (GameObject slice in _slices)
        slice.SetActive(true);*/
    }
  }
}