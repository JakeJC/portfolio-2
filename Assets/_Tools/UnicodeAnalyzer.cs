using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using Sirenix.OdinInspector;
using UnityEngine;

namespace _Tools
{
  public class UnicodeAnalyzer : MonoBehaviour
  {
    public string Text;

    [Button]
    public void Analyze()
    {
      string text = Text;

      StringBuilder stringBuilder = new StringBuilder();
      List<int> characters = new List<int>();
      
      for (var i = 0; i < text.Length; i++)
      {
        UnicodeCategory unicodeCategory = Char.GetUnicodeCategory(text[i]);
        int i1 = text[i];
        
        if(characters.Contains(i1))
          continue;
        
        characters.Add(i1);
        
        string hexValue = i1.ToString("X");
        stringBuilder.Append($"{hexValue}");
        stringBuilder.Append(",");
      }
      
      Debug.Log(stringBuilder.ToString());
    }
  }
}