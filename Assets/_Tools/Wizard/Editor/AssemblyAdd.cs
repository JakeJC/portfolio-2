using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using JetBrains.Annotations;
using Sirenix.OdinInspector;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

public class AssemblyAdd : MonoBehaviour
{
  [SerializeField] private AssemblyDefinitionAsset _target;
  
  [Button]
  public void AddAll()
  {
    string guid = AssetDatabase.AssetPathToGUID(AssetDatabase.GetAssetPath(_target));
    List<string> findAssets = AssetDatabase.FindAssets("t:AssemblyDefinitionAsset").ToList();

    findAssets.Remove(guid);

    var asmdefAsset = JsonUtility.FromJson<AsmdefAsset>(_target.text);
    asmdefAsset.references = new string[findAssets.Count];
    
    for (int i = 0; i < findAssets.Count; i++) 
      asmdefAsset.references[i] = $"GUID:{findAssets[i]}";

    string json = JsonUtility.ToJson(asmdefAsset);
    string fullPath = Path.GetFullPath(AssetDatabase.GetAssetPath(_target));
    
    File.WriteAllText(fullPath, json);
    
    EditorUtility.SetDirty(_target);
    AssetDatabase.SaveAssets();
  }
  
  [UsedImplicitly]
  [Serializable]
  public class AsmdefAsset
  {
    public string name;
    public string rootNamespace;
    public string[] references;
    public string[] includePlatforms;
    public string[] excludePlatforms;
    public bool allowUnsafeCode;
    public bool overrideReferences;
    public string[] precompiledReferences;
    public bool autoReferenced;
    public string[] defineConstraints;
    public string[] versionDefines;
    public bool noEngineReferences;
  }
}