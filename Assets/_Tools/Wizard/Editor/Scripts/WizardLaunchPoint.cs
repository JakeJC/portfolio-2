using System.IO;
using System.Threading.Tasks;
using _Project.Scripts.MonoBehaviours.Game;
using _Project.Scripts.Tools.Editor;
using Sirenix.OdinInspector;
using UnityEditor;
using UnityEngine;

namespace _Tools.Wizard.Editor.Scripts
{
  [CreateAssetMenu(fileName = "Mechanism Wizard", menuName = "Mechanism Wizard/Create Mechanism Wizard", order = 0)]
  public class WizardLaunchPoint : ScriptableObject
  {
    public MechanismEnterData MechanismEnterData;
    
    public string TargetWorldId;
    public string TargetGroupId;

    public MechanismsRenderer MechanismsRenderer;

    private WizardLogic _wizardLogic;
    private PartsForMetaCreation _partsForMetaCreation;

    [Button]
    public void CreateMechanismStructure()
    {
      Clear();

      _wizardLogic = new WizardLogic(MechanismEnterData);
      _partsForMetaCreation = _wizardLogic.Execute();

      CreateMechanismMeta(TargetWorldId, TargetGroupId);
      
      RenderCover();
    }

    private void CreateMechanismMeta(string worldId, string groupId)
    {
      ResourcesCreator resourcesCreator = new ResourcesCreator(_wizardLogic.MechanismDataTargetFolderPath, worldId, groupId);
      resourcesCreator.CreateMetaResources(_partsForMetaCreation.PrefabInstance.GetComponentInChildren<Mechanism>(), _partsForMetaCreation.Parts);
    }

    private void RenderCover()
    {
      MechanismsRenderer.mechanismId = _partsForMetaCreation.PrefabInstance.GetComponent<Mechanism>().GetId();
      MechanismsRenderer.StartRender();
    }

    [Button]
    public void Clear()
    {
      string targetFolderPath = AssetDatabase.GetAssetPath(MechanismEnterData.mechanismData.TargetFolderPath);
      AssetDatabase.DeleteAsset(Path.Combine(targetFolderPath, RoughBaseCreator.CreationDirectoryName));
    }
  }
}