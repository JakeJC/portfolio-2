using _Project.Scripts.MonoBehaviours.Game;
using UnityEngine;

namespace _Tools.Wizard.Editor.Scripts
{
  public struct PartsForMetaCreation
  {
    public GameObject PrefabInstance;
    public PartMaterial[] Parts;
  }
}