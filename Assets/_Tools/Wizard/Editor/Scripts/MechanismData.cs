using System;
using System.Collections.Generic;
using _Project.Scripts.MonoBehaviours.Game;
using _Project.Scripts.Services.MaterialService.Data;
using UnityEngine;
using Object = UnityEngine.Object;

namespace _Tools.Wizard.Editor.Scripts
{
  [Serializable]
  public struct MechanismData
  {
    public Object ObjectPath;
    public Object TargetFolderPath;
    public Object[] ExcludeFromParts;
    public List<Object> Columns;
    public Vector3 AnchorLocalPosition;
    public string Name;
    public List<MaterialInfo> Materials;
    public Material ColumnMaterial;
  }
}