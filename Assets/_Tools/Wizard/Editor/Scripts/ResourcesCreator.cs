using System.IO;
using _Project.Scripts.MonoBehaviours.Game;
using UnityEditor;
using UnityEngine;

namespace _Tools.Wizard.Editor.Scripts
{
  public class ResourcesCreator
  {
    private const string DirectoryName = "Resources";
    private readonly string _targetFolder;
    
    private string _path;
    private readonly string _worldId;
    private readonly string _groupId;

    public ResourcesCreator(string targetFolder, string worldId, string groupId)
    {
      _groupId = groupId;
      _worldId = worldId;
      _targetFolder = targetFolder;
    }

    public void CreateMetaResources(Mechanism prefabInstance, PartMaterial[] parts)
    {
      AssetDatabase.CreateFolder(_targetFolder, DirectoryName);
      _path = Path.Combine(_targetFolder, DirectoryName);

      var mechanismData = ScriptableObject.CreateInstance<_Project.Scripts.Data.Scriptables.MechanismData>();
      mechanismData.MechanismId = prefabInstance.GetId();
      mechanismData.WorldId = _worldId;
      mechanismData.CoverAddress = $"M{prefabInstance.GetId()}";
      mechanismData.AssetAddress = prefabInstance.name;
      
      AssetDatabase.CreateAsset(mechanismData, Path.Combine(_path, $"{prefabInstance.name} Mechanism Data.asset"));
    }
  }
}