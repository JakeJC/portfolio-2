using System;
using System.Linq;
using UnityEngine;
using Object = UnityEngine.Object;

namespace _Tools.Wizard.Editor.Scripts
{
  [CreateAssetMenu(fileName = "Mechanism Creation Data", menuName = "Mechanism Wizard/Create Mechanism Creation Data", order = 0)]
  public class MechanismEnterData : ScriptableObject
  {
    public MechanismData mechanismData;

    private void OnValidate()
    {
      foreach (Object column in mechanismData.Columns)
      {
        if (mechanismData.ExcludeFromParts.Contains(column) == false)
        {
          Array.Resize(ref mechanismData.ExcludeFromParts, mechanismData.ExcludeFromParts.Length + 1);
          mechanismData.ExcludeFromParts[mechanismData.ExcludeFromParts.Length - 1] = column;
        }
      }
    }
  }
}