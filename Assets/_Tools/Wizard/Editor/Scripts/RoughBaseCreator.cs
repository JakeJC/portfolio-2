using System.IO;
using System.Linq;
using _Project.Scripts.MonoBehaviours.Game;
using _Project.Scripts.MonoBehaviours.Visual;
using UnityEditor;
using UnityEditor.AddressableAssets;
using UnityEditor.AddressableAssets.Settings;
using UnityEditor.Animations;
using UnityEngine;

namespace _Tools.Wizard.Editor.Scripts
{
  public class RoughBaseCreator
  {   
    public const string CreationDirectoryName = "Generated";
    private const string CanvasName = "Canvas";
    
    private GameObject _mechanism;
    private string _iconsFolder;
    private string _mechanismDataTargetFolderPath;

    public PartsForMetaCreation Execute(MechanismEnterData mechanismEnterData, out string creationFolder)
    {
      GameObject instantiatePrefab = CreateBaseForMechanism(mechanismEnterData);

      _mechanismDataTargetFolderPath = AssetDatabase.GetAssetPath(mechanismEnterData.mechanismData.TargetFolderPath);
      AssetDatabase.CreateFolder(_mechanismDataTargetFolderPath, CreationDirectoryName);
      _mechanismDataTargetFolderPath = Path.Combine(AssetDatabase.GetAssetPath(mechanismEnterData.mechanismData.TargetFolderPath), CreationDirectoryName);

      AddAnimator(instantiatePrefab, mechanismEnterData);
      
      PrepareForGamePartsCreation(instantiatePrefab);

      SetupParts(mechanismEnterData, instantiatePrefab);
      
      GameObject prefabAsset = SaveMechanismToAsset(out creationFolder);

      AssignMaterials(prefabAsset);
      MarkAsAddressable(AssetDatabase.GetAssetPath(prefabAsset));

      EditorUtility.SetDirty(prefabAsset);
      AssetDatabase.Refresh();
      AssetDatabase.SaveAssets();
      
      return new PartsForMetaCreation()
      {
        PrefabInstance = prefabAsset,
        Parts = prefabAsset.GetComponentsInChildren<PartMaterial>()
      };
    }
    
    private void MarkAsAddressable(string assetPath)
    {
      var settings = AddressableAssetSettingsDefaultObject.Settings;
      var group = settings.DefaultGroup;
      
      GUID guid = AssetDatabase.GUIDFromAssetPath(assetPath);
      string fileName = Path.GetFileNameWithoutExtension(assetPath);

      var entry = settings.CreateOrMoveEntry(guid.ToString(), group, readOnly: false, postEvent: false);
      entry.address = fileName;
      entry.labels.Add("Mechanism");

      settings.SetDirty(AddressableAssetSettings.ModificationEvent.EntryMoved, entry, true);
      
      AssetDatabase.SaveAssets();
    }

    private void AssignMaterials(GameObject prefabAsset)
    {
      var mechanism = prefabAsset.GetComponent<Mechanism>();

      foreach (GamePart gamePart in mechanism.GetMechanismParts())
      {
        if (mechanism.Materials == null) 
          continue;
        
        foreach (Renderer parts in gamePart.GetComponentsInChildren<Renderer>()) 
          parts.sharedMaterial = mechanism.Materials[0].Material;
        
        foreach (Renderer partsIcons in gamePart.IconPrefab.GetComponentsInChildren<Renderer>()) 
          partsIcons.sharedMaterial = mechanism.Materials[0].Material;
      }
    }

    private void SetupParts(MechanismEnterData mechanismEnterData, GameObject instantiatePrefab)
    {
      for (int i = 0; i < instantiatePrefab.transform.childCount; i++)
      {
        Transform baseTransform = instantiatePrefab.transform.GetChild(i);
        var transforms = baseTransform.GetComponentsInChildren<Renderer>().Select(p => p.transform);

        foreach (var transform in transforms)
        {
          if (mechanismEnterData.mechanismData.Columns.Any(p => p.name == transform.name))
          {
            transform.gameObject.AddComponent<ColumnColorProvider>();
            transform.gameObject.GetComponent<MeshRenderer>().material = mechanismEnterData.mechanismData.ColumnMaterial;
            continue;
          }

          if (mechanismEnterData.mechanismData.ExcludeFromParts.Any(p => p.name == transform.name))
            continue;

          GameObject icon = Object.Instantiate(transform.gameObject);

          SetupGameIcon(icon, transform);

          GameObject savedIcon = SaveGamePartIcon(icon, transform);

          SetupGamePart(transform, savedIcon);
        }
      }
    }

    private GameObject CreateBaseForMechanism(MechanismEnterData mechanismEnterData)
    {
      _mechanism = new GameObject {name = mechanismEnterData.mechanismData.Name};
      var instantiatePrefab = PrefabUtility.InstantiatePrefab(mechanismEnterData.mechanismData.ObjectPath) as GameObject;

      var anchor = new GameObject {name = "Anchor"};

      anchor.transform.SetParent(_mechanism.transform);
      anchor.transform.localPosition = mechanismEnterData.mechanismData.AnchorLocalPosition;

      instantiatePrefab.transform.SetParent(_mechanism.transform);

      var mechanism = _mechanism.AddComponent<Mechanism>();

      mechanism.BaseLevelAnchor = anchor.transform;
      mechanism.Id = GUID.Generate().ToString();
      
      mechanism.Materials = mechanismEnterData.mechanismData.Materials;

      return instantiatePrefab;
    }

    private void PrepareForGamePartsCreation(GameObject instantiatePrefab)
    {
      _iconsFolder = Path.Combine(_mechanismDataTargetFolderPath, "Icons");

      if (!AssetDatabase.IsValidFolder(_iconsFolder))
        AssetDatabase.CreateFolder(_mechanismDataTargetFolderPath, "Icons");

      PrefabUtility.UnpackPrefabInstance(instantiatePrefab, PrefabUnpackMode.Completely, InteractionMode.AutomatedAction);
    }

    private GameObject SaveGamePartIcon(GameObject icon, Transform transform)
    {
      GameObject savedIcon = PrefabUtility.SaveAsPrefabAsset(icon, Path.Combine(_iconsFolder, $"{transform.gameObject.name}.prefab"));
      Object.DestroyImmediate(icon);
      return savedIcon;
    }

    private static void SetupGamePart(Transform transform, GameObject savedIcon)
    {
      var gamePart = transform.gameObject.AddComponent<GamePart>();
      gamePart.IconPrefab = savedIcon;

      var partMaterial = transform.gameObject.AddComponent<PartMaterial>();
      partMaterial.UniqueId = GUID.Generate().ToString();
    }

    private static void SetupGameIcon(GameObject icon, Transform transform)
    {
      var gamePartIcon = icon.AddComponent<GamePartIcon>();

      gamePartIcon.setProgrammaticaly = true;
      gamePartIcon.WeightedScaleInsideBox = 0.5f;

      gamePartIcon.transform.localPosition = Vector3.zero;
      gamePartIcon.transform.localPosition -= 50 * Vector3.forward;
      gamePartIcon.transform.localRotation = transform.localRotation;

      Transform cachedParent = transform.parent;
      int cachedSibling = transform.GetSiblingIndex();

      transform.SetParent(GameObject.Find(CanvasName).transform);
      gamePartIcon.transform.localRotation = transform.localRotation;

      transform.SetParent(cachedParent);
      transform.SetSiblingIndex(cachedSibling);
    }

    private GameObject SaveMechanismToAsset(out string creationFolder)
    {
      GameObject prefabAsset = PrefabUtility.SaveAsPrefabAsset(_mechanism, Path.Combine(_mechanismDataTargetFolderPath,
        $"{_mechanism.name}.prefab"));

      Object.DestroyImmediate(_mechanism);

      creationFolder = _mechanismDataTargetFolderPath;
      return prefabAsset;
    }

    private void AddAnimator(GameObject partObject, MechanismEnterData mechanismEnterData)
    {
      Object[] assetsInObject = AssetDatabase.LoadAllAssetRepresentationsAtPath(AssetDatabase.GetAssetPath(mechanismEnterData.mechanismData.ObjectPath));
      Object[] animationClips = assetsInObject.Where(p => p as AnimationClip != null).ToArray();
      
      if (animationClips.Length > 0)
      {
        var animator = partObject.AddComponent<Animator>();

        string animatorPath = Path.Combine(_mechanismDataTargetFolderPath, $"{animator.name}.controller");

        var controller = AnimatorController.CreateAnimatorControllerAtPath(animatorPath);

        AnimatorState animatorState = controller.AddMotion(animationClips[0] as Motion);
        animatorState.name = "Start";
        animatorState.motion = null;

        controller.AddMotion(animationClips[0] as Motion).name = "Final";

        controller.layers[0].stateMachine.defaultState = animatorState;

        animator.runtimeAnimatorController = controller;
      }
    }
  }
}