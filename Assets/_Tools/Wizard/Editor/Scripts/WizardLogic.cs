using UnityEditor;
using UnityEngine;

namespace _Tools.Wizard.Editor.Scripts
{
  public class WizardLogic
  {   
    private readonly MechanismEnterData _mechanismEnterData;
    public string MechanismDataTargetFolderPath;

    public WizardLogic(MechanismEnterData mechanismEnterData) => 
      _mechanismEnterData = mechanismEnterData;

    public PartsForMetaCreation Execute()
    {
      ChangeModelImporter();
      
      var roughBaseCreator = new RoughBaseCreator();
      return roughBaseCreator.Execute(_mechanismEnterData, out MechanismDataTargetFolderPath);
    }

    private void ChangeModelImporter()
    {
      if (_mechanismEnterData.mechanismData.ObjectPath)
      {
        string assetPath = AssetDatabase.GetAssetPath(_mechanismEnterData.mechanismData.ObjectPath);
        AssetImporter importer = AssetImporter.GetAtPath(assetPath);

        if (importer)
        {
          ModelImporter modelImporter = importer as ModelImporter;

          if (modelImporter)
          {
            ModelImporterClipAnimation[] animations = modelImporter.defaultClipAnimations;

            if (modelImporter.isReadable == false)
              modelImporter.isReadable = true;

            foreach (ModelImporterClipAnimation anim in animations)
            {
              if (anim.loopTime == false)
                anim.loopTime = true;
            }

            modelImporter.clipAnimations = animations;
            Debug.Log("Imported fbx with custom settings.");

            EditorUtility.SetDirty(modelImporter);
            modelImporter.SaveAndReimport();
          }
        }
      }
    }
  }
}