using UnityEngine;
using UnityEditor;
using UnityEditor.Callbacks;
using System.Collections;
#if UNITY_IOS
using UnityEditor.iOS.Xcode;
#endif
using System.IO;
 
public class PostProcessIOSMeta
{

    [PostProcessBuild]
    public static void SetupPangleConvertionTracking(BuildTarget buildTarget, string pathToBuiltProject)
    {
#if UNITY_IOS

        if (buildTarget == BuildTarget.iOS)
        {

            // Get plist
            string plistPath = pathToBuiltProject + "/Info.plist";
            PlistDocument plist = new PlistDocument();
            plist.ReadFromString(File.ReadAllText(plistPath));

            // Get root
            PlistElementDict rootDict = plist.root;

            PlistElementArray array = rootDict.CreateArray("SKAdNetworkItems");

            PlistElementDict dict1 = array.AddDict();
            dict1.SetString("SKAdNetworkIdentifier", "238da6jt44.skadnetwork");

            PlistElementDict dict2 = array.AddDict();
            dict2.SetString("SKAdNetworkIdentifier", "22mmun2rn5.skadnetwork");

            // Write to file
            File.WriteAllText(plistPath, plist.WriteToString());
        }
#endif
    }
}