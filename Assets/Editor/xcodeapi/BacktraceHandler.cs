using System.IO;
using Newtonsoft.Json;
using UnityEngine;
using UnityEditor;
using UnityEditor.PackageManager;
using UnityEditor.PackageManager.Requests;

[InitializeOnLoad]
public class BacktraceHandler
{
  private const string BacktraceUrl = "https://github.com/backtrace-labs/backtrace-unity.git";
  static Request _request;

  static BacktraceHandler()
  {
    Debug.Log("Up and running BacktraceHandler");

#if UNITY_ANDROID
    Add();
#elif UNITY_IOS
    Remove();
#endif
  }

  static void Add()
  {
    // Add a package to the project
    _request = Client.Add(BacktraceUrl);
    EditorApplication.update += Progress;
  }

  static void Remove()
  {
    _request = Client.Remove("io.backtrace.unity");  
    EditorApplication.update += Progress;
  }
  
  static void Progress()
  {
    if (_request.IsCompleted)
    {
      if (_request.Status == StatusCode.Success)
        Debug.Log("Installed: " + _request.Status);
      else if (_request.Status >= StatusCode.Failure)
        Debug.Log(_request.Error.message);

      EditorApplication.update -= Progress;
    }
  }
}