﻿using System;
using System.IO;
using System.Linq;
using JetBrains.Annotations;
using UnityEditor;
using UnityEditor.AddressableAssets;
using UnityEditor.AddressableAssets.Settings;
using UnityEngine;


class BuildCommand
{
  [UsedImplicitly]
  static void PerformAndroidBuild() => 
    Build("");

  [UsedImplicitly]
  static void PerformAndroidDirectiveBuild()
  {    
    AddScriptingDefineSymbolsForGroup(BuildTargetGroup.Android);
    Build("_test");
  }

  private static void Build(string namePart)
  {
    EditorUserBuildSettings.development = false;
    EditorUserBuildSettings.allowDebugging = false;

    BuildAddresables(BuildTargetGroup.Android, BuildTarget.Android);

    string[] scenes = EditorBuildSettings.scenes.Select(p => p.path).ToArray();

    EditorUserBuildSettings.buildAppBundle = true;

    SetupArchitecture(GetArgument("light_build") == "True");

    string keyStoreFile = SetupKeystore();

    var name = GetArgument("customBuildName") + $"{namePart}.aab";
    var path = $"{Path.Combine(GetArgument("customBuildPath"), name)}";

    BuildPipeline.BuildPlayer(scenes, path, BuildTarget.Android, BuildOptions.None);

    File.Delete(keyStoreFile);
  }

  [UsedImplicitly]
  static void PerformWindowsBuild()
  {
    EditorUserBuildSettings.development = true;
    EditorUserBuildSettings.allowDebugging = true;
  
    BuildAddresables(BuildTargetGroup.Standalone, BuildTarget.StandaloneWindows64);
    
    string[] scenes = EditorBuildSettings.scenes.Select(p => p.path).ToArray();
    
    var name = GetArgument("customBuildName") + ".exe";
    var path = $"{Path.Combine(GetArgument("customBuildPath"), name)}";

    AddScriptingDefineSymbolsForGroup(BuildTargetGroup.Standalone);

    BuildPipeline.BuildPlayer(scenes, path, BuildTarget.StandaloneWindows64, BuildOptions.Development);
  }

  private static void AddScriptingDefineSymbolsForGroup(BuildTargetGroup targetGroup)
  {
    var defines = GetArgument("extra_define_symbols");
    string scriptingDefineSymbolsForGroup = PlayerSettings.GetScriptingDefineSymbolsForGroup(targetGroup);
    scriptingDefineSymbolsForGroup += defines;
    PlayerSettings.SetScriptingDefineSymbolsForGroup(targetGroup, scriptingDefineSymbolsForGroup);
  }
  
  private static string SetupKeystore()
  {
    PlayerSettings.Android.keyaliasName = GetArgument("alias");
    PlayerSettings.Android.keyaliasPass = GetArgument("password");
    PlayerSettings.Android.keystorePass = GetArgument("password");

    string key = File.ReadAllText(GetArgument("keystorePath"));
    Debug.Log($"[Key] {key}");
    
    string keyStoreFile = CreateKeyStoreFile(GetArgument("customBuildPath"), key);

    PlayerSettings.Android.keystoreName = keyStoreFile;

    PlayerSettings.Android.useCustomKeystore = true;
    return keyStoreFile;
  }

  static void BuildAddresables(BuildTargetGroup buildTargetGroup, BuildTarget buildTarget)
  {
    EditorUserBuildSettings.SwitchActiveBuildTarget(buildTargetGroup, buildTarget);
    AddressableAssetSettings.BuildPlayerContent();
  }

  private static void SetupArchitecture(bool light)
  {
    if (light)
    {
      PlayerSettings.Android.targetArchitectures = AndroidArchitecture.ARMv7;
      PlayerSettings.SetScriptingBackend(BuildTargetGroup.Android, ScriptingImplementation.Mono2x);
    }
    else
    {
      PlayerSettings.Android.targetArchitectures = (AndroidArchitecture) 3;
      PlayerSettings.SetScriptingBackend(BuildTargetGroup.Android, ScriptingImplementation.IL2CPP);
    }
  }

  static string CreateKeyStoreFile(string path, string value)
  {
    Directory.CreateDirectory(path);
    string keyStoreFile = Path.Combine(path, "key.keystore");
    File.WriteAllBytes(keyStoreFile, Convert.FromBase64String(value));
    
    return keyStoreFile;
  }

  static string GetArgument(string argumentName)
  {
    string[] args = System.Environment.GetCommandLineArgs();
    string input = "";
    
    for (int i = 0; i < args.Length; i++)
    {
      if (args[i].Contains(argumentName))
      {
        input = args[i + 1];
        Debug.Log($"ARG {args[i]} Return: {args[i + 1]}");
        return input;
      }
    }

    return "";
  }
}