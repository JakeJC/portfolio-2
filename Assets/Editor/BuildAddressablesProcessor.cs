// using UnityEditor;
// using UnityEditor.AddressableAssets;
// using UnityEditor.AddressableAssets.Settings;
// using UnityEditor.Build;
// using UnityEditor.Build.Reporting;
// using UnityEngine;
//
// class BuildAddressablesProcessor : IPreprocessBuildWithReport
// {
//   public int callbackOrder => 
//     0;
//
//   static void BuildAddresables()
//   {
//     EditorUserBuildSettings.SwitchActiveBuildTarget(BuildTargetGroup.Android, BuildTarget.Android);
//
//     Debug.Log("BuildAddressablesProcessor.PreExport start");
//     AddressableAssetSettings.CleanPlayerContent(AddressableAssetSettingsDefaultObject.Settings.ActivePlayerDataBuilder);
//     AddressableAssetSettings.BuildPlayerContent();
//     Debug.Log("BuildAddressablesProcessor.PreExport done");
//   }
//
//   public void OnPreprocessBuild(BuildReport report) => 
//     BuildAddresables();
// }