using System.Collections.Generic;
using System.Linq;
using FluentAssertions;
using NUnit.Framework;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Tests.EditMode
{
	public class ValidateMissingComponentsTest
	{
		[TestCaseSource(nameof(AllScenesPaths))]
		public void ShouldNotHaveMissingComponentsAtScenes(string scenePath)
		{
			Scene scene = EditorSceneManager.OpenScene(scenePath, OpenSceneMode.Additive);

			List<string> gameObjectsWithMissingComponents =
				AllGameObjectsAt(scene)
					.Where(HasMissingScripts)
					.GroupBy(gameObject => gameObject.name)
					.Select(group => $"{group.Key} ({group.Count()})")
					.ToList();

			gameObjectsWithMissingComponents.Should().BeEmpty();

			EditorSceneManager.CloseScene(scene, true);
		}

		[TestCaseSource(nameof(AllScenesPaths))]
		public void ShouldNotHaveBrokenPrefabsConnections(string scenePath)
		{
			Scene scene = EditorSceneManager.OpenScene(scenePath, OpenSceneMode.Additive);

			List<string> prefabsWithBrokenConnections =
				AllPrefabObjectsAt(scene)
					.Where(PrefabHasBrokenConnections)
					.GroupBy(gameObject => gameObject.name)
					.Select(PrintGroupObjectsInfoByCount)
					.ToList();

			prefabsWithBrokenConnections.Should().BeEmpty();

			EditorSceneManager.CloseScene(scene, true);

			string PrintGroupObjectsInfoByCount(IGrouping<string, GameObject> group) =>
				group.Count() > 1?
					$"{group.Key} ({group.Count()})" :
					group.Key;
		}

		[Test]
		public void ShouldNotHaveMissingComponentsInPrefabs()
		{
			IEnumerable<string> prefabsWithMissingComponents =
				AllProjectPrefabObjects()
					.Where(HasMissingScripts)
					.Select(gameObject => gameObject.name);

			prefabsWithMissingComponents.Should().BeEmpty();
		}

		private static bool PrefabHasBrokenConnections(GameObject gameObject) =>
			PrefabUtility.IsPrefabAssetMissing(gameObject);

		private static bool HasMissingScripts(GameObject obj) =>
			GameObjectUtility.GetMonoBehavioursWithMissingScriptCount(obj) > 0;

		private static IEnumerable<string> AllScenesPaths() =>
			AssetDatabase
				.FindAssets("t:Scene", new[] {"Assets"})
				.Select(AssetDatabase.GUIDToAssetPath);

		private static IEnumerable<GameObject> AllProjectPrefabObjects()
		{
			IEnumerable<GameObject> prefabs = GetAllPrefabsObjectsFromAssets();

			var queue = new Queue<GameObject>(prefabs);

			while (queue.Count != 0)
			{
				GameObject obj = queue.Dequeue();
				yield return obj;

				Transform parent = obj.transform;
				foreach (Transform child in parent)
					queue.Enqueue(child.gameObject);
			}

			IEnumerable<GameObject> GetAllPrefabsObjectsFromAssets() =>
				AssetDatabase
					.FindAssets("t:Prefab", new[] {"Assets"})
					.Select(p => AssetDatabase.LoadAssetAtPath<GameObject>(AssetDatabase.GUIDToAssetPath(p)));
		}

		private static IEnumerable<GameObject> AllGameObjectsAt(Scene scene)
		{
			var queue = new Queue<GameObject>(scene.GetRootGameObjects());

			while (queue.Count != 0)
			{
				GameObject obj = queue.Dequeue();
				yield return obj;

				Transform parent = obj.transform;
				foreach (Transform child in parent)
					queue.Enqueue(child.gameObject);
			}
		}

		private static IEnumerable<GameObject> AllPrefabObjectsAt(Scene scene)
		{
			var queue = new Queue<GameObject>(scene.GetRootGameObjects());

			while (queue.Count != 0)
			{
				GameObject obj = queue.Dequeue();

				if (PrefabUtility.IsPartOfAnyPrefab(obj))
					yield return obj;
				else
				{
					Transform parent = obj.transform;
					foreach (Transform child in parent)
						queue.Enqueue(child.gameObject);
				}
			}
		}
	}
}
