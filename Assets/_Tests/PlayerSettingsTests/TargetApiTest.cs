using NUnit.Framework;
using UnityEditor;

namespace _Tests.PlayerSettingsTests
{
  public class TargetApiTest
  {
    [Test]
    public void TestTargetApi()
    {
      AndroidSdkVersions androidSdkVersions = PlayerSettings.Android.targetSdkVersion;
      Assert.IsTrue(androidSdkVersions >= AndroidSdkVersions.AndroidApiLevel30);
    }
  }
}