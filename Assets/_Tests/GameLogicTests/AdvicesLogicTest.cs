using System.Collections.Generic;
using System.Linq;
using _Project.Scripts.Data.Serializable;
using _Project.Scripts.Logic.GameLogic.Advices;
using _Project.Scripts.Logic.GameLogic.Advices.Interfaces;
using _Project.Scripts.Services._Interfaces;
using _Project.Scripts.Services.AssetProvider;
using NUnit.Framework;
using UnityEngine;

namespace _Tests.GameLogicTests
{
  public class AdvicesLogicTest
  {
    private const string AdvicesConfigsPath = "_Advices Configs";
    
    private IAdvices _advices;

    [SetUp]
    public void PrepareTest()
    {
      PlayerPrefs.DeleteKey("TestGameSaveData.1");
      _advices = new Advices(new AssetProvider(), new FakeGameSaveSystem());
    }

    [Test]
    public void WhenOpenAdvicesInSequence_ThenReturnCorrectOrderedAdvice()
    {
      foreach (string adviceId in GetAdviceId())
      {
        AdviceData openNextAdviceNext = _advices.OpenNextAdvice();
        AdviceData openNextAdviceGet = _advices.GetAdvice();

        Assert.AreEqual(adviceId, openNextAdviceNext.Id);
        Assert.AreEqual(adviceId, openNextAdviceGet.Id);
      }
    }
    
    [Test]
    public void WhenOpenAdvicesInSequence_ThenReturnCorrectAdvicesCollection()
    {
      foreach (List<string> advicesIds in GetExtendedAdvicesIds())
      {
        _advices.OpenNextAdvice();
        List<string> openedAdvices = _advices.GetOpenedAdvices().Select(p => p.Id).ToList();

        Assert.AreEqual(advicesIds, openedAdvices);
      }
    }
    
    private static IEnumerable<string> GetAdviceId()
    {
      List<AdviceData> adviceData = new AssetProvider().GetAllResources<AdviceData>(AdvicesConfigsPath).ToList();
      adviceData.AddRange(adviceData);
      
      foreach (AdviceData data in adviceData)
        yield return data.Id;
    }
    
    private static IEnumerable<List<string>> GetExtendedAdvicesIds()
    {
      AdviceData[] advices = new AssetProvider().GetAllResources<AdviceData>(AdvicesConfigsPath);
      List<string> advicesIds = new List<string>();

      foreach (AdviceData data in advices)
      {
        advicesIds.Add(data.Id);
        yield return advicesIds;
      }
    }
  }

  public class FakeGameSaveSystem : IGameSaveSystem
  {
    private const string SaveKey = "TestGameSaveData.1";

    private GameSaveData _gameSaveData;

    public GameSaveData Get() =>
      _gameSaveData ??= new GameSaveData();

    public void Save() =>
      PlayerPrefs.SetString(SaveKey, JsonUtility.ToJson(_gameSaveData));
  }
}