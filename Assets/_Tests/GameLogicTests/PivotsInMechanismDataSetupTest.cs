using System.Linq;
using System.Text;
using _Project.Scripts.Data.Scriptables;
using _Project.Scripts.Services._Interfaces;
using _Project.Scripts.Services.AssetProvider;
using NUnit.Framework;

namespace _Tests.GameLogicTests
{
  public class PivotsInMechanismDataSetupTest
  {
    private const string MechanismDataPath = "_Mechanism Configs";
    
    private IAssetProvider _assetProvider;
    private MechanismData[] _mechanismData;

    [SetUp]
    public void PrepareTest() => 
      _assetProvider = new AssetProvider();
      
    [Test]
    public void PivotsInMechanismDataSetupTestPasses()
    {
      _mechanismData = _assetProvider.GetAllResources<MechanismData>(MechanismDataPath);
      
      if (_mechanismData.All(p => p.InterviewYValue > 0))
        Assert.Pass("All mechanism must have interview Y value in MechanismData");
      else
      {
        var mechanismData = _mechanismData.Where(p => p.InterviewYValue <= 0);

        StringBuilder stringBuilder = new StringBuilder();
        
        stringBuilder.AppendLine("Next mechanisms has unhandled Y pivot value in MechanismData");
        foreach (MechanismData data in mechanismData) 
          stringBuilder.AppendLine(data.name);
        
        Assert.Fail(stringBuilder.ToString());
      }
    }
  }
}