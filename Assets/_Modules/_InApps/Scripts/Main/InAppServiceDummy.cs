using System;
using System.Collections.Generic;
using System.Linq;
using _Modules._InApps.Interfaces;
using UnityEngine;
using UnityEngine.Purchasing;

namespace _Modules._InApps.Scripts.Main
{
  public class InAppServiceDummy : IInAppService
  {
    private IStoreController _controller;
    private readonly ConfigurationBuilder _builder;

    private readonly List<IInAppCallback> _inAppCallbacks = new List<IInAppCallback>();

    public event Action OnInitializedEvent;

    public event Action OnInitializeFailEvent;

    private bool _isInitialized;

        public void Initialize()
        {
            _isInitialized = true;

            FindNotPurchasedProducts();

            OnInitializedEvent?.Invoke();

            Debug.Log("[InApp] Initialized");
        }

        public void Purchase(string productId) =>
      Debug.Log($"[InApp] Purchase {productId}");

    public void AddProduct(Product productInfo) => 
      Debug.Log($"[InApp] AddProduct {productInfo}");

    public bool IsProductInitialized(string productId) => 
      true;

    public bool IsInAppsInitialized()
    {
      Debug.Log("[InApp] IsInAppsInitialized Called");
      return _isInitialized;
    }

    public string GetPrice(string productId) => 
      "Dummy";

    public float GetPriceFloat(string productId)=> 
      0;

    public void AddInAppCallback(IInAppCallback inAppCallback) =>
      _inAppCallbacks.Add(inAppCallback);

    public void OnInitializeFailed(InitializationFailureReason error)
    {
      OnInitializeFailEvent?.Invoke();

      Debug.Log("[InApp] Initialize Fail");

      foreach (IInAppCallback inAppCallback in _inAppCallbacks)
        inAppCallback.OnNotPurchased();
    }
    
    public bool IsProductPurchased(string id) => 
      false;

    public bool IsSubscriptionPurchased() => 
      false;

    public void RestorePurchases() => 
      Debug.Log("[InApp] Restore");

    private void FindNotPurchasedProducts()
    {
      foreach (IInAppCallback inAppCallback in _inAppCallbacks)
      {
        if (!IsProductPurchased(inAppCallback.ProductsId.ToString()))
          inAppCallback.OnNotPurchased();
      }
    }
  }
}