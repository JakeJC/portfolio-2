using System;
using System.Collections.Generic;
using AppacheAnalytics.Scripts.Interfaces;
using UnityEngine;

namespace _Modules._Analytics
{
  public class AnalyticsStandaloneStub : IAnalytics
  {
    public bool IsFirstLaunch { get; set; }
    public event Action OnInitialize;

    public void Initialize() => 
      OnInitialize?.Invoke();

    public void Send(string s, Dictionary<string, object> parameters, bool withoutBaseParameters = false) => 
      Debug.Log($"[Standalone Fake Event] {s}");

    public void Send(string s, bool withoutBaseParameters = false) => 
      Debug.Log($"[Standalone Fake Event] {s}");

    public bool ShouldSendSessionStart() => 
      false;
  }
}