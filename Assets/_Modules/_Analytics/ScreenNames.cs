namespace _Modules._Analytics
{
  public static class ScreenNames
  {
    public const string Meditation = "meditation";
    public const string Splash = "splash";
    public const string Privacy = "privacy";
    public const string Phones = "headphones";
    public const string Interview = "interview";
    public const string Game = "game";
    public const string Gallery = "gallery";
    public const string Result = "result";
    public const string Wait = "wait";
    public const string InterviewSubscription = "interview_subscription";
    public const string Subscription = "subscription";
    public const string NoConnection = "no_connection";
    public const string Choose = "choose";
  }
}