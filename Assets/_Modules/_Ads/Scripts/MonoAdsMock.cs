using System;
using System.Collections;
using System.Timers;
using TMPro;
using UnityEngine;

namespace _Modules._Ads.Scripts
{
  public class MonoAdsMock : MonoBehaviour
  {
    [SerializeField] private TextMeshProUGUI timer;
    [SerializeField] private int timerRemain = 5;
    private Action _onEnd;

    public void Show(Action onEnd)
    {
      _onEnd = onEnd;
      StartCoroutine(Timer());
    }
    
    private IEnumerator Timer()
    {
      Time.timeScale = 0;
      
      var waitForSecondsRealtime = new WaitForSecondsRealtime(1);
      int time = timerRemain;
      
      timer.text = time.ToString();

      while (time > 0)
      {
        yield return waitForSecondsRealtime;
        time--;    
        timer.text = time.ToString();
      }   
      
      Time.timeScale = 1;
      timer.text = time.ToString();
      
      _onEnd?.Invoke();
    }
  }
}