using System;
using UnityEngine;
using Object = UnityEngine.Object;

namespace _Modules._Ads.Scripts
{
  public class MockAdsLogic
  {
    private MonoAdsMock _monoAdsMock;

    public void Show(Action onEnd)
    {
      Spawn();
      _monoAdsMock.Show(() =>
      {
        onEnd?.Invoke();
        Clear();
      });
    }

    private void Spawn()
    {
      var adsPrefab = Resources.Load<GameObject>("Ads/Mock Ads");
      _monoAdsMock = Object.Instantiate(adsPrefab).GetComponent<MonoAdsMock>();
    }

    private void Clear()
    {
      if(_monoAdsMock)
        Object.Destroy(_monoAdsMock.gameObject);
    }
  }
}
