using System.Collections;
using _Project.Scripts.Services._Interfaces;
using LeanTouch.Examples.Scripts;
using UnityEngine;

namespace _Project.Scripts.Services.LeanTouchCamera.Scripts.Logic
{
  public class AutoRotation : MonoBehaviour
  {
    private const string ClickPanelPath = "UI/Common/Click Panel";
    private const string Canvas = "Canvas";

    private const int Speed = 7;
    
    private LeanPitchYawSmooth _leanPitchYawSmooth;
    private Coroutine _rotate;
    private IAssetProvider _assetProvider;
    
    private ClickPanel _clickPanel;

    public void Construct(IAssetProvider assetProvider, LeanPitchYawSmooth leanPitchYawSmooth)
    {
      _assetProvider = assetProvider;
      _leanPitchYawSmooth = leanPitchYawSmooth;
      _rotate = StartCoroutine(Rotate());

      Transform parent = GameObject.Find(Canvas).transform;
      _clickPanel = Instantiate(_assetProvider.GetResource<ClickPanel>(ClickPanelPath), parent);
      _clickPanel.transform.SetAsFirstSibling();
      _clickPanel.Construct(Stop);
    }

    private IEnumerator Rotate()
    {       
      var waitForEndOfFrame = new WaitForFixedUpdate();
      float targetYaw = _leanPitchYawSmooth.Yaw + 360;

      while (_leanPitchYawSmooth.Yaw <= targetYaw)
      {
        _leanPitchYawSmooth.Yaw += Speed * Time.fixedDeltaTime;
        yield return waitForEndOfFrame;
      }

      _rotate = null;
      Destroy(this);
    }

    public void Stop()
    {
      if(_rotate != null)
        StopCoroutine(_rotate);

      _rotate = null;
      Destroy(this);
      
      if(_clickPanel)
        Destroy(_clickPanel.gameObject);
    }
  }
}