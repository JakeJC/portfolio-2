﻿using UnityEngine;

namespace _Project.Scripts.Services.LeanTouchCamera.Scripts.Logic
{
  public class CameraPoints : MonoBehaviour
  {
    public Transform Start;
    public Transform End;
  }
}