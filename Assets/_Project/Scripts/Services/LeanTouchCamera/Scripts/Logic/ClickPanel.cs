using System;
using UnityEngine;
using UnityEngine.EventSystems;

namespace _Project.Scripts.Services.LeanTouchCamera.Scripts.Logic
{
  public class ClickPanel : MonoBehaviour, IPointerDownHandler
  {
    private Action _onPress;

    public void Construct(Action onPress) => 
      _onPress = onPress;

    public void OnPointerDown(PointerEventData eventData) => 
      _onPress?.Invoke();
  }
}