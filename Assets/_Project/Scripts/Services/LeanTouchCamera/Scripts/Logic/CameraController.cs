﻿using System;
using _Project.Scripts.Data.Serializable;
using _Project.Scripts.Services._Interfaces;
using _Project.Scripts.Services.LeanTouchCamera.Scripts.Interfaces;
using DG.Tweening;
using Lean.Touch;
using LeanTouch.Examples.Scripts;
using UnityEngine;
using Object = UnityEngine.Object;

namespace _Project.Scripts.Services.LeanTouchCamera.Scripts.Logic
{
  public class CameraController : ICameraController, IDisposable
  {
    private const string CameraPivot = "Camera Pivot";
    private const string LeanTouch = "LeanTouch";

    private const float ZoomMin = 1.7f;
    private const int RotationPitchAngleMin = 15;
    private const int RotationPitchAngleMax = 30;
    private const float RotationSensitivity = 0.18f;
    private const float DefaultDampening = 3f;
    
    private readonly LeanPitchYawSmooth _rotation;
    private readonly LeanCameraZoomSmooth _zoom;
    private readonly LeanCameraMoveSmooth _move;
    private readonly LeanConstrainToBox _constraint;
    private readonly Lean.Touch.LeanTouch _leanTouch;

    private readonly Camera _camera;
    private readonly GameObject _pivot;
    
    private float _originalZoom;
    private float _originalYaw;
    private float _originalPitch;

    private bool _initializeOnEnableCall;

    private float _cachedPositionY;
    private AutoRotation _autoRotation;

    public LeanCameraZoomSmooth Zoom =>
      _zoom;

    public LeanPitchYawSmooth Rotation => 
      _rotation;

    private readonly IAssetProvider _assetProvider;
    
    public CameraController(IAssetProvider assetProvider)
    {
      _assetProvider = assetProvider;
      
      _camera = Camera.main;
      _pivot = GameObject.Find(CameraPivot);

      _leanTouch = Object.Instantiate(assetProvider.GetResource<GameObject>(LeanTouch)).GetComponent<Lean.Touch.LeanTouch>();
      
      _rotation = _pivot.AddComponent<LeanPitchYawSmooth>();
      _zoom = _camera.gameObject.AddComponent<LeanCameraZoomSmooth>();
      _move = _pivot.AddComponent<LeanCameraMoveSmooth>();
      _constraint = _pivot.AddComponent<LeanConstrainToBox>();
      
      _rotation.MainComponent = _leanTouch;
      _zoom.MainComponent = _leanTouch;
      _move.MainComponent = _leanTouch;
      
      EnableComponents(false);
    }

    private void Initialize()
    {
      ClearAutoRotation();
      
      _rotation.Dampening = DefaultDampening;
      
      if (_initializeOnEnableCall)
        return;
      
      RotationSetup();
      ZoomSetup();
      MoveSetup();
      
      _initializeOnEnableCall = true;

      SaveOriginalPosition();
    }

    public void InInterview(bool isInInterview, float yValue, Action onInterviewEnd)
    {
      if (isInInterview)
      {
        _cachedPositionY = _pivot.transform.position.y;
        _pivot.transform.DOMoveY(yValue, 1f);
      }
      else
        _pivot.transform.DOMoveY(_cachedPositionY, 1f).onComplete += () => 
          onInterviewEnd?.Invoke();
    }

    public void SwitchToGame()
    {
      EnableComponents(true);
      DisableWorldMove();
      Initialize();
    }

    public void ToBaseState(bool relative = false)
    {
      _zoom.Zoom = _originalZoom;
      _rotation.Pitch = _originalPitch;

      if (relative)
      {
        _rotation.Yaw = ClosestAngle();
      }
      else
      {
        _rotation.Yaw = _originalYaw;
      }

      _zoom.IgnoreGuiFingers = false;
    }

    public void SaveOriginalPosition()
    {
      _originalZoom = _zoom.Zoom;
      _originalPitch = _rotation.Pitch;
      _originalYaw = _rotation.Yaw;
    }

    public void PrepareToGameSwitch()
    {
      _constraint.enabled = false;
      DisableWorldMove();
    }

    public void SetCameraData(CameraData cameraData)
    {
      if(cameraData.Size != 0)
        _camera.orthographicSize = cameraData.Size;

      _pivot.transform.position = cameraData.Position;
    }

    public bool GetBeezyInput() =>
      _rotation.IsBeezy || _zoom.IsBeezy || _move.IsBeezy;

    public void DisableRotation() =>
      _rotation.enabled = false;

    public void EnableRotation() => 
      _rotation.enabled = true;

    public void Dispose()
    {
      Object.Destroy(_zoom);
      Object.Destroy(_rotation);
      Object.Destroy(_move);
      Object.Destroy(_constraint);
      Object.Destroy(_leanTouch.gameObject);
    }

    public void FinishingRotation()
    {    
      _autoRotation = _pivot.AddComponent<AutoRotation>();
      _autoRotation.Construct(_assetProvider, _rotation);
    }

    private float ClosestAngle()
    {
      if (_rotation.Yaw > 0)
      {
        int rotateCount = (int) _rotation.Yaw / 360;
        return rotateCount * 360 + _originalYaw;
      }
      else
      {
        int rotateCount = (int) _rotation.Yaw / 360;
        return rotateCount * 360 - (360 -_originalYaw);
      }
    }

    private void DisableWorldMove()
    {
      _move.enabled = false;
      _move.IsBeezy = false;
    }

    private void ClearAutoRotation()
    {
      if (!_autoRotation)
        return;
      
      _autoRotation.Stop();
      Object.Destroy(_autoRotation);
    }

    private void RotationSetup()
    {
      _rotation.PitchClamp = true;
      _rotation.PitchMin = RotationPitchAngleMin;
      _rotation.PitchMax = RotationPitchAngleMax;

      _rotation.PitchSensitivity = RotationSensitivity;
      _rotation.YawSensitivity = RotationSensitivity;
    }

    private void ZoomSetup()
    {
      _zoom.ZoomClamp = true;
      _zoom.ZoomMin = ZoomMin;
      _zoom.ZoomMax = _camera.orthographicSize;
    }

    private void MoveSetup()
    {
      _move.Dampening = 1;
      _move.Sensitivity = 1;
    }

    private void EnableComponents(bool value)
    {
      _leanTouch.enabled = value;
      _zoom.enabled = value;
      _zoom.IsBeezy = false;
      _rotation.IsBeezy = false;
    }
  }
}