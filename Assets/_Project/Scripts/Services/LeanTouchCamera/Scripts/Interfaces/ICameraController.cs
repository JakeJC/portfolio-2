using System;
using _Project.Scripts.Data.Serializable;
using _Project.Scripts.Service_Base;
using Lean.Touch;
using LeanTouch.Examples.Scripts;

namespace _Project.Scripts.Services.LeanTouchCamera.Scripts.Interfaces
{
  public interface ICameraController : IService
  {
    void SwitchToGame();
    void ToBaseState(bool relative = false);
    void SetCameraData(CameraData cameraData);
    LeanPitchYawSmooth Rotation { get; }
    LeanCameraZoomSmooth Zoom { get; }
    void DisableRotation();
    void EnableRotation();
    void PrepareToGameSwitch();
    void FinishingRotation();
    void SaveOriginalPosition();
    void InInterview(bool isInInterview, float yValue, Action onInterviewEnd);
  }
}