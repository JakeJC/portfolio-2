﻿using _Project.Scripts.Services.MaterialService.Data;

namespace _Project.Scripts.Services.MaterialService.Interfaces
{
  public interface IColor : IScheme
  {
    void Initialize();
    ColorSchemeData[] GetAllColorScheme();
    void AddObject(IColorProvider colorProvider);
    void AddRange(IColorProvider[] colorProviders);
    void RemoveObject(IColorProvider colorProvider);
    void Clear();
    bool DependsOnMaterial { get; set; }
  }
}