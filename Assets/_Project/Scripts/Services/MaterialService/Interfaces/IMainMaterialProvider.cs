using UnityEngine;

namespace _Project.Scripts.Services.MaterialService.Interfaces
{
  public interface IMainMaterialProvider
  {
    Material GetMaterial(string guid);
  }
}