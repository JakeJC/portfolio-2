﻿using UnityEngine;

namespace _Project.Scripts.Services.MaterialService.Interfaces
{
  public interface IMaterialProvider
  {
    void SetMaterial(Material material);
    Renderer[] GetRenders();
    bool IsMechanism();
  }
}