using System;

namespace _Project.Scripts.Services.MaterialService.Interfaces
{
  public interface IScheme
  {
    string GetCurrentSchemeGuid();
    void SetScheme(string schemeGuid);
    string[] GetAllSchemeGuid();
    event Action<string> OnSetScheme;
  }
}