﻿using _Project.Scripts.Services.MaterialService.Data;

namespace _Project.Scripts.Services.MaterialService.Interfaces
{
  public interface IColorProvider
  {
    void SetColor(ColorData colorData, bool dependsOnMaterial);
  }
}