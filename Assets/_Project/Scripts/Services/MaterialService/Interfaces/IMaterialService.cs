using _Project.Scripts.MonoBehaviours.Game.Interfaces;
using _Project.Scripts.Service_Base;
using UnityEngine;

namespace _Project.Scripts.Services.MaterialService.Interfaces
{
  public interface IMaterialService : IService
  {
    IMaterial Material { get; }
    IColor Color { get; }
    void BaseMaterialsState();
    void Initialize(IMainMaterialProvider mainMaterialProvider, string worldId);
    void Clear();
    void RemoveFromBothModules(Component component);
    void SetMechanism(IMechanism mechanism);
    void SetData(string colorGuid);
  }
}