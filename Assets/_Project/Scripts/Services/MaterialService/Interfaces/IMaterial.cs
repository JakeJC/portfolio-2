using System;
using _Project.Scripts.MonoBehaviours.Game.Interfaces;
using _Project.Scripts.Services.MaterialService.Data;
using UnityEngine;

namespace _Project.Scripts.Services.MaterialService.Interfaces
{
  public interface IMaterial : IScheme
  {
    void Initialize();
    void AddRange(IMaterialProvider[] materialProviders);
    void RemoveObject(IMaterialProvider materialProvider);
    public MaterialSchemeData[] GetAllMaterialsScheme();
    int GetCurrentSchemeIndex();
    void Clear();
    void SetMechanism(IMechanism mechanism);
    bool IsRematerialActive();
    void SetColorForInstance(Color color);
    event Action OnChangeMaterialEnd;
  }
}