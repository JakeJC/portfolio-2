using _Project.Scripts.Services.MaterialService.Interfaces;
using Sirenix.OdinInspector;
using UnityEngine;
using Zenject;

namespace _Project.Scripts.Services.MaterialService.Test
{
  public class TestMaterialService : MonoBehaviour
  {
    private IMaterialService _materialService;

    [Inject]
    public void Construct(IMaterialService materialService) => 
      _materialService = materialService;

    [Button]
    public void SetMaterialScheme(string guid) => 
      _materialService.Material.SetScheme(guid);

    [Button]
    public void SetColorScheme(string guid) =>      
      _materialService.Color.SetScheme(guid);

  }
}