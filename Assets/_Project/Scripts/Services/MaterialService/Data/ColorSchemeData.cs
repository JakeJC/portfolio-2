﻿using Sirenix.OdinInspector;
using UnityEditor;
using UnityEngine;

namespace _Project.Scripts.Services.MaterialService.Data
{
  [CreateAssetMenu(fileName = "Color Scheme", menuName = "Material Service/Create Color Scheme", order = 0)]
  public class ColorSchemeData : ScriptableObject, ISchemeData
  {
    public ColorData ColorData;
    public Sprite Icon;
    public string Guid;

    public Sprite GetIcon() => 
      Icon;

    public string GetGuid() => 
      Guid;

#if UNITY_EDITOR
    [Button]
    public void GenerateGuid() => 
      Guid = GUID.Generate().ToString();
#endif
  }
}