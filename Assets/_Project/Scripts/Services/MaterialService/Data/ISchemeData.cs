namespace _Project.Scripts.Services.MaterialService.Data
{
  public interface ISchemeData
  {
    string GetGuid();
  }
}