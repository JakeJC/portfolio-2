using UnityEngine;

namespace _Project.Scripts.Services.MaterialService.Data
{
  [System.Serializable]
  public struct ColorData
  {
    [Header("Main")]
    public Color Color;
    public Color MechanismColor;
    public Color EnvironmentColor;
    public Color FloorColor;
    public Color CameraSolidColor;
    public Color ColumnColor;
    
    [Header("LightWorld")]
    public Color LightWorldCameraSolidColor;
    public Color  LightWorldFloorColor;
    public LightWorldColorData LightWorldColorData;
    public LightWorldColorData SpecialLightWorldColorData;
  }

  [System.Serializable]
  public struct LightWorldColorData
  {
    public Color FakeLightColor;
    public GlowColorData GlowColor;
    public DissolveColorData DissolveColor;
  }
  
  [System.Serializable]
  public struct GlowColorData
  {
    public Color MainColor;
    public Color SecondColor;
  }

  [System.Serializable]
  public struct DissolveColorData
  {
    public Texture Texture;
    public Color OuterEdgeColor;
    public Color InnerEdgeColor;
    public Color GlowColor;
  }
}