using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEditor;
using UnityEngine;

namespace _Project.Scripts.Services.MaterialService.Data
{
  [CreateAssetMenu(fileName = "Material Scheme", menuName = "Material Service/Create Material Scheme", order = 0)]
  public class MaterialSchemeData : ScriptableObject, ISchemeData
  {
    [SerializeField] private Sprite Icon;
    [SerializeField] private string Guid;
    [SerializeField] private bool _isDissolve;

    public bool IsDissolve => _isDissolve;

    public Sprite GetIcon() => 
      Icon;

    public string GetGuid() =>
      Guid;
    
#if UNITY_EDITOR
    [Button]
    public void GenerateGuid() => 
      Guid = GUID.Generate().ToString();
#endif
  }
}