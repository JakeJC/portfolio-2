﻿using System.Collections.Generic;
using System.Linq;
using _Project.Scripts.Services._Interfaces;
using _Project.Scripts.Services.MaterialService.Data;
using _Project.Scripts.Services.MaterialService.Interfaces;

namespace _Project.Scripts.Services.MaterialService.Main
{
  public class ColorScheme : BaseScheme, IColor
  {
    private const string MaterialsColorsPath = "_Materials & Colors/Colors";
    protected override string CurrentSchemeKey => "ColorScheme.CurrentSchemeGuid";

    private ColorSchemeData[] _colorSchemeData;
    private List<IColorProvider> _objects = new List<IColorProvider>();
    
    private readonly IAssetProvider _assetProvider;
    private readonly IMaterial _materialModule;

    public bool DependsOnMaterial { get; set; }

    public ColorScheme(IMaterial materialModule, IAssetProvider assetProvider)
    {
      _materialModule = materialModule;
      _assetProvider = assetProvider;
    }

    public void Initialize()
    {
      _colorSchemeData = _assetProvider.GetAllResources<ColorSchemeData>(MaterialsColorsPath);
      var schemeData = _colorSchemeData.Select(p => p as ISchemeData).ToArray();
      
      Initialize(schemeData, _colorSchemeData[0].GetGuid());
    }
    
    public void AddRange(IColorProvider[] colorProviders) => 
      _objects.AddRange(colorProviders);

    public void AddObject(IColorProvider colorProvider) => 
      _objects.Add(colorProvider);

    public void RemoveObject(IColorProvider colorProvider) => 
      _objects.Remove(colorProvider);

    public void Clear() => 
      _objects.Clear();

    public ColorSchemeData GetCurrentColorScheme(string guid) => 
      _colorSchemeData?.FirstOrDefault(p => p.Guid == guid);

    public ColorSchemeData[] GetAllColorScheme() => 
      _colorSchemeData;

    protected override void ApplyScheme(string schemeGuid)
    {
      ColorSchemeData currentColorScheme = GetCurrentColorScheme(schemeGuid);
      _objects = _objects.Where(p => p != null).ToList();

      if (currentColorScheme != null)
      {
        _materialModule.SetColorForInstance(currentColorScheme.ColorData.MechanismColor);
        
        foreach (IColorProvider colorProvider in _objects)
          colorProvider?.SetColor(currentColorScheme.ColorData, DependsOnMaterial);
      }

      base.ApplyScheme(schemeGuid);
    }
  }
}