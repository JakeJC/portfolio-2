using System;
using System.Collections.Generic;
using System.Linq;
using _Project.FMOD_AudioService.Logic;
using _Project.FMOD_AudioService.Logic.Enums;
using _Project.FMOD_AudioService.Logic.Events;
using _Project.Scripts.MonoBehaviours.Game.Interfaces;
using _Project.Scripts.Services._Interfaces;
using _Project.Scripts.Services.FX.RematerialLogic;
using _Project.Scripts.Services.MaterialService.Data;
using _Project.Scripts.Services.MaterialService.Interfaces;
using UnityEngine;

namespace _Project.Scripts.Services.MaterialService.Main
{
  public class MaterialScheme : BaseScheme, IMaterial
  {
    private const string MaterialsColorsPath = "_Materials & Colors/Materials";
    private static readonly int SpecialColor = Shader.PropertyToID("_SpecialColor");
    protected override string CurrentSchemeKey => "MaterialScheme.CurrentSchemeGuid";

    private MaterialSchemeData[] _materialSchemeData;
    private List<IMaterialProvider> _objects = new List<IMaterialProvider>();

    private IMechanism _mechanism;
    private readonly IAudioService _audioService;

    private Material _currentMaterialInstance;
    
    private IRematerial _rematerial;
    private readonly IMainMaterialProvider _mainMaterialProvider;

    public event Action OnChangeMaterialEnd;

    private readonly Dictionary<string, Material> _instances = new Dictionary<string, Material>();
    private readonly Dictionary<string, Material> _uiInstances = new Dictionary<string, Material>();

    private readonly IAssetProvider _assetProvider;

    public MaterialScheme(IAssetProvider assetProvider, IAudioService audioService, IMainMaterialProvider mainMaterialProvider)
    {
      _assetProvider = assetProvider;
      _mainMaterialProvider = mainMaterialProvider;
      _audioService = audioService;
    }

    public void Initialize()
    {
      _materialSchemeData = _assetProvider.GetAllResources<MaterialSchemeData>(MaterialsColorsPath);
      
      _rematerial = new Rematerial();

      Initialize(_materialSchemeData, _materialSchemeData[0].GetGuid());
      
      CreateMaterialInstances();

      OnSetScheme += ChangeSounds;
    }

    public void AddObject(IMaterialProvider materialProvider) =>
      _objects.Add(materialProvider);

    public void AddRange(IMaterialProvider[] materialProviders) =>
      _objects.AddRange(materialProviders);

    public void RemoveObject(IMaterialProvider materialProvider) =>
      _objects.Remove(materialProvider);

    public MaterialSchemeData[] GetAllMaterialsScheme() =>
      _materialSchemeData;

    public int GetCurrentSchemeIndex() => 
      _materialSchemeData.ToList().IndexOf(GetCurrentMaterialScheme(GetCurrentSchemeGuid()));

    public void Clear()
    {
      _instances?.Clear();
      _uiInstances?.Clear();
      _rematerial?.Clear();
      _objects?.Clear();
    }

    public void SetMechanism(IMechanism mechanism) => 
      _mechanism = mechanism;

    public MaterialSchemeData GetCurrentMaterialScheme(string guid) =>
      _materialSchemeData.FirstOrDefault(p => p.GetGuid() == guid);

    public bool IsRematerialActive() =>
      _rematerial.IsActive();

    public void SetColorForInstance(Color color)
    {
      if(_currentMaterialInstance)
        _currentMaterialInstance.SetColor(SpecialColor, color);
    }

    protected override void ApplyScheme(string schemeGuid)
    {
      if(_mechanism == null)
        return;

      _objects = _objects.Where(p => p != null).ToList();

      var material = _instances[schemeGuid];
      var uiMaterial = _uiInstances[schemeGuid];
      
      ChangeMaterial(material, uiMaterial);
      
      _currentMaterialInstance = material;
      
      base.ApplyScheme(schemeGuid);
    }

    private void CreateMaterialInstances()
    {
      foreach (MaterialSchemeData materialSchemeData in _materialSchemeData)
      {
        _instances.Add(materialSchemeData.GetGuid(), new Material(_mainMaterialProvider.GetMaterial(materialSchemeData.GetGuid())));
        _uiInstances.Add(materialSchemeData.GetGuid(), _mainMaterialProvider.GetMaterial(materialSchemeData.GetGuid()));
      }
    }

    private void ChangeMaterial(Material material, Material uiMaterial)
    {
      if (_currentMaterialInstance == null)
      {
        foreach (IMaterialProvider materialProvider in _objects) 
          materialProvider.SetMaterial(materialProvider.IsMechanism()? material : uiMaterial);
      }
      else
      {
         var rematerialRenderers = new List<Renderer>();
 
         foreach (IMaterialProvider materialProvider in _objects)
         {
           if (materialProvider.IsMechanism())
             AddToRematerial(materialProvider, ref rematerialRenderers);
           else
             materialProvider.SetMaterial(uiMaterial);
         }
 
         ApplyRematerial(rematerialRenderers, material);
      }
    }

    private void ChangeSounds(string schemeGuid)
    {
      int index = GetCurrentSchemeIndex();

      if (_mechanism == null)
        return;

      AudioEventLink audioEventLink = _audioService.GetLinkByEvent(_mechanism.GetSoundEvent());
      if (audioEventLink.Hash != 0)
        _audioService.SetParameter(audioEventLink, FMOD_ParameterType.current_skin, index);
    }

    private void AddToRematerial(IMaterialProvider materialProvider, ref List<Renderer> rematerialRenderers)
    {
      Renderer[] renderers = materialProvider.GetRenders();
      foreach (var renderer in renderers)
        rematerialRenderers.Add(renderer);
    }
    
    private void ApplyRematerial(List<Renderer> rematerialRenderers, Material material)
    {
      _rematerial.SetLight(_mechanism.GetIsLight());
      
      float Duration = 4.5f; // TODO: в вынести куда-нибудь
      Vector3 center = _mechanism.GetBaseAnchorPosition + Vector3.up * 2.1f;
      _rematerial.Exchange(rematerialRenderers.ToArray(), _currentMaterialInstance, material, center, Duration,
        delegate { Debug.Log("Rematerial Finished"); OnChangeMaterialEnd?.Invoke();});
    }
  }
}