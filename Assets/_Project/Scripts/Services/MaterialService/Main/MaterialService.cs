using _Project.FMOD_AudioService.Logic;
using _Project.Scripts.MonoBehaviours.Game.Interfaces;
using _Project.Scripts.Services._Interfaces;
using _Project.Scripts.Services.MaterialService.Interfaces;
using UnityEngine;

namespace _Project.Scripts.Services.MaterialService.Main
{
  public class MaterialService : IMaterialService
  {
    private readonly IAssetProvider _assetProvider;
    private readonly IAudioService _audioService;
    private string _colorGuid;

    public IMaterial Material { get; private set; }
    public IColor Color { get; private set; }

    public MaterialService(IAssetProvider assetProvider, IAudioService audioService)
    {
       _audioService = audioService;
      _assetProvider = assetProvider;
    }

    public void Initialize(IMainMaterialProvider mainMaterialProvider, string worldId)
    {
      Material = new MaterialScheme(_assetProvider, _audioService, mainMaterialProvider);
      Color = new ColorScheme(Material, _assetProvider);
      Color.SetScheme(_colorGuid);
      
      Material.OnSetScheme += ColorMaterialDependency;

      Color.Clear();
      Material.Clear();
      
      Material.Initialize();
      Color.Initialize();
    }

    public void Clear()
    {
      Material.OnSetScheme -= ColorMaterialDependency;

      Color.Clear();
      Material.Clear();
    }

    public void BaseMaterialsState()
    {
      Material.SetScheme(Material.GetCurrentSchemeGuid());
      Color.SetScheme(Color.GetCurrentSchemeGuid());
    }

    public void SetMechanism(IMechanism mechanism) =>
      Material.SetMechanism(mechanism);

    public void SetData(string colorGuid) => 
      _colorGuid = colorGuid;

    public void RemoveFromBothModules(Component component)
    {
      foreach (IMaterialProvider provider in component.GetComponentsInChildren<IMaterialProvider>())
        Material.RemoveObject(provider);

      foreach (IColorProvider provider in component.GetComponentsInChildren<IColorProvider>())
        Color.RemoveObject(provider);
    }

    private void ColorMaterialDependency(string s) => 
      Color.DependsOnMaterial = s == "4c5ba727c26aa1244844d0847e3b7508";
  }
}