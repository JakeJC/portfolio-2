using System;
using System.Linq;
using _Project.Scripts.Services.MaterialService.Data;
using _Project.Scripts.Services.MaterialService.Interfaces;
using UnityEngine;

namespace _Project.Scripts.Services.MaterialService.Main
{
  public abstract class BaseScheme : IScheme
  {
    protected abstract string CurrentSchemeKey { get; }
    private ISchemeData[] _schemeData;

    private string _baseGuid;

    private string CurrentSchemeGuid
    {
      get => PlayerPrefs.GetString(CurrentSchemeKey, _baseGuid);
      set => PlayerPrefs.SetString(CurrentSchemeKey, value);
    }

    public event Action<string> OnSetScheme;

    protected void Initialize(ISchemeData[] schemeData, string baseGuid)
    {
      _baseGuid = baseGuid;
      _schemeData = schemeData;
    }

    public string GetCurrentSchemeGuid() => 
      CurrentSchemeGuid;

    public string[] GetAllSchemeGuid() => 
      _schemeData.Select(p => p.GetGuid()).ToArray();

    public void SetScheme(string schemeGuid)
    {
      CurrentSchemeGuid = schemeGuid;
      ApplyScheme(CurrentSchemeGuid);
    }

    protected virtual void ApplyScheme(string schemeGuid) => 
      OnSetScheme?.Invoke(schemeGuid);
  }
}