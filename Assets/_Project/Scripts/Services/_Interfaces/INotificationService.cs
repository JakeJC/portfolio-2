using System;
using _Project.Scripts.Service_Base;

namespace _Project.Scripts.Services._Interfaces
{
  public interface INotificationService : IService
  {
    void Send(DateTime targetTime);
    void ClearAll();
  }
}