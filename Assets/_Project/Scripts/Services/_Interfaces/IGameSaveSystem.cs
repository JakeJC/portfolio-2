using _Project.Scripts.Data.Serializable;
using _Project.Scripts.Service_Base;

namespace _Project.Scripts.Services._Interfaces
{
  public interface IGameSaveSystem : IService
  {
    GameSaveData Get();
    void Save();
  }
}