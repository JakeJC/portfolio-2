using _Project.Scripts.Service_Base;
using UnityEngine;

namespace _Project.Scripts.Services.FX
{
  public interface IFXController : IService
  {
    void PlayPlaceParticle(Renderer renderer);
    void EnableFingerTrail();
    void DisableTrail();
  }
}