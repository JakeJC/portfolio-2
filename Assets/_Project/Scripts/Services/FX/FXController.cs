using _Project.Scripts.Services._Interfaces;
using _Project.Scripts.Services.FX.Mono;
using _Project.Scripts.Services.LeanTouchCamera.Scripts.Interfaces;
using UnityEngine;
using Input = _Project.Scripts.Services.FX.Mono.Input;

namespace _Project.Scripts.Services.FX
{
  public class FXController : IFXController
  {
    private const string Parent = "Blurred UI";

    private const string ClickFXPath = "FX/Click/Click Particle";
    private const string PlaceParticlePath = "FX/SpawnParticles/Spawn Particles";
    private const string FingerTrailPath = "FX/Finger Trail/Finger Trail";

    private readonly Input _controller;
    
    private readonly Transform _parent;

    private readonly GameObject _clickPrefab;
    private readonly GameObject _placeParticlePrefab;
    private ParticleSystem _fingerTrail;
    
    private readonly IAssetProvider _assetProvider;
    private readonly ICameraController _cameraController;
    
    private Vector2 _speed;

    public FXController(ICameraController cameraController, IAssetProvider assetProvider)
    {
      _cameraController = cameraController;
      _assetProvider = assetProvider;
      
      _controller = new GameObject(){name = "Particle Input"}.AddComponent<Input>();
      _controller.OnClick += PlayClick;

      _parent = GameObject.Find(Parent).transform;

      _clickPrefab = assetProvider.GetResource<GameObject>(ClickFXPath);
      _placeParticlePrefab = assetProvider.GetResource<GameObject>(PlaceParticlePath);
    }

    ~FXController() => 
      _controller.OnClick -= PlayClick;

    public void PlayPlaceParticle(Renderer renderer)
    {
      SpawnParticlesController placeParticle = Object.Instantiate(_placeParticlePrefab).GetComponent<SpawnParticlesController>();
      placeParticle.Configure(renderer);
    }

    public void EnableFingerTrail()
    {
      if (_fingerTrail == null)
      {
        var particleSystem = _assetProvider.GetResource<GameObject>(FingerTrailPath);
        _fingerTrail = Object.Instantiate(particleSystem, GameObject.Find(Parent).transform)
          .GetComponentInChildren<ParticleSystem>();
        
        _controller.OnDrag += MoveFingerTrail;
        _controller.OnNotDrag += HideTrail;
        
        HideTrail();
      }
    }

    public void DisableTrail()
    {
      if (_fingerTrail == null) 
        return;
      
      Object.Destroy(_fingerTrail.transform.parent.gameObject);
        
      _controller.OnDrag -= MoveFingerTrail;
      _controller.OnNotDrag -= HideTrail;
    }

    private void PlayClick(Vector3 clickPosition) => 
      Object.Instantiate(_clickPrefab, clickPosition, Quaternion.identity, _parent);

    private void ShowTrail()
    {
      ParticleSystem.EmissionModule fingerTrailEmission = _fingerTrail.emission;
      fingerTrailEmission.enabled = true;
    }

    private void HideTrail()
    {
      ParticleSystem.EmissionModule fingerTrailEmission = _fingerTrail.emission;
      fingerTrailEmission.enabled = false;
    }

    private void MoveFingerTrail(Vector3 position)
    {
      if (!_cameraController.Rotation.IsBeezy)
      {
        HideTrail();
        return;
      }

      ShowTrail();
      
      Transform fingerTrailTransform = _fingerTrail.transform;
      
      fingerTrailTransform.forward = Vector2.SmoothDamp(fingerTrailTransform.forward,  fingerTrailTransform.position - position, ref _speed, 1);

      ParticleSystem.MainModule fingerTrailMain = _fingerTrail.main;
      
      float eulerAnglesX = fingerTrailTransform.localEulerAngles.x;
      float eulerAnglesY = fingerTrailTransform.localEulerAngles.y;
      
      int sign = eulerAnglesY > 180 ? -1 : 1;
      
      fingerTrailMain.startRotation = eulerAnglesX;
      fingerTrailMain.flipRotation = sign;
      
      fingerTrailTransform.position = position;
    }
  }
}