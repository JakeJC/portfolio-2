using System.Collections.Generic;
using System.Reflection;
using DG.Tweening;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Experimental.Rendering.Universal;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;

namespace _Project.Scripts.Services.FX.RematerialLogic
{
  public class Rematerial : IRematerial
  {
    private const string LayerName = "Rematerial";
    
    private const string RematerialFeatureName = "Rematerial";
    private const string RematerialFeatureLightName = "Rematerial Light";
    
    private const string ShaderPath = "Universal Render Pipeline/Custom/MatCapImagine";

    private static readonly Vector3 TargetScale = new Vector3(4, 4, 4); // TODO: в вынести куда-нибудь
    private static readonly Vector3 DefaultScale = new Vector3(0, 0, 0);

    private readonly Vector3 _clippingSphereHidePos = new Vector3(1000, 1000, 1000);

    private ClippingSphere _clippingSphere;
    private Material _transitionMat;

    private bool _activated;
    private bool _isLight;

    public Rematerial()
    {
      _transitionMat = GetRendererFeatureMaterial();

      SetActiveRendererFeature(false);
    }

    public void SetLight(bool isLight)
    {
      _isLight = isLight;
      _transitionMat = GetRendererFeatureMaterial();
    }

    public void Exchange(Renderer[] meshRenderers, Material current, Material target, Vector3 point, float duration, UnityAction callback)
    {
      if (_activated)
        return;

      if (_clippingSphere == null)
        _clippingSphere = Object.FindObjectOfType<ClippingSphere>();
      
      _activated = true;

      _transitionMat.shader = Shader.Find(ShaderPath);

      _transitionMat.CopyPropertiesFromMaterial(current);
      _transitionMat.EnableKeyword("SPHERECLIP_ON");
      _transitionMat.DisableKeyword("SPHERECLIPSHADOW_ON");
      _transitionMat.EnableKeyword("EXTRUDE_ON");
      _clippingSphere.transform.localScale = DefaultScale;

      target.EnableKeyword("SPHERECLIPSHADOW_ON");

      SetActiveRendererFeature(true);

      LayerMask[] layerMasks = new LayerMask[meshRenderers.Length];

      for (int i = 0; i < layerMasks.Length; i++)
        layerMasks[i] = meshRenderers[i].gameObject.layer;

      for (int i = 0; i < meshRenderers.Length; i++)
      {
        meshRenderers[i].gameObject.layer = LayerMask.NameToLayer(LayerName);
        meshRenderers[i].material = target;
      }

      _clippingSphere.transform.position = point;
      _clippingSphere.transform.DOScale(TargetScale, duration).OnComplete(delegate { Finish(target, meshRenderers, layerMasks, callback); })
        .SetEase(Ease.Linear);
    }

    public bool IsActive() =>
      _activated;

    public void Clear()
    {}

    private void SetActiveRendererFeature(bool value)
    {
      UniversalRenderPipelineAsset pipelineAsset = (UniversalRenderPipelineAsset)GraphicsSettings.renderPipelineAsset;
      List<ScriptableRendererFeature> rendererFeatures = pipelineAsset.scriptableRenderer.GetType()
        .GetProperty("rendererFeatures", BindingFlags.NonPublic | BindingFlags.Instance)
        ?.GetValue(pipelineAsset.scriptableRenderer, null) as List<ScriptableRendererFeature>;

      ScriptableRendererFeature rematerialRendererFeature = null;

      if (rendererFeatures != null)
      {
        foreach (ScriptableRendererFeature feature in rendererFeatures)
        {
          if (feature.name.Equals(_isLight? RematerialFeatureLightName : RematerialFeatureName))
          {
            rematerialRendererFeature = feature;
            break;
          }
        }

        if (rematerialRendererFeature)
          rematerialRendererFeature.SetActive(value);
      }
    }

    private Material GetRendererFeatureMaterial()
    {
      int asdas;

      UniversalRenderPipelineAsset pipelineAsset = ((UniversalRenderPipelineAsset)GraphicsSettings.renderPipelineAsset);
      List<ScriptableRendererFeature> rendererFeatures = pipelineAsset.scriptableRenderer.GetType()
        .GetProperty("rendererFeatures", BindingFlags.NonPublic | BindingFlags.Instance)
        ?.GetValue(pipelineAsset.scriptableRenderer, null) as List<ScriptableRendererFeature>;

      ScriptableRendererFeature rematerialRendererFeature = null;

      if (rendererFeatures != null)
      {
        foreach (ScriptableRendererFeature feature in rendererFeatures)
        {
          if (feature.name.Equals(_isLight? RematerialFeatureLightName : RematerialFeatureName))
          {
            rematerialRendererFeature = feature;
            break;
          }
        }

        if (rematerialRendererFeature)
          return ((RenderObjects)rematerialRendererFeature)?.settings.overrideMaterial;
      }

      return null;
    }

    private void Finish(Material target, Renderer[] meshRenderers, LayerMask[] layerMasks, UnityAction callback)
    {
      _clippingSphere.transform.position = _clippingSphereHidePos;
      _clippingSphere.transform.localScale = Vector3.zero;

      target.DisableKeyword("SPHERECLIPSHADOW_ON");

      for (int i = 0; i < meshRenderers.Length; i++)
        meshRenderers[i].gameObject.layer = layerMasks[i];

      SetActiveRendererFeature(false);
      _activated = false;

      callback?.Invoke();
    }
  }
}