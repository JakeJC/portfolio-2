using UnityEngine;
using UnityEngine.Events;

namespace _Project.Scripts.Services.FX.RematerialLogic
{
  public interface IRematerial
  {
    public void Exchange(Renderer[] meshRenderers, Material current, Material target, Vector3 point, float duration, UnityAction callback);
    bool IsActive();
    void Clear();
    void SetLight(bool isLight);
  }
}