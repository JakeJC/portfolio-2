using UnityEngine;

namespace _Project.Scripts.Services.FX.RematerialLogic
{
  [ExecuteInEditMode]
  public class ClippingSphere: MonoBehaviour
  {
    [SerializeField] [Range(-2, 2)] private float sphereFade = 1;
    [SerializeField] private Color glowColor1 = Color.white;
    [SerializeField] private Color glowColor2 = Color.blue;
    [SerializeField] private Color glowColor3 = Color.magenta;
    
    [Space(7)]
    [SerializeField] [Range(-2, 2)] private float sphereShadowFade = 1;
    [SerializeField] private Color shadowColor = Color.black;
    
    [Space(7)]
    [SerializeField] [Range(0, 10)] private float _extrudeAnimationSpeed = 0.7f;
    [SerializeField] [Range(0, 10)] private float _extrudePower = 0.07f;
    
    private void Update()
    {
      Shader.SetGlobalFloat(Shader.PropertyToID("_GLOBALSphereRadius"), transform.localScale.x);
      Shader.SetGlobalVector(Shader.PropertyToID("_GLOBALSpherePosition"), transform.position);
      Shader.SetGlobalFloat(Shader.PropertyToID("_GLOBALSphereFade"), sphereFade);
      Shader.SetGlobalFloat(Shader.PropertyToID("_GLOBALSphereShadowFade"), sphereShadowFade);
      Shader.SetGlobalColor(Shader.PropertyToID("_GLOBALClipColor1"), glowColor1);
      Shader.SetGlobalColor(Shader.PropertyToID("_GLOBALClipColor2"), glowColor2);
      Shader.SetGlobalColor(Shader.PropertyToID("_GLOBALClipColor3"), glowColor3);
      Shader.SetGlobalColor(Shader.PropertyToID("_GLOBALClipShadowColor"), shadowColor);
      Shader.SetGlobalFloat(Shader.PropertyToID("_GLOBALExtrudeAnimationSpeed"), _extrudeAnimationSpeed);
      Shader.SetGlobalFloat(Shader.PropertyToID("_GLOBALExtrudePower"), _extrudePower);
    }

    private void OnDrawGizmos()
    {
      Gizmos.color = glowColor1;
      Gizmos.DrawWireSphere(transform.position, transform.localScale.x);
    }
  }
}