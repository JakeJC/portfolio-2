using UnityEngine;

namespace _Project.Scripts.Services.FX.Mono
{
  public class SpawnParticlesController : MonoBehaviour
  {
    [SerializeField] private ParticleSystem _particleSystem;

    public void Configure(Renderer renderer)
    {
      ParticleSystem.ShapeModule shape = _particleSystem.shape;
      shape.scale = renderer.bounds.size;

      _particleSystem.transform.position = renderer.bounds.center;
    }
  }
}