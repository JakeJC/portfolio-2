﻿using UnityEngine;

namespace _Project.Scripts.Services.FX.Mono
{
  [RequireComponent(typeof(ParticleSystem))]
  public class ParticlesScaler : MonoBehaviour
  {
    private const float BaseSizeModifier = 23f;
    private Camera _camera;

    private void Awake() =>
      _camera = Camera.main;
    
    private void OnParticleSystemStopped() => 
      Destroy(transform.parent.gameObject);

    private void Update()
    {
      float scaleModifier =_camera.orthographicSize / BaseSizeModifier;
      transform.localScale = scaleModifier * Vector3.one;
    }
  }
}
