using UnityEngine;
using UnityEngine.Events;

namespace _Project.Scripts.Services.FX.Mono
{
  public class Input : MonoBehaviour
  {
    public event UnityAction<Vector3> OnClick;
    public event UnityAction<Vector3> OnDrag;
    public event UnityAction OnNotDrag;

    private Vector3? _previousPosition;

    private void LateUpdate()
    {
      if (UnityEngine.Input.GetKeyDown(KeyCode.Mouse0)) 
        OnClick?.Invoke(UnityEngine.Input.mousePosition);

      if (UnityEngine.Input.GetKey(KeyCode.Mouse0))
      {
        if (!_previousPosition.HasValue)
          _previousPosition = UnityEngine.Input.mousePosition;
        else
        {
          Vector3 delta = UnityEngine.Input.mousePosition - _previousPosition.Value;

          if (Mathf.Approximately(delta.sqrMagnitude, 0))
            OnNotDrag?.Invoke();
          else
          {
            var screenToWorldPoint = UnityEngine.Input.mousePosition;
            OnDrag?.Invoke(screenToWorldPoint);
            
            _previousPosition = screenToWorldPoint;
          }
        }
      }
      else
        OnNotDrag?.Invoke();
    }
  }
}