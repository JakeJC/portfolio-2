﻿using System;
using System.Threading.Tasks;

namespace _Project.Scripts.Services.LoadScreen.Logic
{
  public abstract class LoadScreen
  {
    public abstract void Start(Action onFirsPartEnd, float startAlphaValue = 0);

    public abstract void End(Action onEnd);
  }
}