﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using _Project.Scripts.Animations.UI;
using UnityEngine;

namespace _Project.Scripts.Services.LoadScreen.Logic
{
  public class LoadScreenService : ILoadScreenService
  {
    private readonly Dictionary<Type, LoadScreen> _screens;

    public delegate Task ActionReturnTask();
    
    public LoadScreenService()
    {
      _screens = new Dictionary<Type, LoadScreen>()
      {
        [typeof(BlackTransition)] = new BlackTransition()
      };
    }

    public void Launch<T>(Action onEnd, ActionReturnTask loadTask, float startAlphaValue = 0) where T : LoadScreen
    {
      _screens[typeof(T)].Start(async () =>
      {
        await loadTask.Invoke();
      
        _screens[typeof(T)].End(onEnd);
      }, startAlphaValue);
    }

    public void ForceRemoveScreens()
    {
      foreach (KeyValuePair<Type, LoadScreen> screen in _screens) 
        screen.Value.End(null);
    }

    public void ToQuit()
    {
      if (Application.isEditor)
      {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#endif
      }
      else
        Application.Quit();
    }
  }
}