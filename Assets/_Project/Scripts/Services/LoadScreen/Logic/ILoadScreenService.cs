﻿using System;
using System.Threading.Tasks;
using _Project.Scripts.Service_Base;

namespace _Project.Scripts.Services.LoadScreen.Logic
{
  public interface ILoadScreenService : IService
  {
    void Launch<T>(Action onEnd, LoadScreenService.ActionReturnTask loadTask, float startAlphaValue = 0) where T : LoadScreen;
    void ToQuit();
    void ForceRemoveScreens();
  }
}