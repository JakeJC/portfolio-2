﻿using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using _Project.Scripts.Services._Interfaces;
using UnityEngine;
using Newtonsoft.Json;

namespace _Project.Scripts.Services.Localization.Logic
{
  public class LocaleSystem : ILocaleSystem
  {
    private const string ActiveLanguageKey = "ActiveLanguageKey";
    
    private const string PathMain = "Main";
    private const string PathInterview = "Interview";
    private const string PathTutorial = "Tutorial";
    private const string PathAdvicesTexts = "UI/Advices/Advices";
    private const string PathAdvicesTitles = "UI/Advices/AdviceTitles";
    
    private readonly Dictionary<string, Dictionary<string, Dictionary<string, string>>> _dictionary;

    private string ActiveLanguage
    {
      get => PlayerPrefs.GetString(ActiveLanguageKey, "");
      set => PlayerPrefs.SetString(ActiveLanguageKey, value);
    }
      
    public LocaleSystem(IAssetProvider assetProvider)
    {
      string jsonMain = assetProvider.GetResource<TextAsset>(PathMain).text;
      string jsonInterview = assetProvider.GetResource<TextAsset>(PathInterview).text;
      string jsonTutorial = assetProvider.GetResource<TextAsset>(PathTutorial).text;
      string jsonAdvicesText = assetProvider.GetResource<TextAsset>(PathAdvicesTexts).text;
      string jsonAdvicesTitles = assetProvider.GetResource<TextAsset>(PathAdvicesTitles).text;

      _dictionary = new Dictionary<string, Dictionary<string, Dictionary<string, string>>>
      {
        {PathMain, JsonToDictionary(jsonMain)},
        {PathInterview, JsonToDictionary(jsonInterview)},
        {PathTutorial, JsonToDictionary(jsonTutorial)},
        {PathAdvicesTexts, JsonToDictionary(jsonAdvicesText)},
        {PathAdvicesTitles, JsonToDictionary(jsonAdvicesTitles)}
      };
      
      FetchLanguage(Application.systemLanguage);
    }

    public void SetLanguage(SystemLanguage language) => 
      FetchLanguage(language);

    public string GetText(string name, string key)
    {
      _dictionary[name][ActiveLanguage].TryGetValue(key, out  string text);
      return text;
    }
    
    public string GetKey(string name, string text) => 
      _dictionary.ContainsKey(name) ? _dictionary[name]["ru"].First(x => x.Value == text).Key : "";

    public bool IsRightToLeft()
    {
      var cultureInfo = new CultureInfo(ActiveLanguage);
      return cultureInfo.TextInfo.IsRightToLeft;
    }

    private Dictionary<string, Dictionary<string, string>> JsonToDictionary(string json) => 
      JsonConvert.DeserializeObject<Dictionary<string, Dictionary<string, string>>>(json);

    private void FetchLanguage(SystemLanguage language)
    {
      ActiveLanguage = language switch
      {
        SystemLanguage.English => "en",
        SystemLanguage.Russian => "ru",
        SystemLanguage.Portuguese => "pt",
        SystemLanguage.Arabic => "ar",
        SystemLanguage.Danish => "da",
        SystemLanguage.German => "de",
        SystemLanguage.Spanish => "es",
        SystemLanguage.French => "fr",
        SystemLanguage.Indonesian => "id",
        SystemLanguage.Italian => "it",
        SystemLanguage.Japanese => "ja",
        SystemLanguage.Korean => "ko",
        SystemLanguage.Dutch => "nl",
        SystemLanguage.Norwegian => "no",
        SystemLanguage.Polish => "pl",
        SystemLanguage.Swedish => "sv",
        SystemLanguage.Slovak => "sk",
        SystemLanguage.Turkish => "tr",
        SystemLanguage.Vietnamese => "vi",
        SystemLanguage.Thai => "th",
        SystemLanguage.Chinese => "zh",
        _ => "en"
      };
    }
  }
}