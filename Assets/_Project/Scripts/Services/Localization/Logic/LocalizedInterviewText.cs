using TMPro;
using UnityEngine;

namespace _Project.Scripts.Services.Localization.Logic
{
  [RequireComponent(typeof(TMP_Text))]
  public class LocalizedInterviewText : MonoBehaviour
  {
    private const string LocalizeName = "Interview";

    [SerializeField] private TMP_Text textComponent;
    [SerializeField] private string key;

    private ILocaleSystem _localeSystem;
    
    public void Construct(ILocaleSystem localeSystem) =>
      _localeSystem = localeSystem;

    public void Awake()
    {
      if (_localeSystem != null)
        textComponent.text = _localeSystem.GetText(LocalizeName, key);
    }
  }
}