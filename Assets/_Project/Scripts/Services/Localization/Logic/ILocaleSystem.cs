using _Project.Scripts.Service_Base;
using UnityEngine;

namespace _Project.Scripts.Services.Localization.Logic
{
  public interface ILocaleSystem : IService
  {
    void SetLanguage(SystemLanguage language);
    string GetText(string name, string key);
    string GetKey(string name, string key);
    bool IsRightToLeft();
  }
}