using TMPro;
using UnityEngine;
using Zenject;

namespace _Project.Scripts.Services.Localization.Logic
{
  [RequireComponent(typeof(TMP_Text))]
  public class LocalizedTutorialText : MonoBehaviour
  {
    private const string LocalizeName = "Tutorial";

    [SerializeField] private TMP_Text textComponent;
    [SerializeField] private string key;

    private ILocaleSystem _localeSystem;

    [Inject]
    private void Injected(ILocaleSystem localeSystem) =>
      _localeSystem = localeSystem;

    public void Construct(ILocaleSystem localeSystem) =>
      _localeSystem = localeSystem;
    
    public void Awake()
    {
      if (_localeSystem != null)
        textComponent.text = _localeSystem.GetText(LocalizeName, key);
    }
  }
}