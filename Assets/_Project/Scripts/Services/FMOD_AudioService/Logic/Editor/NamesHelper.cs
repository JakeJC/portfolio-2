namespace _Project.Scripts.Services.FMOD_AudioService.Logic.Editor
{
  public static class NamesHelper
  {
    public static string PrepareNameForEnum(string s) => 
      s.Replace(" ", "_").Replace("-", "_");
  }
}
