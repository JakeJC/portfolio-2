using System.IO;
using _Project.FMOD_AudioService.Logic.Events;
using _Project.Scripts.Services.FMOD_AudioService.Logic.Editor;
using UnityEditor;
using UnityEngine;

namespace _Project.FMOD_AudioService.Logic.Editor
{
  public class EventTypeEnumCreator : AssetPostprocessor
  {
    private const string EnumPath = "Assets/_Project/FMOD_AudioService/Logic/Enums/";
    private const string EnumNamespace = "_Project.FMOD_AudioService.Logic.Enums";
    private const string EnumName = "FMOD_EventType";

    private const string AudioResourcesFolderName = "FMOD Configs";

    private static string AudioResourcesFolderPath =>
      "Resources/" + AudioResourcesFolderName + "/";
    
    public static void Generate()
    {
      string[] enumEntries = GetEnumNames();
      string filePathAndName = EnumPath
        .Replace('/', Path.DirectorySeparatorChar) + EnumName + ".cs"; 

      if (File.Exists(filePathAndName))
      {
        Debug.Log("Exist");
        File.Delete(filePathAndName);
      }

      using (StreamWriter streamWriter = new StreamWriter(filePathAndName))
      {
        streamWriter.WriteLine("namespace " + EnumNamespace);
        streamWriter.WriteLine("{");
        streamWriter.WriteLine("public enum " + EnumName);
        streamWriter.WriteLine("{");

        for (int i = 0; i < enumEntries.Length; i++)
          streamWriter.WriteLine("\t" + enumEntries[i] + ",");

        streamWriter.WriteLine("}");
        streamWriter.WriteLine("}");
      }

      AssetDatabase.Refresh(ImportAssetOptions.ImportRecursive);
      Debug.Log("Generate Audio Enum");
    }

    private static string[] GetEnumNames()
    {
      AudioEventConfig[] configs = Resources.LoadAll<AudioEventConfig>(AudioResourcesFolderName);
      string[] names = new string[configs.Length + 1];

      for (int i = 0; i < names.Length; i++)
      {
        if (i == 0)
        {
          names[i] = "Default";
          continue;
        }

        names[i] = NamesHelper.PrepareNameForEnum(configs[i - 1].name);
      }

      return names;
    }

    private static void OnPostprocessAllAssets(string[] importedAssets, string[] deletedAssets, string[] movedAssets, string[] movedFromAssetPaths)
    {
      OnReinportedAsset(importedAssets);
      OnDeletedAsset(deletedAssets);
      OnMovedAsset(movedAssets, movedFromAssetPaths);
    }

    private static void OnReinportedAsset(string[] importedAssets)
    {
      foreach (string str in importedAssets)
      {
        if (str.Contains(AudioResourcesFolderPath
          .Replace('/', Path.DirectorySeparatorChar)))
        {
          Generate();
          break;
        }
      }
    }

    private static void OnDeletedAsset(string[] deletedAssets)
    {
      foreach (string str in deletedAssets)
      {
        if (str.Contains(AudioResourcesFolderPath
          .Replace('/', Path.DirectorySeparatorChar)))
        {
          Generate();
          break;
        }
      }
    }

    private static void OnMovedAsset(string[] movedAssets, string[] movedFromAssetPaths)
    {
      for (int i = 0; i < movedAssets.Length; i++)
      {
        if (movedAssets[i].Contains(AudioResourcesFolderPath
              .Replace('/', Path.DirectorySeparatorChar)) ||
            movedFromAssetPaths[i].Contains(AudioResourcesFolderPath
              .Replace('/', Path.DirectorySeparatorChar)))
        {
          Generate();
          break;
        }
      }
    }
  }
}
