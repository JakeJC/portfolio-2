using System;
using System.Collections.Generic;
using System.IO;
using _Project.FMOD_AudioService.Logic.Enums;
using _Project.FMOD_AudioService.Logic.Events;
using _Project.Scripts.Services.FMOD_AudioService.Logic.Editor;
using FMODUnity;
using UnityEditor;
using UnityEngine;

namespace _Project.FMOD_AudioService.Logic.Editor
{
  public class WizardWindow : EditorWindow
  {
    private const string ConfigsPathSaveKey = "FMOD_AudioService_ConfigsPathSaveKey";
    private const char DirectorySeparatorChar = '/';
    private const string FileExtension = ".asset";
    private const string RootFolder = "Assets";
    private const string ConfigTypeMarker = "t:AudioEventConfig";
    private const string EventMarker = "event:";
    private const string WizardLogoPath = "Wizard Logo";

    private Texture _wizardLogo;

    [MenuItem("FMOD AudioService/Wizard")]
    private static void Init() =>
      CreateInstance<WizardWindow>().Show();

    private void OnGUI()
    {
      WizardWindow window = (WizardWindow) GetWindow(typeof(WizardWindow));

      window.maxSize = new Vector2(530f, 915f);
      window.minSize = window.maxSize;

      GUILayout.BeginVertical();

      GUILayout.Space(10);
      DrawHeader();
      GUILayout.Space(10);

      DrawPath();
      DrawConfigCreationButton();

      GUILayout.Space(10);
      DrawLogo();

      GUILayout.EndVertical();
    }

    private void DrawHeader()
    {
      GUIStyle guiStyle = EditorStyles.boldLabel;
      guiStyle.alignment = TextAnchor.UpperCenter;
      GUILayout.Label("FMOD Audio Service Wizard", guiStyle);
    }

    private void DrawPath()
    {
      GUIStyle guiStyle = EditorStyles.label;
      guiStyle.alignment = TextAnchor.UpperCenter;
      GUILayout.Label($"Path: {GetFullPath()}", guiStyle);
    }

    private void DrawLogo()
    {
      _wizardLogo ??= Resources.Load<Texture>(WizardLogoPath);

      GUIStyle guiStyle = EditorStyles.label;
      guiStyle.alignment = TextAnchor.UpperCenter;
      GUILayout.Label(_wizardLogo, guiStyle);
    }

    private string GetFullPath()
    {
      string savedFullPath = EditorPrefs.GetString(ConfigsPathSaveKey, string.Empty);
      string fullPath = string.IsNullOrEmpty(savedFullPath) ? Application.dataPath : savedFullPath;

      if (!Directory.Exists(fullPath))
        fullPath = Application.dataPath;

      return fullPath;
    }

    private string GetShortPath()
    {
      string fullPath = GetFullPath();

      int indexOfRootFolder = fullPath.IndexOf(RootFolder, StringComparison.Ordinal);
      string shortPath = fullPath.Substring(indexOfRootFolder);

      if (!Directory.Exists(shortPath))
        return String.Empty;

      return shortPath;
    }

    private void DrawConfigCreationButton()
    {
      if (GUILayout.Button("Change Configs Path"))
      {
        string newPath = EditorUtility.OpenFolderPanel("", GetFullPath(), "");
        newPath = string.IsNullOrEmpty(newPath) ? GetFullPath() : newPath;
        EditorPrefs.SetString(ConfigsPathSaveKey, newPath);
      }

      if (GUILayout.Button("Create Configs and Enums"))
      {
        CreateConfigs();
        EventTypeEnumCreator.Generate();
        ParameterTypeEnumCreator.Generate();
        SetEnumsToConfig();
        AssetDatabase.Refresh();
      }

      GUILayout.Space(20);

      if (GUILayout.Button("Cleanup"))
        Cleanup();
    }

    private void CreateConfigs()
    {
      Cleanup();

      string shortPath = GetShortPath();

      List<EditorEventRef> events = EventManager.Events;

      foreach (EditorEventRef e in events)
      {
        if (!e.Path.Contains(EventMarker))
          continue;

        AudioEventConfig config = CreateInstance<AudioEventConfig>();

        string assetName = GetName(e);
        config.Initialize(e.Path, e.IsOneShot, GetParameters(e.Parameters.ToArray()));

        AssetDatabase.CreateAsset(config, shortPath + DirectorySeparatorChar + assetName + FileExtension);
      }

      AssetDatabase.SaveAssets();

      string GetName(EditorEventRef e)
      {
        string[] splitedPath = e.Path.Split(DirectorySeparatorChar);
        string assetName = NamesHelper.PrepareNameForEnum(splitedPath[splitedPath.Length - 1]);
        return assetName;
      }

      AudioEventParameter[] GetParameters(EditorParamRef[] parameters)
      {
        AudioEventParameter[] eventParameters = new AudioEventParameter[parameters.Length];

        for (var i = 0; i < parameters.Length; i++)
        {
          EditorParamRef p = parameters[i];
          eventParameters[i] = new AudioEventParameter();
          eventParameters[i].Initialize(p.Name, p.Min, p.Max, p.Default);
        }

        return eventParameters;
      }
    }

    private void SetEnumsToConfig()
    {
      AudioEventConfig[] eventConfigs = Resources.LoadAll<AudioEventConfig>("");
      foreach (AudioEventConfig eventConfig in eventConfigs)
      {
        if (SetEventType(out FMOD_EventType eventType, NamesHelper.PrepareNameForEnum(eventConfig.name)))
          eventConfig.Initialize(eventType);

        foreach (AudioEventParameter parameter in eventConfig.Parameters)
        {
          if (SetParameterType(out FMOD_ParameterType parameterType, NamesHelper.PrepareNameForEnum(parameter.Name)))
            parameter.Initialize(parameterType);
        }

        EditorUtility.SetDirty(eventConfig);
      }

      CheckEnums();
      
      AssetDatabase.SaveAssets();

      bool SetEventType(out FMOD_EventType type, string name)
      {
        int names = Enum.GetNames(typeof(FMOD_EventType)).Length;

        for (int i = 0; i < names; i++)
        {
          FMOD_EventType typeI = (FMOD_EventType) i;
          if (name == ((FMOD_EventType) i).ToString())
          {
            type = typeI;
            return true;
          }
        }

        type = FMOD_EventType.Default;
        return false;
      }

      bool SetParameterType(out FMOD_ParameterType type, string name)
      {
        int names = Enum.GetNames(typeof(FMOD_ParameterType)).Length;

        for (int i = 0; i < names; i++)
        {
          FMOD_ParameterType typeI = (FMOD_ParameterType) i;

          if (name == ((FMOD_ParameterType) i).ToString())
          {
            type = typeI;
            return true;
          }
        }

        type = FMOD_ParameterType.Default;
        return false;
      }
      
      AssetDatabase.SaveAssets();

      void CheckEnums()
      {
        foreach (AudioEventConfig eventConfig in eventConfigs)
        {
          if(eventConfig.Type == FMOD_EventType.Default)
            Debug.Log($"{eventConfig.name} has Default Type");

          foreach (AudioEventParameter parameter in eventConfig.Parameters)
          {
            if(parameter.Type == FMOD_ParameterType.Default)
              Debug.Log($"{eventConfig.name} {parameter.Name} has Default Type");
          }
        }
      }
    }

    private void Cleanup()
    {
      string shortPath = GetShortPath();

      if (string.IsNullOrEmpty(shortPath))
        return;

      string[] assets = AssetDatabase.FindAssets(ConfigTypeMarker);

      foreach (string asset in assets)
      {
        string path = AssetDatabase.GUIDToAssetPath(asset);
        AssetDatabase.DeleteAsset(path);
      }

      AssetDatabase.SaveAssets();
    }
  }
}