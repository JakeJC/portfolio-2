using System.Collections.Generic;
using System.Linq;
using _Project.FMOD_AudioService.Logic;
using _Project.FMOD_AudioService.Logic.Enums;
using _Project.FMOD_AudioService.Logic.Events;
using _Project.FMOD_AudioService.Logic.Updater;
using _Project.Scripts.Services._Interfaces;
using _Project.Scripts.Services.FMOD_AudioService.Logic.Events;
using _Project.Scripts.Services.FMOD_AudioService.Logic.Updater;
using FMOD.Studio;
using Sirenix.Utilities;
using UnityEngine;

namespace _Project.Scripts.Services.FMOD_AudioService.Logic
{
  public class AudioService : IAudioService, IAudioServiceUpdate
  {
    private const int DefaultHash = 0;
    private const string AudioEventConfigsPath = "_Audio Event Configs";

    private readonly AudioServiceUpdater _updater;
    private List<AudioEventConfig> _eventConfigs;
    
    private readonly Dictionary<int, IAudioEvent> _events;
    private readonly Dictionary<FMOD_EventType, AudioEventLink> _links = new Dictionary<FMOD_EventType, AudioEventLink>();
    
    private bool _musicIsMute;
    private bool _soundsIsMute;
    private bool _vibrationIsMute;
    private readonly IAssetProvider _assetProvider;
    
    private AudioEventLink _currentMechanism;
    
    private FMOD.Studio.VCA _musicSlider;
    private FMOD.Studio.VCA _mechSlider;
    
    public AudioService(IAssetProvider assetProvider)
    {
      _assetProvider = assetProvider;
      _musicIsMute = false;
      _soundsIsMute = false;
      
      _events = new Dictionary<int, IAudioEvent>();

      GameObject updaterObject = new GameObject("[FMOD Audio Service Updater]");
      _updater = updaterObject.AddComponent<AudioServiceUpdater>();
      _updater.Initialize(this);
    }

    ~AudioService()
    {
      if(_updater)
        Object.Destroy(_updater.gameObject);
    }

    public void InitializeEvents()
    {
      _eventConfigs = _assetProvider.GetAllResources<AudioEventConfig>(AudioEventConfigsPath).ToList();
      InitializeSliders();
    }


    public void Update() =>
      _events.Values.ForEach(audioEvent => audioEvent.UpdateEmitterPosition());

    // bool OneShot

    public AudioEventChainer CreateEvent(out AudioEventLink link, FMOD_EventType eventType, GameObject emitterObject, bool isMusic = false)
    {
      link = CreateAudioEvent(eventType, emitterObject, isMusic);

      if (eventType != FMOD_EventType.Music && eventType != FMOD_EventType.UI && eventType != FMOD_EventType.Default)
        _currentMechanism = link;
      
      return GetChainer(link);
    }

    public AudioEventChainer SetParameter(AudioEventLink link, FMOD_ParameterType parameterType, float value01)
    {
      GetEvent(link)?.SetParameter(parameterType, value01);
      return GetChainer(link);
    }

    public void Play(AudioEventLink link) => 
      GetEvent(link)?.Play();

    public void StopCurrentMechanism() =>
      Stop(_currentMechanism);

    public void Pause(AudioEventLink link) => 
      GetEvent(link)?.PauseSound();
    
    public void Resume(AudioEventLink link) => 
      GetEvent(link)?.Resume();
    
    public void Stop(AudioEventLink link) =>
      GetEvent(link)?.Stop();

    public void Cleanup(AudioEventLink link)
    {
      Stop(link);
      GetEvent(link)?.Cleanup();
      _events.Remove(link.Hash);
      _links.Remove(_links.FirstOrDefault(p => p.Value.Hash == link.Hash).Key);
    }
    
    public void ResumeAll()
    {
      foreach (KeyValuePair<int, IAudioEvent> keyValuePair in _events) 
        keyValuePair.Value.Resume();
    }
    
    public void PauseAll()
    {
      foreach (KeyValuePair<int, IAudioEvent> keyValuePair in _events) 
        keyValuePair.Value.PauseSound();
    }
    
    public AudioEventLink GetLinkByEvent(FMOD_EventType fmodEventType)
    {
      if (!_links.ContainsKey(fmodEventType))
        return new AudioEventLink(0);
      
      return _links[fmodEventType];
    }

    public bool IsActive(AudioEventLink link) =>
      GetEvent(link)?.IsActive() ?? false;

    public bool MusicIsMute() => 
      _musicIsMute;

    public bool SoundsIsMute() => 
      _soundsIsMute;

    public bool VibrationIsMute() => 
      _vibrationIsMute;

    public void MuteMusic(bool value)
    {
      if (_musicIsMute == value)
        return;
      
      _musicIsMute = value;
      _events.Values.Where(e => e.IsMusic()).ForEach(a => a.Mute(_musicIsMute));
    }

    public void MuteSounds(bool value)
    {
      if (_soundsIsMute == value)
        return;
      
      _soundsIsMute = value;
      _events.Values.Where(e => !e.IsMusic()).ForEach(a => a.Mute(_soundsIsMute));
      SfxMusicFilter(!value);
    }

    public void MuteVibration(bool value) => 
      _vibrationIsMute = value;

    public void SetMechanismInLaunch(bool isEnabled) => 
      FMODUnity.RuntimeManager.StudioSystem.setParameterByName("music_mode", isEnabled? 1 : 0);

    public VCA GetMechanismsSoundVolumeSlider() => 
      _mechSlider;
    
    public VCA GetMediationMusicVolumeSlider() => 
      _musicSlider;
    
    public void Clean()
    {
      if(_updater)
        Object.Destroy(_updater.gameObject);
      
      foreach (KeyValuePair<FMOD_EventType, AudioEventLink> audioEventLink in _links)
        Stop(audioEventLink.Value);

      _eventConfigs?.Clear();

      _events.Clear();
      _links.Clear();
    }

    private void InitializeSliders()
    {
      _musicSlider = FMODUnity.RuntimeManager.GetVCA("vca:/Music");
      _mechSlider = FMODUnity.RuntimeManager.GetVCA("vca:/Mech");
    }
    
    private void SfxMusicFilter(bool isEnabled) => 
      FMODUnity.RuntimeManager.StudioSystem.setParameterByName("sfx_enable", isEnabled? 1 : 0);

    private AudioEventLink CreateAudioEvent(FMOD_EventType eventType, GameObject emitterObject, bool isMusic)
    {
      AudioEventConfig audioEventConfig = _eventConfigs.FirstOrDefault(c => c.Type.Equals(eventType));

      if (audioEventConfig == null)
      {
        Debug.Log("I can't find config");
        return new AudioEventLink(DefaultHash);
      }

      AudioEvent audioEvent = new AudioEvent(audioEventConfig, emitterObject, isMusic);
      int hash = audioEvent.GetHashCode();
      
      if(!_events.ContainsKey(hash))
        _events.Add(hash, audioEvent);

      var audioEventLink = new AudioEventLink(hash);

      if (_links.ContainsKey(eventType))
        _links[eventType] = audioEventLink;
      else
        _links.Add(eventType, audioEventLink);

      return audioEventLink;
    }

    private IAudioEvent GetEvent(AudioEventLink link)
    {
      if (_events.ContainsKey(link.Hash)) 
        return _events[link.Hash];
      
      Debug.Log("EventsPack doesn't contains link");
      return null;
    }

    private AudioEventChainer GetChainer(AudioEventLink link) =>
      new AudioEventChainer(this, link);
  }
}