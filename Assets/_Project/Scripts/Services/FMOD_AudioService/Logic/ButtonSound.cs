using System;
using System.Linq;
using _Project.FMOD_AudioService.Logic;
using _Project.FMOD_AudioService.Logic.Enums;
using _Project.FMOD_AudioService.Logic.Events;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Assets._Project.FMOD_AudioService.Logic
{
  public class ButtonSound
  {
    private readonly GameObject _emitterObject;
    
    private EventTrigger _eventTrigger;
    
    private EventTrigger.Entry _entryUp;
    private EventTrigger.Entry _entryDown;

    private readonly IAudioService _audioService;
    private readonly CanvasGroup[] _parentGroups;
    
    private readonly AudioEventLink _audioEventLink;

    private int _type;
    private bool _isMute;

    public ButtonSound(IAudioService audioService, GameObject emitterObject, int type)
    {
      _audioService = audioService;
      _emitterObject = emitterObject;
      _type = type;

      _parentGroups = emitterObject.GetComponentsInParent<CanvasGroup>();
      
      _audioService.CreateEvent(out AudioEventLink soundLink, FMOD_EventType.UI, _emitterObject);
      _audioEventLink = soundLink;
      
      var objectWithAnimation = emitterObject.AddComponent<ButtonSoundComponent>();
      objectWithAnimation.PointerDown += ButtonDown;
      objectWithAnimation.PointerUp += ButtonUp;
      
      //InitializeTriggers(emitterObject);
    }

    public void SetNewSound(int type) =>
      _type = type;

    public void MuteButtonSound(bool isMute) => 
      _isMute = isMute;
    
    public void Clear()
    {
      _audioService.Stop(_audioEventLink);
      _audioService.Cleanup(_audioEventLink);
    }
    
    private void ButtonDown(PointerEventData eventData)
    {
      if(!IsButtonInteractable())
        return;

      if(_isMute)
        return;

      _audioService.Play(_audioEventLink);
      _audioService.SetParameter(_audioEventLink, FMOD_ParameterType.current_ui, (float)_type);
    }

    private void ButtonUp(PointerEventData eventData)
    {
      if(!IsButtonInteractable())
        return;
    }
    
    private bool IsButtonInteractable()
    {
      bool parentsInteractable = _parentGroups.All(p => p != null && p.interactable);
      var canvasGroup = _emitterObject.GetComponent<CanvasGroup>();
      
      bool ignoreParentGroups = false;
      
      if(canvasGroup != null) 
        ignoreParentGroups = canvasGroup.ignoreParentGroups;
      
      return parentsInteractable || ignoreParentGroups;
    }
  }
  
  public class ButtonSoundComponent : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
  {
    public event Action<PointerEventData> PointerDown;
    public event Action<PointerEventData> PointerUp;

    public void OnPointerDown(PointerEventData eventData) => PointerDown?.Invoke(eventData);
    public void OnPointerUp(PointerEventData eventData) => PointerUp?.Invoke(eventData);
  }
  
}