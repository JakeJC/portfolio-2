using _Project.FMOD_AudioService.Logic.Enums;
using _Project.FMOD_AudioService.Logic.Events;
using _Project.Scripts.Service_Base;
using _Project.Scripts.Services._Interfaces;
using FMOD.Studio;
using UnityEngine;

namespace _Project.FMOD_AudioService.Logic
{
  public interface IAudioService : IService
  {
    AudioEventChainer CreateEvent(out AudioEventLink audioEventLink, FMOD_EventType eventType, GameObject emitterObject, bool isMusic = false);
    AudioEventChainer SetParameter(AudioEventLink link, FMOD_ParameterType parameterType, float value01);
    void Play(AudioEventLink link);
    void StopCurrentMechanism();
    void Stop(AudioEventLink link);
    void Cleanup(AudioEventLink link);
    bool IsActive(AudioEventLink link);
    bool MusicIsMute();
    bool SoundsIsMute();
    bool VibrationIsMute();
    void MuteMusic(bool value);
    void MuteSounds(bool value);
    void MuteVibration(bool value);
    AudioEventLink GetLinkByEvent(FMOD_EventType fmodEventType);
    void Clean();
    void SetMechanismInLaunch(bool isEnabled);
    void Pause(AudioEventLink link);
    void InitializeEvents();
    VCA GetMechanismsSoundVolumeSlider();
    VCA GetMediationMusicVolumeSlider();
    void Resume(AudioEventLink link);
    void ResumeAll();
    void PauseAll();
  }
}