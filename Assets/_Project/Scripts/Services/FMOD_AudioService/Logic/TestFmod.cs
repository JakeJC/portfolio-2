using Sirenix.OdinInspector;
using UnityEngine;

namespace _Project.FMOD_AudioService.Logic
{
  public class TestFmod : MonoBehaviour
  {
    [Button]
    public void SetMusicTrack(int value) =>
      FMODUnity.RuntimeManager.StudioSystem.setParameterByName("current_music", value);
  }
}