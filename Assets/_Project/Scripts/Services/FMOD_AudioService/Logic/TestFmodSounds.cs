using Sirenix.OdinInspector;
using UnityEngine;

namespace _Project.FMOD_AudioService.Logic
{
  public class TestFmodSounds : MonoBehaviour
  {
    [FMODUnity.EventRef] public string Mech_1;

    FMOD.Studio.EventInstance _mechInstance1;

    [Button]
    public void Launch()
    {
      _mechInstance1 = FMODUnity.RuntimeManager.CreateInstance(Mech_1);
      _mechInstance1.start();
    }

    [Button]
    public void SetSkin(int value) => 
      _mechInstance1.setParameterByName("current_skin", value);

    [Button]
    public void Stop()
    {
      _mechInstance1.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
      _mechInstance1.release();
    }
  }
}