using _Project.FMOD_AudioService.Logic.Updater;
using UnityEngine;

namespace _Project.Scripts.Services.FMOD_AudioService.Logic.Updater
{
  public class AudioServiceUpdater : MonoBehaviour
  {
    private IAudioServiceUpdate _audioService;

    private void Awake() =>
      DontDestroyOnLoad(gameObject);

    public void Initialize(IAudioServiceUpdate audioService) =>
      _audioService = audioService;

    private void Update() =>
      _audioService?.Update();
  }
}