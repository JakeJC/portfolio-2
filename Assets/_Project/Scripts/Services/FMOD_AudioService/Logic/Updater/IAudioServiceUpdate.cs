namespace _Project.FMOD_AudioService.Logic.Updater
{
  public interface IAudioServiceUpdate
  {
    public void Update();
  }
}