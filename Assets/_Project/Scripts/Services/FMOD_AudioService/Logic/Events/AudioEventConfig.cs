using _Project.FMOD_AudioService.Logic.Enums;
using UnityEngine;

namespace _Project.FMOD_AudioService.Logic.Events
{
  public class AudioEventConfig : ScriptableObject
  {
    public FMOD_EventType Type => _eventType;
    public string Path => _path;
    public bool IsOneShot => _isOneShot;
    public AudioEventParameter[] Parameters => _parameters;

    [SerializeField] private FMOD_EventType _eventType;
    [SerializeField] private string _path;
    [SerializeField] private bool _isOneShot;
    [SerializeField] private AudioEventParameter[] _parameters;

    public void Initialize(FMOD_EventType eventType) => 
      _eventType = eventType;

    public void Initialize(string path, bool isOneShot, params AudioEventParameter[] parameters)
    {
      _path = path;
      _isOneShot = isOneShot;
      _parameters = parameters;
    }
  }
}