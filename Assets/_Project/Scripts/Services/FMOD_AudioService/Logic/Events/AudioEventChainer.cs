using _Project.FMOD_AudioService.Logic.Enums;

namespace _Project.FMOD_AudioService.Logic.Events
{
  public readonly struct AudioEventChainer
  {
    private readonly IAudioService _audioService;
    private readonly AudioEventLink _audioEventLink;

    public AudioEventChainer(IAudioService audioService, AudioEventLink audioEventLink)
    {
      _audioEventLink = audioEventLink;
      _audioService = audioService;
    }

    public AudioEventChainer SetParameter(FMOD_ParameterType parameterType, float value01) =>
      _audioService.SetParameter(_audioEventLink, parameterType, value01);
  }
}