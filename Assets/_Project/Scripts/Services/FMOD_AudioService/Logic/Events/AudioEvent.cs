using System.Linq;
using _Project.FMOD_AudioService.Logic.Enums;
using _Project.FMOD_AudioService.Logic.Events;
using FMOD.Studio;
using UnityEngine;
using Debug = UnityEngine.Debug;

namespace _Project.Scripts.Services.FMOD_AudioService.Logic.Events
{
  public class AudioEvent : IAudioEvent
  {
    private const string MusicSnapshotPath = "snapshot:/music_off";
    private const string SoundsSnapshotPath = "snapshot:/sfx_off";

    private readonly AudioEventConfig _config;
    private readonly GameObject _emitterObject;
    
    private readonly bool _isMusic;

    private EventInstance _eventInstance;
    
    private EventInstance _soundEventInstance;

    public AudioEvent(AudioEventConfig config, GameObject emitterObject, bool isMusic)
    {
      _config = config;
      _emitterObject = emitterObject;
      _isMusic = isMusic;

      _soundEventInstance = FMODUnity.RuntimeManager.CreateInstance(_config.Path);
      _soundEventInstance.getVolume(out float _);

      UpdateEmitterPosition();
    }

    public void SetParameter(FMOD_ParameterType parameterType, float value01)
    {
      AudioEventParameter parameter = _config.Parameters.FirstOrDefault(p => p.Type.Equals(parameterType));

      if (parameter == null)
      {
        Debug.Log("I can't find param");
        return;
      }

      if (_isMusic)
        FMODUnity.RuntimeManager.StudioSystem.setParameterByName(parameter.Name, value01);
      else
        _soundEventInstance.setParameterByName(parameter.Name, value01);
    }

    public void Play() => 
      _soundEventInstance.start();

    public void Mute(bool value)
    {
      if (value)
      {
        _eventInstance = FMODUnity.RuntimeManager.CreateInstance(_isMusic ? MusicSnapshotPath : SoundsSnapshotPath);
        _eventInstance.start();
      }
      else
      {
        _soundEventInstance.setPaused(false);
        _eventInstance.stop(STOP_MODE.ALLOWFADEOUT);
        _eventInstance.release();
      }
    }

    public void Resume() => 
      _soundEventInstance.setPaused(false);
    
    public void PauseSound() => 
      _soundEventInstance.setPaused(true);

    public void Stop() =>
      _soundEventInstance.stop(STOP_MODE.ALLOWFADEOUT);

    public void Cleanup() =>
      _soundEventInstance.release();

    public bool IsActive()
    {
      _soundEventInstance.getPlaybackState(out PLAYBACK_STATE state);
      return state == PLAYBACK_STATE.PLAYING;
    }

    public bool IsMusic() =>
      _isMusic;

    public void UpdateEmitterPosition()
    {
      if (_emitterObject != null)
        _soundEventInstance.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(_emitterObject));
    }
  }
}