namespace _Project.FMOD_AudioService.Logic.Events
{
  public readonly struct AudioEventLink
  {
    public readonly int Hash;

    public AudioEventLink(int hash) => 
      Hash = hash;
  }
}