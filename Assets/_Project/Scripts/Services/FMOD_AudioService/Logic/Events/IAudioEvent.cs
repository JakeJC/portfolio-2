using _Project.FMOD_AudioService.Logic.Enums;

namespace _Project.FMOD_AudioService.Logic.Events
{
  public interface IAudioEvent
  {
    void SetParameter(FMOD_ParameterType parameterType, float value);
    void Play();
    void Mute(bool value);
    void Stop();
    void Cleanup();
    bool IsActive();
    bool IsMusic();
    void UpdateEmitterPosition();
    void PauseSound();
    void Resume();
  }
}