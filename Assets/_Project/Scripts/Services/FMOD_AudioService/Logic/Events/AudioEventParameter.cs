using _Project.FMOD_AudioService.Logic.Enums;
using UnityEngine;

namespace _Project.FMOD_AudioService.Logic.Events
{
  [System.Serializable]
  public class AudioEventParameter
  {
    public FMOD_ParameterType Type => _parameterType;
    public string Name => _name;
    public float MinValue => _minValue;
    public float MaxValue => _maxValue;
    public float DefaultValue => _defaultValue;

    [SerializeField] private FMOD_ParameterType _parameterType;
    [SerializeField] private string _name;
    [SerializeField] private float _minValue;
    [SerializeField] private float _maxValue;
    [SerializeField] private float _defaultValue;

    public void Initialize(FMOD_ParameterType parameterType) => 
      _parameterType = parameterType;
    public void Initialize(string name, float min, float max,float defaultValue)
    {
      _name = name;
      _minValue = min;
      _maxValue = max;
      _defaultValue = defaultValue;
    }
  }
}