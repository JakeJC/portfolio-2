using _Project.Scripts.Logic.GameLogic.Interfaces;
using _Project.Scripts.MonoBehaviours.Visual;
using UnityEngine;

namespace _Project.Scripts.Services.Environment
{
  public class EnvironmentController : IEnvironmentController
  {
    public string MechanismId { get; private set; }
    public IGameDataProvider GameDataProvider { get; }

    public EnvironmentController(IGameDataProvider gameDataProvider)
    {
      MechanismId = string.Empty;
      GameDataProvider = gameDataProvider;
      
      Object.FindObjectOfType<EnvironmentColorProvider>().Construct(this); // TODO: убрать как норм окружение будет
    }

    public void SetEnvironment(string mechanismId) => 
      MechanismId = mechanismId;
  }
}