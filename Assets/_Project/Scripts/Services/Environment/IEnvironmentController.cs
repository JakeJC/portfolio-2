using _Project.Scripts.Logic.GameLogic.Interfaces;
using _Project.Scripts.Service_Base;

namespace _Project.Scripts.Services.Environment
{
  public interface IEnvironmentController: IService
  {
    public string MechanismId { get;}
    public IGameDataProvider GameDataProvider { get; }
    
    public void SetEnvironment(string mechanismId);
  }
}