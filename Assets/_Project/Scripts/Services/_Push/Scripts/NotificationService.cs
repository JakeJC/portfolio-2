using System;
using System.Collections.Generic;
using _Project.Scripts.Services._Interfaces;
using _Project.Scripts.Services.Localization.Logic;
using UnityEngine;

#if UNITY_ANDROID
using Unity.Notifications.Android;
using UnityEngine.Rendering;
#endif

namespace _Project.Scripts.Services._Push.Scripts
{
  public class NotificationService : INotificationService
  {
    private const string DefaultChannel = "default_channel";
    private const string NotificationsSendKey = "Notifications.Send";

    private readonly ILocaleSystem _localeSystem;

    public Info Identifiers = new Info();

    public NotificationService(ILocaleSystem localeSystem)
    {
      _localeSystem = localeSystem;
      
#if UNITY_ANDROID
      RegisterChannel();
      ClearDelivered();
#endif
    }

    public void Send(DateTime targetTime)
    {
#if UNITY_ANDROID
      var notification = new AndroidNotification();
      
      notification.Title = "Imagine";
      notification.Text = _localeSystem.GetText("Main", $"Push.1");
      
      notification.FireTime = targetTime;
      
      notification.SmallIcon = $"icon_small";
      notification.LargeIcon = "icon_large";

      int notificationId = AndroidNotificationCenter.SendNotification(notification, DefaultChannel);
      
      Identifiers.Ids ??= new List<int>();
      Identifiers.Ids.Add(notificationId);

      string value = JsonUtility.ToJson(Identifiers);
      PlayerPrefs.SetString(NotificationsSendKey, value);
#endif
    }

    public void ClearAll()
    {
#if UNITY_ANDROID
      AndroidNotificationCenter.CancelAllNotifications();
      Debug.Log("[Notify] Cancel All");
#endif
    }

#if UNITY_ANDROID
    private void RegisterChannel()
    {
      var channel = new AndroidNotificationChannel()
      {
        Id = DefaultChannel,
        Name = "Default Channel",
        Importance = Importance.High,
        Description = "Generic notifications",
      };
      
      AndroidNotificationCenter.RegisterNotificationChannel(channel);
    }
#endif
    
    private void ClearDelivered()
    {
#if UNITY_ANDROID
      string json = PlayerPrefs.GetString(NotificationsSendKey, "{}");
      Identifiers = JsonUtility.FromJson<Info>(json);

      Identifiers.Ids ??= new List<int>();

      foreach (var identifier in Identifiers.Ids)
      {
        if ( AndroidNotificationCenter.CheckScheduledNotificationStatus(identifier) == NotificationStatus.Delivered) 
          AndroidNotificationCenter.CancelNotification(identifier);
      }
      
      Identifiers.Ids.Clear();
      PlayerPrefs.SetString(NotificationsSendKey, "{}");
#endif
    }
  }

  [Serializable]
  public struct Info
  {
    public List<int> Ids;
  }
}