using System;

namespace _Project.Scripts.Timer.Time_Providers
{
  public class SimpleTimeProvider : ITimeProvider
  {
    public DateTime GetCurrentTime() => 
      DateTime.UtcNow;
  }
}