using System;

namespace _Project.Scripts.Timer.Time_Providers
{
  public interface ITimeProvider
  {
    DateTime GetCurrentTime();
  }
}