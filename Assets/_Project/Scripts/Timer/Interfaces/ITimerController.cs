using System;
using _Project.Scripts.Timer.Time_Providers;
using AppacheRemote.Scripts.Interfaces;

namespace _Project.Scripts.Timer.Interfaces
{
  public interface ITimerController : IService
  {
    void LaunchService();
    void CreateTimer(string guid, DateTime targetDatetime);
    void CompleteTimer(string guid);
    bool IsUnderTimer(string guid);
    Mono.Timer GetTimer(string guid);
    void AddCallback(ICallbackExecutor callbackExecutor);
    void RemoveCallback(ICallbackExecutor callbackExecutor);
    void Clean();
    ITimeProvider TimeProvider { get; }
  }
}