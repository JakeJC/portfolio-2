using _Modules._InApps.Interfaces;
using _Modules._InApps.Scripts.Main;
using _Project.FMOD_AudioService.Logic;
using _Project.Scripts.Features.Stages;
using _Project.Scripts.Features.Stages.Interfaces;
using _Project.Scripts.Logic.GameLogic.Interfaces;
using _Project.Scripts.Logic.GameLogic.Meta;
using _Project.Scripts.Services._Interfaces;
using _Project.Scripts.Services._Push.Scripts;
using _Project.Scripts.Services.AssetProvider;
using _Project.Scripts.Services.Environment;
using _Project.Scripts.Services.FMOD_AudioService.Logic;
using _Project.Scripts.Services.FX;
using _Project.Scripts.Services.InputResolver;
using _Project.Scripts.Services.LeanTouchCamera.Scripts.Interfaces;
using _Project.Scripts.Services.LeanTouchCamera.Scripts.Logic;
using _Project.Scripts.Services.LoadScreen.Logic;
using _Project.Scripts.Services.Localization.Logic;
using _Project.Scripts.Services.MaterialService.Interfaces;
using _Project.Scripts.Services.MaterialService.Main;
using _Project.Scripts.Services.SaveSystem;
using AppacheRemote;
using AppacheRemote.Scripts.Interfaces;
using Zenject;
using IService = _Project.Scripts.Service_Base.IService;

namespace _Project.Scripts.Installers.Services
{
  public class ServicesRegistration : MonoInstaller
  {
    public override void InstallBindings()
    {      
      BindSingleService<IGameSaveSystem, GameSaveSystem>();
      BindSingleService<IAssetProvider, AssetProvider>();
      BindSingleService<IInputResolver, InputResolver>();
      BindSingleService<IAudioService, AudioService>();
      BindSingleService<IFXController, FXController>();
      BindSingleService<IMaterialService, MaterialService>();
      BindSingleService<ILocaleSystem, LocaleSystem>();
      BindSingleService<ILoadScreenService, LoadScreenService>();
      BindSingleService<IGameDataProvider, GameMetaDataService>();
      BindSingleService<ICameraController, CameraController>();
#if IOS_FIRST_RELEASE
      BindSingleService<IInAppService, InAppServiceDummy>();
#else
      BindSingleService<IInAppService, InAppService>();
#endif
      BindSingleService<INotificationService, NotificationService>();
      BindSingleService<IStagesService, StagesService>();
      BindSingleService<IEnvironmentController, EnvironmentController>();

      Container.Bind<IRemote>().To<Remote>().AsSingle(); 
    }

    private void BindSingleService<T, T2>() where T : IService where T2 : T => 
      Container.Bind<T>().To<T2>().AsSingle();
  }
}