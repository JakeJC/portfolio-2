﻿using System;
using System.Collections.Generic;
using _Modules._InApps.Interfaces;
using _Project.FMOD_AudioService.Logic;
using _Project.Scripts.InApp.Interfaces;
using _Project.Scripts.Logic.Factories.Interfaces;
using _Project.Scripts.Logic.GameLogic.Interfaces;
using _Project.Scripts.RestrictWindow;
using _Project.Scripts.RestrictWindow.Interfaces;
using _Project.Scripts.Services._Interfaces;
using _Project.Scripts.Services.InputResolver;
using _Project.Scripts.Services.LoadScreen.Logic;
using _Project.Scripts.Services.Localization.Logic;
using _Project.Scripts.Timer.Interfaces;
using AppacheAnalytics.Scripts.Interfaces;
using AppacheRemote.Scripts.Interfaces;
using UnityEngine.Purchasing;
using Product = _Modules._InApps.Scripts.Main.Product;

namespace _Project.Scripts.InApp
{
  public class InAppLauncher : IInAppAccessor
  {
    private readonly IInAppService _inAppService;
    private readonly IAssetProvider _assetProvider;
    private readonly IAudioService _audioService;
    private readonly ILocaleSystem _localeSystem;

    private readonly IFailConnectionFactory _failConnectionFactory;
    private readonly ILoadScreenService _loadScreenService;
    private readonly IGameDataProvider _gameDataProvider;
    private readonly IAnalytics _analytics;
    private readonly ICurrentScreen _currentScreen;
    private readonly IGameSaveSystem _gameSaveSystem;
    private readonly IInputResolver _inputResolver;
    private readonly IPrivacyPolicyFactory _privacyPolicyFactory;
    private readonly IRemote _remote;
    private readonly ITimerController _timerController;

    public ISubscription Subscription { get; private set; }

    public InAppLauncher(IInAppService inAppService, IAssetProvider assetProvider, IAudioService audioService, ILocaleSystem localeSystem,
      IFailConnectionFactory failConnectionFactory, ILoadScreenService loadScreenService, IGameDataProvider gameDataProvider,
      IAnalytics analytics, ICurrentScreen currentScreen, IGameSaveSystem gameSaveSystem,
      IInputResolver inputResolver, IPrivacyPolicyFactory privacyPolicyFactory, IRemote remote, ITimerController timerController)
    {
      _timerController = timerController;
      _remote = remote;
      _inputResolver = inputResolver;
      _privacyPolicyFactory = privacyPolicyFactory;
      _gameSaveSystem = gameSaveSystem;
      _currentScreen = currentScreen;
      _analytics = analytics;
      _gameDataProvider = gameDataProvider;
      _loadScreenService = loadScreenService;
      _failConnectionFactory = failConnectionFactory;
      _localeSystem = localeSystem;
      _audioService = audioService;
      _assetProvider = assetProvider;
      _inAppService = inAppService;

      AddSubscription();
    }

    public void Initialize() =>
      _inAppService.Initialize();

    public void SetCallback(Action onInitialized)
    {
      _inAppService.OnInitializedEvent += onInitialized;
      _inAppService.OnInitializeFailEvent += onInitialized;
    }

    private void AddSubscription()
    {
      _inAppService.AddProduct(new Product()
      {
        ProductType = ProductType.Subscription,
#if UNITY_IOS
        ProductName = "imagine_premium",
        IDs = new Dictionary<string, string>()
        {
          { "imagine_premium", AppleAppStore.Name }
        }
#else
        ProductName = "imagine_subs_worldwide",
        IDs = new Dictionary<string, string>()
        {
          {"imagine_subs_worldwide", GooglePlay.Name}
        }
#endif
      });

      var subscription = new Subscription(_inAppService, _assetProvider, _audioService, _localeSystem, _failConnectionFactory,
        _loadScreenService, _analytics, _currentScreen, _gameSaveSystem, _inputResolver, _privacyPolicyFactory, _remote, _timerController, _gameDataProvider);
      _inAppService.AddInAppCallback(subscription);

      Subscription = subscription;
    }

    public bool IsSubscriptionBought() =>
      _inAppService.IsSubscriptionPurchased();

    public void RestorePurchases() =>
      _inAppService.RestorePurchases();
  }
}