﻿using System;
using System.Linq;
using _Project.FMOD_AudioService.Logic;
using _Project.Scripts.Animations.UI;
using _Project.Scripts.Services.Localization.Logic;
using Assets._Project.FMOD_AudioService.Logic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace _Project.Scripts.InApp.Mono
{
  public class SubscriptionWindowLight : MonoBehaviour
  {   
    public TextMeshProUGUI Period;

    public Button Close;
    public Button Purchase;
    
    public CanvasGroup TopContainer;
    public CanvasGroup MiddleContainer;
    public CanvasGroup BottomContainer;

    private FadeCanvasGroup _fadeTopContainer;
    private FadeCanvasGroup _fadeMiddleContainer;
    private FadeCanvasGroup _fadeBottomContainer;
    
    private IAudioService _audioService;
    private ILocaleSystem _localeSystem;
    
    private Action _onClose;

    public bool IsOpen {get; private set; }

    public void Construct(ILocaleSystem localeSystem, IAudioService audioService, Action onClick, Action onClose)
    {
      gameObject.SetActive(false);

      _onClose = onClose;
      
      _localeSystem = localeSystem;
      _audioService = audioService;

      Purchase.onClick.AddListener(onClick.Invoke);
      Close.onClick.AddListener(Hide);

      InitializeAnimations();
      InitializeSounds();
      
      Localize(localeSystem);
    }
    
    public void Open()
    {     
      IsOpen = true;

      _fadeTopContainer.Appear(() => gameObject.SetActive(true));
      _fadeMiddleContainer.Appear();
      _fadeBottomContainer.Appear();
    }
    
    public void Hide(Action onClose)
    {
      _fadeTopContainer.Disappear();
      _fadeMiddleContainer.Disappear();
      _fadeBottomContainer.Disappear(null, () =>
      {
        IsOpen = false;
        
        gameObject.SetActive(false);
        onClose?.Invoke();
      });
    }

    private void Hide() => 
      Hide(_onClose);

    public void SetPeriod(string period) => 
      Period.text = string.Format(_localeSystem.GetText("Main", "Subscription.Description"), period);

    private void InitializeAnimations()
    {
      _fadeTopContainer = new FadeCanvasGroup(TopContainer);
      _fadeMiddleContainer = new FadeCanvasGroup(MiddleContainer, 0.25f);
      _fadeBottomContainer = new FadeCanvasGroup(BottomContainer, 0.5f);
      
      new ButtonAnimation(Close.transform);
      new ButtonAnimation(Purchase.transform);
    }

    private void InitializeSounds()
    {
      new ButtonSound(_audioService, Close.gameObject, 17);
      new ButtonSound(_audioService, Purchase.gameObject, 0);
    }
    
    private void Localize(ILocaleSystem localeSystem)
    {
      var localizedTexts =
        transform.GetComponentsInChildren<LocalizedMainText>(true).ToList();
      
      localizedTexts.ForEach(item =>
      {
        item.Construct(localeSystem);
        item.Awake();
      });
    }
  }
}