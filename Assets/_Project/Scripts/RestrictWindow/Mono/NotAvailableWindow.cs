﻿using System;
using System.Linq;
using _Project.FMOD_AudioService.Logic;
using _Project.Scripts.Animations.UI;
using _Project.Scripts.Services.Localization.Logic;
using Assets._Project.FMOD_AudioService.Logic;
using UnityEngine;
using UnityEngine.UI;

namespace _Project.Scripts.InApp.Mono
{
  public class NotAvailableWindow : MonoBehaviour
  {
    public Button Leave;
    
    public CanvasGroup TopContainer;
    public CanvasGroup MiddleContainer1;
    public CanvasGroup MiddleContainer2;
    public CanvasGroup BottomContainer;

    private FadeCanvasGroup _fadeTopContainer;
    private FadeCanvasGroup _fadeMiddle1Container;
    private FadeCanvasGroup _fadeMiddle2Container;
    private FadeCanvasGroup _fadeBottomContainer;

    private IAudioService _audioService;
    private Action _onClose;

    public void Construct(ILocaleSystem localeSystem, IAudioService audioService)
    {
      gameObject.SetActive(false);

      _audioService = audioService;
      
      InitializeAnimations();
      InitializeSounds();
      
      Localize(localeSystem);
    }

    public void SetCloseAction(Action onClose)
    {
      _onClose = onClose;
      Leave.onClick.AddListener(_onClose.Invoke);
    }

    public void Open()
    {
      _fadeTopContainer.Appear(() => gameObject.SetActive(true));
      _fadeMiddle1Container.Appear();
      _fadeMiddle2Container.Appear();
      _fadeBottomContainer.Appear();
    }

    private void InitializeAnimations()
    {
      _fadeTopContainer = new FadeCanvasGroup(TopContainer);
      _fadeMiddle1Container = new FadeCanvasGroup(MiddleContainer1, 0.25f);
      _fadeMiddle2Container = new FadeCanvasGroup(MiddleContainer2, 0.5f);
      _fadeBottomContainer = new FadeCanvasGroup(BottomContainer, 0.75f);
      
      new ButtonAnimation(Leave.transform);
    }

    private void InitializeSounds() => 
      new ButtonSound(_audioService, Leave.gameObject, 0);

    private void Localize(ILocaleSystem localeSystem)
    {
      var localizedTexts =
        transform.GetComponentsInChildren<LocalizedMainText>(true).ToList();
      
      localizedTexts.ForEach(item =>
      {
        item.Construct(localeSystem);
        item.Awake();
      });
    }
  }
}