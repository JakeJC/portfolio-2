﻿using System;
using System.Linq;
using _Project.FMOD_AudioService.Logic;
using _Project.Scripts.Animations.UI;
using _Project.Scripts.InApp.Interfaces;
using _Project.Scripts.RestrictWindow.Interfaces;
using _Project.Scripts.Services.Localization.Logic;
using Assets._Project.FMOD_AudioService.Logic;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.Experimental.GlobalIllumination;
using UnityEngine.UI;

namespace _Project.Scripts.RestrictWindow.Mono
{
  public class SubscriptionWindow : MonoBehaviour, ISubscriptionWindow
  {
    private const float FieldPrivacyPolicySpacing = -55;
    
    public TextMeshProUGUI Price;
    public TextMeshProUGUI Period;
    
    public Button Close;
    public Button Purchase;
    public Button PrivacyPolicyCheckbox;
    public Button PrivacyPolicy;

    public Button Terms;

    public VerticalLayoutGroup FieldLayoutGroup;

    public Sprite CheckBoxEnabled;
    public Sprite CheckBoxDisabled;
    
    public CanvasGroup TopContainer;
    public CanvasGroup MiddleContainer;
    public CanvasGroup ButtonContainer;
    public CanvasGroup BottomContainer;
    public CanvasGroup PrivacyPolicyContainer;

    public GameObject DescriptionPanel;
    public GameObject NotAvailablePanel;
    
    private FadeCanvasGroup _fadeTopContainer;
    private FadeCanvasGroup _fadeMiddleContainer;
    private FadeCanvasGroup _fadeButtonContainer;
    private FadeCanvasGroup _fadeBottomContainer;

    protected IAudioService AudioService;
    protected ILocaleSystem LocaleSystem;

    public bool IsOpen {get; private set; }

    public void Construct(ILocaleSystem localeSystem, IAudioService audioService, Action onClick, Action onClose,
      Action onClickPrivacyPolicyCheckbox, Action onClickPrivacyPolicy, Action onClickTerms)
    {
      gameObject.SetActive(false);
      
      LocaleSystem = localeSystem;
      AudioService = audioService;
      
      Purchase.onClick.AddListener(onClick.Invoke);
      Close.onClick.AddListener(onClose.Invoke);
      PrivacyPolicyCheckbox.onClick.AddListener(onClickPrivacyPolicyCheckbox.Invoke);
      PrivacyPolicy.onClick.AddListener(onClickPrivacyPolicy.Invoke);
#if UNITY_IOS
      Terms.onClick.AddListener(onClickTerms.Invoke);
#endif
      
      InitializeAnimations();
      InitializeSounds();
      
      Localize(localeSystem);
    }

    public void SetUnavailable()
    {
      NotAvailablePanel.SetActive(true);
      Purchase.gameObject.SetActive(false);
      DescriptionPanel.SetActive(false);
    }

    public void ShowPrivacyPolicyContainer()
    {
      FieldLayoutGroup.spacing = FieldPrivacyPolicySpacing;
      PrivacyPolicyContainer.DOFade(1, 1);
    }

    public void SwitchPrivacyPolicyCheckbox(bool enabled)
    {
      ButtonContainer.interactable = enabled;
      
      PrivacyPolicyCheckbox.GetComponent<Image>().sprite = enabled 
        ? CheckBoxEnabled 
        : CheckBoxDisabled;
    }

    public virtual void Open()
    {
      IsOpen = true;
      
      _fadeTopContainer.Appear(() => gameObject.SetActive(true));
      _fadeMiddleContainer.Appear();
      _fadeButtonContainer.Appear();
      _fadeBottomContainer.Appear();
    }
    
    public virtual void Hide(Action onClose)
    {
      _fadeTopContainer.Disappear();
      _fadeMiddleContainer.Disappear();
      _fadeButtonContainer.Disappear();
      _fadeBottomContainer.Disappear(null, () =>
      {
        IsOpen = false;

        gameObject.SetActive(false);
        onClose?.Invoke();
      });
    }

    public virtual void SetPeriod(string period) => 
      Period.text = string.Format(LocaleSystem.GetText("Main", "Subscription.FreeDays"), period);

    public virtual void SetPrice(string price)
    {
#if UNITY_IOS
      Price.text = string.Format(LocaleSystem.GetText("Main", "Subscription.IOS.Price"), price);
#else
      Price.text = string.Format(LocaleSystem.GetText("Main", "Subscription.PerYear"), price);
#endif
    }

    protected virtual void InitializeAnimations()
    {
      _fadeTopContainer = new FadeCanvasGroup(TopContainer);
      _fadeMiddleContainer = new FadeCanvasGroup(MiddleContainer, 0.25f);
      _fadeButtonContainer = new FadeCanvasGroup(ButtonContainer, 0.5f);
      _fadeBottomContainer = new FadeCanvasGroup(BottomContainer, 0.75f);
      
      new ButtonAnimation(Close.transform);
      new ButtonAnimation(Purchase.transform);
    }

    protected virtual void InitializeSounds()
    {
      new ButtonSound(AudioService, Close.gameObject, 17);
      new ButtonSound(AudioService, Purchase.gameObject, 0);
      new ButtonSound(AudioService, PrivacyPolicy.gameObject, 17);
      new ButtonSound(AudioService, PrivacyPolicyCheckbox.gameObject, 17);
    }

    private void Localize(ILocaleSystem localeSystem)
    {
      var localizedTexts =
        transform.GetComponentsInChildren<LocalizedMainText>(true).ToList();
      
      localizedTexts.ForEach(item =>
      {
        item.Construct(localeSystem);
        item.Awake();
      });
    }
  }
}