using System.Collections.Generic;
using System.Linq;
using DG.Tweening;
using TMPro;
using UnityEngine;

namespace _Project.Scripts.RestrictWindow.Mono
{
  public class SubscriptionsCounter : MonoBehaviour
  {
    [SerializeField] private Transform container;
    [SerializeField] private List<TMP_Text> texts;

    [Tooltip("Time interval between digits")]
    [SerializeField] private float interval = 0.3f;

    [Tooltip("Time duration for single digit")]
    [SerializeField] private float duration = 0.5f;

    private Sequence _sequenceCounting;
    private Sequence _sequenceAnimating;

    private void OnValidate()
    {
      Validate();
    }

    private void Awake()
    {
      Validate();
    }

    private void OnDestroy()
    {
      _sequenceCounting.Kill();
      _sequenceAnimating.Kill();
    }

    private void Validate()
    {
      if (container == null) container = transform;
      if (container == null) return;
      texts = container.GetComponentsInChildren<TMP_Text>().ToList();
    }

    public void StartCounter(SubscriptionWindowWithCounter.SubscriptionsCountProperty countProperty)
    {
      SetCounter(countProperty.Value);

      _sequenceCounting = DOTween.Sequence();
      _sequenceCounting.SetDelay(2, false);

      _sequenceCounting.AppendCallback(() =>
      {
        countProperty.Value += 1;
        AnimateCounter(countProperty.Value);
      });

      _sequenceCounting.AppendInterval(20);
      _sequenceCounting.SetLoops(-1);
    }

    public void SetCounter(int number)
    {
      var str = number.ToString();

      Debug.Assert(str.Length == texts.Count);

      for (var i = 0; i < str.Length; i++)
      {
        texts[i].text = str[i].ToString();
      }
    }

    private void AnimateCounter(int number)
    {
      texts = container.GetComponentsInChildren<TMP_Text>().ToList();

      var str = number.ToString();

      Debug.Assert(str.Length == texts.Count);

      _sequenceAnimating = DOTween.Sequence();

      // Animate digits from right to left.
      var textsRtl = Enumerable.Reverse(texts).ToList();
      var strRtl = string.Join("", str.Reverse());

      for (var i = 0; i < strRtl.Length; i++)
      {
        var digitText = textsRtl[i];
        var digitString = strRtl[i].ToString();

        if (digitText.text == digitString)
          break;

        var prevText = digitText;
        var nextText = Duplicate(digitText);

        nextText.text = digitString;

        var size = prevText.rectTransform.rect;
        nextText.rectTransform.anchoredPosition = Vector2.up * size.height;

        var start = i * interval;
        _sequenceAnimating.Insert(start, prevText.rectTransform.DOAnchorPosY(-size.height, duration));
        _sequenceAnimating.Insert(start, nextText.rectTransform.DOAnchorPosY(0, duration));
        _sequenceAnimating.InsertCallback(start + duration, () =>
        {
          Destroy(prevText.gameObject);
          prevText = nextText;
          nextText = null;
        });
      }
    }

    private T Duplicate<T>(T original) where T : Component
    {
      var duplicate = Instantiate(original, original.transform.parent, true);
      duplicate.name = duplicate.name.Replace("(Clone)", "");
      return duplicate;
    }
  }
}