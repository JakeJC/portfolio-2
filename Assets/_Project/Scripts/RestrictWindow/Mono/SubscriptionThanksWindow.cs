using System;
using System.Linq;
using _Project.FMOD_AudioService.Logic;
using _Project.Scripts.Animations.UI;
using _Project.Scripts.Services.Localization.Logic;
using Assets._Project.FMOD_AudioService.Logic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace _Project.Scripts.InApp.Mono
{
  public class SubscriptionThanksWindow : MonoBehaviour
  {
    public Button Close;
    
    public CanvasGroup TopContainer;
    public CanvasGroup MiddleContainer;
    public CanvasGroup BottomContainer;

    private FadeCanvasGroup _fadeTopContainer;
    private FadeCanvasGroup _fadeMiddleContainer;
    private FadeCanvasGroup _fadeBottomContainer;
    
    private IAudioService _audioService;
    private ILocaleSystem _localeSystem;

    public bool IsOpen {get; private set; }

    public void Construct(ILocaleSystem localeSystem, IAudioService audioService, Action onClose)
    {
      gameObject.SetActive(false);

      _localeSystem = localeSystem;
      _audioService = audioService;
      
      Close.onClick.AddListener(onClose.Invoke);

      InitializeAnimations();
      InitializeSounds();
      
      Localize(localeSystem);
    }
    
    public void Open()
    {     
      IsOpen = true;

      _fadeTopContainer.Appear(() => gameObject.SetActive(true));
      _fadeMiddleContainer.Appear();
      _fadeBottomContainer.Appear();
    }
    
    public void Hide(Action onClose)
    {
      _fadeTopContainer.Disappear();
      _fadeMiddleContainer.Disappear();
      _fadeBottomContainer.Disappear(null, () =>
      {
        IsOpen = false;
        
        gameObject.SetActive(false);
        onClose?.Invoke();
      });
    }
    
    private void InitializeAnimations()
    {
      _fadeTopContainer = new FadeCanvasGroup(TopContainer);
      _fadeMiddleContainer = new FadeCanvasGroup(MiddleContainer, 0.25f);
      _fadeBottomContainer = new FadeCanvasGroup(BottomContainer, 0.5f);
      
      new ButtonAnimation(Close.transform);
    }

    private void InitializeSounds()
    {
      new ButtonSound(_audioService, Close.gameObject, 0);
    }
    
    private void Localize(ILocaleSystem localeSystem)
    {
      var localizedTexts =
        transform.GetComponentsInChildren<LocalizedMainText>(true).ToList();
      
      localizedTexts.ForEach(item =>
      {
        item.Construct(localeSystem);
        item.Awake();
      });
    }
  }
}