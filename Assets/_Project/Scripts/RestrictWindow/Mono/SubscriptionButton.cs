using System;
using System.Collections.Generic;
using System.Linq;
using _Project.FMOD_AudioService.Logic;
using _Project.Scripts.Animations.UI;
using _Project.Scripts.Services.Localization.Logic;
using Assets._Project.FMOD_AudioService.Logic;
using UnityEngine;
using UnityEngine.UI;

namespace _Project.Scripts.InApp.Mono
{
  public class SubscriptionButton : MonoBehaviour
  {
    public Button Button;
    
    public void Construct(ILocaleSystem localeSystem, IAudioService audioService, Action onClick)
    {
      Button.onClick.AddListener(onClick.Invoke);
      
      new ButtonAnimation(Button.transform);
      new ButtonSound(audioService, Button.gameObject, 0);

      Localize(localeSystem);
    }
    
    private void Localize(ILocaleSystem localeSystem)
    {
      List<LocalizedMainText> localizedTexts = transform.GetComponentsInChildren<LocalizedMainText>(true).ToList();
      localizedTexts.ForEach(item =>
      {
        item.Construct(localeSystem);
        item.Awake();
      });
    }
  }
}