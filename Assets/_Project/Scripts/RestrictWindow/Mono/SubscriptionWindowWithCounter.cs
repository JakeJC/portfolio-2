using System;
using _Project.Scripts.Animations.UI;
using UnityEngine;
using UnityEngine.UI;

namespace _Project.Scripts.RestrictWindow.Mono
{
  public class SubscriptionWindowWithCounter : SubscriptionWindow
  {
    [SerializeField] private SubscriptionsCounter counter;

    [SerializeField] private CanvasGroup counterContainer;
    [SerializeField] private HorizontalLayoutGroup counterContainerDirection;

    private FadeCanvasGroup _fadeCounterContainer;

    private readonly SubscriptionsCountProperty _countProperty =
      new SubscriptionsCountProperty();

    protected override void InitializeAnimations()
    {
      base.InitializeAnimations();

      _fadeCounterContainer = new FadeCanvasGroup(counterContainer, 0.35f);
    }

    public override void Open()
    {
      base.Open();

      counterContainerDirection.reverseArrangement = LocaleSystem.IsRightToLeft();
      _fadeCounterContainer.Appear();
    }

    public override void Hide(Action onClose)
    {
      _fadeCounterContainer.Disappear();

      base.Hide(onClose);
    }

    public void StartCounter()
    {
      if (_countProperty.Value == 0)
        _countProperty.Value = 198;
      else
        _countProperty.Value += 15;

      counter.StartCounter(_countProperty);
    }

    public class SubscriptionsCountProperty
    {
      public int Value
      {
        get => PlayerPrefs.GetInt("SubscriptionsCount");
        set => PlayerPrefs.SetInt("SubscriptionsCount", value);
      }
    }
  }
}