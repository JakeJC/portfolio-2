﻿using System;
using UnityEngine;

namespace _Project.Scripts.InApp.Mono
{
  public class TestSkipSubscription : MonoBehaviour
  {
    public Action OnClick;

    public void OnGUI()
    {
#if SKIP_SUBSCRIPTION
      if (GUI.Button(new Rect(Screen.width/2 - 200, Screen.height - 100, 400, 60), "Skip Subscription"))
        OnClick?.Invoke();
#endif
    }
  }
}