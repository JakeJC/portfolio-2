﻿using System;
using System.Linq;
using _Project.FMOD_AudioService.Logic;
using _Project.Scripts.Animations.UI;
using _Project.Scripts.InApp.Interfaces;
using _Project.Scripts.RestrictWindow.Interfaces;
using _Project.Scripts.Services.Localization.Logic;
using Assets._Project.FMOD_AudioService.Logic;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace _Project.Scripts.InApp.Mono
{
  public class SubscriptionWindowReturn : MonoBehaviour, ISubscriptionWindow
  {
    private const float AnimationGalleryButtonDuration = 0.3f;
    
    public Button Close;
    public Button Purchase;
    public Button Gallery;

    public CanvasGroup TopContainer;
    public CanvasGroup MiddleContainer;
    public CanvasGroup ButtonContainer;

    private FadeCanvasGroup _fadeTopContainer;
    private FadeCanvasGroup _fadeMiddleContainer;
    private FadeCanvasGroup _fadeButtonContainer;
    
    private IAudioService _audioService;
    
    public GameObject NotAvailablePanel;

    public RectTransform AnimationGalleryButtonRect;

    private Image _animationGalleryButtonImage;
    private Vector2 _animationGalleryButtonDefaultSize;
    private Color _animationGalleryButtonDefaultColor;
    
    private Color AnimationGalleryButtonHideColor =>
      new Color(_animationGalleryButtonDefaultColor.r, _animationGalleryButtonDefaultColor.g, _animationGalleryButtonDefaultColor.b, 0);

    public bool IsOpen {get; private set; }

    public void Construct(ILocaleSystem localeSystem, IAudioService audioService, Action onClick, Action onClose, 
      Action onClickPrivacyPolicyCheckbox, Action onClickPrivacyPolicy, Action onTermsClick)
    {
      gameObject.SetActive(false);
      
      _audioService = audioService;
      
      Purchase.onClick.AddListener(onClick.Invoke);
      Close.onClick.AddListener(onClose.Invoke);
      
      InitializeAnimations();
      InitializeSounds();
      
      Localize(localeSystem);
    }
    
    public void SetUnavailable()
    {
      NotAvailablePanel.SetActive(true);
      Purchase.gameObject.SetActive(false);
    }
    
    public void ShowPrivacyPolicyContainer()
    {
    }

    public void SwitchPrivacyPolicyCheckbox(bool enabled)
    {
    }

    public void Open()
    {
      IsOpen = true;
      
      _fadeTopContainer.Appear(() => gameObject.SetActive(true));
      _fadeMiddleContainer.Appear();
      _fadeButtonContainer.Appear();
    }
    
    public void Hide(Action onClose)
    {
      _fadeTopContainer.Disappear();
      _fadeMiddleContainer.Disappear();
      _fadeButtonContainer.Disappear(null, () =>
      {
        IsOpen = false;

        gameObject.SetActive(false);
        onClose?.Invoke();
      });
    }

    public void SetTransitionOnGallery(Action onGallery) => 
      Gallery.onClick.AddListener(onGallery.Invoke);

    private void InitializeAnimations()
    {
      _fadeTopContainer = new FadeCanvasGroup(TopContainer);
      _fadeMiddleContainer = new FadeCanvasGroup(MiddleContainer, 0.25f);
      _fadeButtonContainer = new FadeCanvasGroup(ButtonContainer, 0.5f);
      
      new ButtonAnimation(Close.transform);
      new ButtonAnimation(Purchase.transform);
      
      InitializeAnimationGalleryButton();
    }

    private void InitializeSounds()
    {
      new ButtonSound(_audioService, Close.gameObject, 17);
      new ButtonSound(_audioService, Purchase.gameObject, 0);
      new ButtonSound(_audioService, Gallery.gameObject, 0);
    }
    
    private void InitializeAnimationGalleryButton()
    {
      _animationGalleryButtonImage = AnimationGalleryButtonRect.GetComponent<Image>();
      _animationGalleryButtonDefaultSize = AnimationGalleryButtonRect.sizeDelta;
      _animationGalleryButtonDefaultColor = _animationGalleryButtonImage.color;
      _animationGalleryButtonImage.color = AnimationGalleryButtonHideColor;

      EventTrigger trigger = Gallery.GetComponent<EventTrigger>();
      EventTrigger.Entry entry = new EventTrigger.Entry();
      entry.eventID = EventTriggerType.PointerDown;
      entry.callback.AddListener((eventData) =>
      {
        _animationGalleryButtonImage.color = AnimationGalleryButtonHideColor;
        _animationGalleryButtonImage.DOColor(_animationGalleryButtonDefaultColor, AnimationGalleryButtonDuration);

        AnimationGalleryButtonRect.sizeDelta = new Vector2(_animationGalleryButtonDefaultSize.x / 2, _animationGalleryButtonDefaultSize.y);
        AnimationGalleryButtonRect.DOSizeDelta(_animationGalleryButtonDefaultSize, AnimationGalleryButtonDuration);
      });
      trigger.triggers.Add(entry);

      entry = new EventTrigger.Entry();
      entry.eventID = EventTriggerType.PointerExit;
      entry.callback.AddListener((eventData) => { _animationGalleryButtonImage.DOColor(AnimationGalleryButtonHideColor, AnimationGalleryButtonDuration); });
      trigger.triggers.Add(entry);
    }

    private void Localize(ILocaleSystem localeSystem)
    {
      var localizedTexts =
        transform.GetComponentsInChildren<LocalizedMainText>(true).ToList();
      
      localizedTexts.ForEach(item =>
      {
        item.Construct(localeSystem);
        item.Awake();
      });
    }
  }
}