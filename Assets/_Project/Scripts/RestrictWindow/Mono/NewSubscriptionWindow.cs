using _Project.Scripts.RestrictWindow.Mono;
using TMPro;

namespace _Project.Scripts.InApp.Mono
{
  public class NewSubscriptionWindow : SubscriptionWindow
  {
    public TextMeshProUGUI InfoText;
    
    private string _periodText;
    private string _priceText;

    public override void SetPeriod(string period)
    {
      _periodText = string.Format(LocaleSystem.GetText("Main", "Subscription.Period"), period);
      InfoText.text = $"<#CCADF0>{_periodText},</color> {_priceText}";
    }

    public override void SetPrice(string price)
    {
      _priceText = string.Format(LocaleSystem.GetText("Main", "Subscription.Price"), price).ToLowerInvariant();
      InfoText.text = $"<#CCADF0>{_periodText},</color> {_priceText}";
    }
  }
}