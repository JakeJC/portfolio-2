using _Project.Scripts.InApp.Mono;
using _Project.Scripts.RestrictWindow.Mono;

namespace _Project.Scripts.InApp
{
  public interface ISubscriptionFactory
  {
    SubscriptionWindow CreateInterviewSubscriptionWindow();
    void Clear();
  }
}