﻿using System;
using _Modules._InApps.Interfaces;
using _Modules._InApps.Scripts.Main;
using _Project.FMOD_AudioService.Logic;
using _Project.Scripts.Features.Disable_Ads;
using _Project.Scripts.InApp;
using _Project.Scripts.InApp.Interfaces;
using _Project.Scripts.InApp.Mono;
using _Project.Scripts.Logic.Factories.Interfaces;
using _Project.Scripts.Logic.GameLogic.Interfaces;
using _Project.Scripts.MonoBehaviours.UI;
using _Project.Scripts.RestrictWindow.Interfaces;
using _Project.Scripts.RestrictWindow.Mono;
using _Project.Scripts.Services._Interfaces;
using _Project.Scripts.Services.InputResolver;
using _Project.Scripts.Services.LoadScreen.Logic;
using _Project.Scripts.Services.Localization.Logic;
using _Project.Scripts.Timer.Interfaces;
using AppacheAds.Scripts.Interfaces;
using AppacheAnalytics.Scripts.Interfaces;
using AppacheRemote.Scripts.Interfaces;
using Mycom.Tracker.Unity;
using UnityEngine;
using Product = UnityEngine.Purchasing.Product;
using ProductId = _Modules._InApps.Scripts.ProductId;

#if SKIP_SUBSCRIPTION
using Object = System.Object;
#endif

namespace _Project.Scripts.RestrictWindow
{
  public class Subscription : IInAppCallback, ISubscription
  {
    private const string TermsUrl = "https://www.apple.com/legal/internet-services/itunes/dev/stdeula/";
    
    private readonly IInAppService _inAppService;
    private readonly IAudioService _audioService;
    private readonly ILocaleSystem _localeSystem;
    private readonly IGameSaveSystem _gameSaveSystem;
    private readonly IInputResolver _inputResolver;
    private readonly IPrivacyPolicyFactory _privacyPolicyFactory;
    private readonly ILoadScreenService _loadScreenService;
    private readonly IAnalytics _analytics;
    private readonly ITimerController _timerController;
    private readonly IGameDataProvider _gameDataProvider;
    private readonly IAdsService _ads;
    
#if UNITY_ANDROID
    private readonly IFailConnectionFactory _failConnectionFactory;
#endif
    private readonly ISubscriptionFactory _subscriptionFactory;

    private ISubscriptionWindow _subscriptionWindow;
    private readonly IRemote _remote;

    private Action _onNext;
    private Action _onClose;

    private PrivacyPolicy _privacyPolicy;

#if SKIP_SUBSCRIPTION
    private TestSkipSubscription _tester;
#endif

    public ProductId ProductsId { set; get; }

    public Subscription(IInAppService inAppService, IAssetProvider assetProvider, IAudioService audioService,
      ILocaleSystem localeSystem, IFailConnectionFactory failConnectionFactory, ILoadScreenService loadScreenService,
      IAnalytics analytics, ICurrentScreen currentScreen,
      IGameSaveSystem gameSaveSystem, IInputResolver inputResolver, IPrivacyPolicyFactory privacyPolicyFactory,
      IRemote remote, ITimerController timerController, IGameDataProvider gameDataProvider)
    {
      _gameDataProvider = gameDataProvider;
      _timerController = timerController;
      _remote = remote;
      _gameSaveSystem = gameSaveSystem;
      _inputResolver = inputResolver;
      _privacyPolicyFactory = privacyPolicyFactory;
      _analytics = analytics;
      _loadScreenService = loadScreenService;
#if UNITY_ANDROID
      _failConnectionFactory = failConnectionFactory;
#endif
      _localeSystem = localeSystem;
      _audioService = audioService;
      _inAppService = inAppService;
      
#if UNITY_IOS
      ProductsId = ProductId.imagine_premium;
#else
      ProductsId = ProductId.imagine_subs_worldwide;
#endif

      _subscriptionFactory = new SubscriptionFactory(assetProvider);
    }

    public void ShowInterviewSubscription(Action onNext, Action onClose)
    {
#if IOS_FIRST_RELEASE
      onClose?.Invoke();
      return;
#endif
      _onClose = onClose;
      _onNext = onNext;
      
      if (CheckBreakingRules())
        return;

      _privacyPolicy = _privacyPolicyFactory.SpawnPrivacyPolicy();
      _privacyPolicy.Construct(_gameSaveSystem, _audioService, _inputResolver, _localeSystem, _analytics, _remote);

      ShowInterviewSubscriptionWindow();
    }

    public void OnPurchaseComplete(Product product)
    {
      _timerController.Clean();

      SendPurchaseAnalytics(product);

      if (_subscriptionWindow != null)
        if (_subscriptionWindow.IsOpen)
          _subscriptionWindow.Hide(Next);

      _gameDataProvider.FirstSession.IsSubscriptionDeny = false;

      Debug.Log("[Subscription] OnPurchaseComplete");

      void Next()
      {
        _analytics.Send("btn_next");
        _subscriptionFactory.Clear();
        _onNext?.Invoke();
#if SKIP_SUBSCRIPTION
        ClearSkipSubscription();
#endif
      }
    }

    public void OnPurchaseFailed() =>
      Debug.Log("[Subscription] OnPurchaseFailed");

    public void OnNotPurchased() =>
      Debug.Log("[Subscription] OnNotPurchased");

    private void Purchase()
    {
      Debug.Log("[Subscription] Purchase");

      _inAppService.Purchase(ProductsId.ToString());

      _analytics.Send("btn_try_free");
    }

    private void ShowInterviewSubscriptionWindow()
    {
      _subscriptionWindow = _subscriptionFactory.CreateInterviewSubscriptionWindow();

      _subscriptionWindow.Construct(_localeSystem, _audioService, Purchase, () =>
        {
          _analytics.Send("btn_skip_subscription");
          _analytics.Send("btn_with_ads");
          _gameDataProvider.FirstSession.IsSubscriptionDeny = true;
          _onClose?.Invoke();
          _subscriptionFactory.Clear();
        },
        SwitchPrivacyPrivacyCheckBox, ShowPrivacyPolicy, OpenTerms);
      
      var window2 = (_subscriptionWindow as SubscriptionWindow);
      if (window2 != null)
      {
        window2.SetPrice(_inAppService.GetPrice(ProductsId.ToString()));
        window2.SetPeriod("7");
      }

      var window3 = (_subscriptionWindow as SubscriptionWindowWithCounter);
      if (window3 != null)
      {
        window3.SetPrice(_inAppService.GetPrice(ProductsId.ToString()));
        window3.SetPeriod("7");
        window3.StartCounter();
      }

      var window = (_subscriptionWindow as InterviewSubscription);
      if (window != null)
      {
        window.Restore.onClick.AddListener(() => _inAppService.Initialize());
        window.Policy.onClick.AddListener(() => _privacyPolicy.OpenWindow());

        window.SetInterviewPrices(_inAppService.GetPrice(ProductsId.ToString()), "7",
          _inAppService.GetPrice(ProductsId.ToString()));

        window.SetOtherInfo(_inAppService.GetPriceFloat(ProductsId.ToString()),
          _inAppService.GetPriceFloat(ProductsId.ToString()));
      }
      
#if UNITY_IOS
      if(SubscriptionNotReady())
        _subscriptionWindow.SetUnavailable();
#endif
      _subscriptionWindow.Open();
    }

    private void OpenTerms() => 
      Application.OpenURL(TermsUrl);

#if UNITY_ANDROID
    private void ShowNotAvailableWindow()
    {
      _failConnectionFactory.CreateNotAvailableWindow();

      NotAvailableWindow notAvailableWindow = _failConnectionFactory.NotAvailableWindow;

      notAvailableWindow.SetCloseAction(_loadScreenService.ToQuit);
      notAvailableWindow.Open();
    }
#endif
    
    private bool CheckBreakingRules()
    {
      if (_inAppService.IsSubscriptionPurchased())
      {
        _timerController.Clean();
        _onNext?.Invoke();
        return true;
      }

#if UNITY_ANDROID
      if (SubscriptionNotReady())
      {
        ShowNotAvailableWindow();
        return true;
      }
#endif
      
      Debug.Log("[Interview Subscription] Show Window");

#if SKIP_SUBSCRIPTION
      SkipSubscription();
#endif
      return false;
    }
    
    private bool SubscriptionNotReady() =>
      !_inAppService.IsInAppsInitialized() || !_inAppService.IsProductInitialized(ProductsId.ToString());
    
    private void SendPurchaseAnalytics(Product product)
    {
#if UNITY_ANDROID
      if (!Application.isEditor)
      {
        MyTracker.TrackPurchaseEvent(product);
        Debug.Log("[Subscription] Send Purchase To MyTracker");
      }

      AppMetrica.Instance.ReportRevenue(GetRevenueForAppmetrica(product));
      Debug.Log(AppMetrica.Instance == null);

      Debug.Log("[Subscription] Send Purchase To Appmetrica");

      _analytics.Send("ev_open");
#endif
    }

    private YandexAppMetricaRevenue GetRevenueForAppmetrica(Product product)
    {
      string currency = product.metadata.isoCurrencyCode;
      decimal price = product.metadata.localizedPrice;

      YandexAppMetricaRevenue revenue = new YandexAppMetricaRevenue(price, currency);
      if (product.receipt != null)
      {
        YandexAppMetricaReceipt yaReceipt = new YandexAppMetricaReceipt();
        Receipt receipt = JsonUtility.FromJson<Receipt>(product.receipt);
#if UNITY_ANDROID
        PayloadAndroid payloadAndroid = JsonUtility.FromJson<PayloadAndroid>(receipt.Payload);
        yaReceipt.Signature = payloadAndroid.Signature;
        yaReceipt.Data = payloadAndroid.Json;
#elif UNITY_IPHONE
        yaReceipt.TransactionID = receipt.TransactionID;
        yaReceipt.Data = receipt.Payload;
#endif

        Debug.Log(yaReceipt.Signature);
        Debug.Log(yaReceipt.Data);
        revenue.Receipt = yaReceipt;
      }

      return revenue;
    }

    private void ShowPrivacyPolicy() => 
      _privacyPolicy.OpenWindow(true);

    private void SwitchPrivacyPrivacyCheckBox() => 
      SwitchPrivacyPrivacyCheckBox(!_gameSaveSystem.Get().IsPolicyAccepted);

    private void SwitchPrivacyPrivacyCheckBox(bool value)
    {
      _privacyPolicy.AcceptPrivacyPolicy(value);
      _subscriptionWindow.SwitchPrivacyPolicyCheckbox(_gameSaveSystem.Get().IsPolicyAccepted);
    }

#if SKIP_SUBSCRIPTION
    private void SkipSubscription()
    {
      _tester = new GameObject("[InApp] Skip Subscription").AddComponent<TestSkipSubscription>();
      _tester.OnClick = () =>
      {
        _onNext?.Invoke();
        ClearSkipSubscription();
      };
    }

    private void ClearSkipSubscription()
    {
      if (_tester)
        UnityEngine.Object.Destroy(_tester.gameObject);
      
      _subscriptionFactory.Clear();
    }
#endif
  }
}