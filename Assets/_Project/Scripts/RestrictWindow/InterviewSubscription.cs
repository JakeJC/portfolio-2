using _Project.Scripts.Animations.UI;
using _Project.Scripts.InApp.Mono;
using _Project.Scripts.RestrictWindow.Mono;
using Assets._Project.FMOD_AudioService.Logic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace _Project.Scripts.InApp
{
  public class InterviewSubscription : SubscriptionWindow
  {
    public Button Year;
    public Button Restore;
    public Button Policy;
    
    public TextMeshProUGUI InfoFreeDays;
    public TextMeshProUGUI InfoYear;
    public TextMeshProUGUI InfoSavePerYear;

    public Sprite OfferOff;
    public Sprite OfferOn;
    
    private string _periodText;
    private string _priceText;

    public virtual void SetInterviewPrices(string monthPrice, string yearPeriod, string yearPrice)
    {
      _priceText = string.Format(LocaleSystem.GetText("Main", "Subscription.PerMonth"), monthPrice).ToLowerInvariant();
      
      _periodText = string.Format(LocaleSystem.GetText("Main", "Subscription.Period"), yearPeriod);
      InfoFreeDays.text = $"<#CCADF0>{_periodText},</color>";
      
      _priceText = string.Format(LocaleSystem.GetText("Main", "Subscription.Year"), yearPrice).ToLowerInvariant();
      InfoYear.text = _priceText;
    }

    public virtual void SetOtherInfo(float monthPrice, float yearPrice)
    {
      var cost = (monthPrice * 12) - yearPrice;
      _priceText = string.Format(LocaleSystem.GetText("Main", "Subscription.SavePerYear"), cost.ToString("F2")).ToLowerInvariant();
      InfoSavePerYear.text = _priceText;
      
      cost = yearPrice / 12;
      _priceText = string.Format(LocaleSystem.GetText("Main", "Subscription.OrPerMonth"), cost.ToString("F2")).ToLowerInvariant();
    }

    protected override void InitializeAnimations()
    {
      base.InitializeAnimations();
      
      new ButtonAnimation(Year.transform);
      new ButtonAnimation(Restore.transform);
      new ButtonAnimation(Policy.transform);
    }

    protected override void InitializeSounds()
    {
      base.InitializeSounds();
      
      new ButtonSound(AudioService, Year.gameObject, 0);
      new ButtonSound(AudioService, Restore.gameObject, 0);
      new ButtonSound(AudioService, Policy.gameObject, 0);
    }

    public void Initialize()
    {
      
    }
  }
}