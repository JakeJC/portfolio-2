﻿using System;

namespace _Project.Scripts.RestrictWindow.Interfaces
{
  public interface ISubscription
  {
    void ShowInterviewSubscription(Action onShow, Action onClose);
  }
}