using System;
using _Project.FMOD_AudioService.Logic;
using _Project.Scripts.Services.Localization.Logic;

namespace _Project.Scripts.RestrictWindow.Interfaces
{
  public interface ISubscriptionWindow
  {
    void Hide(Action onClose);
    void Open();
    void SetUnavailable();
    bool IsOpen { get; }
    void Construct(ILocaleSystem localeSystem, IAudioService audioService, Action onClick, Action onClose,
      Action onClickPrivacyPolicyCheckbox, Action onClickPrivacyPolicy, Action onTermsClick);
    void ShowPrivacyPolicyContainer();
    void SwitchPrivacyPolicyCheckbox(bool enabled);
  }
}