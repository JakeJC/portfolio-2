﻿using System;
using _Project.Scripts.RestrictWindow.Interfaces;

namespace _Project.Scripts.InApp.Interfaces
{
  public interface IInAppAccessor
  {
    ISubscription Subscription { get; }
    bool IsSubscriptionBought();
    void SetCallback(Action onInitialized);
    void Initialize();
    void RestorePurchases();
  }
}