﻿using _Project.Scripts.InApp.Mono;
using _Project.Scripts.RestrictWindow.Mono;
using _Project.Scripts.Services._Interfaces;
using AppacheRemote.Scripts.Interfaces;
using UnityEngine;

namespace _Project.Scripts.InApp
{
  public class SubscriptionFactory : ISubscriptionFactory
  {
    private const string CanvasId = "Canvas";

#if UNITY_IOS
    private const string InterviewWindowReviewsCounterPath = "UI/Subscription Window/Interview Subscription Window Reviews Variant (IOS)";
#else
    private const string InterviewWindowReviewsCounterPath = "UI/Subscription Window/Interview Subscription Window Reviews Variant";
#endif

    private readonly IAssetProvider _assetProvider;
    private readonly IRemote _remote;

    private SubscriptionWindow _interviewSubscriptionWindow;

    public SubscriptionFactory(IAssetProvider assetProvider)
    {
      _assetProvider = assetProvider;
    }

    public SubscriptionWindow CreateInterviewSubscriptionWindow()
    {
      var path = InterviewWindowReviewsCounterPath;
      
      var original = _assetProvider.GetResource<GameObject>(path);
      
      var parent = GameObject.Find(CanvasId).transform;
      var gameObject = Object.Instantiate(original, parent);
      _interviewSubscriptionWindow = gameObject.GetComponent<SubscriptionWindow>();
      return _interviewSubscriptionWindow;
    }

    public void Clear()
    {
      if (_interviewSubscriptionWindow)
        Object.Destroy(_interviewSubscriptionWindow.gameObject);
    }
  }
}