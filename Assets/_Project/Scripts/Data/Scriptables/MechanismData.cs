using UnityEngine;

namespace _Project.Scripts.Data.Scriptables
{
  [CreateAssetMenu(fileName = "Mechanism Data", menuName = "Create Mechanism Data/Mechanism Data", order = 0)]
  public class MechanismData : ScriptableObject
  {
    public string AssetAddress;
    public string MechanismId;
    public string WorldId;

    public string CoverAddress;
    public float InterviewYValue = -2.22f;
  }
}