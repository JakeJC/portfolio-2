using Sirenix.OdinInspector;
using UnityEditor;
using UnityEngine;

namespace _Project.Scripts.Data.Scriptables
{ 
  [CreateAssetMenu(fileName = "World Data", menuName = "Create World Data/World Data", order = 0)]
  public class WorldData : ScriptableObject
  {
    public string WorldId;
    public string KeyName;
    public Sprite WorldIcon;
    public RuntimeAnimatorController WorldIconAnimatorController;
    public int MusicIndex;
    public bool IsLightWorld;
    
    public string AnalyticsKey;
    
#if UNITY_EDITOR
    [Button]
    public void GenerateGuid() => 
      WorldId = GUID.Generate().ToString();
#endif
  }
}