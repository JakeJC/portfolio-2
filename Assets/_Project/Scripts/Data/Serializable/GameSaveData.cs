using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using _Project.Scripts.Features.Meditation.Scripts.Scriptables;
using UnityEngine;

namespace _Project.Scripts.Data.Serializable
{
  [Serializable]
  public class GameSaveData
  {
    [SerializeField] private List<MechanismState> _mechanismsSaveData = new List<MechanismState>();
    [SerializeField] private List<string> _labelsInfo = new List<string>();
    [SerializeField] private List<string> _openWorlds = new List<string>();

    [SerializeField] private List<MechanismTimerData> _mechanismDeadlines = new List<MechanismTimerData>();
    
    public string Name = "";

    public bool TutorialComplete;
    public bool GroupWithTutorialComplete;
    
    public bool IsFirstSession = true;

    public bool IsSoundDisabled;
    public bool IsMusicDisabled;
    public bool IsPolicyAccepted;

    public string showedAdvices;
    public List<string> showedAdvicesIds = new List<string>();
    
    public List<string> showedStages = new List<string>();

    public List<CompleteMechanismData> CompleteMechanismsInfo = new List<CompleteMechanismData>();
    public List<string> UnlockedMeditationPresets = new List<string>();
    
    public bool FirstEnterInMeditation = true;

    public void SetState(string mechanismId, State state)
    {
      if (_mechanismsSaveData.Any(p => p.MechanismId == mechanismId))
        _mechanismsSaveData.First(p => p.MechanismId == mechanismId).State = state;
      else
      {
        var mechanismState = new MechanismState(mechanismId, state);
        _mechanismsSaveData.Add(mechanismState);
      }
    }

    public State GetState(string mechanismId) =>
      _mechanismsSaveData.Any(p => p.MechanismId == mechanismId) ? _mechanismsSaveData.First(p => p.MechanismId == mechanismId).State : State.Incomplete;

    public bool IsUnlocked(string mechanismId) =>
      _mechanismsSaveData.Any(p => p.MechanismId == mechanismId) &&
      _mechanismsSaveData.First(p => p.MechanismId == mechanismId).State == State.Launched;
    
    public void SetMechanismDeadline(string mechanismId, DateTime deadline)
    {
      if (_mechanismDeadlines.All(m => m.mechanismId != mechanismId))
        _mechanismDeadlines.Add(new MechanismTimerData(mechanismId, deadline.ToString(CultureInfo.InvariantCulture)));
    }

    public void RemoveMechanismDeadline(string mechanismId) => 
      _mechanismDeadlines.RemoveAll(m => m.mechanismId == mechanismId);
    

    public bool TryGetMechanismDeadline(string mechanismId, out DateTime value)
    {
      var mechanismTimerData = _mechanismDeadlines.FirstOrDefault(m => m.mechanismId == mechanismId);
      if (mechanismTimerData != null)
      {
        value = DateTime.Parse(mechanismTimerData.deadlineString, CultureInfo.InvariantCulture);
        return true;
      }

      value = default;
      return false;
    }

    public bool CheckMechanismTimerRunning(string mechanismId)
    {
      TryGetMechanismDeadline(mechanismId, out var value);
      return value > DateTime.Now;
    }

    public void AddOpenWorld(string worldId)
    {
      if(!_openWorlds.Contains(worldId))
        _openWorlds.Add(worldId);
    }

    public List<string> GetOpenWorlds() => 
      _openWorlds;
    
    public void AddLabelsToPersistentStorage(List<string> labelsInfo)
    {
      var labs = labelsInfo.Where(p => !_labelsInfo.Contains(p));
      _labelsInfo.AddRange(labs);
    }

    public bool LabelIsAlreadyShown(string mechanismId) =>
      _labelsInfo.Contains(mechanismId);
  }
}