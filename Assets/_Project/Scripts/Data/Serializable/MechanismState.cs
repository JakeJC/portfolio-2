﻿using System;
using UnityEngine;

namespace _Project.Scripts.Data.Serializable
{
  [Serializable]
  public class MechanismState
  {
    public string MechanismId;
    [SerializeField] private State _state;

    public State State
    {
      get => _state;
      set => _state = value;
    }

    public MechanismState(string mechanismId, State state)
    {
      MechanismId = mechanismId; 
      State = state;
    }
  }
  
  public enum State
  {
    Incomplete,
    Launched
  }
}