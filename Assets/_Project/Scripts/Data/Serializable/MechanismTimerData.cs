using System;

namespace _Project.Scripts.Data.Serializable
{
  [Serializable]
  public class MechanismTimerData
  {
    public string mechanismId;
    public string deadlineString;

    public MechanismTimerData(string mechanismId, string deadlineString)
    {
      this.mechanismId = mechanismId;
      this.deadlineString = deadlineString;
    }
  }
}