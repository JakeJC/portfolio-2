using System;
using UnityEngine;

namespace _Project.Scripts.Data.Serializable
{
  [Serializable]
  public struct CameraData
  {
    public Vector3 Position;
    public float Size;
  }
}