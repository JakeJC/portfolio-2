using System;

namespace _Project.Scripts.Data.Serializable
{
  [Serializable]
  public class GroupState
  {
    public string GroupId;
    public bool IsComplete;
    
    public GroupState(string groupId, bool isComplete)
    {
      IsComplete = isComplete;
      GroupId = groupId;
    }
  }
}