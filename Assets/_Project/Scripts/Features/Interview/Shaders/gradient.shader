// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Unlit/LinearGradient"
 {
     Properties{
         _TopColor("Color1", Color) = (1,1,1,1)
         _MiddleColor("Color2", Color) = (1,1,1,1)
         _BottomColor("Color3", Color) = (1,1,1,1)
         _MainTex ("Main Texture", 2D) = "white" {}
         _Middle ("Middle", Range(0.001, 0.999)) = 1
         _Alpha ("Alpha", Range(0, 1)) = 1
         
         _StencilComp ("Stencil Comparison", Float) = 8
         _Stencil ("Stencil ID", Float) = 0
         _StencilOp ("Stencil Operation", Float) = 0
         _StencilWriteMask ("Stencil Write Mask", Float) = 255
         _StencilReadMask ("Stencil Read Mask", Float) = 255
         _ColorMask ("Color Mask", Float) = 15
     }
         SubShader
     {
         Tags { "RenderType" = "Transparent" "Queue" = "Transparent" }
         
         Stencil
         {
             Ref [_Stencil]
             Comp [_StencilComp]
             Pass [_StencilOp] 
             ReadMask [_StencilReadMask]
             WriteMask [_StencilWriteMask]
         }
         ColorMask [_ColorMask]
         
         Pass
         {
             ZWrite Off
             Blend SrcAlpha OneMinusSrcAlpha
 
             CGPROGRAM
             #pragma vertex vert
             #pragma fragment frag
 
             #include "UnityCG.cginc"
             
             sampler2D _MainTex;
             float4 _MainTex_ST;
 
             fixed4 _TopColor;
             fixed4 _MiddleColor;
             fixed4 _BottomColor;
             float  _Middle;
             float  _Alpha;
             half _Value;
             float4 _ClipRect;

             struct v2f {
                 float4 position : SV_POSITION;
                 fixed4 color : COLOR;
                 float2 uv : TEXCOORD0;
             };
 
             v2f vert (appdata_full v)
             {
                 v2f o;
                 o.position = UnityObjectToClipPos (v.vertex);
                 o.uv = TRANSFORM_TEX (v.texcoord, _MainTex);
                 fixed4 c = lerp (_TopColor, _MiddleColor, v.texcoord.x / _Middle * 0.2f) * step(v.texcoord.x, _Middle);
                 c += lerp(_MiddleColor, _BottomColor, (v.texcoord.x - _Middle) / (1 - _Middle * 0.2f)) * (1 - step(v.texcoord.x, _Middle));
                 c.a = _Alpha;
                 o.color = c;
                 return o;
             }
 
             fixed4 frag(v2f i) : SV_Target
             {
                 float4 color;
                 color.rgb = i.color.rgb;
                 color.a = tex2D (_MainTex, i.uv).a * i.color.a;
                 return color;
             }
             ENDCG
         }
     }
 }