using UnityEngine;

namespace _Project.Scripts.Interview.Resources.Scriptables.ReasonButtons
{
  [CreateAssetMenu(fileName = "ReasonButton_", menuName = "Interview/Reason Button Data", order = 0)]
  public class ReasonButton : ScriptableObject
  {
    public Sprite IconOn;
    public Sprite IconOff;
    public string Description;
  }
}
