﻿using UnityEngine;

namespace _Project.Scripts.Features.Interview.Resources.Scriptables.LocaliseInterview
{
  [CreateAssetMenu(fileName = "LocaliseInterview_", menuName = "Interview/Localise Interview Data", order = 0)]
  public class LocaliseInterviewData : ScriptableObject
  {
    public string TitleKey;
    public string TitleKeyNoName;
    public string DescriptionKey;
    public string ContinueKey;
  }
}