﻿using System;
using _Project.Scripts.Features.Interview.Resources.Scriptables.LocaliseInterview;
using AppacheAnalytics.Scripts.Interfaces;
using AppacheRemote.Scripts.Interfaces;
using UnityEngine;

namespace _Project.Scripts.Features.Interview.Scripts.Interviews
{
  public class InterviewPart : MonoBehaviour
  {
    [SerializeField] private bool isColumnsOn;
    [SerializeField] private LocaliseInterviewData _localiseInterviewData;
    
    protected IInterviewUI InterviewUI;
    private CanvasGroup _canvasGroup;
    
    protected Action<float, bool> OnCompleteStepProgress;
    protected Action OnCompleteAll;
    protected IRemote _remote;

    public virtual void Construct(IAnalytics analytics, IInterviewUI interviewUI, IRemote remote)
    {
      _remote = remote;
      InterviewUI = interviewUI;
      _canvasGroup = GetComponent<CanvasGroup>();
    }

    public void SetCompleteAction(Action<float, bool> onCompleteStepProgress) => 
      OnCompleteStepProgress = onCompleteStepProgress;
    
    public void SetAllCompleteAction(Action onCompleteStepProgress) => 
      OnCompleteAll = onCompleteStepProgress;
    
    public virtual void Activate()
    {
      InterviewUI.ActivateNewPage(_localiseInterviewData, _canvasGroup, isColumnsOn);
      gameObject.SetActive(true);
    }

    public virtual void Deactivate() => 
      gameObject.SetActive(false);
  }
}