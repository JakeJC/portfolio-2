﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using _Project.Scripts.InApp.Mono;
using _Project.Scripts.Logic.Factories.Interfaces;
using _Project.Scripts.Services._Interfaces;
using _Project.Scripts.Services.LoadScreen.Logic;
using _Project.Scripts.Services.Localization.Logic;
using DG.Tweening;
using DG.Tweening.Core;
using DG.Tweening.Plugins.Options;
using TMPro;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.UI;
using Random = UnityEngine.Random;

namespace _Project.Scripts.Features.Interview.Scripts.Interviews
{
  public class InterviewLoading : InterviewPart
  {
    private const float Duration = 2f;
    private const float FinishedDuration = .75f;
    
    [SerializeField] private Image _percentImage;
    [SerializeField] private Image _finishedPercentBackground;
    [SerializeField] private CanvasGroup _ProgressBar;
    
    [SerializeField] private Sprite _finishedPercentSprite;
    [SerializeField] private Sprite _backgroundGradient;
    
    [SerializeField] private Image _flower;
    [SerializeField] private List<Sprite> _flowerFrame;
    
    [SerializeField] private ParticleSystem _particleSystem;
    [SerializeField] private TextMeshProUGUI _loadingText;
    
    private readonly Vector3 finishedBackgroundSize = new Vector3(1.2f, 3f, 1f);
    
    private IAssetProvider _assetProvider;
    private IFailConnectionFactory _failConnectionFactory;
    private ILoadScreenService _loadScreenService;

    public override async void Activate()
    {
      Localize();
      InterviewUI.DeactivateParticles();
      
      base.Activate();

      InterviewUI.GetTitle().text += InterviewUI.GetNameOperator().GetName();
        
      InterviewUI.GetBackgroundGradient().sprite = _backgroundGradient;
      
      StartCoroutine(FlowerAnimation());

      TweenerCore<float, float, FloatOptions> barAnimation = _percentImage.DOFillAmount(1f, Duration).SetDelay(0.5f);
      barAnimation.onUpdate += () =>
      {
        OnCompleteStepProgress?.Invoke(_percentImage.fillAmount, true);

        if (_percentImage.fillAmount > .75 && !_particleSystem.isPlaying)
          _particleSystem.Play();
      };
      barAnimation.onComplete += CompleteLoad;
      
      try
      {
        await Task.Delay(5000);
      }
      catch (ArgumentException)
      {
        CallNoConnectionWindow();
        throw;
      }
    }

    public void SetServices(IAssetProvider assetProvider, IFailConnectionFactory failConnectionFactory, ILoadScreenService loadScreenService)
    {
      _loadScreenService = loadScreenService;
      _failConnectionFactory = failConnectionFactory;
      _assetProvider = assetProvider;
    }
    
    private void CompleteLoad()
    {
      _percentImage.type = Image.Type.Sliced;
      _percentImage.sprite = _finishedPercentSprite;

      _finishedPercentBackground.enabled = true;

      Sequence sequence2 = DOTween.Sequence();
      sequence2
        .Append(_finishedPercentBackground.transform.DOScale(finishedBackgroundSize, FinishedDuration))
        .Join(_finishedPercentBackground.DOFade(0f, FinishedDuration))
        .OnComplete(() => { _ProgressBar.DOFade(0, FinishedDuration); });

      SetRandomLoadingText();

      InterviewUI.UnlockButton();

      OnCompleteAll?.Invoke();
    }
    
    private void CallNoConnectionWindow()
    {
      Debug.Log("[Addressables Remote Host] Connection Lost");

      _failConnectionFactory.CreateNotAvailableWindow();
      NotAvailableWindow notAvailableWindow = _failConnectionFactory.NotAvailableWindow;

      notAvailableWindow.SetCloseAction(_loadScreenService.ToQuit);
      notAvailableWindow.Open();
      
      _loadScreenService.ForceRemoveScreens();
    }
    
    private IEnumerator FlowerAnimation()
    {
      while (_flower.sprite != _flowerFrame[_flowerFrame.Count - 1])
      {
        _flower.sprite = _flowerFrame[_flowerFrame.FindIndex(item => item == _flower.sprite) + 1];
        yield return new WaitForSecondsRealtime(Duration / _flowerFrame.Count);
      }
    }

    private void SetRandomLoadingText()
    {
      string text = InterviewUI.GetLocaleSystem().GetText("Interview", $"Interview.End.{Random.Range(1, 7)}");
      _loadingText.DOFade(0, 0.3f).onComplete += () =>
      {
        _loadingText.text = text;
        _loadingText.DOFade(1, 0.3f);
      };
    }

    private void Localize()
    {
      List<LocalizedInterviewText> localizedTexts = transform.GetComponentsInChildren<LocalizedInterviewText>().ToList();
      localizedTexts.ForEach(item =>
      {
        item.Construct(InterviewUI.GetLocaleSystem());
        item.Awake();
      });
    }
  }
}