﻿using System;
using System.Collections.Generic;
using System.Linq;
using _Modules._Analytics;
using _Project.Scripts.Logic.GameLogic.Interfaces;
using AppacheAds.Scripts.Interfaces;
using AppacheAnalytics.Scripts.Interfaces;
using AppacheRemote.Scripts.Interfaces;
using Assets._Project.FMOD_AudioService.Logic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace _Project.Scripts.Features.Interview.Scripts.Interviews
{
  public class InterviewColor : InterviewPart
  {
    private const float Time = .25f;
    private const int MAXCountAnswers = 6;
    
    private int _activeIndex;
    
    private readonly List<Variant> _pushedButtons = new List<Variant>();
    
    [SerializeField] private List<Variant> _variants;
    
    private IAnalytics _analytics;
    private IAdsService _ads;
    private IGameDataProvider _gameDataProvider;

    private void OnValidate()
    {
      foreach (Variant item in _variants.Where(item => item.Guid.Equals(""))) 
        item.Guid = Guid.NewGuid().ToString();
    }

    public override void Construct(IAnalytics analytics, IInterviewUI interviewUI, IRemote remote)
    {
      base.Construct(_analytics, interviewUI, remote);
      
      _analytics = analytics;
      
      for (var i = 0; i < _variants.Count; i++)
      {
        int index = i;
        _variants[i].Button.onClick.AddListener(() =>
        {
          _activeIndex = index;
          OnClickButton(index);
        });
        
        new ButtonSound(interviewUI.GetAudioSystem(),_variants[i].Button.gameObject, 2);
      }
    }

    public void InjectAdditionalServices(IAdsService ads, IGameDataProvider gameDataProvider)
    {
      _gameDataProvider = gameDataProvider;
      _ads = ads;
    }

    public List<string> GetColorsGuids() => 
      (from item in _pushedButtons where !item.Guid.Equals("") select item.Guid).ToList();

    private void OnClickButton(int index)
    {
      if (_variants[index].Icon.localScale == Vector3.one)
      {
        _variants[index].IconBack.DOScale(1f, Time);//ToDo: Анимацию на Бахину
        _variants[index].Icon.DOScale(.5f, Time)
          .OnComplete(() =>
          {
            _pushedButtons.Add(_variants[index]);
            CheckNextAccess();
            CheckOverload();
          });
      }
      else if (_variants[index].Icon.localScale == Vector3.one / 2)
      {
        _variants[index].IconBack.DOScale(.5f, Time);//ToDo: Анимацию на Бахину
        _variants[index].Icon.DOScale(1f, Time)
          .OnComplete(() =>
          {
            _pushedButtons.Remove(_variants[index]);
            CheckNextAccess();
            CheckOverload();
          });
      }
    }

    public override void Deactivate()
    {
      base.Deactivate();
      
      var descriptionLine = "";
      _pushedButtons.ForEach(item => descriptionLine += item.AnalyticsDescription + "_");
      descriptionLine = descriptionLine.Substring(0, descriptionLine.Length - 1);

      _analytics.Send("ev_interview_color", new Dictionary<string, object>()
      {
        {"color", descriptionLine}
      });
    }

    private void CheckNextAccess()
    {
      if (_pushedButtons.Count > 0)
      {
        InterviewUI.UnlockButton();
        OnCompleteStepProgress?.Invoke(1, false);
      }
      else
      {
        InterviewUI.LockButton();
        OnCompleteStepProgress?.Invoke(0, false);
      }
    }

    private void CheckOverload()
    {
      if (_variants.FindAll(item => item.Icon.localScale == Vector3.one / 2).Count <= MAXCountAnswers)
        return;

      _pushedButtons[0].Icon.DOScale(1f, Time);//ToDo: Анимацию на Бахину
      _pushedButtons.RemoveAt(0);
    }

    [Serializable]
    private class Variant
    {
      public string Guid;
      public Transform IconBack;
      public Transform Icon;
      public Button Button;
      
      public string AnalyticsDescription;
    }
  }
}