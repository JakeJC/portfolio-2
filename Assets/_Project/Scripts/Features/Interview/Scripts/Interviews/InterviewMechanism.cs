﻿using System.Collections.Generic;
using System.Linq;
using _Project.FMOD_AudioService.Logic;
using _Project.Scripts.Data.Scriptables;
using _Project.Scripts.Logic.GameLogic.Interfaces;
using _Project.Scripts.Services._Interfaces;
using AppacheAnalytics.Scripts.Interfaces;
using AppacheRemote.Scripts.Interfaces;
using UnityEngine;
using UnityEngine.UI.Extensions;

namespace _Project.Scripts.Features.Interview.Scripts.Interviews
{
  public class InterviewMechanism : InterviewPart
  {
    private const string WorldButtonPath = "UI/Interview/World/World Button";
    private int _pageMaxCount = 5;

    private readonly List<WorldButton> _worldButtons = new List<WorldButton>();
    private List<WorldButton> _pushedButton;

    public RadialLayout RadialLayout;
    public CustomMechanismsLayout GridLayout;

    private IGameDataProvider _gameDataProvider;
    private IAnalytics _analytics;
    private IAssetProvider _assetProvider;
    private IAudioService _audioService;

    public override void Construct(IAnalytics analytics, IInterviewUI interviewUI, IRemote remote)
    {
      base.Construct(analytics, interviewUI, remote);
      
      _analytics = analytics;
      
      _pushedButton = new List<WorldButton>();

      CreateWorldButtons();

      RadialLayout.StartAngle = _worldButtons.Count % 2 == 0 ? 0 : 90;
      RadialLayout.fDistance = _worldButtons.Count % 2 == 0 ? 175 : 190;
    }

    public void SetGameDataProvider(IAssetProvider assetProvider, IGameDataProvider gameDataProvider, IAudioService audioService)
    {
      _audioService = audioService;
      _assetProvider = assetProvider;
      _gameDataProvider = gameDataProvider;
    }

    private void CreateWorldButtons()
    {
      WorldData[] worldsData = FilterAvailableWorlds();

      var worldButton = _assetProvider.GetResource<WorldButton>(WorldButtonPath);
      int worldsCount = Mathf.Clamp(worldsData.Length, 0, _pageMaxCount);

      for (int i = 0; i < worldsCount; i++)
      {
        WorldButton button = Instantiate(worldButton, RadialLayout.transform);

        GridLayout.AddChild(button.GetComponent<RectTransform>());

        button.SetClickAction(OnClickButton, i);
        button.Construct(_audioService, worldsData[i]);

        _worldButtons.Add(button);
      }

      WorldData[] FilterAvailableWorlds()
      {
        WorldData[] worldDatas = _gameDataProvider.WorldsInfo.GetWorldsData();
        WorldData[] filterAvailableWorlds = worldDatas
          .Where(p => !_gameDataProvider.WorldsInfo.WorldIsOutOfMechanisms(p.WorldId))
          .ToArray();
        return filterAvailableWorlds;
      }
    }

    private void LaunchAllWorldsAnimation()
    {
      foreach (WorldButton worldButton in _worldButtons)
      {
        worldButton.SetBackground(false);
        worldButton.PlayAnimation(true);
      }
    }

    private void OnClickButton(int index)
    {
      if (_pushedButton?.FirstOrDefault(item => item == _worldButtons[index]) == null)
        _pushedButton?.Add(_worldButtons[index]);
      else
        _pushedButton.Remove(_worldButtons[index]);
      
      print(_pushedButton?.Find(item => item == _worldButtons[index]));

      foreach (WorldButton t in _worldButtons)
        t.SetCurrent(_pushedButton != null && _pushedButton?.Find(item => item == t));
      
      CheckNextAccess();
    }

    public override void Deactivate()
    {
      base.Deactivate();
      
      var descriptionLine = "";
      _pushedButton.ForEach(item => descriptionLine += item.WorldData.AnalyticsKey + "_");
      
      if(descriptionLine.Length > 0)
        descriptionLine = descriptionLine.Substring(0, descriptionLine.Length - 1);
      
      Debug.Log(descriptionLine);
      _analytics.Send("ev_interview_world", new Dictionary<string, object>()
      {
        {"world", descriptionLine}
      });

      string mechanismAddressById = MechanismAddressByWorld();

      _assetProvider.PrepareGroup(mechanismAddressById);
    }

    private string MechanismAddressByWorld()
    {
      MechanismData mechanismData = _gameDataProvider.MechanismsInfo.GetAvailableMechanismForWorld(_pushedButton[0].WorldData.WorldId);
      string mechanismAddressById = _gameDataProvider.MechanismsInfo.GetMechanismAddressById(mechanismData.MechanismId);
      return mechanismAddressById;
    }

    public override void Activate()
    {
      base.Activate();
      LaunchAllWorldsAnimation();
    }

    public string GetActiveId() =>
      _pushedButton[0].WorldData.WorldId;

    private void CheckNextAccess()
    {
      if (_pushedButton.Count == 0)
      {
        InterviewUI.LockButton();
        OnCompleteStepProgress?.Invoke(0, false);
      }
      else
      {
        InterviewUI.UnlockButton();
        OnCompleteStepProgress?.Invoke(1, false);
      }
    }
  }
}