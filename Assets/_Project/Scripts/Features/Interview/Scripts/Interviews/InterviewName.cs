﻿using System;
using _Modules._Analytics;
using _Project.Scripts.InApp.Interfaces;
using _Project.Scripts.Logic.GameLogic.Interfaces;
using AppacheAds.Scripts.Interfaces;
using AppacheAnalytics.Scripts.Interfaces;
using AppacheRemote.Scripts.Interfaces;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;

namespace _Project.Scripts.Features.Interview.Scripts.Interviews
{
  public class InterviewName : InterviewPart
  {
    [SerializeField] private TMP_InputField _name;

    private string _textCache;
    private string _oldSave;

    private IAnalytics _analytics;
    private IAdsService _ads;
    private IGameDataProvider _gameDataProvider;
    private ICurrentScreen _screenManager;
    private IInAppAccessor _inAppAccessor;

    public override void Construct(IAnalytics analytics, IInterviewUI interviewUI, IRemote remote)
    {
      base.Construct(_analytics, interviewUI, remote);

      _analytics = analytics;

      _name.text = interviewUI.GetNameOperator().GetName();

      _name.onValueChanged.AddListener(delegate { CheckNextAccess(); });

      InterviewUI.Next.onClick.AddListener(SetName);
    }

    public void InjectAdditionalServices(IAdsService adsService, IGameDataProvider gameDataProvider, ICurrentScreen screenManager, IInAppAccessor inAppAccessor)
    {
      _inAppAccessor = inAppAccessor;
      _screenManager = screenManager;
      _gameDataProvider = gameDataProvider;
      _ads = adsService;
    }

    public override void Activate()
    {
      base.Activate();

      AddListeners();

      _name.ActivateInputField();
      InterviewUI.MoveButtonUp();
    }

    public override void Deactivate()
    {
      base.Deactivate();

      _analytics.Send("ev_authorization");
    }
    
    private void ShowSubscription()
    {
      _screenManager.SetCurrentScreen(ScreenNames.Subscription);
      _inAppAccessor.Subscription.ShowInterviewSubscription(OnBuySubscription, ShowInterstitialAfterCloseSubscription);
    }

    private void OnBuySubscription() => 
      _ads.DisableAds();

    private void ShowInterstitialAfterCloseSubscription()
    {
      _ads.CurrentInterstitial.ShowInterstitial(ScreenNames.Interview, null, null);
      base.Deactivate();
    }

    private void AddListeners()
    {
      var eventTrigger = _name.gameObject.AddComponent<EventTrigger>();
      var entry = new EventTrigger.Entry { eventID = EventTriggerType.PointerDown };
      entry.callback.AddListener((data) => InterviewUI.MoveButtonUp());
      eventTrigger.triggers.Add(entry);

      _name.onEndEdit.AddListener(delegate
      {
        _name.text = _textCache;
        InterviewUI.MoveButtonDown();
      });
    }

    private void CheckNextAccess()
    {
      if (_name.text.Length > 0)
      {
        InterviewUI.UnlockButton();
        OnCompleteStepProgress?.Invoke(1, false);
      }
      else
      {
        InterviewUI.LockButton();
        OnCompleteStepProgress?.Invoke(0, false);
      }
    }

    private void SetName()
    {
      InterviewUI.GetNameOperator().SetNewName(_name.text);
      InterviewUI.Next.onClick.RemoveListener(SetName);
    }

    private void LateUpdate()
    {
      if (!string.Equals(_name.text, _textCache, StringComparison.Ordinal))
        _textCache = _name.text;
    }
  }
}