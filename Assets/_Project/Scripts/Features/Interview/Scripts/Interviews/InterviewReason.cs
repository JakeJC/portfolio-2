﻿using System.Collections.Generic;
using System.Linq;
using _Project.Scripts.Animations.UI;
using _Project.Scripts.Interview.Resources.Scriptables.ReasonButtons;
using AppacheAnalytics.Scripts.Interfaces;
using AppacheRemote.Scripts.Interfaces;
using Assets._Project.FMOD_AudioService.Logic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace _Project.Scripts.Features.Interview.Scripts.Interviews
{
  public class InterviewReason : InterviewPart
  {
    private const int MAXCountAnswers = 4;
    private const string LocalizeName = "Interview";
    
    private int _activeIndex;
    
    [SerializeField] private List<Variant> _variants;
    [SerializeField] private List<ReasonButton> _reasonButtons;
    
    private readonly List<Variant> _pushedButtons = new List<Variant>();
    
    private IAnalytics _analytics;

    public override void Construct(IAnalytics analytics, IInterviewUI interviewUI, IRemote remote)
    {
      base.Construct(analytics, interviewUI, remote);
      
      _analytics = analytics;

      for (var i = 0; i < _variants.Count; i++)
      {
        int index = i;
        _variants[i].Button.onClick.AddListener(() =>
        {
          _activeIndex = index;
          OnClickButton(index);
        });

        new ButtonAnimation(_variants[i].Button.transform);
        new ButtonSound(interviewUI.GetAudioSystem(),_variants[i].Button.gameObject, 1);
        
        _variants[i].Description.text = InterviewUI.GetLocaleSystem().GetText(LocalizeName, _reasonButtons[i].Description);
      }
    }

    public int GetActiveIndex() =>
      _activeIndex;

    public override void Activate()
    {
      base.Activate();

      InterviewUI.GetTitle().text = InterviewUI.GetNameOperator().GetName() + InterviewUI.GetTitle().text;
    }

    public override void Deactivate()
    {
      base.Deactivate();

      var descriptionLine = "";
      _pushedButtons.ForEach(item => descriptionLine += item.AnalyticsDescription + "_");
      
      if(descriptionLine.Length > 0)
        descriptionLine = descriptionLine.Substring(0, descriptionLine.Length - 1);
      
      _analytics.Send("ev_interview_help", new Dictionary<string, object>()
      {
        {"help", descriptionLine}
      });
    }

    private void OnClickButton(int index)
    {
      _variants[index].Frame.enabled = !_variants[index].Frame.enabled;
      _variants[index].Icon.sprite = _variants[index].Frame.enabled ? _reasonButtons[index].IconOn : _reasonButtons[index].IconOff;

      if(_variants[index].Frame.enabled)
        _pushedButtons.Add(_variants[index]);
      else
        _pushedButtons.Remove(_variants[index]);

      CheckNextAccess();
      
      CheckOverload();
    }

    private void CheckNextAccess()
    {
      if (_pushedButtons.Count > 0)
      {
        InterviewUI.UnlockButton();
        OnCompleteStepProgress?.Invoke(1, false);
      }
      else
      {
        InterviewUI.LockButton();
        OnCompleteStepProgress?.Invoke(0, false);
      }
    }

    private void CheckOverload()
    {
      if (_variants.FindAll(item => item.Frame.enabled).Count <= MAXCountAnswers) return;

      _pushedButtons[0].Frame.enabled = false;
      _pushedButtons[0].Icon.sprite = _reasonButtons[_variants.FindIndex(item => item.Icon == _pushedButtons[0].Icon)].IconOff;
      _pushedButtons.RemoveAt(0);
    }

    [System.Serializable]
    private struct Variant
    {
      public Image Icon;
      public Image Frame;
      public TMP_Text Description;
      public Button Button;

      public string AnalyticsDescription;
    }
  }
}