using System.Collections.Generic;
using System.Linq;
using _Modules._Analytics;
using _Project.Scripts.Logic.GameLogic.Interfaces;
using _Project.Scripts.Services.Localization.Logic;
using AppacheAds.Scripts.Interfaces;
using AppacheAnalytics.Scripts.Interfaces;
using AppacheRemote.Scripts.Interfaces;
using UnityEngine;

namespace _Project.Scripts.Features.Interview.Scripts.Interviews
{
  public class InterviewHello : InterviewPart
  {
    private IAnalytics _analytics;
    private IRemote _remote;
    private IAdsService _ads;
    private IGameDataProvider _gameDataProvider;

    public override void Construct(IAnalytics analytics, IInterviewUI interviewUI, IRemote remote)
    {
      base.Construct(analytics, interviewUI, remote);
      _remote = remote;
      _analytics = analytics;
    }

    public void InjectAdditionalServices(IAdsService ads, IGameDataProvider gameDataProvider)
    {
      _gameDataProvider = gameDataProvider;
      _ads = ads;
    }

    public override void Activate()
    {
      Localize();
      base.Activate();

      InterviewUI.MoveColumns();
      InterviewUI.UnlockButton();

      InterviewUI.GetTitle().text += InterviewUI.GetNameOperator().GetName();
      
      OnCompleteStepProgress?.Invoke(1, false);
    }

    private void Localize()
    {
      List<LocalizedInterviewText> localizedTexts = transform.GetComponentsInChildren<LocalizedInterviewText>().ToList();
      localizedTexts.ForEach(item =>
      {
        item.Construct(InterviewUI.GetLocaleSystem());
        item.Awake();
      });
    }
  }
}