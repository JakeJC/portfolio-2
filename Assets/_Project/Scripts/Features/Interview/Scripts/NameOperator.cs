﻿using _Project.Scripts.Services._Interfaces;

namespace _Project.Scripts.Features.Interview.Scripts
{
  public class NameOperator
  {
    private readonly IGameSaveSystem _gameSaveSystem;
    
    public NameOperator(IGameSaveSystem gameSaveSystem)
    {
      _gameSaveSystem = gameSaveSystem;
    }
    
    public void SetNewName(string newName)
    {
      _gameSaveSystem.Get().Name = newName;
      _gameSaveSystem.Save();
    }

    public string GetName() =>
      _gameSaveSystem.Get().Name;
  }
}