﻿using System.Collections.Generic;

namespace _Project.Scripts.Features.Interview.Scripts
{
  public interface IInterview
  {
    int GetReasonIndex();
    List<string> GetColorGuids();
    string GetWorldId();
  }
}