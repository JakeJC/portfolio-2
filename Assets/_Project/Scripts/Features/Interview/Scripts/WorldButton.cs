using System;
using System.Collections;
using System.Collections.Generic;
using _Project.FMOD_AudioService.Logic;
using _Project.Scripts.Animations.UI;
using _Project.Scripts.Data.Scriptables;
using Assets._Project.FMOD_AudioService.Logic;
using DG.Tweening;
using DG.Tweening.Core;
using DG.Tweening.Plugins.Options;
using UnityEngine;
using UnityEngine.UI;

namespace _Project.Scripts.Features.Interview.Scripts
{
  public class WorldButton : MonoBehaviour
  {   
    private const float Time = .25f;

    private readonly Color _defaultColor = new Color(0, 0, 0, .1f);
    private readonly Color _chosenColor = new Color(1, 1, 1, .1f);

    public WorldButtonStruct Button;

    private Action<int> _onClickButton;
    
    private int _index;
    private TweenerCore<Color, Color, ColorOptions> _changeColorTween;
    
    public WorldData WorldData { get; private set; }

    public void Construct(IAudioService audioService, WorldData worldData)
    {
      WorldData = worldData;
      
      Button.Animator.runtimeAnimatorController = worldData.WorldIconAnimatorController;
      
      Button.Button.onClick.AddListener(Click);

      new ButtonAnimation(Button.Button.transform);
      new ButtonSound(audioService, Button.Button.gameObject, 3);
    }

    public void SetClickAction(Action<int> onClickButton, int index)
    {
      _index = index;
      _onClickButton = onClickButton;
    }

    public void SetCurrent(bool isCurrent)
    {
      _changeColorTween?.Kill();
      _changeColorTween = Button.CircleBack.DOColor(isCurrent? _chosenColor : _defaultColor, Time);
      
      PlayAnimation(isCurrent);
    }

    public void SetBackground(bool isCurrent) => 
      Button.CircleBack.color = isCurrent? _chosenColor : _defaultColor;

    public void PlayAnimation(bool isCurrent) => 
      Button.Animator.Play(isCurrent? "Idle" : "Stop");

    private void Click() => 
      _onClickButton?.Invoke(_index);

    [Serializable]
    public struct WorldButtonStruct
    {
      public Image CircleBack;
      public RectTransform Icon;
      public Button Button;
      public Animator Animator;
    }
  }
}