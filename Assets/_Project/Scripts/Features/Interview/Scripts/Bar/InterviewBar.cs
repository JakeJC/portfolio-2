﻿using System;
using ModestTree;
using UnityEngine;

namespace _Project.Scripts.Features.Interview.Scripts.Bar
{
  public class InterviewBar : MonoBehaviour
  {
    [SerializeField] private Color _startColor;
    [SerializeField] private Color _endColor;

    private Section[] _sections;

    public void Construct(Section[] sections)
    {
      _sections = sections;

      foreach (Section section in _sections)
      {
        CalculateColors(section, out Color startColor, out Color endColor);

        section.Construct(startColor, endColor);
        section.ClearFilling();
      }
    }

    public void Complete()
    {
      foreach (Section section in _sections)
        section.Complete();
    }

    public void StartStep(int step)
    {
      if (step < 0 || step > _sections.Length - 1)
        throw new Exception("Нет шага в диапазоне прогресс бара");

      _sections[step].FillFirstLayer(1);
    }

    public void CompleteStep(int step, float progress, bool controlled)
    {
      if (step < 0 || step > _sections.Length - 1)
        throw new Exception("Нет шага в диапазоне прогресс бара");

      _sections[step].FillSecondLayer(progress, controlled);
    }

    private void CalculateColors(Section section, out Color startColor, out Color endColor)
    {
      float startColorRange = (float) _sections.IndexOf(section) / _sections.Length;
      float endColorRange = (float) (_sections.IndexOf(section) + 1) / _sections.Length;

      startColor = Color.Lerp(_startColor, _endColor, startColorRange);
      endColor = Color.Lerp(_startColor, _endColor, endColorRange);
    }
  }
}