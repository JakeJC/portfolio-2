﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace _Project.Scripts.Features.Interview.Scripts.Bar
{
  public class Section : MonoBehaviour
  {
    private const int CompleteDuration = 1;
    
    [SerializeField] private Image _fillLayer1;
    [SerializeField] private Image _fillLayer2;
    
    [SerializeField] private Image _scale;
    [SerializeField] private Image _glow;
    
    [SerializeField] private CanvasGroup group;
    
    private static readonly int Fill = Shader.PropertyToID("_Fill");
    
    private static readonly int StartColor = Shader.PropertyToID("_Start");
    private static readonly int EndColor = Shader.PropertyToID("_End");

    private float _fillValue1;
    private float _fillValue2;
    
    public void Construct(Color startColor, Color endColor)
    {
      SetGradientColors(startColor, endColor);
      
      _fillLayer1.material = new Material(_fillLayer1.material);
      _fillLayer2.material = new Material(_fillLayer2.material);
     
      _fillLayer1.material.SetFloat(Fill, -0.01f);
      _fillLayer2.material.SetFloat(Fill, -0.01f);

      _scale.gameObject.SetActive(false);
      _glow.gameObject.SetActive(false);
    }

    private void SetGradientColors(Color startColor, Color endColor)
    {
      _fillLayer2.material.SetColor(StartColor, startColor);
      _fillLayer2.material.SetColor(EndColor, endColor);
    }

    public void Complete()
    {
      _scale.gameObject.SetActive(true);
      _glow.gameObject.SetActive(true);

      _fillLayer1.DOFade(0, 0.2f);
      _fillLayer2.DOFade(0, 0.2f);
      
      _scale.DOFade(0, CompleteDuration);
      _scale.transform.DOScale(2f, CompleteDuration);
      
      _glow.DOFade(1, .2f).onComplete += () =>
      {
        group.DOFade(0, 0.2f).SetDelay(0.2f);
      };
    }
    
    public void FillFirstLayer(float value)
    {
      Tweener tweener = DOTween.To(x => _fillValue1 = x, _fillValue1, value, 1f).SetEase(Ease.OutExpo);
      tweener.onUpdate += () =>
      {
        _fillLayer1.material.SetFloat(Fill, _fillValue1);
      };
    }

    public void FillSecondLayer(float value, bool controlled)
    {
      if(controlled)
        _fillLayer2.material.SetFloat(Fill, value);
      else
      {
        Tweener tweener = DOTween.To(x => _fillValue2 = x, _fillValue2, value, 1f).SetEase(Ease.OutExpo);
        tweener.onUpdate += () =>
        {
          _fillLayer2.material.SetFloat(Fill, _fillValue2);
        };
      }
    }

    public void ClearFilling()
    {
      _fillValue1 = 0;
      _fillValue2 = 0;
      
      _fillLayer1.material.SetFloat(Fill, -0.01f);
      _fillLayer2.material.SetFloat(Fill, -0.01f);
    }
  }
}