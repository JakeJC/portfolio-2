﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using _Modules._Analytics;
using _Project.Scripts.Animations.UI;
using _Project.Scripts.Data.Scriptables;
using _Project.Scripts.Features.Interview.Scripts.Bar;
using _Project.Scripts.Features.Interview.Scripts.Interviews;
using _Project.Scripts.InApp.Interfaces;
using _Project.Scripts.Logic.Factories.Interfaces;
using _Project.Scripts.Logic.GameLogic.Interfaces;
using _Project.Scripts.Services.LoadScreen.Logic;
using AppacheAds.Scripts.Interfaces;
using AppacheAnalytics.Scripts.Interfaces;
using AppacheRemote.Scripts.Interfaces;
using FMOD;

#if TEST
#endif

namespace _Project.Scripts.Features.Interview.Scripts
{
  public class InterviewLogic : IInterview
  {
    private readonly ILoadScreenService _loadScreenService;
    private readonly IInterviewUI _interviewUI;
    private readonly IAnalytics _analytics;
    private readonly IRemote _remote;
    private readonly IAdsService _ads;
    private readonly ICurrentScreen _screenManager;
    private readonly IInAppAccessor _inAppAccessor;

    private readonly InterviewReason _interviewReason;
    private readonly InterviewColor _interviewColor;
    private InterviewMechanism _interviewMechanism;

    private readonly InterviewBar _interviewBar;
    private byte _interviewBarStepCounter;

    private readonly List<InterviewPart> _interviewParts = new List<InterviewPart>();
    private string _lastWorldId;

    private readonly LoadScreenService.ActionReturnTask _loadWorld;

    private readonly IGameDataProvider _gameDataProvider;

    private readonly bool _namePartIsEnabled;

    public InterviewLogic(ICurrentScreen screenManager, IInAppAccessor inAppAccessor, IGameDataProvider gameDataProvider, ILoadScreenService loadScreenService, IAnalytics analytics,
      IRemote remote, IAdsService ads, IInterviewFactory interviewFactory, LoadScreenService.ActionReturnTask loadWorld)
    {
      _inAppAccessor = inAppAccessor;
      _screenManager = screenManager;
      _gameDataProvider = gameDataProvider;
      _loadScreenService = loadScreenService;
      _loadWorld = loadWorld;
      _analytics = analytics;
      _remote = remote;
      _ads = ads;

      _interviewUI = interviewFactory.SpawnInterviewUi();
      _interviewUI.Next.onClick.AddListener(OnClickNext);
      
      if (_interviewUI.GetNameOperator().GetName().Equals(""))
      {
        InterviewName interviewName = interviewFactory.CreateNamePart();
        interviewName.InjectAdditionalServices(_ads, _gameDataProvider, screenManager, inAppAccessor);
        _interviewParts.Add(interviewName);
        
        _namePartIsEnabled = true;

        InterviewHello interviewHello = interviewFactory.CreateHelloPart();
        interviewHello.InjectAdditionalServices(_ads, _gameDataProvider);
        _interviewParts.Add(interviewHello);

        _interviewReason = interviewFactory.CreateReasonPart();
        _interviewParts.Add(_interviewReason);
      }

      _interviewColor = interviewFactory.CreateColorPart();
      _interviewColor.InjectAdditionalServices(_ads, _gameDataProvider);
      _interviewParts.Add(_interviewColor);

      InitializeMechanismStep(interviewFactory);

      InterviewLoading interviewLoading = interviewFactory.CreateLoadingPart();
      _interviewParts.Add(interviewLoading);

      _interviewBar = interviewFactory.SpawnInterviewBar(_interviewParts.Count);
      interviewLoading.SetAllCompleteAction(_interviewBar.Complete);

      ShowInterviewPage();
    }

    public int GetReasonIndex() =>
      _interviewReason.GetActiveIndex();

    public List<string> GetColorGuids() =>
      _interviewColor.GetColorsGuids();

    public string GetWorldId() =>
      _interviewMechanism == null ? _lastWorldId : _interviewMechanism.GetActiveId();

    private void InitializeMechanismStep(IInterviewFactory interviewFactory)
    {
      if (OnlyOneWorldHaveGroups(out WorldData lastAvailableWorld))
      {
        _lastWorldId = lastAvailableWorld.WorldId;
        return;
      }

      _interviewMechanism = interviewFactory.CreateMechanismsPart();
      _interviewParts.Add(_interviewMechanism);

      bool OnlyOneWorldHaveGroups(out WorldData lastAvailableWorld)
      {
        bool onlyOneWorldHaveGroups = _gameDataProvider.WorldsInfo.GetWorldsData()
          .Count(p => !_gameDataProvider.WorldsInfo.WorldIsOutOfMechanisms(p.WorldId)) == 1;

        lastAvailableWorld = onlyOneWorldHaveGroups
          ? _gameDataProvider.WorldsInfo.GetWorldsData()
            .FirstOrDefault(p => !_gameDataProvider.WorldsInfo.WorldIsOutOfMechanisms(p.WorldId))
          : null;

        return onlyOneWorldHaveGroups;
      }
    }

    private void OnClickNext()
    {
      if (_interviewParts.Count == 1)
      {
        LoadWorldWithInterstitial();
        return;
      }
      
      _interviewUI.Hide(() =>
      {
        InterviewPart part = _interviewParts.First(item => item.gameObject.activeSelf);
        part.Deactivate();
        _interviewParts.Remove(part);

        ShowInterviewPage();

        if (_interviewParts.Count == 1)
          _interviewUI.SetNewSound(4);
      });
    }

    private async Task ToWorld()
    {
      _interviewUI.Close();
      await LoadWorld();
    }

    private void ShowInterviewPage()
    {
      InterviewPart interviewPart = _interviewParts.First(item => !item.gameObject.activeSelf);
      FillBar(interviewPart);
      interviewPart.Activate();
    }

    private void FillBar(InterviewPart interviewPart)
    {
      _interviewBar.StartStep(_interviewBarStepCounter);

      int step = _interviewBarStepCounter;

      interviewPart.SetCompleteAction((value, controlled) => { _interviewBar.CompleteStep(step, value, controlled); });

      _interviewBarStepCounter++;
    }

    private async Task LoadWorld() =>
      await _loadWorld.Invoke();

    private void LoadWorldWithInterstitial()
    {
      _ads.CurrentInterstitial.ShowInterstitial(ScreenNames.Game, null, null);
      _loadScreenService.Launch<BlackTransition>(null, ToWorld);
    }

    private void ShowSubscription()
    {
      _screenManager.SetCurrentScreen(ScreenNames.Subscription);
      _inAppAccessor.Subscription.ShowInterviewSubscription(OnBuySubscription, ShowInterstitialAfterCloseSubscription);
    }

    private void OnBuySubscription()
    {
      _ads.DisableAds();
      _loadScreenService.Launch<BlackTransition>(null, ToWorld);
    }

    private void ShowInterstitialAfterCloseSubscription()
    {
      _ads.CurrentInterstitial.ShowInterstitial(ScreenNames.Interview, null, null);
      _loadScreenService.Launch<BlackTransition>(null, ToWorld);
    }
  }
}