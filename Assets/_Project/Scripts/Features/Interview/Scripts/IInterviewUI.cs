﻿using System;
using _Project.FMOD_AudioService.Logic;
using _Project.Scripts.Features.Interview.Resources.Scriptables.LocaliseInterview;
using _Project.Scripts.Services.Localization.Logic;
using AppacheAnalytics.Scripts.Interfaces;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace _Project.Scripts.Features.Interview.Scripts
{
  public interface IInterviewUI
  {
    void Close();
    void Hide(Action loadStep);
    void LockButton();
    void UnlockButton();
    void MoveButtonUp();
    void MoveButtonDown();
    
    Button Next { get; set; }
    TMP_Text GetTitle();
    Image GetBackgroundGradient();
    NameOperator GetNameOperator();
    void MoveColumns();
    void DeactivateParticles();
    void SetNewSound(int type);

    ILocaleSystem GetLocaleSystem();
    IAudioService GetAudioSystem();
    IAnalytics GetAnalytics();
    
    void ActivateNewPage(LocaliseInterviewData localiseInterviewData, CanvasGroup canvasGroup, bool isColumnsOn);
  }
}