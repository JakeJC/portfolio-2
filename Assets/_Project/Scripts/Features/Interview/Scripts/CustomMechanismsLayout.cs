using System;
using System.Collections.Generic;
using System.Linq;
using Sirenix.OdinInspector;
using UnityEngine;

namespace _Project.Scripts.Features.Interview.Scripts
{
  public class CustomMechanismsLayout : MonoBehaviour
  {
    public RectTransform ContentRoot;
    
    public List<LayerData> LayersData = new List<LayerData>();

    private readonly List<RectTransform> _childs = new List<RectTransform>();
    
    public void AddChild(RectTransform tr)
    {
      if (_childs.Count == GetMaxObjectInLayer())
        throw new Exception("Can't add object, have no layout for this number of objects");

      tr.SetParent(ContentRoot);
      _childs.Add(tr);
      
      Sort();
    }
    
    public void RemoveChild(RectTransform tr)
    {
      _childs.Remove(tr);
      tr.SetParent(null);
    }

    [Button]
    public void AddToLayersData()
    {
      var layerData = new LayerData {Objects = new List<ObjectData>()};

      foreach (RectTransform child in ContentRoot)
      {
        ObjectData objectData = new ObjectData
        {
          Position = child.anchoredPosition, 
          Size = child.sizeDelta
        };
        layerData.Objects.Add(objectData);
      }
      
      LayersData.Add(layerData);
    }

    private void Sort()
    {
      if (LayersData.All(p => p.Objects.Count != _childs.Count)) 
        return;

      LayerData layerData = LayersData.FirstOrDefault(p => p.Objects.Count == _childs.Count);

      for (int i = 0; i < layerData.Objects.Count; i++)
      {
        _childs[i].anchoredPosition = layerData.Objects[i].Position;
        _childs[i].sizeDelta = layerData.Objects[i].Size;
      }
    }

    private int GetMaxObjectInLayer() => 
      LayersData.Max(p => p.Objects.Count);
  }
  
  [Serializable]
  public struct LayerData
  {
    public List<ObjectData> Objects;
  }
  
  [Serializable]
  public struct ObjectData
  {
    public Vector2 Position;
    public Vector2 Size;
  }
}