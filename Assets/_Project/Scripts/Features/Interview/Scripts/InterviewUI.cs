﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using _Project.FMOD_AudioService.Logic;
using _Project.Scripts.Animations.UI;
using _Project.Scripts.Features.Interview.Resources.Scriptables.LocaliseInterview;
using _Project.Scripts.Logic.GameLogic.Interfaces;
using _Project.Scripts.Services.Localization.Logic;
using AppacheAnalytics.Scripts.Interfaces;
using AppacheRemote.Scripts.Interfaces;
using Assets._Project.FMOD_AudioService.Logic;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace _Project.Scripts.Features.Interview.Scripts
{
  public class InterviewUI : MonoBehaviour, IInterviewUI
  {
    private const string LocalizeName = "Interview";
    
    private const float Time = .5f;
    private const float TimeColumn = 2f;

    private const float UpperBorder = 175f;
    private const float BottomBorder = -500f;

    private TMP_Text _textNext;
    private CanvasGroup _nextGroup;

    [SerializeField] private TMP_Text _title;
    [SerializeField] private TMP_Text _description;
    [SerializeField] private Image _gradient;
    [SerializeField] private Button _next;
    [SerializeField] private List<CanvasGroup> _canvasGroup;

    [SerializeField] private GameObject _columns;
    [SerializeField] private GameObject _particles;

    private ButtonSound _sound;

    private NameOperator _nameOperator;

    private ILocaleSystem _localeSystem;
    private IAudioService _audioService;
    private IAnalytics _analytics;
    private IGameDataProvider _gameDataProvider;
    private IRemote _remote;

    private readonly int _alpha = Shader.PropertyToID("_Alpha");

    public Button Next
    {
      get => _next;
      set => _next = value;
    }

    public void Construct(IGameDataProvider gameDataProvider, ILocaleSystem localeSystem, IAudioService audioService, IAnalytics analytics, IRemote remote, NameOperator nameOperator)
    {
      _remote = remote;
      _gameDataProvider = gameDataProvider;
      _analytics = analytics;
      _localeSystem = localeSystem;
      _audioService = audioService;

      _nameOperator = nameOperator;

      _nextGroup = _next.GetComponent<CanvasGroup>();
      _textNext = _next.GetComponentInChildren<TMP_Text>();

      _next.GetComponent<Image>().material = new Material(_next.GetComponent<Image>().material);
      _next.GetComponent<Image>().material.SetFloat(_alpha, _nextGroup.alpha);

      new ButtonAnimation(_next.transform);
      _sound = new ButtonSound(_audioService, _next.gameObject, 0);

      var canvasGroup = _gradient.GetComponent<CanvasGroup>();
      canvasGroup.alpha = 0;
      canvasGroup.DOFade(1, 0.5f);

      var canvasGroup2 = _columns.GetComponent<CanvasGroup>();
      canvasGroup2.alpha = 0;
      canvasGroup2.DOFade(1, 0.5f);
    }

    public void Close() =>
      gameObject.SetActive(false);

    public void Hide(Action loadStep)
    {
      LockButton();
      StartCoroutine(FadeAnimation(0f, loadStep));
    }

    public void LockButton()
    {
      _nextGroup.DOFade(.1f, Time).onUpdate += () => _next.GetComponent<Image>().material.SetFloat(_alpha, _nextGroup.alpha);
      _nextGroup.blocksRaycasts = false;
    }

    public void UnlockButton()
    {
      _nextGroup.DOFade(1f, Time).onUpdate += () => _next.GetComponent<Image>().material.SetFloat(_alpha, _nextGroup.alpha);
      _nextGroup.blocksRaycasts = true;
    }

    public TMP_Text GetTitle() =>
      _title;

    public Image GetBackgroundGradient() =>
      _gradient;

    public NameOperator GetNameOperator() =>
      _nameOperator;

    public ILocaleSystem GetLocaleSystem() =>
      _localeSystem;

    public IAudioService GetAudioSystem() =>
      _audioService;

    public IAnalytics GetAnalytics() =>
      _analytics;

    public void MoveButtonUp()
    {
      if (_next.transform.localPosition.y == BottomBorder)
        _next.transform.DOLocalMoveY(UpperBorder, Time / 2);
    }

    public void MoveButtonDown()
    {
      if (_next.transform.localPosition.y == UpperBorder)
        _next.transform.DOLocalMoveY(BottomBorder, Time / 2);
    }

    public void MoveColumns() =>
      _columns.GetComponentsInChildren<Transform>().Last(item => item != null)
        .DOLocalMoveY(-25, TimeColumn);

    public void DeactivateParticles() =>
      _particles.SetActive(false);

    public void SetNewSound(int type) =>
      _sound?.SetNewSound(type);

    public void ActivateNewPage(LocaliseInterviewData localiseInterviewData, CanvasGroup canvasGroup, bool isColumnsOn)
    {
      StartCoroutine(FadeAnimation(1f, null, canvasGroup));

      _title.text = _localeSystem.GetText(LocalizeName, localiseInterviewData.TitleKey);

      _description.text = _localeSystem.GetText(LocalizeName, localiseInterviewData.DescriptionKey);
      _textNext.text = _localeSystem.GetText(LocalizeName, localiseInterviewData.ContinueKey);
      
      if (_columns.activeSelf)
        _columns.SetActive(isColumnsOn);
    }

    private IEnumerator FadeAnimation(float fadeValue, Action loadStep = null, CanvasGroup canvasGroup = null)
    {
      if (canvasGroup != null)
        _canvasGroup.Add(canvasGroup);

      foreach (CanvasGroup t in _canvasGroup)
      {
        t.DOFade(fadeValue, Time);
        yield return new WaitForSecondsRealtime(.1f);
      }

      yield return new WaitForSecondsRealtime(Time);

      if (fadeValue != 0f || canvasGroup != null) yield break;

      _canvasGroup.Remove(_canvasGroup.Last(item => item.gameObject.activeSelf));

      loadStep?.Invoke();
    }
  }
}