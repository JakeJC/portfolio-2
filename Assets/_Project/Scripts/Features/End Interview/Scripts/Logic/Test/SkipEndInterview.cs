#if TEST
using System;
using UnityEngine;

namespace _Project.Scripts.Features.End_Interview.Scripts.Logic.Test
{
  public class SkipEndInterview : MonoBehaviour
  {
    public Action OnClick;
    
    public void OnGUI()
    {
      if (GUI.Button(new Rect(20, 120 + 70, 200, 60), "Skip Interview"))
        OnClick?.Invoke();
    }
  }
}
#endif