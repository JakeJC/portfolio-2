using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using _Project.FMOD_AudioService.Logic;
using _Project.Scripts.Features.End_Interview.Scripts.Factories;
using _Project.Scripts.Features.End_Interview.Scripts.Factories.Interfaces;
using _Project.Scripts.Features.End_Interview.Scripts.Interfaces;
using _Project.Scripts.Features.End_Interview.Scripts.Logic.States;
#if TEST
using _Project.Scripts.Features.End_Interview.Scripts.Logic.Test;
#endif
using _Project.Scripts.Features.End_Interview.Scripts.Mono.Additional;
using _Project.Scripts.Logic.GameLogic.Interfaces;
using _Project.Scripts.MonoBehaviours.Game.Interfaces;
using _Project.Scripts.Services._Interfaces;
using _Project.Scripts.Services.LeanTouchCamera.Scripts.Interfaces;
using _Project.Scripts.Services.Localization.Logic;
using AppacheAnalytics.Scripts.Interfaces;
using DG.Tweening;
using UnityEngine;
#if TEST
using Object = UnityEngine.Object;
#endif

namespace _Project.Scripts.Features.End_Interview.Scripts.Logic
{
  public class EndInterview
  {
    private readonly Dictionary<Type, IEndInterviewState> _steps;
    private IEndInterviewState _activeStep;

    private readonly IEndInterviewFactory _endInterviewFactory;
    private readonly ICameraController _cameraController;
    
    private readonly Action _onInterviewEnd;
    private readonly IMechanism _mechanism;
    private readonly IGameDataProvider _gameDataProvider;

#if TEST
    private GameObject _tester;
#endif

    public EndInterview(IAssetProvider assetProvider, ILocaleSystem localeSystem, IGameDataProvider gameDataProvider,
      IAudioService audioService, ICameraController cameraController, IAnalytics analytics, IMechanism mechanism, Action onInterviewEnd, bool isEnd)
    {
      _gameDataProvider = gameDataProvider;
      _mechanism = mechanism;
      _cameraController = cameraController;
      _onInterviewEnd = onInterviewEnd;

      _endInterviewFactory = new EndInterviewFactory(this, assetProvider, localeSystem, gameDataProvider, audioService, analytics, mechanism, isEnd);

      _steps = new Dictionary<Type, IEndInterviewState>
      {
        [typeof(EnterState)] = _endInterviewFactory.CreateEnterState(),
        [typeof(LikeState)] = _endInterviewFactory.CreateLikeState(),
        [typeof(RelaxState)] = _endInterviewFactory.CreateRelaxState()
      };
#if TEST
        AddSkip();
#endif
    }

    public void Launch()
    {
      _endInterviewFactory.EndInterviewFactoryUI.CreateBaseScreen();
      _endInterviewFactory.EndInterviewFactoryUI.CreateBar(_steps.Count);

      _endInterviewFactory.EndInterviewFactoryUI.MainCanvasGroup.alpha = 0;
      _endInterviewFactory.EndInterviewFactoryUI.MainCanvasGroup.DOFade(1f, 0.25f).onComplete += Enter<EnterState>;

      float interviewYValue = _gameDataProvider.MechanismsInfo.GetMechanismDataById(_mechanism.GetId()).InterviewYValue;
      _cameraController.InInterview(true, interviewYValue, null);
    }

    public void Enter<T>() where T : class, IEndInterviewState
    {
      _activeStep?.Exit();
      _activeStep = _steps[typeof(T)];
      _activeStep.Enter();

      UpdateLayout();
    }

    public void Exit()
    {
      _activeStep?.Exit();
      _activeStep = null;

      _endInterviewFactory.EndInterviewFactoryUI.ClearUI();
      
      _cameraController.InInterview(false, 0, _onInterviewEnd);
#if TEST
      if(_tester)
        Object.Destroy(_tester.gameObject);
#endif
    }

    private async void UpdateLayout()
    {
      var updateLayoutGroup = _endInterviewFactory.EndInterviewFactoryUI.MainCanvasGroup.GetComponent<UpdateLayoutGroup>();

      updateLayoutGroup.enabled = true;
      await Task.Delay(500);

      if (updateLayoutGroup)
        updateLayoutGroup.enabled = false;
    }

#if TEST
    private void AddSkip()
    {
      _tester = new GameObject("[Test] Skip Interview");
      var skipLevelTestFeature = _tester.AddComponent<SkipEndInterview>();
      skipLevelTestFeature.OnClick = Exit;
    }
#endif
  }
}