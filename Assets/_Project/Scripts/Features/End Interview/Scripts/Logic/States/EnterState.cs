using _Project.Scripts.Features.End_Interview.Scripts.Animations;
using _Project.Scripts.Features.End_Interview.Scripts.Factories.Interfaces;
using _Project.Scripts.Features.End_Interview.Scripts.Interfaces;
using _Project.Scripts.Features.End_Interview.Scripts.Mono.States;
using _Project.Scripts.Services.Localization.Logic;
using UnityEngine;

namespace _Project.Scripts.Features.End_Interview.Scripts.Logic.States
{
  public class EnterState : IEndInterviewState
  {
    private readonly IEndInterviewFactoryUI _endInterviewFactory;
    private readonly EndInterview _endInterview;
    private readonly ILocaleSystem _localeSystem;

    private EnterStateUI _enterStateUI;

    public EnterState(EndInterview endInterview, IEndInterviewFactoryUI endInterviewFactory, ILocaleSystem localeSystem)
    {
      _localeSystem = localeSystem;

      _endInterview = endInterview;
      _endInterviewFactory = endInterviewFactory;
    }

    public void Enter()
    {
      _enterStateUI = _endInterviewFactory.CreateEnterStateUI();
      InitializeState(_enterStateUI);
    }

    public void Exit() =>
      _endInterviewFactory.ClearFrameUI();

    private void InitializeState(EnterStateUI enterStateUI)
    {
      int index = Random.Range(1, 3);  
        
      enterStateUI.DisplayView1.SetText(_localeSystem.GetText("Main",$"Congrats.{index}.heading"));
      enterStateUI.DisplayView2.SetText(_localeSystem.GetText("Main", $"Congrats.{index}.text"));
      
      enterStateUI.ActionView.SetComplete(Complete);
      
      Fade.Animate(enterStateUI.DisplayView1, enterStateUI.DisplayView2, enterStateUI.ActionView);
    }

    private void Complete()
    {    
      _endInterviewFactory.Bar.AddStep();
      Fade.Hide(_enterStateUI.DisplayView1, _enterStateUI.DisplayView2, _enterStateUI.ActionView,
        _endInterview.Enter<LikeState>);
    }
  }
}