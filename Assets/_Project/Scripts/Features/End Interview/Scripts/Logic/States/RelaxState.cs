using System.Collections.Generic;
using _Project.Scripts.Features.End_Interview.Scripts.Animations;
using _Project.Scripts.Features.End_Interview.Scripts.Factories.Interfaces;
using _Project.Scripts.Features.End_Interview.Scripts.Interfaces;
using _Project.Scripts.Features.End_Interview.Scripts.Mono.States;
using _Project.Scripts.Logic.GameLogic.Interfaces;
using _Project.Scripts.Logic.GameLogic.Meta.Interfaces;
using _Project.Scripts.MonoBehaviours.Game.Interfaces;
using _Project.Scripts.Services.Localization.Logic;
using AppacheAnalytics.Scripts.Interfaces;
using DG.Tweening;

namespace _Project.Scripts.Features.End_Interview.Scripts.Logic.States
{ 
  public class RelaxState : IEndInterviewState
  {
    private readonly IEndInterviewFactoryUI _endInterviewFactory;
    private readonly EndInterview _endInterview;
    
    private readonly ILocaleSystem _localeSystem;
    private readonly ITextsInfo _relaxStateTextInfo;
    private readonly IAnalytics _analytics;
    private readonly IMechanism _mechanism;
    private readonly IGameDataProvider _gameDataProvider;

    private RelaxStateUI _relaxStateUI;
    private bool _appeared;

    public RelaxState(EndInterview endInterview, IEndInterviewFactoryUI endInterviewFactory, ILocaleSystem localeSystem,
      ITextsInfo relaxStateTextInfo, IAnalytics analytics, IMechanism mechanism, IGameDataProvider gameDataProvider)
    {
      _gameDataProvider = gameDataProvider;
      _mechanism = mechanism;
      _analytics = analytics;
      _relaxStateTextInfo = relaxStateTextInfo;
      _localeSystem = localeSystem;
      
      _endInterview = endInterview;
      _endInterviewFactory = endInterviewFactory;
    }
    
    public void Enter()
    {
      _relaxStateUI = _endInterviewFactory.CreateRelaxStateUI();
      InitializeState(_relaxStateUI);
    }

    public void Exit()
    {
      _gameDataProvider.EndStateTextInfo.ClearUsedIndexes();
      _gameDataProvider.LikeStateTextInfo.ClearUsedIndexes();
      _gameDataProvider.RelaxStateTextInfo.ClearUsedIndexes();
      
      _endInterviewFactory.ClearFrameUI();
    }

    private void InitializeState(RelaxStateUI relaxStateUI)
    {
      relaxStateUI.DisplayView2.SetText(_localeSystem.GetText("Main", $"RelaxState.{_relaxStateTextInfo.GetUniqueIndex()}"));
      relaxStateUI.ActionView1.SetupSlider(relaxStateUI.DisplayView1.GetFrames(), SetValue);
      relaxStateUI.ActionView2.SetComplete(Complete);

      relaxStateUI.DisplayView1.SetValue(0);
      _relaxStateUI.ActionView2.CanvasGroup.interactable = false;

      Fade.Animate(relaxStateUI.DisplayView1, relaxStateUI.DisplayView2, relaxStateUI.ActionView1, relaxStateUI.ActionView2);
    }

    private void SetValue(int frame)
    {
      _relaxStateUI.DisplayView1.SetValue(frame);

      if (_appeared)
      {
        _relaxStateUI.ActionView2.CanvasGroup.alpha = 1;
        _relaxStateUI.ActionView2.CanvasGroup.interactable = true;
        return;
      }

      _relaxStateUI.ActionView2.CanvasGroup.DOFade(1f, .25f);
      _relaxStateUI.ActionView2.CanvasGroup.interactable = true;

      _appeared = true;
    }

    private void Complete()
    {
      _analytics.Send("ev_rating_relax", new Dictionary<string, object>()
      {
        {"level_id", _mechanism.GetId()},
        {"rating", _relaxStateUI.ActionView1.GetValue()}
      });
      
      _endInterviewFactory.Bar.AddStep();
      Fade.Hide(_relaxStateUI.DisplayView1, _relaxStateUI.DisplayView2, _relaxStateUI.ActionView1, _relaxStateUI.ActionView2,
        _endInterview.Exit);
    }
  }
}