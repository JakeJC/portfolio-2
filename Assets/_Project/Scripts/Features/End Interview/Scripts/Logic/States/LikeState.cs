using System.Collections.Generic;
using _Project.Scripts.Features.End_Interview.Scripts.Animations;
using _Project.Scripts.Features.End_Interview.Scripts.Factories.Interfaces;
using _Project.Scripts.Features.End_Interview.Scripts.Interfaces;
using _Project.Scripts.Features.End_Interview.Scripts.Mono.States;
using _Project.Scripts.Logic.GameLogic.Meta.Interfaces;
using _Project.Scripts.MonoBehaviours.Game.Interfaces;
using _Project.Scripts.Services.Localization.Logic;
using AppacheAnalytics.Scripts.Interfaces;
using DG.Tweening;

namespace _Project.Scripts.Features.End_Interview.Scripts.Logic.States
{
  public class LikeState : IEndInterviewState
  {
    private readonly IEndInterviewFactoryUI _endInterviewFactory;
    private readonly EndInterview _endInterview;

    private readonly ILocaleSystem _localeSystem;
    private readonly ITextsInfo _likeStateInfo;

    private LikeStateUI _likeStateUI;
    private bool _appeared;

    private IAnalytics _analytics;
    private IMechanism _mechanism;

    public LikeState(EndInterview endInterview, IEndInterviewFactoryUI endInterviewFactory, ILocaleSystem localeSystem, ITextsInfo likeStateInfo, IAnalytics analytics, IMechanism mechanism)
    {
      _mechanism = mechanism;
      _analytics = analytics;
      _likeStateInfo = likeStateInfo;
      _localeSystem = localeSystem;

      _endInterview = endInterview;
      _endInterviewFactory = endInterviewFactory;
    }

    public void Enter()
    {
      _likeStateUI = _endInterviewFactory.CreateLikeStateUI();
      InitializeState(_likeStateUI);
    }

    public void Exit() =>
      _endInterviewFactory.ClearFrameUI();

    private void InitializeState(LikeStateUI likeStateUI)
    {
      likeStateUI.DisplayView2.SetText(_localeSystem.GetText("Main", $"LikeState.{_likeStateInfo.GetUniqueIndex()}"));
      likeStateUI.ActionView1.SetupSlider(likeStateUI.DisplayView1.GetFrames(), SetValue);
      likeStateUI.ActionView2.SetComplete(Complete);

      _likeStateUI.DisplayView1.SetValue(0);
      _likeStateUI.ActionView2.CanvasGroup.interactable = false;

      Fade.Animate(likeStateUI.DisplayView1, likeStateUI.DisplayView2, likeStateUI.ActionView1, likeStateUI.ActionView2);
    }

    private void SetValue(int frame)
    {
      _likeStateUI.DisplayView1.SetValue(frame);

      if (_appeared)
      {
        _likeStateUI.ActionView2.CanvasGroup.alpha = 1;
        _likeStateUI.ActionView2.CanvasGroup.interactable = true;
        return;
      }

      _likeStateUI.ActionView2.CanvasGroup.DOFade(1f, .25f);
      _likeStateUI.ActionView2.CanvasGroup.interactable = true;

      _appeared = true;
    }

    private void Complete()
    {
      _analytics.Send("ev_rating_level", new Dictionary<string, object>()
      {
        {"level_id", _mechanism.GetId()},
        {"rating", _likeStateUI.ActionView1.GetValue()}
      });

      _endInterviewFactory.Bar.AddStep();
      Fade.Hide(_likeStateUI.DisplayView1, _likeStateUI.DisplayView2, _likeStateUI.ActionView1, _likeStateUI.ActionView2,
        _endInterview.Enter<RelaxState>);
    }
  }
}