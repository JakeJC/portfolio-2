﻿using System;
using _Project.Scripts.Features.End_Interview.Scripts.Mono.Views.Base;
using DG.Tweening;

namespace _Project.Scripts.Features.End_Interview.Scripts.Animations
{
  public static class Fade
  {
    public static void Animate(ViewUI viewState1, ViewUI viewState2, ActionUI actionState)
    {
      viewState1.CanvasGroup.alpha = 0;
      viewState2.CanvasGroup.alpha = 0;
      actionState.CanvasGroup.alpha = 0;

      actionState.CanvasGroup.interactable = false;
      
      actionState.CanvasGroup.DOFade(1f, 0.5f).SetEase(Ease.Linear).SetDelay(0.15f);
      viewState2.CanvasGroup.DOFade(1f, 0.5f).SetEase(Ease.Linear).SetDelay(0.3f);
      viewState1.CanvasGroup.DOFade(1f, 0.5f).SetEase(Ease.Linear).SetDelay(.45f).onComplete += () =>
      {
        actionState.CanvasGroup.interactable = true;
      };
    }

    public static void Animate(ViewUI viewState1, ViewUI viewState2, ActionUI actionState1, ActionUI actionState2)
    {
      viewState1.CanvasGroup.alpha = 0;
      viewState2.CanvasGroup.alpha = 0;
      actionState1.CanvasGroup.alpha = 0;
      actionState2.CanvasGroup.alpha = 0;
      
      actionState1.CanvasGroup.interactable = false;
      actionState2.CanvasGroup.interactable = false;
      
      actionState2.CanvasGroup.DOFade(.1f, 0.5f).SetDelay(0.15f).SetEase(Ease.Linear);
      actionState1.CanvasGroup.DOFade(1f, 0.5f).SetDelay(0.3f).SetEase(Ease.Linear);
      viewState2.CanvasGroup.DOFade(1f, 0.5f).SetDelay(.45f).SetEase(Ease.Linear);
      viewState1.CanvasGroup.DOFade(1f, 0.5f).SetDelay(.6f).SetEase(Ease.Linear).onComplete += () =>
      {
        actionState1.CanvasGroup.interactable = true;
        actionState2.CanvasGroup.interactable = false;
      };
    }

    public static void Hide(ViewUI viewState1, ViewUI viewState2, ActionUI actionState, Action onComplete)
    {
      actionState.CanvasGroup.interactable = false;
      
      viewState1.CanvasGroup.DOFade(0f, 0.5f).SetEase(Ease.Linear);
      viewState2.CanvasGroup.DOFade(0f, 0.5f).SetDelay(.15f).SetEase(Ease.Linear);
      actionState.CanvasGroup.DOFade(0f, 0.5f).SetDelay(.3f).SetEase(Ease.Linear).onComplete += () =>
      {
        actionState.CanvasGroup.interactable = true;
        
        onComplete?.Invoke();
      };
    }
    
    public static void Hide(ViewUI viewState1, ViewUI viewState2, ActionUI actionState1, ActionUI actionState2, Action onComplete)
    {    
      actionState1.CanvasGroup.interactable = false;
      actionState2.CanvasGroup.interactable = false;

      viewState1.CanvasGroup.DOFade(0f, 0.5f).SetEase(Ease.Linear);
      viewState2.CanvasGroup.DOFade(0f, 0.5f).SetDelay(.15f).SetEase(Ease.Linear);
      actionState1.CanvasGroup.DOFade(0f, 0.5f).SetDelay(.3f).SetEase(Ease.Linear);
      actionState2.CanvasGroup.DOFade(0f, 0.5f).SetDelay(.45f).SetEase(Ease.Linear).onComplete += () =>
      {
        actionState1.CanvasGroup.interactable = true;
        actionState2.CanvasGroup.interactable = true;
        
        onComplete?.Invoke();
      };
    }
  }
}