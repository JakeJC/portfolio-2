using _Project.Scripts.Features.End_Interview.Scripts.Mono.States;
using _Project.Scripts.Features.End_Interview.Scripts.Mono.Views;
using UnityEngine;

namespace _Project.Scripts.Features.End_Interview.Scripts.Factories.Interfaces
{
  public interface IEndInterviewFactoryUI
  {
    CanvasGroup MainCanvasGroup { get; }
    BarUI Bar { get; }
    void CreateBaseScreen();
    void CreateBar(int stepsCount);
    EnterStateUI CreateEnterStateUI();
    LikeStateUI CreateLikeStateUI();
    RelaxStateUI CreateRelaxStateUI();
    EndStateUI CreateEndStateUI();
    void ClearFrameUI();
    void ClearUI();
  }
}