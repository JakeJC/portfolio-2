using _Project.Scripts.Features.End_Interview.Scripts.Logic.States;

namespace _Project.Scripts.Features.End_Interview.Scripts.Factories.Interfaces
{
  public interface IEndInterviewFactory
  {
    IEndInterviewFactoryUI EndInterviewFactoryUI { get; }
    LikeState CreateLikeState();
    RelaxState CreateRelaxState();
    EnterState CreateEnterState();
  }
}