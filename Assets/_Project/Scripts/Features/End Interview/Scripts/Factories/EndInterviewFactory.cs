using _Project.FMOD_AudioService.Logic;
using _Project.Scripts.Features.End_Interview.Scripts.Factories.Interfaces;
using _Project.Scripts.Features.End_Interview.Scripts.Logic;
using _Project.Scripts.Features.End_Interview.Scripts.Logic.States;
using _Project.Scripts.Logic.GameLogic.Interfaces;
using _Project.Scripts.MonoBehaviours.Game.Interfaces;
using _Project.Scripts.Services._Interfaces;
using _Project.Scripts.Services.Localization.Logic;
using AppacheAnalytics.Scripts.Interfaces;

namespace _Project.Scripts.Features.End_Interview.Scripts.Factories
{
  public class EndInterviewFactory : IEndInterviewFactory
  {
    private readonly EndInterview _endInterview;
    private readonly ILocaleSystem _localeSystem;
    private readonly IGameDataProvider _gameDataProvider;
    private readonly IAnalytics _analytics;
    private readonly IMechanism _mechanism;

    private readonly bool _isEnd;

    public IEndInterviewFactoryUI EndInterviewFactoryUI { get; }

    public EndInterviewFactory(EndInterview endInterview, IAssetProvider assetProvider, ILocaleSystem localeSystem,
      IGameDataProvider gameDataProvider, IAudioService audioService, IAnalytics analytics, IMechanism mechanism, bool isEnd)
    {
      _mechanism = mechanism;
      _analytics = analytics;
      _isEnd = isEnd;
      _gameDataProvider = gameDataProvider;
      _localeSystem = localeSystem;
      _endInterview = endInterview;
      
      EndInterviewFactoryUI = new EndInterviewFactoryUI(assetProvider, audioService, localeSystem);
    }

    public LikeState CreateLikeState() => 
      new LikeState(_endInterview, EndInterviewFactoryUI, _localeSystem, _gameDataProvider.LikeStateTextInfo, _analytics, _mechanism);
    
    public RelaxState CreateRelaxState() => 
      new RelaxState(_endInterview, EndInterviewFactoryUI, _localeSystem, _gameDataProvider.RelaxStateTextInfo, _analytics, _mechanism, _gameDataProvider);
    
    public EnterState CreateEnterState() => 
      new EnterState(_endInterview, EndInterviewFactoryUI, _localeSystem);
  }
}