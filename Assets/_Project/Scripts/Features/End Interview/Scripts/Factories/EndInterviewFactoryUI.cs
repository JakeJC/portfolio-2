using System;
using System.Collections.Generic;
using _Project.FMOD_AudioService.Logic;
using _Project.Scripts.Features.End_Interview.Scripts.Factories.Interfaces;
using _Project.Scripts.Features.End_Interview.Scripts.Mono.States;
using _Project.Scripts.Features.End_Interview.Scripts.Mono.Views;
using _Project.Scripts.Features.End_Interview.Scripts.Mono.Views.Base;
using _Project.Scripts.Services._Interfaces;
using _Project.Scripts.Services.Localization.Logic;
using DG.Tweening;
using UnityEngine;
using Object = UnityEngine.Object;

namespace _Project.Scripts.Features.End_Interview.Scripts.Factories
{
  public class EndInterviewFactoryUI : IEndInterviewFactoryUI
  {
    private const string CanvasPath = "Canvas";
    private const string UIEndInterviewBaseScreen = "UI/End Interview/Base Screen";

    private readonly IAssetProvider _assetProvider;
    private readonly IAudioService _audioService;
    private readonly ILocaleSystem _localeSystem;

    private Transform _baseScreen;

    private readonly Dictionary<Type, string> _paths;

    public CanvasGroup MainCanvasGroup { get; private set; }
    public BarUI Bar { get; private set; }

    private readonly List<ViewUI> _forClearing = new List<ViewUI>();

    public EndInterviewFactoryUI(IAssetProvider assetProvider, IAudioService audioService, ILocaleSystem localeSystem)
    {
      _localeSystem = localeSystem;
      _audioService = audioService;
      _assetProvider = assetProvider;

      _paths = new Dictionary<Type, string>()
      {
        [typeof(QuestionUI)] = "UI/End Interview/Question",
        [typeof(NextUI)] = "UI/End Interview/Next",
        [typeof(BarUI)] = "UI/End Interview/Bar",
        [typeof(LotusUI)] = "UI/End Interview/Lotus",
        [typeof(QuestionSmallUI)] = "UI/End Interview/Question Small",
        [typeof(SliderUI)] = "UI/End Interview/Slider",
        [typeof(SpaceUI)] = "UI/End Interview/Space",
        [typeof(LargeSpaceUI)] = "UI/End Interview/Large Space",
        [typeof(TitleUI)] = "UI/End Interview/Title"
      };
    }

    public void CreateBaseScreen()
    {
      _baseScreen = Create<Transform>(UIEndInterviewBaseScreen, GameObject.Find(CanvasPath).transform).transform;
      MainCanvasGroup = _baseScreen.gameObject.GetComponent<CanvasGroup>();
    }

    public void CreateBar(int stepsCount)
    {
      Bar = CreateDisplayView<BarUI>();
      Bar.Construct();
      Bar.SetStepsCount(stepsCount);
    }

    public EnterStateUI CreateEnterStateUI()
    {
      TitleUI viewState1 = CreateDisplayView<TitleUI>();
      SpaceUI space1 = CreateDisplayView<SpaceUI>();
      QuestionUI viewState2 = CreateDisplayView<QuestionUI>();
      LargeSpaceUI space2 = CreateDisplayView<LargeSpaceUI>();
      NextUI actionState = CreateActionView<NextUI>();

      var stateUI = _baseScreen.gameObject.AddComponent<EnterStateUI>();
      stateUI.Construct(viewState1, viewState2, actionState);

      _forClearing.Add(viewState1);
      _forClearing.Add(space1);
      _forClearing.Add(viewState2);
      _forClearing.Add(space2);
      _forClearing.Add(actionState);
      
      return stateUI;
    }

    public LikeStateUI CreateLikeStateUI()
    {
      LotusUI lotus = CreateDisplayView<LotusUI>();
      QuestionSmallUI viewState = CreateDisplayView<QuestionSmallUI>();
      SliderUI actionState1 = CreateActionView<SliderUI>();
      SpaceUI space = CreateDisplayView<SpaceUI>();
      NextUI actionState2 = CreateActionView<NextUI>();

      var stateUI = _baseScreen.gameObject.AddComponent<LikeStateUI>();
      stateUI.Construct(lotus, viewState, actionState1, actionState2);

      _forClearing.Add(lotus);
      _forClearing.Add(viewState);
      _forClearing.Add(space);
      _forClearing.Add(actionState1);
      _forClearing.Add(actionState2);
      
      return stateUI;
    }

    public RelaxStateUI CreateRelaxStateUI()
    {
      LotusUI lotus = CreateDisplayView<LotusUI>();
      QuestionSmallUI viewState = CreateDisplayView<QuestionSmallUI>();
      SliderUI actionState1 = CreateActionView<SliderUI>();
      SpaceUI space = CreateDisplayView<SpaceUI>();
      NextUI actionState2 = CreateActionView<NextUI>();
      
      var stateUI = _baseScreen.gameObject.AddComponent<RelaxStateUI>();
      stateUI.Construct(lotus, viewState, actionState1, actionState2);
      
      _forClearing.Add(lotus);
      _forClearing.Add(viewState);
      _forClearing.Add(space);
      _forClearing.Add(actionState1);
      _forClearing.Add(actionState2);
      
      return stateUI;
    }

    public EndStateUI CreateEndStateUI()
    {
      QuestionSmallUI viewState1 = CreateDisplayView<QuestionSmallUI>();
      LargeSpaceUI viewState2 = CreateDisplayView<LargeSpaceUI>();
      NextUI actionState = CreateActionView<NextUI>();

      var stateUI = _baseScreen.gameObject.AddComponent<EndStateUI>();
      stateUI.Construct(viewState1, viewState2, actionState);

      _forClearing.Add(viewState1);
      _forClearing.Add(viewState2);
      _forClearing.Add(actionState);
      
      return stateUI;
    }
    
    public void ClearFrameUI()
    {
      foreach (ViewUI viewUI in _forClearing) 
        Object.Destroy(viewUI.gameObject);
      
      _forClearing.Clear();
    }

    public void ClearUI()
    {
      MainCanvasGroup.DOFade(0f, .2f).onComplete += () =>
      {
        if(Bar != null)
          Object.Destroy(Bar.gameObject);
      
        if (_baseScreen != null)
          Object.Destroy(_baseScreen.gameObject);
      };
    }

    private TView CreateDisplayView<TView>() where TView : ViewUI
    {
      var view = Create<TView>(_paths[typeof(TView)], _baseScreen);
      view.Construct();

      return view;
    }

    private TView CreateActionView<TView>() where TView : ActionUI
    {
      var view = Create<TView>(_paths[typeof(TView)], _baseScreen);

      view.Construct();
      view.Construct(_audioService, _localeSystem);

      return view;
    }

    private T Create<T>(string assetPath, Transform parent) where T : Component =>
      Object.Instantiate(_assetProvider.GetResource<T>(assetPath), parent);
  }
}