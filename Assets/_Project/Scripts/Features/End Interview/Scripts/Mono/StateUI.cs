using UnityEngine;

namespace _Project.Scripts.Features.End_Interview.Scripts.Mono
{
  public class StateUI<T1, T2, T3> : MonoBehaviour where T1 : Component where T2 : Component  where T3 : Component
  {
    public T1 DisplayView1;
    public T2 DisplayView2;
    public T3 ActionView;
    
    public void Construct(T1 displayView1, T2 displayView2, T3 actionView)
    {
      DisplayView1 = displayView1;
      DisplayView2 = displayView2;
      ActionView = actionView;
    }
  }
}