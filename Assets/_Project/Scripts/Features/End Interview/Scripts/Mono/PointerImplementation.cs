﻿using System;
using UnityEngine;

namespace _Project.Scripts.Features.End_Interview.Scripts.Mono
{
  public class PointerImplementation : MonoBehaviour
  {
    private Action _onPointerDown;
    private Action _onPointerUp;

    public void Construct(Action onPointerDown, Action onPointerUp)
    {
      _onPointerUp = onPointerUp;
      _onPointerDown = onPointerDown;
    }
    
    private void LateUpdate()
    {
      if (Input.GetKeyDown(KeyCode.Mouse0)) 
        _onPointerDown?.Invoke();

      if (Input.GetKeyUp(KeyCode.Mouse0)) 
        _onPointerUp?.Invoke();
    }
  }
}