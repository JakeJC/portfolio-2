using UnityEngine;

namespace _Project.Scripts.Features.End_Interview.Scripts.Mono
{
  public class ExtendedStateUI<T1, T2, T3, T4> : MonoBehaviour where T1 : Component where T2 : Component  where T3 : Component where T4: Component
  {
    public T1 DisplayView1;
    public T2 DisplayView2;
    public T3 ActionView1;
    public T4 ActionView2;
    
    public void Construct(T1 displayView1, T2 displayView2, T3 actionView1, T4 actionView2)
    {
      DisplayView1 = displayView1;
      DisplayView2 = displayView2;
      ActionView1 = actionView1;
      ActionView2 = actionView2;
    }
  }
}