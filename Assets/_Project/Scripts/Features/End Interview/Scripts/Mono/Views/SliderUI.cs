﻿using System;
using _Project.FMOD_AudioService.Logic;
using _Project.Scripts.Features.End_Interview.Scripts.Mono.Views.Base;
using _Project.Scripts.Services.Localization.Logic;
using UnityEngine;
using UnityEngine.UI;

namespace _Project.Scripts.Features.End_Interview.Scripts.Mono.Views
{
  public class SliderUI : ActionUI
  {   
    private CanvasGroup _canvasGroup;
    
    public Slider Slider;
    private Action<int> _onSliderChange;
    
    private bool _isPressed;
    private bool _isChanged;
    
    private Action _completeAction;

    public override CanvasGroup CanvasGroup 
      => _canvasGroup;

    public override void Construct()
    {
      _canvasGroup = GetComponent<CanvasGroup>();
      
      var sliderONValueChanged = new Slider.SliderEvent();
      sliderONValueChanged.AddListener(ChangeValue);
      
      Slider.value = 0;
      Slider.onValueChanged = sliderONValueChanged;

      var implementation = Slider.targetGraphic.gameObject.AddComponent<PointerImplementation>();
      implementation.Construct(OnPointerDown, OnPointerUp);
    }

    public override void Construct(IAudioService audioService, ILocaleSystem localeSystem)
    {}

    public void SetupSlider(int maxValue, Action<int> onSliderChange)
    {
      Slider.minValue = 0;
      Slider.maxValue = maxValue;
      _onSliderChange = onSliderChange;
    }

    public float GetValue() =>
      Slider.value/Slider.maxValue;
    
    private void Complete() => 
      _completeAction?.Invoke();

    private void ChangeValue(float value)
    {
      _onSliderChange?.Invoke((int) value);
      _isChanged = true;
    }

    private void OnDestroy() => 
      Slider.onValueChanged = null;

    private void OnPointerDown() => 
      _isPressed = true;

    private void OnPointerUp()
    {
      if(_isChanged)
        Complete();
      
      _isPressed = false;
      _isChanged = false;
    }
  }
}