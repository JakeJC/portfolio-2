﻿using _Project.Scripts.Features.End_Interview.Scripts.Mono.Views.Base;
using UnityEngine;
using UnityEngine.UI;

namespace _Project.Scripts.Features.End_Interview.Scripts.Mono.Views
{
  [RequireComponent(typeof(Image))]
  public class LotusUI : ViewUI
  {
    private CanvasGroup _canvasGroup;

    public ParticleSystem ParticleSystem;
    
    public Sprite[] Sprites;
    private Image _image;

    public override CanvasGroup CanvasGroup => _canvasGroup;

    public override void Construct()
    {
      _image = GetComponent<Image>();
      _canvasGroup = GetComponent<CanvasGroup>();
    }

    public int GetFrames() => 
      Sprites.Length - 1;
    
    public void SetValue(int value)
    {
      if(value >= 0 && value < Sprites.Length)
        _image.sprite = Sprites[value];

      ParticleSystem.EmissionModule particleSystemEmission = ParticleSystem.emission;
      particleSystemEmission.enabled = value >= Sprites.Length - 1;
    }
  }
}