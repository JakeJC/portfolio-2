using _Project.Scripts.Features.End_Interview.Scripts.Mono.Views.Base;
using TMPro;
using UnityEngine;

namespace _Project.Scripts.Features.End_Interview.Scripts.Mono.Views
{
  [RequireComponent(typeof(TextMeshProUGUI))]
  public class QuestionUI : ViewUI
  {
    private TextMeshProUGUI _text;
    
    private CanvasGroup _canvasGroup;
    public override CanvasGroup CanvasGroup => 
      _canvasGroup;
    
    public override void Construct()
    {
      _canvasGroup = GetComponent<CanvasGroup>();
      _text = GetComponent<TextMeshProUGUI>();
    }

    public void SetText(string text) => 
      _text.text = text;
  }
}