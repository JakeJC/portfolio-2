﻿using _Project.Scripts.Features.End_Interview.Scripts.Mono.Views.Base;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace _Project.Scripts.Features.End_Interview.Scripts.Mono.Views
{
  public class BarUI : ViewUI
  {
    [SerializeField] private Image _fillLayer;

    private static readonly int Fill = Shader.PropertyToID("_Fill");
    private float _fillValue;
    
    private int _stepsCount;
    
    private CanvasGroup _canvasGroup;

    public override CanvasGroup CanvasGroup =>
      _canvasGroup;

    public override void Construct()
    {
      _canvasGroup = GetComponent<CanvasGroup>();
      
      _fillLayer.material = new Material(_fillLayer.material);
      _fillLayer.material.SetFloat(Fill, -0.01f);
    }

    public void SetStepsCount(int stepsCount) => 
      _stepsCount = stepsCount;

    public void AddStep()
    {
      float target = _fillValue + 1f/_stepsCount;
      
      Tweener tweener = DOTween.To(x => _fillValue = x, _fillValue, target, 1f).SetEase(Ease.OutExpo);
      tweener.onUpdate += () => { _fillLayer.material.SetFloat(Fill, _fillValue); };
      tweener.onComplete += () =>
      {
        if (Mathf.Approximately(target, 1f))
          Complete();
      };
    }

    private void Complete() => 
      CanvasGroup.DOFade(0, 0.2f);
  }
}