using System;
using _Project.FMOD_AudioService.Logic;
using _Project.Scripts.Animations.UI;
using _Project.Scripts.Features.End_Interview.Scripts.Mono.Views.Base;
using _Project.Scripts.Services.Localization.Logic;
using Assets._Project.FMOD_AudioService.Logic;
using UnityEngine;
using UnityEngine.UI;

namespace _Project.Scripts.Features.End_Interview.Scripts.Mono.Views
{
  [RequireComponent(typeof(Button))]
  public class NextUI : ActionUI
  {
    private Action _complete;
    private Button _button;
    
    private CanvasGroup _canvasGroup;
    
    private ButtonSound _buttonSound;
    
    public override CanvasGroup CanvasGroup => 
      _canvasGroup;

    public override void Construct()
    {
      _canvasGroup = GetComponent<CanvasGroup>();
      _button = GetComponent<Button>();
    }

    public override void Construct(IAudioService audioService, ILocaleSystem localeSystem)
    {
      new ButtonAnimation(_button.transform);
      _buttonSound = new ButtonSound(audioService, _button.gameObject, 0);

      Localize(localeSystem);
    }

    public void SetComplete(Action complete)
    {
      _complete = complete;
      _button.onClick.AddListener(OnNext);
    }

    private void OnNext()
    {
      _button.interactable = false;
      Complete();
    }

    private void Complete() => 
      _complete?.Invoke();
    
    private void Localize(ILocaleSystem localeSystem)
    {
      LocalizedMainText localizedText = transform.GetComponentInChildren<LocalizedMainText>();
      localizedText.Construct(localeSystem);
      localizedText.Awake();
    }
  }
}