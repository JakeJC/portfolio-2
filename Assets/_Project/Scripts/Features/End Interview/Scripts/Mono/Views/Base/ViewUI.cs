using UnityEngine;

namespace _Project.Scripts.Features.End_Interview.Scripts.Mono.Views.Base
{
  public abstract class ViewUI : MonoBehaviour
  {
    public abstract CanvasGroup CanvasGroup { get; }
    public abstract void Construct();
  }
}