﻿using _Project.FMOD_AudioService.Logic;
using _Project.Scripts.Services.Localization.Logic;

namespace _Project.Scripts.Features.End_Interview.Scripts.Mono.Views.Base
{
  public abstract class ActionUI : ViewUI
  {    
    public abstract void Construct(IAudioService audioService, ILocaleSystem localeSystem);
  }
}