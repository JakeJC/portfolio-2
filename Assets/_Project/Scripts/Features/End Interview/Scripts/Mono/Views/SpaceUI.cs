﻿using _Project.Scripts.Features.End_Interview.Scripts.Mono.Views.Base;
using UnityEngine;

namespace _Project.Scripts.Features.End_Interview.Scripts.Mono.Views
{
  public class SpaceUI : ViewUI
  {
    private CanvasGroup _group;
    public override CanvasGroup CanvasGroup =>
      _group;

    public override void Construct() => 
      _group = GetComponent<CanvasGroup>();
  }
}