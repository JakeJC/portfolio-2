namespace _Project.Scripts.Features.End_Interview.Scripts.Interfaces
{
  public interface IEndInterviewState
  {
    void Enter();
    void Exit();
  }
}