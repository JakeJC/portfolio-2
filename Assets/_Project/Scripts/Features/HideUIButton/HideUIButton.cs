using _Project.FMOD_AudioService.Logic;
using _Project.Scripts.MonoBehaviours.UI;
using _Project.Scripts.Services._Interfaces;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace _Project.Scripts.Features.HideUIButton
{
  public class HideUIButton
  {
    private const string IconShowPath = "icon_show";
    private const string IconHidePath = "icon_hide";
    
    private readonly IHideUIButtonFactory _hideUIButtonFactory;
    private readonly GameStatePanel _gameStatePanel;

    private bool _isHidden;
    private bool _inTransition;

    private readonly Sprite _showSprite;
    private readonly Sprite _hiddenSprite;

    private readonly IGameSaveSystem _gameSaveSystem;
    
    private Button HideButton => _hideUIButtonFactory.Button;
    public CanvasGroup CanvasGroup => _hideUIButtonFactory.CanvasGroup;
    public HandleClick HandleClick => _hideUIButtonFactory.HandleClick;
    public RectTransform RectTransform => _hideUIButtonFactory.RectTransform;
    
    public HideUIButton(IGameSaveSystem gameSaveSystem, IAssetProvider assetProvider, IAudioService audioService, GameStatePanel gameStatePanel)
    {
      _gameSaveSystem = gameSaveSystem;
      _gameStatePanel = gameStatePanel;

      _showSprite = assetProvider.GetResource<Sprite>(IconShowPath);
      _hiddenSprite = assetProvider.GetResource<Sprite>(IconHidePath);
      
      var hideUIButtonFactory = new HideUIButtonFactory(assetProvider, audioService);
      _hideUIButtonFactory = hideUIButtonFactory;
      _hideUIButtonFactory.CreateButton(OnClick, gameStatePanel.transform);
      
      CanvasGroup.gameObject.SetActive(false);
    }
    
    public void Clear() =>
      _hideUIButtonFactory.Clear();

    public void Appear()
    {
      CanvasGroup.gameObject.SetActive(true);
      CanvasGroup.alpha = 0;
      CanvasGroup.DOFade(1, 0.2f);
    }
    
    private void OnClick()
    {
      if(_inTransition)
        return;
      
      _inTransition = true;

      ChangeButtonState();

      _gameStatePanel.ShiftGamePanel(!_isHidden, () => _inTransition = false, _gameSaveSystem.Get().TutorialComplete? 1: 0.1f);
      
      _isHidden = !_isHidden;
    }

    private void ChangeButtonState()
    {
      var transform = _gameStatePanel.transform;
      HideButton.transform.SetParent(_isHidden ? transform : transform.parent);
      HideButton.GetComponent<Image>().sprite = _isHidden ? _hiddenSprite : _showSprite;
    }
  }
}