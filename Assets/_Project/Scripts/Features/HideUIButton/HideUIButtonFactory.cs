using System;
using _Project.FMOD_AudioService.Logic;
using _Project.Scripts.Animations.UI;
using _Project.Scripts.Services._Interfaces;
using Assets._Project.FMOD_AudioService.Logic;
using UnityEngine;
using UnityEngine.UI;
using Object = UnityEngine.Object;

namespace _Project.Scripts.Features.HideUIButton
{
  public class HideUIButtonFactory : IHideUIButtonFactory
  {
    private const string HideButtonPath = "UI/Game/Hide UI Button";

    private readonly IAssetProvider _assetProvider;
    private readonly IAudioService _audioService;
    
    public Button Button { get; private set; }
    public CanvasGroup CanvasGroup { get; private set; }
    public HandleClick HandleClick { get; private set; }
    public RectTransform RectTransform { get; private set; }

    public HideUIButtonFactory(IAssetProvider assetProvider, IAudioService audioService)
    {
      _audioService = audioService;
      _assetProvider = assetProvider;
    }
    
    public void CreateButton(Action onClick, Transform parent)
    {
      var prefab = _assetProvider.GetResource<GameObject>(HideButtonPath);
      Button = Object.Instantiate(prefab, parent).GetComponentInChildren<Button>();
      Button.onClick.AddListener(() => onClick?.Invoke());

      new ButtonSound(_audioService, Button.gameObject, 0);
      new ButtonAnimation(Button.transform);

      CanvasGroup = Button.GetComponent<CanvasGroup>();
      HandleClick = Button.gameObject.AddComponent<HandleClick>();
      RectTransform = Button.GetComponent<RectTransform>();
    }

    public void Clear()
    {
      if(Button)
        Object.Destroy(Button.transform.parent.gameObject);
    }
  }
}