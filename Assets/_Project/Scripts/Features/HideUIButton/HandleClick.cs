using System;
using UnityEngine;
using UnityEngine.UI;

namespace _Project.Scripts.Features.HideUIButton
{
  [RequireComponent(typeof(Button))]
  public class HandleClick : MonoBehaviour
  {
    private Button _button;
    public event Action OnClick;

    private void Start()
    {
      _button = GetComponent<Button>();
      _button.onClick.AddListener(Click);
    }

    private void OnDestroy()
    {
      if(_button != null)
        _button.onClick.RemoveListener(Click);
    }  

    private void Click() => 
      OnClick?.Invoke();
  }
}