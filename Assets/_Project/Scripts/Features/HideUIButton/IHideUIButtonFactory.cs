using System;
using UnityEngine;
using UnityEngine.UI;

namespace _Project.Scripts.Features.HideUIButton
{
  public interface IHideUIButtonFactory
  {
    Button Button { get; }
    CanvasGroup CanvasGroup { get; }
    HandleClick HandleClick { get; }
    RectTransform RectTransform { get; }
    void CreateButton(Action onClick, Transform parent);
    void Clear();
  }
}