using System;
using System.Collections.Generic;
using System.Linq;
using _Project.FMOD_AudioService.Logic;
using _Project.Scripts.Animations.UI;
using _Project.Scripts.Services.Localization.Logic;
using AppacheAnalytics.Scripts.Interfaces;
using Assets._Project.FMOD_AudioService.Logic;
using TMPro;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace _Project.Scripts.Features.Disable_Ads
{
  public class DisableAdsWindow : MonoBehaviour
  {
    private const float BtnScale = 1.1f;
    private const float BtnMaxDuration = 0.7f;

    [Header("Windows")] public RectTransform MainWindow;
    public RectTransform AfterShowingAdsWindow;

    [Header("Main")] public Button ShowAdsBtn;
    public Button NotShowAdsBtn;

    [FormerlySerializedAs("AdsCounterTxt")]
    public TextMeshProUGUI AdsAmountTxt;

    [Header("After showing ads")] public Button ContinueBtn;
    public TextMeshProUGUI TimerTxt;
    
    [Header("Canvas Groups")] 
    public CanvasGroup TopContainer;
    public CanvasGroup MiddleContainer;
    public CanvasGroup ButtonContainer;
    public CanvasGroup BottomContainer;
    
    [Header("Canvas Groups 2")] 
    public CanvasGroup TopContainer2;
    public CanvasGroup MiddleContainer2;
    public CanvasGroup ButtonContainer2;
    public CanvasGroup BottomContainer2;

    private Action _showAds;
    private Action _destroyWindow;
    private Action _showSplashInterstitials;
    private Action _continueAfterAds;
    
    private FadeCanvasGroup _fadeTopContainer;
    private FadeCanvasGroup _fadeMiddleContainer;
    private FadeCanvasGroup _fadeButtonContainer;
    private FadeCanvasGroup _fadeBottomContainer;
    
    private FadeCanvasGroup _fadeTopContainer2;
    private FadeCanvasGroup _fadeMiddleContainer2;
    private FadeCanvasGroup _fadeButtonContainer2;
    private FadeCanvasGroup _fadeBottomContainer2;
    
    private IAnalytics _analytics;
    private IAudioService _audioService;

    private void OnDestroy()
    {
      CleanupButtonsEvents();

      void CleanupButtonsEvents()
      {
        ShowAdsBtn.onClick.RemoveAllListeners();
        NotShowAdsBtn.onClick.RemoveAllListeners();
        ContinueBtn.onClick.RemoveAllListeners();
      }
    }

    public void Init(ILocaleSystem locale, IAudioService audioService, IAnalytics analytics,
      Action showAds, Action destroyWindow, Action continueAfterAds, Action showSplashInterstitial)
    {
      _analytics = analytics;
      _audioService = audioService;
      _showAds = showAds;
      _destroyWindow = destroyWindow;
      _continueAfterAds = continueAfterAds;
      _showSplashInterstitials = showSplashInterstitial;

      SetButtonsEvents();
      Localize();
      
      InitializeAnimations();
      InitializeSounds();
      
      void SetButtonsEvents()
      {
        ShowAdsBtn.onClick.AddListener(ShowAds);
        NotShowAdsBtn.onClick.AddListener(NotShowAds);
        ContinueBtn.onClick.AddListener(Continue);
      }

      void Localize()
      {
        List<LocalizedMainText> localizedTexts = transform.GetComponentsInChildren<LocalizedMainText>(true).ToList();
        localizedTexts.ForEach(item =>
        {
          item.Construct(locale);
          item.Awake();
        });
      }
    }

    public void SwitchWindows()
    {
      OpenThanks();
      
      MainWindow.gameObject.SetActive(false);
      AfterShowingAdsWindow.gameObject.SetActive(true);
    }

    public void SetShowedAdsAmount(string localizedBtnText, int showedAdsAmount, int showedAdsMaxAmount) =>
      AdsAmountTxt.text = $"{localizedBtnText} ({showedAdsAmount}/{showedAdsMaxAmount})";

    public void SetTimerInfo(int seconds)
    {
      TimeSpan timeSpan = TimeSpan.FromSeconds(seconds);
      TimerTxt.text = $"{(FormatValue((int)timeSpan.TotalHours))}:{FormatValue(timeSpan.Minutes)}:{FormatValue(timeSpan.Seconds)}";

      string FormatValue(int value) =>
        value < 10 ? "0" + value : value.ToString();
    }

    public void Open()
    {
      _fadeTopContainer.Appear(() => gameObject.SetActive(true));
      _fadeMiddleContainer.Appear();
      _fadeButtonContainer.Appear();
      _fadeBottomContainer.Appear();
    }

    private void OpenThanks()
    {
      _fadeTopContainer2.Appear(() => gameObject.SetActive(true));
      _fadeMiddleContainer2.Appear();
      _fadeButtonContainer2.Appear();
      _fadeBottomContainer2.Appear();
    }
    
    protected virtual void InitializeAnimations()
    {
      _fadeTopContainer = new FadeCanvasGroup(TopContainer);
      _fadeMiddleContainer = new FadeCanvasGroup(MiddleContainer, 0.25f);
      _fadeButtonContainer = new FadeCanvasGroup(ButtonContainer, 0.5f);
      _fadeBottomContainer = new FadeCanvasGroup(BottomContainer, 0.75f);
      
      _fadeTopContainer2 = new FadeCanvasGroup(TopContainer2);
      _fadeMiddleContainer2 = new FadeCanvasGroup(MiddleContainer2, 0.25f);
      _fadeButtonContainer2 = new FadeCanvasGroup(ButtonContainer2, 0.5f);
      _fadeBottomContainer2 = new FadeCanvasGroup(BottomContainer2, 0.75f);
      
      new ButtonAnimation(ShowAdsBtn.transform);
      new ButtonAnimation(NotShowAdsBtn.transform);
      new ButtonAnimation(ContinueBtn.transform);
    }

    protected virtual void InitializeSounds()
    {
      new ButtonSound(_audioService, ShowAdsBtn.gameObject, 17);
      new ButtonSound(_audioService, ContinueBtn.gameObject, 17);
      new ButtonSound(_audioService, NotShowAdsBtn.gameObject, 0);
    }
    
    private void ShowAds() =>
      _showAds?.Invoke();

    private void Continue()
    {
      _continueAfterAds?.Invoke();
      _destroyWindow?.Invoke();
    }

    private void NotShowAds()
    {
      _analytics.Send("btn_with_ads");
      _showSplashInterstitials?.Invoke();
      _destroyWindow?.Invoke();
    }
  }
}