using System;
using _Modules._Analytics;
using _Project.FMOD_AudioService.Logic;
using _Project.Scripts.Features.Stages.Interfaces;
using _Project.Scripts.Services.Localization.Logic;
using _Project.Scripts.Timer.Interfaces;
using AppacheAds.Scripts.Interfaces;
using AppacheAnalytics.Scripts.Interfaces;
using UnityEngine;
using Object = UnityEngine.Object;

namespace _Project.Scripts.Features.Disable_Ads
{
  public class DisableAdsLogic : ICallbackExecutor, IDisableAdsLogic
  {
    private const string TargetCanvasName = "Canvas";
    private const string WindowPath = "UI/Disable Ads Window";
    private const string TimerGuid_SaveKey = "DisableAdsWindow-TimerGuid_SaveKey";
    private const string ShowedAdsAmount_SaveKey = "DisableAdsWindow-ShowedAdsAmount_SaveKey";
    private const string AdsIsDisabled_SaveKey = "DisableAdsWindow-AdsIsDisabled_SaveKey";
    
    private const int ShowedAdsMaxAmount = 2;
    private const int TimeDelayHours = 24;
    
#if TEST
    private const int TimeDelayTestSec = 40;
#endif
    
    private readonly ILocaleSystem _locale;
    private readonly IAdsService _ads;
    private readonly ITimerController _timerController;
    
    private readonly IAnalytics _analytics;
    private readonly IAudioService _audioService;
    private readonly IStagesService _stagesService;

    private DisableAdsWindow _disableAdsWindow;

    private bool _isDisableAdsWindowShowed;
    
    private Action _nextTransition;
    private Action _onClose;

    private bool _calledThroughOnceCall;
    
    public DisableAdsLogic(ILocaleSystem locale, IAdsService ads, ITimerController timerController,
      IAudioService audioService, IStagesService stagesService, IAnalytics analytics)
    {
      _stagesService = stagesService;
      _analytics = analytics;
      _audioService = audioService;
      _locale = locale;
      _ads = ads;
      _timerController = timerController;
    }

    public void SwitchAds()
    {
      if (AdsIsDisabled())
        _ads.DisableAds();
      else
        _ads.EnableAds(true);
    }

    public void ShowOnceInSession(Action nextTransition)
    {
#if IOS_FIRST_RELEASE
      nextTransition?.Invoke();
      return;
#endif
      
      _calledThroughOnceCall = true;
      
      _nextTransition = nextTransition;

      if (AdsIsDisabled() || _isDisableAdsWindowShowed)
      {
        _nextTransition?.Invoke();
        return;
      }
      
      _isDisableAdsWindowShowed = true;
      _stagesService.HideStageCounterView();

      Activate();
    }

    public void ShowWindow(Action nextTransition)
    {    
#if IOS_FIRST_RELEASE
      nextTransition?.Invoke();
      return;
#endif
      _calledThroughOnceCall = false;

      _nextTransition = nextTransition;
      Activate();
    }
    
    private void Deactivate()
    {
      _timerController.CompleteTimer(GetTimerGuid());
      CleanupAdsAmount();
      SetAdsIsDisabled(false);
    }

    private void Activate()
    {
      if (_timerController.IsUnderTimer(GetTimerGuid()))
      {
        AddTimerCallback();
        SpawnTimerWindow();
        
        Debug.Log("[DisableAdsLogic] - IsUnderTimer()");
      }
      else
      {
        if (AdsIsDisabled())
          EnableAds();

        SpawnWindow();
        
        Debug.Log("[DisableAdsLogic] - !IsUnderTimer");
      }

      SetAdsIsDisabled(AdsIsDisabled());
    }

    private void SpawnWindow()
    {
      DestroyWindow();
      
      _disableAdsWindow = Object.Instantiate(Resources.Load<DisableAdsWindow>(WindowPath), GameObject.Find(TargetCanvasName).transform);
      _disableAdsWindow.Init(_locale, _audioService, _analytics ,ShowAds, DestroyWindow, _nextTransition, () =>
      {
        _nextTransition?.Invoke();
        
        if(_calledThroughOnceCall)
          _ads.CurrentInterstitial.ShowInterstitial(ScreenNames.Choose, null, null);
      });
      
      _disableAdsWindow.Open();

      SetShowedAdsAmount();
    }

    private void SpawnTimerWindow()
    {
      DestroyWindow();
      
      _disableAdsWindow = Object.Instantiate(Resources.Load<DisableAdsWindow>(WindowPath), GameObject.Find(TargetCanvasName).transform);
      _disableAdsWindow.Init(_locale, _audioService, _analytics, ShowAds, DestroyWindow, _nextTransition, _nextTransition);
      _disableAdsWindow.Open();
      
      _disableAdsWindow.SwitchWindows();
    }

    public string GetTimerGuid()
    {
      string guid = PlayerPrefs.GetString(TimerGuid_SaveKey, Guid.NewGuid().ToString());
      PlayerPrefs.SetString(TimerGuid_SaveKey, guid);

      return guid;
    }

    public void ExecuteWhenTick(int seconds)
    {
      if (_disableAdsWindow)
        _disableAdsWindow.SetTimerInfo(seconds);

#if UNITY_EDITOR
      Debug.Log("[DisableAdsLogic] - ExecuteWhenTick(): " + seconds);
#endif
    }

    public void ExecuteWhenComplete() =>
      EnableAds();

    public void SetLock(bool isLocked)
    {}

    public void Destroy()
    {}

    public bool AdsIsDisabled() =>
      PlayerPrefs.GetInt(AdsIsDisabled_SaveKey, 0) != 0;

    private void SetAdsIsDisabled(bool value)
    {
      PlayerPrefs.SetInt(AdsIsDisabled_SaveKey, !value ? 0 : 1);
      
      if(AdsIsDisabled())
        _ads.DisableAds();
      else
        _ads.EnableAds(true);
    }

    private void ShowAds() => 
      _ads.Rewarded.ShowReward("disable-ads-24h", AdsCallback, _ads.Rewarded.RequestRewardedVideo);

    private void AdsCallback()
    {
      AddShowedAdsToAmount();
      Debug.Log("[DisableAdsLogic] - AdsCallback() - ShowedAds:" + GetShowedAdsAmount());

      if (AdsAmountIsMaxValue())
        DisableAds();
      else
        SetShowedAdsAmount();
    }

    private void SetShowedAdsAmount()
    {
      int showedAdsAmount = GetShowedAdsAmount();
      string localizedBtnText = _locale.GetText("Main","NoAds.Watch");

      _disableAdsWindow?.SetShowedAdsAmount(localizedBtnText, showedAdsAmount, ShowedAdsMaxAmount);
    }

    private bool AdsAmountIsMaxValue() =>
      GetShowedAdsAmount() == ShowedAdsMaxAmount;

    private int GetShowedAdsAmount() =>
      PlayerPrefs.GetInt(ShowedAdsAmount_SaveKey, 0);

    private void CleanupAdsAmount() =>
      PlayerPrefs.DeleteKey(ShowedAdsAmount_SaveKey);

    private void AddShowedAdsToAmount()
    {
      int amount = GetShowedAdsAmount();
      amount = Mathf.Clamp(amount + 1, 0, ShowedAdsMaxAmount);
      PlayerPrefs.SetInt(ShowedAdsAmount_SaveKey, amount);
    }

    private void DisableAds()
    {
      SetAdsIsDisabled(true);

      _disableAdsWindow.SwitchWindows();
      LaunchTimer();

      Debug.Log("[DisableAdsLogic] - DisableAds()");
    }

    private void EnableAds()
    {
      SetAdsIsDisabled(false);

      CleanupAdsAmount();
      DestroyWindow();
      
      Debug.Log("[DisableAdsLogic] - EnableAds()");
    }

    private void LaunchTimer()
    {
#if TEST
      _timerController.CreateTimer(GetTimerGuid(), _timerController.TimeProvider.GetCurrentTime().AddSeconds(TimeDelayTestSec));
#else
      _timerController.CreateTimer(GetTimerGuid(), _timerController.TimeProvider.GetCurrentTime().AddHours(TimeDelayHours));
#endif

      AddTimerCallback();

      Debug.Log("[DisableAdsLogic] - LaunchTimer()");
    }

    private void AddTimerCallback() =>
      _timerController.AddCallback(this);

    private void DestroyWindow()
    {
      if (_disableAdsWindow)
        Object.Destroy(_disableAdsWindow.gameObject);
    }
  }
}