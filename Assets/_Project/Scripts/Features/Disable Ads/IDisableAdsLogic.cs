using System;

namespace _Project.Scripts.Features.Disable_Ads
{
  public interface IDisableAdsLogic
  {
    void ShowOnceInSession(Action nextTransition);
    void SwitchAds();
    void ShowWindow(Action nextTransition);
    bool AdsIsDisabled();
  }
}