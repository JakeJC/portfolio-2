﻿using System;
using System.Collections;
using _Project.Scripts.Services.Localization.Logic;
using DG.Tweening;
using UnityEngine;

namespace _Project.Scripts.Features.Splashscreen
{
  public class Splash : MonoBehaviour
  {
    private const float SplashTime = 5f;
    private const float AdditionalContentTime = 1f;
    private const float WaitForFetchRemoteTime = 2f;

    public CanvasGroup LogoGroup;
    public Transform LogoTransform;
    private Action _onEnd;
    public CanvasGroup AdditionalContentGroup;

    private bool _isComplete;
    private bool _isAnimationComplete;
    private bool _isFetchComplete;

    public void Construct(Action onEnd, ILocaleSystem localeSystem)
    {
      _onEnd = onEnd;

      Localize(localeSystem);
    }

    public void Complete()
    {
      _isComplete = true;

      if (_isAnimationComplete)
        _onEnd?.Invoke();
    }

    public void Launch()
    {
      LogoTransform.localScale = 0.7f * Vector3.one;
      LogoGroup.alpha = 0;

      AdditionalContentGroup.alpha = 0;

      StartCoroutine(DelayedLaunch());
    }

    public void OnFetchRemote()
    {
      _isFetchComplete = true;
      AdditionalContentGroup.DOFade(1, AdditionalContentTime);
    }

    private IEnumerator DelayedLaunch()
    {
      float timer = 0;
      
      while (_isFetchComplete == false && timer < WaitForFetchRemoteTime)
      {
        timer += Time.deltaTime;
        yield return null;
      }
      
      LaunchAnimation();
    }

    private void LaunchAnimation()
    {
      LogoGroup.DOFade(1, SplashTime);
      LogoTransform.DOScale(1, SplashTime).onComplete += () =>
      {
        _isAnimationComplete = true;

        if (_isComplete)
          _onEnd?.Invoke();
      };
    }

    private void Localize(ILocaleSystem localeSystem)
    {
      LocalizedMainText localizedMainText = AdditionalContentGroup.GetComponentInChildren<LocalizedMainText>();
      localizedMainText.Construct(localeSystem);
      localizedMainText.Awake();
    }
  }
}