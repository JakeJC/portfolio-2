using System;
using _Project.Scripts.Services._Interfaces;
using _Project.Scripts.Services.Localization.Logic;
using AppacheRemote.Scripts.Interfaces;

namespace _Project.Scripts.Features.Splashscreen
{
  public class SplashLogic
  {
    private readonly IRemote _remote;
    private readonly ILocaleSystem _localeSystem;
    private readonly ISplashFactory _splashFactory;
    private readonly Splash _splash;

    public SplashLogic(IAssetProvider assetProvider, IRemote remote, ILocaleSystem localeSystem)
    {
      _remote = remote;
      _localeSystem = localeSystem;
      _splashFactory = new SplashFactory(assetProvider);
      _splash = _splashFactory.CreateSplash();
    }

    public void Launch(Action onEnd)
    {
      _splash.Construct(() =>
        {
          onEnd?.Invoke();
          Clear();
        }, _localeSystem);

      _splash.Launch();
    }

    public void OnFetchRemote()
    {
      
    }

    public void Complete() =>
      _splash.Complete();

    private void Clear() =>
      _splashFactory.Clear();
  }
}