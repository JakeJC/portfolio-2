using System;
using System.Collections.Generic;
using System.Linq;
using _Project.FMOD_AudioService.Logic;
using _Project.Scripts.Animations.UI;
using _Project.Scripts.Services.Localization.Logic;
using Assets._Project.FMOD_AudioService.Logic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace _Project.Scripts.Features.Open_Ad_Panels_Windows
{
  public class ColorAdWindow : MonoBehaviour
  {
    [Header("Buttons")]

    [SerializeField] private Button _watchAdButton;
    [SerializeField] private Button _noButton;
    
    [Header("Canvas Group")]

    [SerializeField] private CanvasGroup TopCanvasGroup;
    [SerializeField] private CanvasGroup MiddleCanvasGroup;
    [SerializeField] private CanvasGroup BottomCanvasGroup;
    [SerializeField] private CanvasGroup ButtonCanvasGroup;
    
    private FadeCanvasGroup _fadeTopCanvasGroup;
    private FadeCanvasGroup _fadeMiddleCanvasGroup;
    private FadeCanvasGroup _fadeBottomCanvasGroup;
    private FadeCanvasGroup _fadeButtonCanvasGroup;
    
    private UnityAction _onNo;
    
    private IAudioService _audioService;

    public void Construct(ILocaleSystem localeSystem, IAudioService audioService, UnityAction onWatch, UnityAction onNo)
    {
      _audioService = audioService;
      _onNo = onNo;
      
      _watchAdButton.onClick.AddListener(onWatch.Invoke);
      _noButton.onClick.AddListener(onNo.Invoke);
      
      InitializeAnimations();
      Localize(localeSystem);
      
      gameObject.SetActive(false);
    }
    
    public void Open()
    {  
      gameObject.SetActive(true);
      AppearAnimation();
    }

    private void Close() => 
      DisappearAnimation();
    
    private void InitializeAnimations()
    {
      _fadeTopCanvasGroup = new FadeCanvasGroup(TopCanvasGroup, 0, 1);
      _fadeMiddleCanvasGroup = new FadeCanvasGroup(MiddleCanvasGroup, 0.25f, 1);
      _fadeBottomCanvasGroup = new FadeCanvasGroup(BottomCanvasGroup, 0.5f, 1);
      _fadeButtonCanvasGroup = new FadeCanvasGroup(ButtonCanvasGroup, 0.75f, 1);

      new ButtonSound(_audioService, _watchAdButton.gameObject, 13);
      new ButtonSound(_audioService, _noButton.gameObject, 12);
      
      new ButtonAnimation(_watchAdButton.transform);
      new ButtonAnimation(_noButton.transform);
    }
    
    private void AppearAnimation()
    {
      _fadeTopCanvasGroup.Appear();
      _fadeMiddleCanvasGroup.Appear();
      _fadeBottomCanvasGroup.Appear();
      _fadeButtonCanvasGroup.Appear();

      Canvas.ForceUpdateCanvases();
    }

    private void DisappearAnimation()
    {
      _fadeTopCanvasGroup.Disappear();
      _fadeMiddleCanvasGroup.Disappear();
      _fadeBottomCanvasGroup.Disappear();
      _fadeButtonCanvasGroup.Disappear(_onNo.Invoke);
    }
    
    private void Localize(ILocaleSystem localeSystem)
    {
      List<LocalizedMainText> localizedTexts = transform.GetComponentsInChildren<LocalizedMainText>(true).ToList();
      localizedTexts.ForEach(item =>
      {
        item.Construct(localeSystem);
        item.Awake();
      });
    }
  }
}