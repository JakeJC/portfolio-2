using System;
using _Project.FMOD_AudioService.Logic;
using _Project.Scripts.Services._Interfaces;
using _Project.Scripts.Services.Localization.Logic;
using AppacheAds.Scripts.Interfaces;
using UnityEngine;

namespace _Project.Scripts.Features.Open_Ad_Panels_Windows
{
  public class AdOpenLogic
  {
    private readonly IOpenAdPanelsWindowsFactory _openAdPanelsWindowsFactory;
    private readonly IAdsService _adsService;

    private readonly Action _onUnlockColor;
    private readonly Action _onUnlockMaterial;

    private readonly ILocaleSystem _localeSystem;
    private readonly IAudioService _audioService;

    public bool IsOpenColor
    {
      get => PlayerPrefs.GetInt("AdOpenLogic_unlock_color", 0) == 1;
      set => PlayerPrefs.SetInt("AdOpenLogic_unlock_color", value ? 1 : 0);
    }

    public bool IsOpenMaterial
    {
      get => PlayerPrefs.GetInt("AdOpenLogic_unlock_material", 0) == 1;
      set => PlayerPrefs.SetInt("AdOpenLogic_unlock_material", value ? 1 : 0);
    }

    public AdOpenLogic(ILocaleSystem localeSystem, IAudioService audioService, IAdsService adsService, IAssetProvider assetProvider, Action onUnlockColor, Action onUnlockMaterial)
    {
      _audioService = audioService;
      _localeSystem = localeSystem;

      _onUnlockMaterial = onUnlockMaterial;
      _onUnlockColor = onUnlockColor;

      _adsService = adsService;

      _openAdPanelsWindowsFactory = new OpenAdPanelsWindowsFactory(assetProvider);
    }

    public void OpenColorWindow()
    {
      _openAdPanelsWindowsFactory.SpawnColorWindow();
      _openAdPanelsWindowsFactory.ColorAdWindow.Construct(_localeSystem, _audioService, OnWatch, OnSkip);
      _openAdPanelsWindowsFactory.ColorAdWindow.Open();

      void OnWatch()
      {
        _adsService.Rewarded.ShowReward("unlock_color", () =>
        {
          _onUnlockColor?.Invoke();
          _openAdPanelsWindowsFactory.ClearColorWindow();

          IsOpenColor = true;
        });
      }
      
      void OnSkip()
      {
        _adsService.Rewarded.RequestRewardedVideo();
        _openAdPanelsWindowsFactory.ClearColorWindow();
      }
    }

    public void OpenMaterialWindow()
    {
      _openAdPanelsWindowsFactory.SpawnMaterialWindow();
      _openAdPanelsWindowsFactory.MaterialAdWindow.Construct(_localeSystem, _audioService, OnReward, OnSkip);

      _openAdPanelsWindowsFactory.MaterialAdWindow.Open();

      void OnReward()
      {
        _adsService.Rewarded.ShowReward("unlock_material", () =>
        {
          _onUnlockMaterial?.Invoke();
          _openAdPanelsWindowsFactory.ClearMaterialWindow();

          IsOpenMaterial = true;
        });
      }

      void OnSkip()
      {
        _adsService.Rewarded.RequestRewardedVideo();
        _openAdPanelsWindowsFactory.ClearMaterialWindow();
      }
    }
  }
}