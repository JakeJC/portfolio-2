using _Project.Scripts.Services._Interfaces;
using UnityEngine;

namespace _Project.Scripts.Features.Open_Ad_Panels_Windows
{
  public class OpenAdPanelsWindowsFactory : IOpenAdPanelsWindowsFactory
  {
    private const string AdMaterialWindowPath = "UI/Open Ad Windows/Material Window";
    private const string AdColorWindowPath = "UI/Open Ad Windows/Color Window";
   
    private const string BlurredCanvas = "Canvas";
    
    private readonly IAssetProvider _assetProvider;

    public MaterialAdWindow MaterialAdWindow { get; set; }
    public ColorAdWindow ColorAdWindow { get; set; }

    public OpenAdPanelsWindowsFactory(IAssetProvider assetProvider)
    {
      _assetProvider = assetProvider;
    }
    
    public void SpawnColorWindow()
    {
      ClearColorWindow();
      
      Transform parent = GameObject.Find(BlurredCanvas).transform;

      var window = _assetProvider.GetResource<ColorAdWindow>(AdColorWindowPath);
      ColorAdWindow = Object.Instantiate(window, parent);
    }

    public void SpawnMaterialWindow()
    {
      ClearMaterialWindow();
      
      Transform parent = GameObject.Find(BlurredCanvas).transform;

      var window = _assetProvider.GetResource<MaterialAdWindow>(AdMaterialWindowPath);
      MaterialAdWindow = Object.Instantiate(window, parent);
    }

    public void ClearColorWindow()
    {
      if (ColorAdWindow != null)
        Object.Destroy(ColorAdWindow.gameObject);
    }

    public void ClearMaterialWindow()
    {
      if (MaterialAdWindow != null)
        Object.Destroy(MaterialAdWindow.gameObject);
    }
  }
}