namespace _Project.Scripts.Features.Open_Ad_Panels_Windows
{
  public interface IOpenAdPanelsWindowsFactory
  {
    MaterialAdWindow MaterialAdWindow { get; set; }
    ColorAdWindow ColorAdWindow { get; set; }
    void SpawnColorWindow();
    void SpawnMaterialWindow();
    void ClearColorWindow();
    void ClearMaterialWindow();
  }
}