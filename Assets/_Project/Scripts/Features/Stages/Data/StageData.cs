using UnityEngine;

namespace _Project.Scripts.Features.Stages.Data
{
  [CreateAssetMenu(fileName = "Stage Data", menuName = "Stages/Create Stage Data", order = 0)]
  public class StageData : ScriptableObject
  {
    public Stages Stage;
    
    public string TitleKey;
    public string DescriptionKey;
    public Sprite StageIcon;
  }
}