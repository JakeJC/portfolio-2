using System;
using _Project.FMOD_AudioService.Logic;
using _Project.Scripts.Features.Stages.Data;
using _Project.Scripts.Features.Stages.Interfaces;
using _Project.Scripts.Features.Stages.Mono;
using _Project.Scripts.Services._Interfaces;
using _Project.Scripts.Services.Localization.Logic;
using AppacheAnalytics.Scripts.Interfaces;
using UnityEngine;
using Object = UnityEngine.Object;

namespace _Project.Scripts.Features.Stages
{
  public class StagesFactory : IStagesFactory
  {
    private const string StageDescriptionWindowPath = "UI/Stages/Stage Description Window";
    private const string ConstructionWindowPath = "UI/Stages/Construction Window";
    private const string ResultDescriptionWindowPath = "UI/Stages/Result Description Window";
    
    private const string StageCounterViewPath = "UI/Stages/Stage Counter";

    private const string CanvasId = "Canvas";

    private readonly IAssetProvider _assetProvider;
    private readonly ILocaleSystem _localeSystem;
    private readonly IAudioService _audioService;

    private StageDescriptionWindow _descriptionWindow;
    public StageCounterView CounterView { get; set; }
    
    public StagesFactory(IAssetProvider assetProvider, ILocaleSystem localeSystem, IAudioService audioService)
    {
      _audioService = audioService;
      _localeSystem = localeSystem;
      _assetProvider = assetProvider;
    }

    public void CreateDescriptionWindow(StageData data, Action onEnd, IAnalytics analytics = null)
    {
      Clear(_descriptionWindow);

      string windowPath = StageDescriptionWindowPath;
      
      if (data.Stage == Stages.Construction)
        windowPath = ConstructionWindowPath;
      
      if (data.Stage == Stages.Advice)
        windowPath = ResultDescriptionWindowPath;
      
      _descriptionWindow = CreateWidget<StageDescriptionWindow>(windowPath);
      _descriptionWindow.Construct(_localeSystem, _audioService, data, () => { _descriptionWindow.CloseWindow(); }, () =>
      {
        if (data.Stage == Stages.Interview)
          analytics.Send("btn_stage_one");
        
        onEnd?.Invoke();
        Clear(_descriptionWindow);
      });

      _descriptionWindow.OpenWindow();
    }

    public void CreateStageCounterView(int stageNumber, int textColorIndex)
    {
      ClearStageCounterView();

      CounterView = CreateWidget<StageCounterView>(StageCounterViewPath);
      CounterView.Construct(_localeSystem, stageNumber, textColorIndex);

      CounterView.transform.SetAsLastSibling();

      CounterView.Appear();
    }

    private T CreateWidget<T>(string path) where T : MonoBehaviour
    {
      var prefab = _assetProvider.GetResource<GameObject>(path);
      var parent = GameObject.Find(CanvasId).transform;

      return Object.Instantiate(prefab, parent).GetComponent<T>();
    }

    public void ClearStageCounterView() =>
      Clear(CounterView);

    private void Clear(MonoBehaviour view)
    {
      if (view != null)
        Object.Destroy(view.gameObject);
    }
  }
}