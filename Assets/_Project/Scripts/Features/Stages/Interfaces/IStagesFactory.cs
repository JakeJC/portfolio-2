using System;
using _Project.Scripts.Features.Stages.Data;
using _Project.Scripts.Features.Stages.Mono;
using AppacheAnalytics.Scripts.Interfaces;

namespace _Project.Scripts.Features.Stages.Interfaces
{
  public interface IStagesFactory
  {
    void CreateDescriptionWindow(StageData getDataForStage, Action onEnd, IAnalytics analytics = null);
    void CreateStageCounterView(int stageNumber, int maxCounter);
    void ClearStageCounterView();
    StageCounterView CounterView { get; set; }
  }
}