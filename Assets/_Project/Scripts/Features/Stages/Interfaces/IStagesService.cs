using System;
using _Project.Scripts.Service_Base;
using AppacheAnalytics.Scripts.Interfaces;

namespace _Project.Scripts.Features.Stages.Interfaces
{
  public interface IStagesService : IService
  {
    bool IsActive { get; }
    void OpenStageDescription(Stages stages, Action onEnd, int textColorIndex, IAnalytics analytics = null);
    void OpenStageDescriptionAfterTutorial(Action onEnd, IAnalytics analytics = null);
    void CompleteStage();
    bool IsStageAlreadyComplete(Stages stages);
    void ShowStageCounterView(Stages currentStage, int textColorIndex);
    void HideStageCounterView();
    void ShowHideCounter(bool isShown);
  }
}