using System;
using System.Collections.Generic;
using _Project.Scripts.Services.Localization.Logic;
using DG.Tweening;
using TMPro;
using UnityEngine;

namespace _Project.Scripts.Features.Stages.Mono
{
  public class StageCounterView : MonoBehaviour
  {
    [SerializeField] private TextMeshProUGUI counter;

    [SerializeField] private List<Color> _colors;

    private ILocaleSystem _localeSystem;

    public void Construct(ILocaleSystem localeSystem, int count, int textColorIndex)
    {
      _localeSystem = localeSystem;
      SetStageCount(count, textColorIndex);
    }

    private void SetStageCount(int count, int textColorIndex)
    {
      counter.text = $"{_localeSystem.GetText("Main", "Stages.Number")} {count}";
      counter.color = _colors[textColorIndex];
    }

    public void Appear()
    {
      counter.alpha = 0;
      counter.DOFade(1, 0.5f).SetDelay(0.5f);
    } 
    
    public void ShowHideView(bool isShown) => 
      counter.DOFade(isShown? 1 : 0, 0.1f);
  }
}