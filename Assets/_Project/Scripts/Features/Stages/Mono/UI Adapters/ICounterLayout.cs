using _Project.Scripts.Features.Stages.Interfaces;

namespace _Project.Scripts.Features.Stages.Mono.UI_Adapters
{
  public interface ICounterLayout
  {
    void Construct(IStagesService stagesService);
  }
}