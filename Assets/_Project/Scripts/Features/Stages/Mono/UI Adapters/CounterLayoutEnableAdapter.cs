using _Project.Scripts.Features.Stages.Interfaces;
using UnityEngine;
using Zenject;

namespace _Project.Scripts.Features.Stages.Mono.UI_Adapters
{
  public class CounterLayoutEnableAdapter : MonoBehaviour, ICounterLayout
  {
    [Inject]
    public void Construct(IStagesService stagesService)
    {
      if (stagesService.IsActive) 
        ToShiftState();
    }

    private void ToShiftState() => 
      gameObject.SetActive(false);

    public void ToBaseState() => 
      gameObject.SetActive(true);
  }
}