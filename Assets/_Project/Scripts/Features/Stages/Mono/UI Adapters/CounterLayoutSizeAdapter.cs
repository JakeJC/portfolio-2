using _Project.Scripts.Features.Stages.Interfaces;
using UnityEngine;
using Zenject;

namespace _Project.Scripts.Features.Stages.Mono.UI_Adapters
{  
  [RequireComponent(typeof(RectTransform))]
  public class CounterLayoutSizeAdapter : MonoBehaviour, ICounterLayout
  {
    private RectTransform _rectTransform;
    private Vector2 _rectBaseSize;

    public void Construct(IStagesService stagesService)
    {
      _rectTransform = GetComponent<RectTransform>();
      _rectBaseSize = _rectTransform.sizeDelta;
      
      if (stagesService.IsActive) 
        ToShiftState();
    }

    private void ToShiftState()
    {
      _rectTransform.anchorMax = new Vector2(1, 0.95f);
      _rectTransform.sizeDelta = Vector2.zero;
    }

    public void ToBaseState()
    {
      _rectTransform.anchorMax = Vector2.one;
      _rectTransform.sizeDelta = _rectBaseSize;
    }
  }
}