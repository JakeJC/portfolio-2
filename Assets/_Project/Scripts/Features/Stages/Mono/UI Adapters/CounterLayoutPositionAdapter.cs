using _Project.Scripts.Features.Stages.Interfaces;
using UnityEngine;
using Zenject;

namespace _Project.Scripts.Features.Stages.Mono.UI_Adapters
{
  [RequireComponent(typeof(RectTransform))]
  public class CounterLayoutPositionAdapter : MonoBehaviour, ICounterLayout
  {
    private RectTransform _rectTransform;
    private Vector2 _rectBasePosition;

    [Inject]
    public void Construct(IStagesService stagesService)
    {
      _rectTransform = GetComponent<RectTransform>();
      _rectBasePosition = _rectTransform.anchoredPosition;
      
      if (stagesService.IsActive) 
        ToShiftState();
    }

    private void ToShiftState() => 
      _rectTransform.anchoredPosition += 75 * Vector2.down;

    public void ToBaseState() => 
      _rectTransform.anchoredPosition = _rectBasePosition;
  }
}