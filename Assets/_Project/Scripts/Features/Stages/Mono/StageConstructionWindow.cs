using System.Collections.Generic;
using System.Linq;
using _Project.Scripts.Features.Stages.Data;
using _Project.Scripts.Services.Localization.Logic;

namespace _Project.Scripts.Features.Stages.Mono
{
  public class StageConstructionWindow : StageDescriptionWindow
  {
    protected override void SetVisualInformation(StageData stage)
    {
      title.text = _localeSystem.GetText(LocaleName, stage.TitleKey);
      description.text = _localeSystem.GetText(LocaleName, stage.DescriptionKey);
      icon.sprite = stage.StageIcon;
    }

    protected override void Localize(ILocaleSystem localeSystem)
    {
      base.Localize(localeSystem);
      List<LocalizedTutorialText> localizedTexts = transform.GetComponentsInChildren<LocalizedTutorialText>(true).ToList();
      localizedTexts.ForEach(item =>
      {
        item.Construct(localeSystem);
        item.Awake();
      });
    }
  }
}