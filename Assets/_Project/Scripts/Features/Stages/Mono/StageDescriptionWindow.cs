using System;
using System.Collections.Generic;
using System.Linq;
using _Project.FMOD_AudioService.Logic;
using _Project.Scripts.Animations.UI;
using _Project.Scripts.Features.Stages.Data;
using _Project.Scripts.Services.Localization.Logic;
using Assets._Project.FMOD_AudioService.Logic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace _Project.Scripts.Features.Stages.Mono
{
  public class StageDescriptionWindow : MonoBehaviour
  {
    protected const string LocaleName = "Main";

    protected ILocaleSystem _localeSystem;
    
    [Header("Buttons")]

    [SerializeField]
    protected TextMeshProUGUI title;
    [SerializeField] protected TextMeshProUGUI description;
    [SerializeField] protected Image icon;
    [SerializeField] private Button next;
    
    [Header("Animation Components")]

    [SerializeField] private CanvasGroup TopGroup;
    [SerializeField] private CanvasGroup MiddleGroup1;
    [SerializeField] private CanvasGroup MiddleGroup2;
    [SerializeField] private CanvasGroup BottomGroup;

    private FadeCanvasGroup _fadeTop;
    private FadeCanvasGroup _fadeMiddle1;
    private FadeCanvasGroup _fadeMiddle2;
    private FadeCanvasGroup _fadeBottom;
    
    private Action _onClose;

    public void Construct(ILocaleSystem localeSystem, IAudioService audioService, StageData stage, Action onEnd, Action onClose)
    {
      _onClose = onClose;
      _localeSystem = localeSystem;
      
      SetVisualInformation(stage);
      AssignListeners(onEnd);
      
      AssignAnimations();
      AssignSounds(audioService);  
      AssignButtonsAnimations();    
      
      Localize(localeSystem);
    }
    
    public void OpenWindow()
    {
      gameObject.SetActive(true);
      AppearAnimation();
      
      Canvas.ForceUpdateCanvases();
    }

    public void CloseWindow()
    {
      _fadeBottom.Disappear(null, () =>
      {
        _onClose?.Invoke();
        gameObject.SetActive(false);
      });

      DisappearAnimation();
    }

    protected virtual void SetVisualInformation(StageData stage)
    {
      if(title)
        title.text = _localeSystem.GetText(LocaleName, stage.TitleKey);
      
      if(description) 
        description.text = _localeSystem.GetText(LocaleName, stage.DescriptionKey);

      if (!icon)
        return;
      
      icon.sprite = stage.StageIcon;
      icon.SetNativeSize();
    }

    protected virtual void Localize(ILocaleSystem localeSystem)
    {
      List<LocalizedMainText> localizedTexts = transform.GetComponentsInChildren<LocalizedMainText>(true).ToList();
      localizedTexts.ForEach(item =>
      {
        item.Construct(localeSystem);
        item.Awake();
      });
    }

    private void AssignSounds(IAudioService audioService) => 
      new ButtonSound(audioService, next.gameObject, 13);

    private void AssignButtonsAnimations() => 
      new ButtonAnimation(next.transform);

    private void AssignListeners(Action onEnd) => 
      next.onClick.AddListener(onEnd.Invoke);

    private void AssignAnimations()
    {
      _fadeTop = new FadeCanvasGroup(TopGroup, 1, .5f);
      _fadeMiddle1 = new FadeCanvasGroup(MiddleGroup1, 1.25f, .5f);
      _fadeMiddle2 = new FadeCanvasGroup(MiddleGroup2, 1.5f, .5f);
      _fadeBottom = new FadeCanvasGroup(BottomGroup, 1.75f, .5f);
    }

    private void AppearAnimation()
    {
      _fadeTop.Appear();
      _fadeMiddle1.Appear();
      _fadeMiddle2.Appear();
      _fadeBottom.Appear();
    }

    private void DisappearAnimation()
    {
      _fadeTop.Disappear();
      _fadeMiddle1.Disappear();
      _fadeMiddle2.Disappear();
    }
  }
}