using System;
using System.Collections.Generic;
using System.Linq;
using _Project.FMOD_AudioService.Logic;
using _Project.Scripts.Features.Stages.Data;
using _Project.Scripts.Features.Stages.Interfaces;
using _Project.Scripts.Services._Interfaces;
using _Project.Scripts.Services.Localization.Logic;
using AppacheAnalytics.Scripts.Interfaces;
using AppacheRemote.Scripts.Interfaces;

namespace _Project.Scripts.Features.Stages
{
  public class StagesService : IStagesService
  {
    private const string StagesDataPath = "_Stages Data";

    private readonly IAssetProvider _assetProvider;
    private readonly ILocaleSystem _localeSystem;
    private readonly IAudioService _audioService;
    private readonly IGameSaveSystem _gameSaveSystem;

    private StagesFactory _stagesFactory;
    private StageData[] _stagesData;

    private Stages _currentStage;
    private readonly IRemote _remote;

    public bool IsActive { get; private set; }

    public StagesService(IAssetProvider assetProvider, ILocaleSystem localeSystem, IAudioService audioService,
      IGameSaveSystem gameSaveSystem, IRemote remote)
    {
      _remote = remote;
      _gameSaveSystem = gameSaveSystem;
      _audioService = audioService;
      _localeSystem = localeSystem;
      _assetProvider = assetProvider;

      Initialize();
    }

    private void Initialize()
    {
      _stagesFactory = new StagesFactory(_assetProvider, _localeSystem, _audioService);
      _stagesData = _assetProvider.GetAllResources<StageData>(StagesDataPath);
    }

    public void OpenStageDescriptionAfterTutorial(Action onEnd, IAnalytics analytics)
    {
      _stagesFactory.CreateDescriptionWindow(GetDataForStage(Stages.Interview), () => { onEnd?.Invoke(); }, analytics);

      StageData GetDataForStage(Stages stage) =>
        _stagesData.FirstOrDefault(p => p.Stage == stage);
    }

    public void OpenStageDescription(Stages stages, Action onEnd, int textColorIndex, IAnalytics analytics = null)
    {
      _currentStage = stages;

      IsActive = true;

      if (IsStageAlreadyComplete(stages))
      {
        onEnd?.Invoke();
        return;
      }

      _stagesFactory.CreateDescriptionWindow(GetDataForStage(stages), () => { onEnd?.Invoke(); }, analytics);

      StageData GetDataForStage(Stages stage) =>
        _stagesData.FirstOrDefault(p => p.Stage == stage);
    }

    public void CompleteStage()
    {
      IsActive = false;

      if (IsStageAlreadyComplete(_currentStage))
        return;

      _gameSaveSystem.Get().showedStages.Add(_currentStage.ToString());
      _gameSaveSystem.Save();
    }

    public bool IsStageAlreadyComplete(Stages stages)
    {
      List<string> showedStages = _gameSaveSystem.Get().showedStages;
      return showedStages.Contains(stages.ToString());
    }

    public void ShowStageCounterView(Stages currentStage, int textColorIndex)
    {
      int stageNumber = (int) currentStage + 1;
      _stagesFactory.CreateStageCounterView(stageNumber, textColorIndex);
    }

    public void HideStageCounterView() =>
      _stagesFactory.ClearStageCounterView();

    public void ShowHideCounter(bool isShown)
    {
      if (_stagesFactory.CounterView != null)
        _stagesFactory.CounterView.ShowHideView(isShown);
    }
  }

  public enum Stages
  {
    Interview,
    Construction,
    Advice
  }
}