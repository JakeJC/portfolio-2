using System;
using _Project.Scripts.Features.Meditation.Scripts.Scriptables;
using UnityEngine;

namespace _Project.Scripts.Features.Meditation.Scripts.Mono
{
  public class TrackTimeline : MonoBehaviour
  {
    private float _length;
    private float _playTime;

    private bool _isPlaying;

    private Action<float> _onChange;
    
    private MeditationTrackData _meditationTrackData;

    public void PlayTrack(MeditationTrackData meditationTrackData)
    {
      if (_meditationTrackData == meditationTrackData)
      {
        Resume();
        return;
      }
        
      _meditationTrackData = meditationTrackData;
      _playTime = 0;
      _length = meditationTrackData.Length;
    }

    public int GetTrackId() => 
      _meditationTrackData.TrackId;

    public void SetAction(Action<float> onChange) => 
      _onChange = onChange;

    public void Pause() => 
      _isPlaying = false;

    private void Resume() => 
      _isPlaying = true;
    
    private void Update()
    {
      if (!_isPlaying) 
        return;
      
      _playTime += Time.deltaTime;

      if (_playTime > _length)
        _playTime = 0;
        
      _onChange?.Invoke(_playTime / _length);
    }
  }
}