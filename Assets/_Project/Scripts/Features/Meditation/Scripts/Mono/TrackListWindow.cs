using System;
using System.Collections.Generic;
using System.Linq;
using _Project.FMOD_AudioService.Logic;
using _Project.Scripts.Animations.UI;
using _Project.Scripts.Services.Localization.Logic;
using Assets._Project.FMOD_AudioService.Logic;
using UnityEngine;
using UnityEngine.UI;

namespace _Project.Scripts.Features.Meditation.Scripts.Mono
{
  public class TrackListWindow : MonoBehaviour
  {
    public Button BackButton; 
    public RectTransform Content; 
    
    public List<Track> Tracks = new List<Track>();

    [Header("Animation Components")]

    [SerializeField] private CanvasGroup TopGroup;
    [SerializeField] private CanvasGroup MiddleGroup;
    [SerializeField] private CanvasGroup BottomGroup;

    private FadeCanvasGroup _fadeTop;
    private FadeCanvasGroup _fadeMiddle;
    private FadeCanvasGroup _fadeBottom;
    
    private Action _onBackPressed;

    public void Construct(IAudioService audioService, ILocaleSystem localeSystem, Action onBackPressed)
    {
      _onBackPressed = onBackPressed;
      
      Localize(localeSystem);
      BackButton.onClick.AddListener(CloseWindow);

      AssignSounds(audioService);
      AssignButtonsAnimations();

      AssignAnimations();
    }

    public void OpenWindow()
    {
      gameObject.SetActive(true);
      AppearAnimation();
      
      Canvas.ForceUpdateCanvases();
    }

    public void CloseWindow()
    {
      _fadeBottom.Disappear(null, () =>
      {
        _onBackPressed?.Invoke();
        gameObject.SetActive(false);
      });

      DisappearAnimation();
    }
    
    protected virtual void Localize(ILocaleSystem localeSystem)
    {
      List<LocalizedMainText> localizedTexts = transform.GetComponentsInChildren<LocalizedMainText>(true).ToList();
      localizedTexts.ForEach(item =>
      {
        item.Construct(localeSystem);
        item.Awake();
      });
    }
    
    private void AssignSounds(IAudioService audioService) => 
      new ButtonSound(audioService, BackButton.gameObject, 13);

    private void AssignButtonsAnimations() => 
      new ButtonAnimation(BackButton.transform);
    
    private void AssignAnimations()
    {
      _fadeTop = new FadeCanvasGroup(TopGroup, 1, .5f);
      _fadeMiddle = new FadeCanvasGroup(MiddleGroup, 1.25f, .5f);
      _fadeBottom = new FadeCanvasGroup(BottomGroup, 1.75f, .5f);
    }
    
    private void AppearAnimation()
    {
      _fadeTop.Appear();
      _fadeMiddle.Appear();
      _fadeBottom.Appear();
    }

    private void DisappearAnimation()
    {
      _fadeTop.Disappear();
      _fadeMiddle.Disappear();
      _fadeBottom.Disappear();
    }
  }
}