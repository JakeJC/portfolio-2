using System;
using _Project.FMOD_AudioService.Logic;
using _Project.Scripts.Animations.UI;
using Assets._Project.FMOD_AudioService.Logic;
using UnityEngine;
using UnityEngine.UI;

namespace _Project.Scripts.Features.Meditation.Scripts.Mono
{
  public class SoundPanel : MonoBehaviour
  {
    public Button MusicButton;
    public Button MechanismsSoundButton;

    public Slider Slider;

    public Image MusicImage;
    public Image MusicBackground;

    public Image SoundImage;
    public Image SoundBackground;

    public Color ActiveColor;
    public Color IconActiveColor;
    public Color PassiveColor;

    private Action<float> _onChangeMusicLevel;
    private Action<float> _onChangeSoundLevel;

    private bool _isSoundActive;
    private bool _isMusicActive = true;
    
    private IAudioService _audioService;

    public void Construct(IAudioService audioService, Action<float> onChangeMusicLevel, Action<float> onChangeSoundLevel)
    {
      _audioService = audioService;
      
      _onChangeSoundLevel = onChangeSoundLevel;
      _onChangeMusicLevel = onChangeMusicLevel;

      MusicButton.onClick.AddListener(OnClickMusic);
      MechanismsSoundButton.onClick.AddListener(OnClickSound);

      _audioService.GetMediationMusicVolumeSlider().getVolume(out float volume);
      SetSlider(volume);
      
      Slider.onValueChanged.AddListener(OnSlide);

      UpdateVisualState();

      AssignSounds();
      AssignButtonsAnimations();
    }

    private void SetSlider(float value) => 
      Slider.value = value;

    private void OnSlide(float value)
    {
      if (_isMusicActive)
        _onChangeMusicLevel?.Invoke(value);

      if (_isSoundActive)
        _onChangeSoundLevel?.Invoke(value);
    }

    private void OnClickMusic()
    {
      _isMusicActive = true;
      _isSoundActive = false;

      UpdateVisualState();
      
      _audioService.GetMediationMusicVolumeSlider().getVolume(out float volume);
      SetSlider(volume);
    }

    private void OnClickSound()
    {
      _isMusicActive = false;
      _isSoundActive = true;
      
      UpdateVisualState();
      
      _audioService.GetMechanismsSoundVolumeSlider().getVolume(out float volume);
      SetSlider(volume);
    }

    private void UpdateVisualState()
    {
      MusicImage.color = _isMusicActive ? IconActiveColor : ActiveColor;
      MusicBackground.color = _isMusicActive ? ActiveColor : PassiveColor;

      SoundImage.color = _isSoundActive ? IconActiveColor : ActiveColor;
      SoundBackground.color = _isSoundActive ? ActiveColor : PassiveColor;
    }
    
    private void AssignSounds()
    {
      new ButtonSound(_audioService, MusicButton.gameObject, 13);
      new ButtonSound(_audioService, MechanismsSoundButton.gameObject, 13);
    }
    
    private void AssignButtonsAnimations()
    {
      new ButtonAnimation(MusicButton.transform);
      new ButtonAnimation(MechanismsSoundButton.transform);
    }
  }
}