using System;
using System.Collections.Generic;
using System.Linq;
using _Project.FMOD_AudioService.Logic;
using _Project.Scripts.Animations.UI;
using _Project.Scripts.Services.Localization.Logic;
using Assets._Project.FMOD_AudioService.Logic;
using UnityEngine;
using UnityEngine.UI;

namespace _Project.Scripts.Features.Meditation.Scripts.Mono
{
  public class MeditationDescriptionWindow : MonoBehaviour
  {
    public Button NextButton;
    
    [Header("Animation Components")]

    [SerializeField] private CanvasGroup TopGroup;
    [SerializeField] private CanvasGroup MiddleGroup1;
    [SerializeField] private CanvasGroup MiddleGroup2;
    [SerializeField] private CanvasGroup BottomGroup;

    private FadeCanvasGroup _fadeTop;
    private FadeCanvasGroup _fadeMiddle1;
    private FadeCanvasGroup _fadeMiddle2;
    private FadeCanvasGroup _fadeBottom;
    
    private Action _toMeditation;

    public void Construct(ILocaleSystem localeSystem, IAudioService audioService, Action toMeditation)
    {
      _toMeditation = toMeditation;

      NextButton.onClick.AddListener(CloseWindow);

      AssignSounds(audioService);
      AssignButtonsAnimations();
      AssignAnimations();
      
      Localize(localeSystem);
    }
    
    public void OpenWindow()
    {
      gameObject.SetActive(true);
      AppearAnimation();
      
      Canvas.ForceUpdateCanvases();
    }

    private void CloseWindow()
    {
      _fadeBottom.Disappear(null, () =>
      {
        _toMeditation?.Invoke();
        gameObject.SetActive(false);
      });

      DisappearAnimation();
    }
    
    private void AssignSounds(IAudioService audioService) => 
      new ButtonSound(audioService, NextButton.gameObject, 13);

    private void AssignButtonsAnimations() => 
      new ButtonAnimation(NextButton.transform);

    private void AssignAnimations()
    {
      _fadeTop = new FadeCanvasGroup(TopGroup, 1, .5f);
      _fadeMiddle1 = new FadeCanvasGroup(MiddleGroup1, 1.25f, .5f);
      _fadeMiddle2 = new FadeCanvasGroup(MiddleGroup2, 1.5f, .5f);
      _fadeBottom = new FadeCanvasGroup(BottomGroup, 1.75f, .5f);
    }

    private void AppearAnimation()
    {
      _fadeTop.Appear();
      _fadeMiddle1.Appear();
      _fadeMiddle2.Appear();
      _fadeBottom.Appear();
    }

    private void DisappearAnimation()
    {
      _fadeTop.Disappear();
      _fadeMiddle1.Disappear();
      _fadeMiddle2.Disappear();
    }

    protected virtual void Localize(ILocaleSystem localeSystem)
    {
      List<LocalizedMainText> localizedTexts = transform.GetComponentsInChildren<LocalizedMainText>(true).ToList();
      localizedTexts.ForEach(item =>
      {
        item.Construct(localeSystem);
        item.Awake();
      });
    }
  }
}