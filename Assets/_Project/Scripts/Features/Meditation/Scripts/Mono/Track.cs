using System;
using _Modules._InApps.Interfaces;
using _Project.FMOD_AudioService.Logic;
using _Project.Scripts.Animations.UI;
using _Project.Scripts.Features.Meditation.Scripts.Scriptables;
using _Project.Scripts.Services.Localization.Logic;
using Assets._Project.FMOD_AudioService.Logic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace _Project.Scripts.Features.Meditation.Scripts.Mono
{
  public class Track : MonoBehaviour
  {
    public TextMeshProUGUI Name;
    public Image FillImage;

    public Image PlayImage;

    public Button Play;
    public Button Ad;

    private ILocaleSystem _localeSystem;

    public Sprite PlaySprite;
    public Sprite PauseSprite;

    private Action<MeditationTrackData> _onPlay;
    private Action<MeditationTrackData> _onPause;
    private Action<MeditationTrackData> _onAdClick;

    public MeditationTrackData MeditationTrackData;

    private bool _isPlaying;
    
    private IAudioService _audioService;
    private IInAppService _inAppService;

    public void Construct(IInAppService inAppService, IAudioService audioService, ILocaleSystem localeSystem, MeditationTrackData meditationTrackData,
      Action<MeditationTrackData> onPlay, Action<MeditationTrackData> onPause, Action<MeditationTrackData> onAdClick)
    {
      _inAppService = inAppService;
      MeditationTrackData = meditationTrackData;
      
      _audioService = audioService;
      _localeSystem = localeSystem;

      _onAdClick = onAdClick;
      _onPause = onPause;
      _onPlay = onPlay;

      Play.onClick.AddListener(Click);
      Ad.onClick.AddListener(AdClick);

      SetVisualInfo();

      AssignSounds();
      AssignButtonsAnimations();

#if IOS_FIRST_RELEASE
      Ad.gameObject.SetActive(false);
#endif

      if (_inAppService.IsSubscriptionPurchased()) 
        Ad.gameObject.SetActive(false);
    }

    public void SetState(bool isPlaying)
    {
      _isPlaying = isPlaying;

      PlayImage.sprite = _isPlaying ? PauseSprite : PlaySprite;
      PlayImage.SetNativeSize();
    }

    public void Unlock() =>
      Ad.gameObject.SetActive(false);

    public void SetVisualInfo()
    {
      Name.text = _localeSystem.GetText("Main", MeditationTrackData.NameKey);
      FillImage.fillAmount = 0;
    }

    public void Fill(float value) => 
      FillImage.fillAmount = value;
    
    private void Click()
    {
      _isPlaying = !_isPlaying;

      if (_isPlaying)
        _onPlay?.Invoke(MeditationTrackData);
      else
        _onPause?.Invoke(MeditationTrackData);

      PlayImage.sprite = _isPlaying ? PauseSprite : PlaySprite;
      PlayImage.SetNativeSize();
    }

    private void AdClick() =>
      _onAdClick?.Invoke(MeditationTrackData);
    
    private void AssignSounds()
    {
      new ButtonSound(_audioService, Play.gameObject, 13);
    }
    
    private void AssignButtonsAnimations()
    {
      new ButtonAnimation(Play.transform);
    }
  }
}