using _Project.Scripts.Features.Meditation.Scripts.Scriptables;
using UnityEngine;

namespace _Project.Scripts.Features.Meditation.Scripts.Mono
{
  public class BackgroundMono : MonoBehaviour
  {
    private const int Offset = 1;
    
    public MeshRenderer Layer1;
    public MeshRenderer Layer2;
    public MeshRenderer Layer3;
    public MeshRenderer Layer4;

    public void Construct(MeditationTrackData data)
    {
      Layer1.material = data.BackgroundMaterials.BackgroundMaterial1;
      Layer2.material = data.BackgroundMaterials.BackgroundMaterial2;
      Layer3.material = data.BackgroundMaterials.BackgroundMaterial3;
      Layer4.material = data.BackgroundMaterials.BackgroundMaterial4;
      
      if(data.BackgroundMaterials.BackgroundMaterial4 == null)
        Layer4.gameObject.SetActive(false);
    }

    public void SetYPosition(float value)
    {
      var tr = transform;
      
      Vector3 position = tr.position;
      position.y = value - Offset;
      tr.position = position;
    }
  }
}