using System;
using _Project.FMOD_AudioService.Logic;
using _Project.Scripts.Animations.UI;
using _Project.Scripts.Features.Meditation.Scripts.Scriptables;
using _Project.Scripts.Services.Localization.Logic;
using Assets._Project.FMOD_AudioService.Logic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace _Project.Scripts.Features.Meditation.Scripts.Mono
{
  public class MeditationWindow : MonoBehaviour
  {
    public TextMeshProUGUI Title; 
    public Button TrackListButton;

    public Button HideButton;
    public Button FavouriteButton;
    
    public Button NextButton;
    public Button PreviousButton;
    public Button PlayButton;

    [Header("Animation Elements")]
    
    public Transform Top;
    public Transform Bottom;
    
    public CanvasGroup CanvasGroup;
    
    [Header("Visual Elements")]

    public Image PlayImage;
    
    public Image FavouriteImage;
    public Image HideImage;
    
    public Sprite PlaySprite;
    public Sprite PauseSprite;
    
    public Sprite FavouriteOn;
    public Sprite FavouriteOff;

    public Sprite HideOn;
    public Sprite HideOff;
    
    private bool _isFavourite;
    private bool _isPanelShowed = true;
    
    private Action _onHide;
    private Action _onFavourite;
    
    private ILocaleSystem _localeSystem;
    
    private bool _isPlaying;

    private Action<MeditationTrackData> _onPlay;
    private Action<MeditationTrackData> _onPause;

    private MeditationTrackData _meditationTrackData;

    public void Open() => 
      gameObject.SetActive(true);

    public void Construct(IAudioService audioService, ILocaleSystem localeSystem, MeditationTrackData meditationTrackData, Action onTrackListPress, 
      Action onHide, Action onFavourite, Action onNext, Action onPrevious, Action<MeditationTrackData> onPlay, Action<MeditationTrackData> onPause)
    {
      _onPause = onPause;
      _onPlay = onPlay;
      _meditationTrackData = meditationTrackData;
      _localeSystem = localeSystem;
      _onFavourite = onFavourite;
      _onHide = onHide;

      SetInfo(meditationTrackData);
      
      TrackListButton.onClick.AddListener(onTrackListPress.Invoke);
      
      HideButton.onClick.AddListener(Hide);
      FavouriteButton.onClick.AddListener(Favourite);
      
      NextButton.onClick.AddListener(onNext.Invoke);
      PreviousButton.onClick.AddListener(onPrevious.Invoke);
      PlayButton.onClick.AddListener(Play);

      AssignSounds(audioService);
      AssignButtonsAnimations();
    }

    public void SetInfo(MeditationTrackData meditationTrackData)
    {
      _meditationTrackData = meditationTrackData;
      Title.text = _localeSystem.GetText("Main", meditationTrackData.NameKey);
    }

    public void SetFavourite(bool isFavourite)
    {
      _isFavourite = isFavourite;
      UpdateButtonsVisual();
    }
    
    public void SetState(bool isPlaying)
    {
      _isPlaying = isPlaying;
      PlayImage.sprite = _isPlaying ? PauseSprite : PlaySprite;
      PlayImage.SetNativeSize();
    }
    
    private void AssignSounds(IAudioService audioService)
    {
      new ButtonSound(audioService, TrackListButton.gameObject, 13);
      
      new ButtonSound(audioService, PreviousButton.gameObject, 13);
      new ButtonSound(audioService, PlayButton.gameObject, 13);
      new ButtonSound(audioService, NextButton.gameObject, 13);
      
      //new ButtonSound(audioService, HideButton.gameObject, 13);
      new ButtonSound(audioService, FavouriteButton.gameObject, 13);
    }

    private void AssignButtonsAnimations()
    {
      new ButtonAnimation(PlayButton.transform);
      new ButtonAnimation(PreviousButton.transform);
      new ButtonAnimation(NextButton.transform);
      
      new ButtonAnimation(HideButton.transform);
      new ButtonAnimation(FavouriteButton.transform);
      
      new ButtonAnimation(TrackListButton.transform);
    }

    private void Play()
    {
      _isPlaying = !_isPlaying;

      if (_isPlaying)
        _onPlay?.Invoke(_meditationTrackData);
      else
        _onPause?.Invoke(_meditationTrackData);

      PlayImage.sprite = _isPlaying ? PauseSprite : PlaySprite;
      PlayImage.SetNativeSize();
    }
    
    private void Hide()
    {  
      _isPanelShowed = !_isPanelShowed;
      _onHide?.Invoke();  
      
      UpdateButtonsVisual();
    }

    private void Favourite()
    {
      _isFavourite = !_isFavourite;
      _onFavourite?.Invoke();

      UpdateButtonsVisual();
    }

    private void UpdateButtonsVisual()
    {
      FavouriteImage.sprite = _isFavourite ? FavouriteOn : FavouriteOff;
      HideImage.sprite = _isPanelShowed ? HideOn : HideOff;
    }
  }
}