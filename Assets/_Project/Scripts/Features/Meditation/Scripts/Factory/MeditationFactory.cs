using System.Collections.Generic;
using _Project.Scripts.Features.Meditation.Scripts.Mono;
using _Project.Scripts.Services._Interfaces;
using UnityEngine;

namespace _Project.Scripts.Features.Meditation.Scripts.Factory
{
  public class MeditationFactory : IMeditationFactory
  {
    private const string MeditationScreenPath = "UI/Meditation/Meditation Screen";
    private const string MeditationSoundsListPath = "UI/Meditation/Meditation Sounds List";
    private const string MeditationDescriptionWindowPath = "UI/Meditation/Meditation Description Window";
    
    private const string TrackPanelPath = "UI/Meditation/Track";
    
    private const string Canvas = "Canvas";
    
    private readonly IAssetProvider _assetProvider;

    public MeditationWindow MeditationWindow { get; set; }
    public TrackListWindow TrackListWindow { get; set; }
    public MeditationDescriptionWindow MeditationDescriptionWindow { get; set; }

    public SoundPanel SoundPanel { get; set; }
    
    public List<Track> Tracks { get; set; }
    
    public MeditationFactory(IAssetProvider assetProvider) => 
      _assetProvider = assetProvider;

    public void SpawnMeditationWindow()
    {
      ClearMeditationWindow();
      
      Transform parent = GameObject.Find(Canvas).transform;

      var window = _assetProvider.GetResource<MeditationWindow>(MeditationScreenPath);
      MeditationWindow = Object.Instantiate(window, parent);

      SoundPanel = MeditationWindow.GetComponentInChildren<SoundPanel>();
    }

    public void SpawnTrackListWindow(int tracksCount)
    {     
      Tracks = new List<Track>();

      ClearTrackListWindow();
      
      Transform parent = GameObject.Find(Canvas).transform;

      var window = _assetProvider.GetResource<TrackListWindow>(MeditationSoundsListPath);
      TrackListWindow = Object.Instantiate(window, parent);
      
      for (int i = 0; i < tracksCount; i++) 
        CreateTrack(TrackListWindow);
    }
    
    public void SpawnMeditationDescriptionWindow()
    {
      ClearMeditationDescriptionWindow();
      
      Transform parent = GameObject.Find(Canvas).transform;

      var window = _assetProvider.GetResource<MeditationDescriptionWindow>(MeditationDescriptionWindowPath);
      MeditationDescriptionWindow = Object.Instantiate(window, parent);
    }
    
    public void ClearMeditationWindow()
    {
      if (MeditationWindow != null)
        Object.Destroy(MeditationWindow.gameObject);
    }

    public void ClearTrackListWindow()
    {
      ClearTracks();
      
      if (TrackListWindow != null)
        Object.Destroy(TrackListWindow.gameObject);
    }
    
    public void ClearMeditationDescriptionWindow()
    {
      if (MeditationDescriptionWindow != null)
        Object.Destroy(MeditationDescriptionWindow.gameObject);
    }

    private void CreateTrack(TrackListWindow trackListWindow)
    {
      Transform parent = trackListWindow.Content;

      var track = _assetProvider.GetResource<Track>(TrackPanelPath);
      Tracks.Add(Object.Instantiate(track, parent));
    }
    
    private void ClearTracks()
    {
      if(Tracks == null)
        return;

      foreach (Track track in Tracks) 
        Object.Destroy(track.gameObject);
      
      Tracks.Clear();
    }
  }
}