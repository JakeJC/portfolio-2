using System.Collections.Generic;
using _Project.Scripts.Features.Meditation.Scripts.Mono;

namespace _Project.Scripts.Features.Meditation.Scripts.Factory
{
  public interface IMeditationFactory
  {
    MeditationWindow MeditationWindow { get; set; }
    TrackListWindow TrackListWindow { get; set; }
    MeditationDescriptionWindow MeditationDescriptionWindow { get; set; }
    List<Track> Tracks { get; set; }
    SoundPanel SoundPanel { get; set; }
    void SpawnMeditationWindow();
    void SpawnTrackListWindow(int tracksCount);
    void SpawnMeditationDescriptionWindow();
    void ClearMeditationWindow();
    void ClearTrackListWindow();
    void ClearMeditationDescriptionWindow();
  }
}