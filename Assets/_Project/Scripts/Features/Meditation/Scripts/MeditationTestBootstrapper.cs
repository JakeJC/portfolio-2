using _Modules._InApps.Scripts.Main;
using _Project.Scripts.Features.Meditation.Scripts.Logic;
using _Project.Scripts.Logic.GameLogic.Meta;
using _Project.Scripts.Services.AssetProvider;
using _Project.Scripts.Services.Environment;
using _Project.Scripts.Services.FMOD_AudioService.Logic;
using _Project.Scripts.Services.LeanTouchCamera.Scripts.Logic;
using _Project.Scripts.Services.LoadScreen.Logic;
using _Project.Scripts.Services.Localization.Logic;
using _Project.Scripts.Services.MaterialService.Main;
using _Project.Scripts.Services.SaveSystem;
using AppacheAds;
using AppacheRemote.Scripts.Data;
using UnityEngine;

namespace _Project.Scripts.Features.Meditation.Scripts
{
  public class MeditationTestBootstrapper : MonoBehaviour
  {
    private IMeditation _meditation;
    private AdsDummy _adsDummy;
    private AudioService _audioService;

    private void Awake()
    {
      var inAppService = new InAppService();
      var assetProvider = new AssetProvider();
      var gameSaveSystem = new GameSaveSystem();

      var gameMetaDataService = new GameMetaDataService(gameSaveSystem, assetProvider);
      gameMetaDataService.Initialize();
      
      _audioService = new AudioService(assetProvider);
      _audioService.InitializeEvents();

      var materialService = new MaterialService(assetProvider, _audioService);
      //materialService.Initialize(null); //ToDo: Допилить потом

      _adsDummy = new AdsDummy();
      _adsDummy.Initialize(PlacementType.@default, () =>
      {
        _meditation = new Logic.Meditation(inAppService, assetProvider, gameSaveSystem,
          gameMetaDataService,
          new LoadScreenService(), new LocaleSystem(assetProvider),
          _audioService, new CameraController(assetProvider), _adsDummy, new EnvironmentController(gameMetaDataService), materialService);
        _meditation.Launch(null);
      });
    }
  }
}