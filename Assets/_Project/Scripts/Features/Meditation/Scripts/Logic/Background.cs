using _Project.Scripts.Features.Meditation.Scripts.Mono;
using _Project.Scripts.Features.Meditation.Scripts.Scriptables;
using _Project.Scripts.Services._Interfaces;
using UnityEngine;

namespace _Project.Scripts.Features.Meditation.Scripts.Logic
{
  public class Background
  {
    private const string BackgroundPrefabPath = "Background Layers";
    private const string ParentPath = "Game Environment";

    private BackgroundMono BackgroundMono { get; set; }
    
    private readonly IAssetProvider _assetProvider;

    public Background(IAssetProvider assetProvider)
    {
      _assetProvider = assetProvider;
    }

    public void CreateBackground(MeditationTrackData meditationTrackData)
    {
      Clear();
        
      var backgroundMono = _assetProvider.GetResource<BackgroundMono>(BackgroundPrefabPath);
      BackgroundMono = Object.Instantiate(backgroundMono, GameObject.Find(ParentPath).transform);
      
      BackgroundMono.Construct(meditationTrackData);
    }

    public void SetYPosition(float y)
    {
      if(BackgroundMono)
        BackgroundMono.SetYPosition(y);
    }

    public void Clear()
    {
      if(BackgroundMono)
        Object.Destroy(BackgroundMono.gameObject);
    }
  }
}