using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using _Modules._Analytics;
using _Modules._InApps.Interfaces;
using _Project.FMOD_AudioService.Logic;
using _Project.FMOD_AudioService.Logic.Enums;
using _Project.FMOD_AudioService.Logic.Events;
using _Project.Scripts.Animations.UI;
using _Project.Scripts.Data.Serializable;
using _Project.Scripts.Features.Meditation.Scripts.Factory;
using _Project.Scripts.Features.Meditation.Scripts.Mono;
using _Project.Scripts.Features.Meditation.Scripts.Scriptables;
using _Project.Scripts.Logic.Factories;
using _Project.Scripts.Logic.Factories.Interfaces;
using _Project.Scripts.Logic.GameLogic.Interfaces;
using _Project.Scripts.Logic.UI.Gallery.Data;
using _Project.Scripts.MonoBehaviours.Game;
using _Project.Scripts.MonoBehaviours.Game.Interfaces;
using _Project.Scripts.Services._Interfaces;
using _Project.Scripts.Services.Environment;
using _Project.Scripts.Services.LeanTouchCamera.Scripts.Interfaces;
using _Project.Scripts.Services.LoadScreen.Logic;
using _Project.Scripts.Services.Localization.Logic;
using _Project.Scripts.Services.MaterialService.Data;
using _Project.Scripts.Services.MaterialService.Interfaces;
using _Project.Shaders.LightWorldAdditional.Controller.Scripts;
using AppacheAds.Scripts.Interfaces;
using DG.Tweening;
using UnityEngine;
using Object = UnityEngine.Object;

namespace _Project.Scripts.Features.Meditation.Scripts.Logic
{
  public class Meditation : IMeditation
  {
    private const string ColorsPath = "_Materials & Colors/Colors";
    private const string MeditationTracksDataPath = "_Tracks";

    private readonly IAssetProvider _assetProvider;
    private readonly IGameDataProvider _gameDataProvider;
    private readonly ILoadScreenService _loadScreenService;
    private readonly ILocaleSystem _localeSystem;
    private readonly IGameSaveSystem _gameSaveSystem;
    private readonly IAudioService _audioService;
    private readonly ICameraController _cameraController;
    private readonly IAdsService _adsService;

    private IMeditationFactory _meditationFactory;
    private IGameLevelFactory _gameLevelFactory;
    private readonly IEnvironmentController _environmentController;
    private readonly IMaterialService _materialService;

    private readonly Background _background;
    private TrackTimeline _trackTimeline;

    private MeditationTrackData[] _meditationTrackData;

    private List<CompleteMechanismData> _completeMechanismsInfo;

    private string _currentMechanismId;

    private FadeCanvasGroup _gameStatePanelFade;
    private Transform _transformParent;

    private AudioEventLink _musicEvent;
    private AudioEventLink _soundEvent;

    private Material _currentMaterial;

    private bool _isTrackPlaying = true;
    private bool _isPanelShowed = true;
    private Action<bool> _onHide;
    private AudioEventLink _previousMusicEvent;
    private Action _afterSpawnMeditation;
    private IInAppService _inAppService;

    private int CurrentDataId
    {
      get => PlayerPrefs.GetInt("CurrentDataId", 0);
      set => PlayerPrefs.SetInt("CurrentDataId", value);
    }

    private float SoundVolume
    {
      get => PlayerPrefs.GetFloat("SoundVolume", 0.5f);
      set => PlayerPrefs.SetFloat("SoundVolume", value);
    }

    private float MusicVolume
    {
      get => PlayerPrefs.GetFloat("MusicVolume", 1);
      set => PlayerPrefs.SetFloat("MusicVolume", value);
    }


    public Meditation(IInAppService inAppService, IAssetProvider assetProvider, IGameSaveSystem gameSaveSystem, IGameDataProvider gameDataProvider,
      ILoadScreenService loadScreenService, ILocaleSystem localeSystem, IAudioService audioService, ICameraController cameraController,
      IAdsService adsService, IEnvironmentController environmentController, IMaterialService materialService)
    {
      _inAppService = inAppService;
      _materialService = materialService;
      _environmentController = environmentController;
      _adsService = adsService;
      _cameraController = cameraController;
      _audioService = audioService;
      _localeSystem = localeSystem;
      _loadScreenService = loadScreenService;
      _gameDataProvider = gameDataProvider;
      _gameSaveSystem = gameSaveSystem;
      _assetProvider = assetProvider;

      _background = new Background(_assetProvider);
    }

    public void Launch(Action afterSpawnMeditation)
    {
      _afterSpawnMeditation = afterSpawnMeditation;
      _meditationFactory = new MeditationFactory(_assetProvider);

      if (_gameSaveSystem.Get().FirstEnterInMeditation)
      {
        _meditationFactory.SpawnMeditationDescriptionWindow();
        _meditationFactory.MeditationDescriptionWindow.Construct(_localeSystem, _audioService, () =>
        {
          _adsService.CurrentInterstitial.ShowInterstitial(ScreenNames.Meditation, Next, Next);
        });

        _meditationFactory.MeditationDescriptionWindow.OpenWindow();

        _gameSaveSystem.Get().FirstEnterInMeditation = false;
        _gameSaveSystem.Save();
      }
      else
        ToMeditation();
    }

    private void Next()
    {
      _meditationFactory.ClearMeditationDescriptionWindow();
      ToMeditation();
    }

    private void ToMeditation()
    {
      _previousMusicEvent = _audioService.GetLinkByEvent(FMOD_EventType.Music);
      _audioService.Stop(_previousMusicEvent);

      _meditationTrackData = _assetProvider.GetAllResources<MeditationTrackData>(MeditationTracksDataPath);
      _completeMechanismsInfo = _gameSaveSystem.Get().CompleteMechanismsInfo;

      PlayMusic();

      _completeMechanismsInfo = _completeMechanismsInfo.OrderByDescending(p => p.IsFavourite ? 1 : 0).ToList();
      _currentMechanismId = _completeMechanismsInfo.First().MechanismId;

      _gameLevelFactory = new GameLevelFactory(_assetProvider, _gameDataProvider,
        new FailConnectionFactory(_assetProvider, _localeSystem, _audioService), _loadScreenService);

      OpenMeditation();
      UpdateArrowsState(0);

      _cameraController.EnableRotation();
      _cameraController.SwitchToGame();
      _cameraController.FinishingRotation();
      
      _afterSpawnMeditation?.Invoke();
    }

    private void SetupLightController()
    {
      IMechanism mechanism = _gameLevelFactory.Mechanism;
      var lightWorldController = mechanism.GetGO().GetComponentInChildren<LightWorldController>();
      if (lightWorldController != null)
      {
        _materialService.Initialize(_gameLevelFactory.MainMaterialProvider, _gameLevelFactory.CurrentWorldId);
        lightWorldController.ForceInitialize();
        lightWorldController.SetColor(GetColorPreset().ColorData, true);
      }

      string GetColorId() =>
        _completeMechanismsInfo
          .FirstOrDefault(p => p.MechanismId == _currentMechanismId).ColorId;

      ColorSchemeData GetColorPreset() =>
        _materialService.Color.GetAllColorScheme()
          .FirstOrDefault(p => p.Guid == GetColorId());
    }

    public void CloseMeditation()
    {
      _audioService.Play(_previousMusicEvent);
      
      _audioService.GetMechanismsSoundVolumeSlider().setVolume(1); 
      _audioService.GetMediationMusicVolumeSlider().setVolume(1);
      
      StopMusic();
      StopSound();

      _cameraController.DisableRotation();
      _gameLevelFactory.Clear();
      _background.Clear();

      _meditationFactory.ClearMeditationWindow();
      _meditationFactory.ClearMeditationDescriptionWindow();
      _meditationFactory.ClearTrackListWindow();
    }

    private MeditationTrackData GetCurrentPresetData() =>
      _meditationTrackData.FirstOrDefault(p => p.TrackId == CurrentDataId);

    private CompleteMechanismData GetCurrentMechanism() =>
      _completeMechanismsInfo.FirstOrDefault(p => p.MechanismId == _currentMechanismId);

    private async void OpenMeditation()
    {
      _audioService.GetMechanismsSoundVolumeSlider().setVolume(SoundVolume);
      _audioService.GetMediationMusicVolumeSlider().setVolume(MusicVolume);

      _meditationFactory.SpawnMeditationWindow();
      _meditationFactory.MeditationWindow.Construct(_audioService, _localeSystem, GetCurrentPresetData(),
        OpenTrackList, Hide, MarkAsFavourite, NextMechanism, PreviousMechanism, PlayTrack, StopTrack);

      _meditationFactory.SoundPanel.Construct(_audioService, OnChangeMusicLevel, OnChangeSoundLevel);

      _meditationFactory.MeditationWindow.Open();

      _gameStatePanelFade = new FadeCanvasGroup(_meditationFactory.MeditationWindow.CanvasGroup, 0, 0.5f);
      _gameStatePanelFade.Appear();

      if (_completeMechanismsInfo.Count == 0)
        return;

      await SpawnMechanism();

      _meditationFactory.MeditationWindow.SetFavourite(GetCurrentMechanism().IsFavourite);

      UnlockFirstTrack();
      _background.CreateBackground(GetCurrentPresetData());

      PlayTrack(GetCurrentPresetData());
    }

    private void OnChangeSoundLevel(float value)
    {
      _audioService.GetMechanismsSoundVolumeSlider().setVolume(value);
      SoundVolume = value;
    }

    private void OnChangeMusicLevel(float value)
    {
      _audioService.GetMediationMusicVolumeSlider().setVolume(value);
      MusicVolume = value;
    }

    private void UnlockFirstTrack()
    {
      if (_gameSaveSystem.Get().UnlockedMeditationPresets.Count != 0)
        return;

      _gameSaveSystem.Get().UnlockedMeditationPresets.Add(_meditationTrackData.First().TrackId.ToString());
      _gameSaveSystem.Save();
    }

    private async Task SpawnMechanism()
    {
      _gameLevelFactory.Clear();

      CompleteMechanismData completeMechanismData = GetCurrentMechanism();

      var info = new LaunchInfo {MechanismId = completeMechanismData.MechanismId};

      await _gameLevelFactory.SpawnLevel(info);

      _environmentController.SetEnvironment(info.MechanismId);

      _cameraController.SetCameraData(new CameraData()
      {
        Position = _gameLevelFactory.Mechanism.GetBaseAnchorPosition
      });

      _currentMaterial = SetMaterial(completeMechanismData);
      SetColor(completeMechanismData, _currentMaterial);
      SetupLightController();

      Vector3 anchorPosition = _gameLevelFactory.Mechanism.GetBaseAnchorPosition;
      _background.SetYPosition(anchorPosition.y);
      
      _gameLevelFactory.Mechanism.FinalLaunch();

      StopSound();
      PlaySound();
      
      UpdateArrowsState(_completeMechanismsInfo.IndexOf(completeMechanismData));
    }

    private Material SetMaterial(CompleteMechanismData completeMechanismData)
    {
      Material material = _gameLevelFactory.Mechanism.GetMaterial(completeMechanismData.MaterialId);

      foreach (PartMaterial materialPart in _gameLevelFactory.Mechanism.GetMaterialParts())
      foreach (Renderer renderer in materialPart.GetRenders())
        renderer.sharedMaterial = material;

      return material;
    }

    private void SetColor(CompleteMechanismData completeMechanismData, Material material)
    {
      ColorSchemeData[] colorSchemeData = _assetProvider.GetAllResources<ColorSchemeData>(ColorsPath);
      ColorSchemeData schemeData = colorSchemeData.First(p => p.Guid == completeMechanismData.ColorId);

      material.SetColor(Shader.PropertyToID("_SpecialColor"), schemeData.ColorData.MechanismColor);
    }

    private void OpenTrackList()
    {
      _meditationFactory.SpawnTrackListWindow(_meditationTrackData.Length);

      for (int i = 0; i < _meditationTrackData.Length; i++)
        _meditationFactory.Tracks[i].Construct(_inAppService, _audioService, _localeSystem, _meditationTrackData[i], PlayTrack, StopTrack, UnlockAdTrack);

      _meditationFactory.TrackListWindow.Construct(_audioService, _localeSystem, () =>
      {
        _trackTimeline.SetAction(null);
        _meditationFactory.ClearTrackListWindow();
      });

      LoadTracksState();

      if (_meditationFactory.Tracks == null)
        return;

      LoadPlayingTrackState();

      _meditationFactory.TrackListWindow.OpenWindow();

      SetupTimeline();
    }

    private void SetupTimeline()
    {
      if (_meditationFactory.Tracks == null)
        return;

      Track track = _meditationFactory.Tracks
        .FirstOrDefault(p => p.MeditationTrackData.TrackId == _trackTimeline.GetTrackId());

      if (track != null)
        _trackTimeline.SetAction(track.Fill);
    }

    private void PreviousMechanism()
    {
      int index = _completeMechanismsInfo.IndexOf(GetCurrentMechanism());
      if (index - 1 < 0)
        return;

      _currentMechanismId = _completeMechanismsInfo[index - 1].MechanismId;

      _loadScreenService.Launch<BlackTransition>(null, SpawnMechanism);

      _meditationFactory.MeditationWindow.SetFavourite(GetCurrentMechanism().IsFavourite);
    }

    private void NextMechanism()
    {
      int index = _completeMechanismsInfo.IndexOf(GetCurrentMechanism());
      if (index + 1 > _completeMechanismsInfo.Count - 1)
        return;

      _currentMechanismId = _completeMechanismsInfo[index + 1].MechanismId;

      _loadScreenService.Launch<BlackTransition>(null, SpawnMechanism);

      _meditationFactory.MeditationWindow.SetFavourite(GetCurrentMechanism().IsFavourite);
    }

    private void UpdateArrowsState(int index)
    {
      _meditationFactory.MeditationWindow.PreviousButton.gameObject.SetActive(index != 0);
      _meditationFactory.MeditationWindow.NextButton.gameObject.SetActive(index != _completeMechanismsInfo.Count - 1);
    }

    private void PlayTrack(MeditationTrackData meditationTrackData)
    {
      _isTrackPlaying = true;

      CurrentDataId = meditationTrackData.TrackId;

      _background.CreateBackground(meditationTrackData);
      _meditationFactory.MeditationWindow.SetInfo(meditationTrackData);
      _meditationFactory.MeditationWindow.SetState(_isTrackPlaying);

      LoadPlayingTrackState();

      _audioService.Resume(_musicEvent);
      _audioService.SetParameter(_musicEvent, FMOD_ParameterType.current_music, CurrentDataId);

      _trackTimeline.PlayTrack(meditationTrackData);

      SetupTimeline();
    }

    private void StopTrack(MeditationTrackData meditationTrackData)
    {
      _isTrackPlaying = false;

      _meditationFactory.MeditationWindow.SetState(_isTrackPlaying);

      LoadPlayingTrackState();

      _audioService.Pause(_musicEvent);
      _trackTimeline.Pause();
    }

    private void PlayMusic()
    {
      var musicObj = new GameObject {name = "Music [Meditation]"};
      _trackTimeline = musicObj.AddComponent<TrackTimeline>();

      _audioService.CreateEvent(out _musicEvent, FMOD_EventType.Meditation, musicObj, true);
      _audioService.Play(_musicEvent);

      _trackTimeline.PlayTrack(GetCurrentPresetData());
    }

    private void PlaySound()
    {
      _audioService.CreateEvent(out _soundEvent, _gameLevelFactory.Mechanism.GetSoundEvent(), _gameLevelFactory.Mechanism.GetGO());

      _audioService.Play(_soundEvent);
      _audioService.SetParameter(_soundEvent, FMOD_ParameterType.current_skin, _gameLevelFactory.Mechanism.GetMaterialsIndex(_currentMaterial));
      _audioService.SetMechanismInLaunch(true);
    }

    private void StopSound()
    {
      _audioService.Stop(_soundEvent);
      _audioService.Cleanup(_soundEvent);
      _audioService.SetMechanismInLaunch(false);
    }

    private void StopMusic()
    {
      if (_trackTimeline)
        Object.Destroy(_trackTimeline.gameObject);

      _audioService.Stop(_musicEvent);
      _audioService.Cleanup(_musicEvent);
    }

    private void LoadPlayingTrackState()
    {
      if (_meditationFactory.Tracks == null)
        return;

      if (_isTrackPlaying)
      {
        foreach (var track in _meditationFactory.Tracks)
        {
          track.SetState(track.MeditationTrackData.TrackId == CurrentDataId);
          track.SetVisualInfo();
        }
      }
      else
      {
        foreach (var track in _meditationFactory.Tracks)
          track.SetState(false);
      }
    }

    private void UnlockAdTrack(MeditationTrackData meditationTrackData)
    {
      _adsService.Rewarded.ShowReward($"unlock_track_{meditationTrackData.TrackId}", OnReward, _adsService.Rewarded.RequestRewardedVideo);

      void OnReward()
      {
        _gameSaveSystem.Get().UnlockedMeditationPresets.Add(meditationTrackData.TrackId.ToString());
        _gameSaveSystem.Save();

        Track track = _meditationFactory.Tracks.FirstOrDefault(p => p.MeditationTrackData == meditationTrackData);
        if (track != null)
          track.Unlock();
      }
    }

    private void LoadTracksState()
    {
      foreach (MeditationTrackData meditationTrackData in _meditationTrackData)
      {
        if (_gameSaveSystem.Get().UnlockedMeditationPresets.Contains(meditationTrackData.TrackId.ToString()))
        {
          Track track = _meditationFactory.Tracks.FirstOrDefault(p => p.MeditationTrackData == meditationTrackData);
          if (track != null)
            track.Unlock();
        }
      }
    }

    private void Hide()
    {
      ShiftGamePanel(_isPanelShowed);
      _isPanelShowed = !_isPanelShowed;

      _onHide?.Invoke(!_isPanelShowed);
    }

    public void AddOnHideUIEvent(Action<bool> onHide) =>
      _onHide += onHide;

    private void ShiftGamePanel(bool isShowed, float targetAlpha = 1f)
    {
      var offset = -Screen.height / 2;
      var duration = .5f;

      _meditationFactory.MeditationWindow.Top.transform.DOLocalMove(
        _meditationFactory.MeditationWindow.Top.transform.localPosition + offset * (isShowed ? Vector3.down : Vector3.up), duration).SetEase(Ease.Linear);
      _meditationFactory.MeditationWindow.Bottom.transform.DOLocalMove(
        _meditationFactory.MeditationWindow.Bottom.transform.localPosition + offset * (isShowed ? Vector3.up : Vector3.down), duration).SetEase(Ease.Linear);

      if (!isShowed)
        _gameStatePanelFade.Appear(() => { _meditationFactory.MeditationWindow.HideButton.interactable = false; }
          , () =>
          {
            _meditationFactory.MeditationWindow.HideButton.interactable = true;
            _meditationFactory.MeditationWindow.HideButton.transform.SetParent(_transformParent);
          },
          0, targetAlpha);
      else
      {
        _transformParent = _meditationFactory.MeditationWindow.HideButton.transform.parent;
        _meditationFactory.MeditationWindow.HideButton.transform.SetParent(_meditationFactory.MeditationWindow.transform);

        _gameStatePanelFade.Disappear(() => { _meditationFactory.MeditationWindow.HideButton.interactable = false; },
          () => { _meditationFactory.MeditationWindow.HideButton.interactable = true; });
      }
    }

    private void MarkAsFavourite()
    {
      CompleteMechanismData mechanismData = _gameSaveSystem.Get().CompleteMechanismsInfo
        .FirstOrDefault(p => p.MechanismId == _currentMechanismId);

      int index = _gameSaveSystem.Get().CompleteMechanismsInfo.IndexOf(mechanismData);
      mechanismData.IsFavourite = !mechanismData.IsFavourite;

      _gameSaveSystem.Get().CompleteMechanismsInfo[index] = mechanismData;
      _gameSaveSystem.Save();
    }
  }
}