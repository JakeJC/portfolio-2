using System;

namespace _Project.Scripts.Features.Meditation.Scripts.Logic
{
  public interface IMeditation
  {
    void Launch(Action afterSpawnMeditation);
    void CloseMeditation();
    void AddOnHideUIEvent(Action<bool> onHide);
  }
}