using System;
using UnityEngine;

namespace _Project.Scripts.Features.Meditation.Scripts.Scriptables
{
  [CreateAssetMenu(fileName = "Meditation Track", menuName = "Meditation/Create Meditation Track", order = 0)]
  public class MeditationTrackData : ScriptableObject
  {
    public int TrackId;
    public string NameKey;
    public TexturePack BackgroundMaterials;
    public float Length;
  }

  [Serializable]
  public struct TexturePack
  {
    public Material BackgroundMaterial1;
    public Material BackgroundMaterial2;
    public Material BackgroundMaterial3;
    public Material BackgroundMaterial4;
  }
}