using System;

namespace _Project.Scripts.Features.Meditation.Scripts.Scriptables
{
  [Serializable]
  public struct CompleteMechanismData
  {
    public string MechanismId;
    public string MaterialId;
    public string ColorId;

    public bool IsFavourite;
  }
}