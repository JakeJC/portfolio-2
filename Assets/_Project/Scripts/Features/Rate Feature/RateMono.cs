using System;
using System.Collections;
#if UNITY_ANDROID
using Google.Play.Review;
#endif
using UnityEngine;

namespace _Project.Scripts.Features.Rate_Feature
{
  public class RateMono : MonoBehaviour
  {
#if UNITY_ANDROID
    private ReviewManager _reviewManager;
    private PlayReviewInfo _playReviewInfo;
#endif
    private Action _launchReview;

    private void Awake()
    {
#if UNITY_ANDROID
      _reviewManager = new ReviewManager();
#endif
    }

    public void RequestReview(Action launchReview)
    {
#if UNITY_ANDROID
      _launchReview = launchReview;
      StartCoroutine(Request());
#else 
      launchReview?.Invoke();
#endif
    }

#if UNITY_ANDROID
    public void LaunchReview() => 
      StartCoroutine(Launch());
#endif

#if UNITY_ANDROID
    private IEnumerator Request()
    {
      var requestFlowOperation = _reviewManager.RequestReviewFlow();
      yield return requestFlowOperation;
      if (requestFlowOperation.Error != ReviewErrorCode.NoError)
      {
        Debug.LogError(requestFlowOperation.Error.ToString());
        yield break;
      }
      _playReviewInfo = requestFlowOperation.GetResult();
      Debug.Log("[Rate] Requested");
        
      _launchReview?.Invoke();
    }
#endif

#if UNITY_ANDROID
    private IEnumerator Launch()
    {
      var launchFlowOperation = _reviewManager.LaunchReviewFlow(_playReviewInfo);
      yield return launchFlowOperation;
      _playReviewInfo = null;

      if (launchFlowOperation.Error != ReviewErrorCode.NoError)
        Debug.LogError(launchFlowOperation.Error.ToString());
      
      Debug.Log("[Rate] Launched");
    }
#endif
  }
}