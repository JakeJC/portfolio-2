using System;
using UnityEngine;
#if UNITY_IOS
using UnityEngine.iOS;
#endif
using Object = UnityEngine.Object;

namespace _Project.Scripts.Features.Rate_Feature
{
  public class Rate
  {
#if UNITY_ANDROID
    private readonly RateMono _mono;
#endif

    public Rate()
    {
#if UNITY_ANDROID 
      _mono = new GameObject("Google Review Controller").AddComponent<RateMono>();
#endif
    }

    public void RequestReview(Action onRequested)
    {
#if UNITY_ANDROID 
      _mono.RequestReview(onRequested);
#elif UNITY_IOS
      LaunchReview();
#endif
    }

    public void LaunchReview()
    {
#if UNITY_ANDROID 
      _mono.LaunchReview();
#elif UNITY_IOS
      Device.RequestStoreReview();
#endif
    }

    public void Clear()
    {
#if UNITY_ANDROID
      if(_mono)
        Object.Destroy(_mono.gameObject);
#endif
    }
  }
}