using System;
using _Project.FMOD_AudioService.Logic;
using _Project.Scripts.Animations.UI;
using _Project.Scripts.Logic.GameLogic.Interfaces;
using _Project.Scripts.Logic.StatesMachine;
using _Project.Scripts.Logic.StatesMachine.States;
using _Project.Scripts.Services._Interfaces;
using _Project.Scripts.Services.LoadScreen.Logic;
using _Project.Scripts.Services.Localization.Logic;

namespace _Project.Scripts.Features.End_Content
{
  public class EndContent
  {
    private readonly EndContentFactory _endContentFactory;

    private readonly GameStateMachine _gameStateMachine;
    private EndContentWindow _endContentWindow;
    
    private readonly ILocaleSystem _localeSystem;
    private readonly IAudioService _audioService;
    private readonly ILoadScreenService _loadScreenService;
    private readonly IGameDataProvider _gameDataProvider;

    public EndContent(GameStateMachine gameStateMachine, IAssetProvider assetProvider, ILocaleSystem localeSystem, IAudioService audioService,
      ILoadScreenService loadScreenService, IGameDataProvider gameDataProvider)
    {
      _gameDataProvider = gameDataProvider;
      _loadScreenService = loadScreenService;
      _audioService = audioService;
      _localeSystem = localeSystem;
      _gameStateMachine = gameStateMachine;
      _endContentFactory = new EndContentFactory(assetProvider);
    }

    public void Open()
    {
      _endContentFactory.CreateEndDecisionWindow();
      _endContentWindow = _endContentFactory.EndContentWindow;
      _endContentWindow.Construct(_localeSystem, _audioService, () => Close(ToGallery), () => Close(ToAdvices));
      
      _endContentWindow.OpenWindow();
    }

    private void Close(Action onClose)
    {
      _endContentWindow.CloseWindow(() =>
      {
        onClose?.Invoke();
        _endContentFactory.Clear();
      });
    }

    private void ToGallery() =>
      _loadScreenService.Launch<BlackTransition>(null, async () 
        => await _gameStateMachine.AsyncEnter<LoadGalleryState, ButtonType>(ButtonType.Worlds));

    private void ToAdvices() =>
      _loadScreenService.Launch<BlackTransition>(null, async () 
        => await _gameStateMachine.AsyncEnter<LoadGalleryState, ButtonType>(_gameDataProvider.IsAllAdvicesLocked()? ButtonType.Worlds: ButtonType.Advices));
  }
}