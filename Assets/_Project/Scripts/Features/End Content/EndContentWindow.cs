using System;
using System.Collections.Generic;
using System.Linq;
using _Project.FMOD_AudioService.Logic;
using _Project.Scripts.Animations.UI;
using _Project.Scripts.Services.Localization.Logic;
using Assets._Project.FMOD_AudioService.Logic;
using UnityEngine;
using UnityEngine.UI;

namespace _Project.Scripts.Features.End_Content
{
  public class EndContentWindow : MonoBehaviour
  {   
    [Header("Buttons")]

    [SerializeField] private Button ToGalleryBtn;
    [SerializeField] private Button ToAdvicesBtn;
    
    [Header("Animation Components")]

    [SerializeField] private CanvasGroup TopGroup;
    [SerializeField] private CanvasGroup MiddleGroup1;
    [SerializeField] private CanvasGroup MiddleGroup2;
    [SerializeField] private CanvasGroup BottomGroup;
   
    private FadeCanvasGroup _fadeTop;
    private FadeCanvasGroup _fadeMiddle1;
    private FadeCanvasGroup _fadeMiddle2;
    private FadeCanvasGroup _fadeBottom;

    public void Construct(ILocaleSystem localeSystem, IAudioService audioService, Action toGallery, Action toAdvices)
    {
      AssignButtonsListeners(toGallery, toAdvices);
      AssignSounds(audioService);
      AssignButtonsAnimations();
      
      AssignAnimations();

      Localize(localeSystem);
    }
    
    public void OpenWindow()
    {
      gameObject.SetActive(true);
      AppearAnimation();
      
      Canvas.ForceUpdateCanvases();
    }

    public void CloseWindow(Action onClose)
    {
      _fadeBottom.Disappear(null, () =>
      {
        onClose?.Invoke();
        gameObject.SetActive(false);
      });

      DisappearAnimation();
    }
    
    private void AssignButtonsListeners(Action toGallery, Action toAdvices)
    {
      ToGalleryBtn.onClick.AddListener(toGallery.Invoke);
      ToAdvicesBtn.onClick.AddListener(toAdvices.Invoke);
    }
    
    private void AssignAnimations()
    {
      _fadeTop = new FadeCanvasGroup(TopGroup, 1, 1);
      _fadeMiddle1 = new FadeCanvasGroup(MiddleGroup1, 1.25f, 1);
      _fadeMiddle2 = new FadeCanvasGroup(MiddleGroup2, 1.5f, 1);
      _fadeBottom = new FadeCanvasGroup(BottomGroup, 1.75f, 1);
    }

    private void AssignButtonsAnimations()
    {
      new ButtonAnimation(ToGalleryBtn.transform);
      new ButtonAnimation(ToAdvicesBtn.transform);
    }
    
    private void AssignSounds(IAudioService audioService)
    {
      new ButtonSound(audioService, ToGalleryBtn.gameObject, 13);
      new ButtonSound(audioService, ToAdvicesBtn.gameObject, 13);
    }
    
    private void AppearAnimation()
    {
      _fadeTop.Appear();
      _fadeMiddle1.Appear();
      _fadeMiddle2.Appear();
      _fadeBottom.Appear();
    }

    private void DisappearAnimation()
    {
      _fadeTop.Disappear();
      _fadeMiddle1.Disappear();
      _fadeMiddle2.Disappear();
    }

    private void Localize(ILocaleSystem localeSystem)
    {
      List<LocalizedMainText> localizedTexts = transform.GetComponentsInChildren<LocalizedMainText>(true).ToList();
      localizedTexts.ForEach(item =>
      {
        item.Construct(localeSystem);
        item.Awake();
      });
    }
  }
}