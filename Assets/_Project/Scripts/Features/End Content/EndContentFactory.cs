using _Project.Scripts.Features.End_Decision_Window;
using _Project.Scripts.Services._Interfaces;
using UnityEngine;

namespace _Project.Scripts.Features.End_Content
{
  public class EndContentFactory
  {
    private const string EndContentWindowPath = "UI/End Content Window/End Content Window";
    private const string CanvasId = "Canvas";

    private readonly IAssetProvider _assetProvider;
    
    public EndContentWindow EndContentWindow { get; private set; }
    
    public EndContentFactory(IAssetProvider assetProvider)
    {
      _assetProvider = assetProvider;
    }
    
    public void CreateEndDecisionWindow()
    { 
      if (EndContentWindow)
        return;
      
      var prefab = _assetProvider.GetResource<GameObject>(EndContentWindowPath);
      var parent = GameObject.Find(CanvasId).transform;
      
      EndContentWindow = Object.Instantiate(prefab, parent).GetComponent<EndContentWindow>();
    }
    
    public void Clear()
    {
      if (EndContentWindow == null)
        return;
      
      Object.Destroy(EndContentWindow.gameObject);
    }

  }
}