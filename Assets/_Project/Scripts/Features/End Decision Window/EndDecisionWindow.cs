using System;
using System.Collections.Generic;
using System.Linq;
using _Project.FMOD_AudioService.Logic;
using _Project.Scripts.Animations.UI;
using _Project.Scripts.Features.Stages.Interfaces;
using _Project.Scripts.Logic.GameLogic.Interfaces;
using _Project.Scripts.Services.Localization.Logic;
using AppacheAnalytics.Scripts.Interfaces;
using Assets._Project.FMOD_AudioService.Logic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace _Project.Scripts.Features.End_Decision_Window
{
  public class EndDecisionWindow : MonoBehaviour
  {
    private const string FirstSessionResultDescription = "FirstSession.ResultDescription";
    private const string NextSessionsResultDescription = "NextSessions.ResultDescription";
    
    [SerializeField] private TextMeshProUGUI Description;

    [Header("Buttons")]

    [SerializeField] private Button ToAdviceBtn;
    [SerializeField] private Button ToNextMechanismBtn;

    [Header("Animation Components")]
    
    [SerializeField] private CanvasGroup TopGroup;
    [SerializeField] private CanvasGroup MiddleGroup;
    [SerializeField] private CanvasGroup BottomGroup;

    private FadeCanvasGroup _fadeTop;
    private FadeCanvasGroup _fadeMiddle;
    private FadeCanvasGroup _fadeBottom;
    
    private IAnalytics _analytics;
    private IStagesService _stagesService;

    private Action _toAdvice;
    private Action _toNextMechanism;

    public void Construct(IStagesService stagesService, IGameDataProvider gameDataProvider, ILocaleSystem localeSystem, IAudioService audioService, IAnalytics analytics,
      Action toAdvice, Action toNextMechanism)
    {
      _stagesService = stagesService;
      _toNextMechanism = toNextMechanism;
      
      _toAdvice = toAdvice;
      _analytics = analytics;
      
      AssignButtonsListeners();
      AssignSounds(audioService);
      AssignButtonsAnimations();
      
      AssignAnimations();
      
      Localize(localeSystem);

      Description.text = localeSystem.GetText("Main", gameDataProvider.FirstSession.IsFirstSession() ? 
        FirstSessionResultDescription : 
        NextSessionsResultDescription);
      
      _stagesService.ShowStageCounterView(Stages.Stages.Construction, 0);
    }
    
    public void OpenWindow()
    {    
      _analytics.Send("ev_choice_screen");

      gameObject.SetActive(true);
      AppearAnimation();
      
      Canvas.ForceUpdateCanvases();
    }

    public void CloseWindow(Action onClose)
    {
      _fadeBottom.Disappear(null, () =>
      {
        onClose?.Invoke();
        gameObject.SetActive(false);
      });

      DisappearAnimation();
    }
    
    private void AssignAnimations()
    {
      _fadeTop = new FadeCanvasGroup(TopGroup, 1, 1);
      _fadeMiddle = new FadeCanvasGroup(MiddleGroup, 1.25f, 1);
      _fadeBottom = new FadeCanvasGroup(BottomGroup, 1.5f, 1);
    }

    private void AssignButtonsAnimations()
    {
      new ButtonAnimation(ToAdviceBtn.transform);
      new ButtonAnimation(ToNextMechanismBtn.transform);
    }

    private void AssignSounds(IAudioService audioService)
    {
      new ButtonSound(audioService, ToAdviceBtn.gameObject, 13);
      new ButtonSound(audioService, ToNextMechanismBtn.gameObject, 13);
    }

    private void AssignButtonsListeners()
    {
      ToAdviceBtn.onClick.AddListener(ToAdvice);
      ToNextMechanismBtn.onClick.AddListener(ToNextMechanism);
    }

    private void ToAdvice()
    {
      _analytics.Send("btn_advice");
      _toAdvice?.Invoke();
    }
    
    private void ToNextMechanism()
    {
      _analytics.Send("btn_next_level");
      _toNextMechanism?.Invoke();
    }

    private void AppearAnimation()
    {
      _fadeTop.Appear();
      _fadeMiddle.Appear();
      _fadeBottom.Appear();
    }

    private void DisappearAnimation()
    {
      _fadeTop.Disappear();
      _fadeMiddle.Disappear();
    }

    private void Localize(ILocaleSystem localeSystem)
    {
      List<LocalizedMainText> localizedTexts = transform.GetComponentsInChildren<LocalizedMainText>(true).ToList();
      localizedTexts.ForEach(item =>
      {
        item.Construct(localeSystem);
        item.Awake();
      });
    }
  }
}