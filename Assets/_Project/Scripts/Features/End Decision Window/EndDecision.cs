using System;
using _Modules._Analytics;
using _Project.FMOD_AudioService.Logic;
using _Project.Scripts.Animations.UI;
using _Project.Scripts.Features.Stages.Interfaces;
using _Project.Scripts.Logic.GameLogic.Advices;
using _Project.Scripts.Logic.GameLogic.Advices.Interfaces;
using _Project.Scripts.Logic.GameLogic.Interfaces;
using _Project.Scripts.Logic.StatesMachine;
using _Project.Scripts.Logic.StatesMachine.States;
using _Project.Scripts.Services._Interfaces;
using _Project.Scripts.Services.LoadScreen.Logic;
using _Project.Scripts.Services.Localization.Logic;
using AppacheAds.Scripts.Interfaces;
using AppacheAnalytics.Scripts.Interfaces;
using AppacheRemote.Scripts.Interfaces;

namespace _Project.Scripts.Features.End_Decision_Window
{
  public class EndDecision
  {
    private readonly EndDecisionFactory _endDecisionFactory;

    private readonly GameStateMachine _gameStateMachine;
    private EndDecisionWindow _endDecisionWindow;

    private readonly Advices _advices;

    private readonly ILocaleSystem _localeSystem;
    private readonly IAudioService _audioService;

    private readonly ILoadScreenService _loadScreenService;
    private readonly IStagesService _stagesService;
    private readonly IAnalytics _analytics;
    private readonly IGameDataProvider _gameDataProvider;
    private readonly IRemote _remote;
    private readonly IAdsService _ads;
    
    public EndDecision(GameStateMachine gameStateMachine, IAssetProvider assetProvider,
      IGameSaveSystem gameSaveSystem, ILocaleSystem localeSystem, IAudioService audioService, ILoadScreenService loadScreenService,
      IStagesService stagesService, IAnalytics analytics, IGameDataProvider gameDataProvider, IRemote remote, IAdsService ads)
    {
      _gameDataProvider = gameDataProvider;
      _analytics = analytics;
      _stagesService = stagesService;
      _loadScreenService = loadScreenService;
      _audioService = audioService;
      _localeSystem = localeSystem;
      _gameStateMachine = gameStateMachine;
      _remote = remote;
      _ads = ads;
      
      _endDecisionFactory = new EndDecisionFactory(assetProvider);
      _advices = new Advices(assetProvider, gameSaveSystem);
    }

    public void Open()
    {
      _endDecisionFactory.CreateEndDecisionWindow();
      _endDecisionWindow = _endDecisionFactory.EndDecisionWindow;
      _endDecisionWindow.Construct(_stagesService, _gameDataProvider, _localeSystem, _audioService, _analytics,
        () => Close(ToAdvice), () => Close(ToNextMechanism));

      _endDecisionWindow.OpenWindow();
    }

    private void Close(Action onClose)
    {
      _endDecisionWindow.CloseWindow(() =>
      {
        onClose?.Invoke();
        _endDecisionFactory.Clear();
      });
    }

    private void ToAdvice()
    {
      _stagesService.CompleteStage();

      _advices.OpenNextAdvice();
      _gameStateMachine.Enter<LoadResultUIState, IAdvices>(_advices);
    }

    private void ToNextMechanism()
    {
      _stagesService.CompleteStage();
      
      if(_ads.AdsEnabled)
        _ads.CurrentInterstitial.ShowInterstitial(ScreenNames.Game, null, null);
      
      _stagesService.OpenStageDescription(Stages.Stages.Construction, () =>
        _loadScreenService.Launch<BlackTransition>(null, async ()
          => await _gameStateMachine.AsyncEnter<LoadGameEnvironmentState>()), 1);
    }
  }
}