using _Project.Scripts.Services._Interfaces;
using UnityEngine;

namespace _Project.Scripts.Features.End_Decision_Window
{
  public class EndDecisionFactory
  {
    private const string EndDecisionWindowPath = "UI/End Decision Window/End Decision Window";
    private const string CanvasId = "Canvas";

    private readonly IAssetProvider _assetProvider;

    public EndDecisionWindow EndDecisionWindow { get; private set; }
    
    public EndDecisionFactory(IAssetProvider assetProvider)
    {
      _assetProvider = assetProvider;
    }
    
    public void CreateEndDecisionWindow()
    { 
      if (EndDecisionWindow)
        return;
      
      var prefab = _assetProvider.GetResource<GameObject>(EndDecisionWindowPath);
      var parent = GameObject.Find(CanvasId).transform;
      
      EndDecisionWindow = Object.Instantiate(prefab, parent).GetComponent<EndDecisionWindow>();
    }
    
    public void Clear()
    {
      if (EndDecisionWindow == null)
        return;
      
      Object.Destroy(EndDecisionWindow.gameObject);
    }
  }
}