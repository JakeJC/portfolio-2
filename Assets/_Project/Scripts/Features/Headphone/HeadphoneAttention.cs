using System;
using System.Linq;
using _Project.FMOD_AudioService.Logic;
using _Project.Scripts.Animations.UI;
using _Project.Scripts.Services._Interfaces;
using _Project.Scripts.Services.Localization.Logic;
using _Project.Scripts.Services.SaveSystem;
using AppacheAnalytics.Scripts.Interfaces;
using AppacheRemote.Scripts.Interfaces;
using Assets._Project.FMOD_AudioService.Logic;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace _Project.Scripts.Features.Headphone
{
  public class HeadphoneAttention : MonoBehaviour
  {
    private const float Time = .5f;
    [SerializeField] private Button next;
    [SerializeField] private CanvasGroup canvasGroup;

    [Header("Privacy Panel")] //If test doesn't apply delete all  in downstairs
    [SerializeField] private CanvasGroup canvasPrivacyGroup;
    [SerializeField] private Button privacyButton;
    [SerializeField] private Toggle privacyToggle;

    public void Construct(ILocaleSystem localeSystem, IAudioService audioService, IAnalytics analytics, IGameSaveSystem gameSaveSystem,
      IRemote remote, Action onHeadphoneEnd, Action onClickPrivacy)
    {
      next.onClick.AddListener(() =>
      {
        canvasGroup.DOFade(0f, Time)
          .SetEase(Ease.Linear)
          .OnComplete(() =>
          {
            analytics.Send("btn_next");
            analytics.Send("btn_headphones");
            onHeadphoneEnd?.Invoke();
          });
        
        canvasPrivacyGroup.DOFade(0f, Time)
          .SetEase(Ease.Linear);
      });

      privacyToggle.onValueChanged.AddListener(OnClickToggle);
      privacyButton.onClick.AddListener(() => onClickPrivacy?.Invoke());

      Localize(localeSystem);

      new ButtonAnimation(next.transform);
      new ButtonSound(audioService, next.gameObject, 0);

      canvasGroup.DOFade(1f, Time)
        .SetDelay(Time)
        .SetEase(Ease.Linear);

      if (gameSaveSystem.Get().IsPolicyAccepted)
        return;

      canvasPrivacyGroup.DOFade(1f, Time)
        .SetDelay(Time)
        .SetEase(Ease.Linear)
        .OnComplete(() => canvasPrivacyGroup.blocksRaycasts = true);
    }

    private void OnClickToggle(bool isAgreed) =>
      next.interactable = isAgreed;

    private void Localize(ILocaleSystem localeSystem)
    {
      var localizedTexts =
        transform.GetComponentsInChildren<LocalizedMainText>(true).ToList();

      localizedTexts.ForEach(item =>
      {
        item.Construct(localeSystem);
        item.Awake();
      });
    }
  }
}