using System;
using DG.Tweening;
using UnityEngine;

namespace _Project.Scripts.Animations.InGame
{
  public class MagnetPartAnimation
  {
    private const float Duration = 0.45f;

    private readonly Transform _target;
    private readonly Vector3 _targetPosition;

    private Tween _tween;

    public MagnetPartAnimation(Transform target, Vector3 targetPosition)
    {
      _target = target;
      _targetPosition = targetPosition;
    }

    public void Magnet(Action<bool> onEnd)
    {
      _tween = _target.DOMove(_targetPosition, Duration).SetEase(Ease.OutExpo); 
      _tween.onComplete += () => onEnd?.Invoke(true);
    }
  }
}