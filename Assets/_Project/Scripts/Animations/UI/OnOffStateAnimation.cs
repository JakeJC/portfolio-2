using System;
using DG.Tweening;
using UnityEngine;

namespace _Project.Scripts.Animations.UI
{
  public class OnOffStateAnimation
  {
    private const float Duration = 0.25f;
    
    private readonly Transform _target;
    private readonly Vector3 _originalScale;

    public OnOffStateAnimation(Transform target)
    {
      _target = target;
      _originalScale = Vector3.one;
    }

    public void Enable(Action onEnd)
    {
      _target.localScale = Vector3.zero;
      _target.DOScale(_originalScale, Duration).onComplete += () => { onEnd?.Invoke(); };
    }

    public void Disable(Action onEnd) => 
      _target.DOScale(Vector3.zero, Duration).onComplete += () => { onEnd?.Invoke();};
  }
}