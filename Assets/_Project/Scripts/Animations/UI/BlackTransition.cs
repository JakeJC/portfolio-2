using System;
using _Project.Scripts.Services.LoadScreen.Logic;
using DG.Tweening;
using UnityEngine;
using Object = UnityEngine.Object;

namespace _Project.Scripts.Animations.UI
{
  public class BlackTransition : LoadScreen
  {
    private const string BlackScreenPath = "UI/Animations/Blackscreen";
    
    private const float InDuration = 0.5f;
    private const float OutDuration = 1f;
    
    private CanvasGroup _screen;

    public override void Start(Action onFirsPartEnd, float startAlphaValue = 0)
    {
      if(_screen != null)
        return;
      
      var blackScreenPrefab = Resources.Load<GameObject>(BlackScreenPath);
      _screen = Object.Instantiate(blackScreenPrefab).GetComponent<CanvasGroup>();
      
      _screen.alpha = startAlphaValue;
      
      _screen.DOFade(1, InDuration).onComplete += () => onFirsPartEnd?.Invoke();
    }

    public override void End(Action onEnd)
    {
      _screen.DOFade(0, OutDuration).onComplete += () =>
      {
        Object.Destroy(_screen.gameObject);
        _screen = null;
        onEnd?.Invoke();
      };
    }
  }
}