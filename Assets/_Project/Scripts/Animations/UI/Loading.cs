using System;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace _Project.Scripts.Animations.UI
{
  [RequireComponent(typeof(Image))]
  public class Loading : MonoBehaviour
  {
    public float AppearAfter = 7;
    private Image _image;

    private void Awake()
    {
      _image = GetComponent<Image>();
      SetZeroAlpha();

      Rotate();
    }

    private void Rotate()
    {
      _image.DOFade(1, 0.2f).SetDelay(AppearAfter);
      _image.transform.DORotate(new Vector3(0,0,359), 2, RotateMode.FastBeyond360)
        .SetLoops(-1, LoopType.Restart)
        .SetEase(Ease.Linear);
    }

    private void SetZeroAlpha()
    {
      var imageColor = _image.color;
      imageColor.a = 0;
      _image.color = imageColor;
    }
  }
}