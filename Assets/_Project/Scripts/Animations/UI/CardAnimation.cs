using System;
using _Project.Scripts.MonoBehaviours.UI;
using DG.Tweening;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace _Project.Scripts.Animations.UI
{
  public class CardAnimation
  {
    private const float Duration = 0.2f;
    private const float TargetMoveY = -20f;

    private readonly Button _button;
    private readonly RectTransform _target;

    private float _originalY;
    private Tween _move;

    public CardAnimation(Button button, RectTransform target)
    {
      var objectWithAnimation = button.gameObject.AddComponent<CardAnimationComponent>();
      objectWithAnimation.Construct(() => _move?.Kill());
      objectWithAnimation.PointerDown += OnPointerDown;
      objectWithAnimation.PointerUp += OnPointerUp;

      _button = button;
      _target = target;
    }

    private void OnPointerDown(PointerEventData eventData)
    {
      if (!_button.interactable) return;

      _move?.Kill();
      _originalY = _target.anchoredPosition.y;
      _move = _target.DOAnchorPosY(_originalY + TargetMoveY, Duration);
    }

    private void OnPointerUp(PointerEventData eventData)
    {
      _move?.Kill();
      _move = _target.DOAnchorPosY(_originalY, Duration);
    }
  }

  public class CardAnimationComponent : ObjectWithAnimation, IPointerDownHandler, IPointerUpHandler
  {
    public event Action<PointerEventData> PointerDown;
    public event Action<PointerEventData> PointerUp;

    public void OnPointerDown(PointerEventData eventData) => PointerDown?.Invoke(eventData);
    public void OnPointerUp(PointerEventData eventData) => PointerUp?.Invoke(eventData);
  }
}