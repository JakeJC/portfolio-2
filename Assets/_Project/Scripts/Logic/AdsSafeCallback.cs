using System;
using UnityEngine;

namespace _Project.Scripts.Logic
{
  public class AdsSafeCallback : MonoBehaviour
  {
    private bool _isCompleted;
    private bool _isActivated;
    
    private Action _onComplete;

    public void Construct(Action onComplete) => 
      _onComplete = onComplete;

    public void Complete() => 
      _isCompleted = true;

    private void Update()
    {
      if (!_isCompleted) 
        return;
      
      if (_isActivated) 
        return;
      
      _onComplete?.Invoke();
      _isActivated = true;
        
      Destroy(gameObject);
    }
  }
}