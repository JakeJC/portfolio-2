using System.Collections.Generic;

namespace _Project.Scripts.Logic.GameLogic.Advices.Interfaces
{
  public interface IAdvices
  {
    AdviceData OpenNextAdvice();
    AdviceData GetAdvice();
#if COMPLETE_GAME || SOUND_DESIGNER
    void OpenAll();
#endif
    List<AdviceData> GetOpenedAdvices();
  }
}