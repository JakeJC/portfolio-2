using System.Collections.Generic;
using System.Linq;
using _Project.Scripts.Logic.GameLogic.Advices.Interfaces;
using _Project.Scripts.Services._Interfaces;

namespace _Project.Scripts.Logic.GameLogic.Advices
{
  public class Advices : IAdvices
  {
    private const string AdvicesPath = "_Advices Configs";
    
    private readonly IAssetProvider _assetProvider;

    private AdviceData[] _advices;
    private Queue<AdviceData> _openAdvices = new Queue<AdviceData>();
    
    private AdvicesKeeper _advicesKeeper;

    private readonly IGameSaveSystem _gameSaveSystem;

    public Advices(IAssetProvider assetProvider, IGameSaveSystem gameSaveSystem)
    {
      _gameSaveSystem = gameSaveSystem;
      _assetProvider = assetProvider;

      Initialize();
    }

    private void Initialize()
    {
      _advices = _assetProvider.GetAllResources<AdviceData>(AdvicesPath);
      
      _advicesKeeper = new AdvicesKeeper(_gameSaveSystem, _advices);
      _openAdvices = new Queue<AdviceData>(_advicesKeeper.Load());
    }

    public AdviceData OpenNextAdvice()
    {
      AdviceData advice = PullAdviceData();
      _advicesKeeper.Save(advice.Id);
      return advice;
    }

    public AdviceData GetAdvice() => 
      _openAdvices.Last();

    public List<AdviceData> GetOpenedAdvices() => 
      _openAdvices.ToList();

#if COMPLETE_GAME || SOUND_DESIGNER
    public void OpenAll()
    {
      foreach (AdviceData adviceData in _advices) 
        _advicesKeeper.Save(adviceData.Id);
    }
#endif

    private AdviceData PullAdviceData()
    {
      var availableAdvices = _advices
        .Where(p => !_openAdvices.Contains(p)).ToArray();

      if (AllAdvicesAreOpen())
        GetAdviceFromOpenQueue();
      else
        GetFirstAvailableAdvice(availableAdvices);

      return _openAdvices.Last();

      bool AllAdvicesAreOpen() => 
        availableAdvices.Length == 0;
    }

    private void GetFirstAvailableAdvice(AdviceData[] availableAdvices) => 
      _openAdvices.Enqueue(availableAdvices.First());

    private void GetAdviceFromOpenQueue()
    {
      AdviceData adviceData = _openAdvices.Dequeue();
      _openAdvices.Enqueue(adviceData);
    }
  }
}