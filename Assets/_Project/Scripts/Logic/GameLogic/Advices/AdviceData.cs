using Sirenix.OdinInspector;
using UnityEditor;
using UnityEngine;

namespace _Project.Scripts.Logic.GameLogic.Advices
{
  [CreateAssetMenu(fileName = "Advice Data", menuName = "Advices/Create Advice Data", order = 0)]
  public class AdviceData : ScriptableObject
  {
    [SerializeField] private string _id;
    [SerializeField] private string _titleKey;
    [SerializeField] private string _textKey;
    [SerializeField] private string _iconPath;
    
    public string Id => _id;
    public string TitleKey => _titleKey;
    public string TextKey => _textKey;
    public string IconPath => _iconPath;
    
#if UNITY_EDITOR
    [Button]
    public void GenerateGuid() => 
      _id = GUID.Generate().ToString();
#endif
  }
}