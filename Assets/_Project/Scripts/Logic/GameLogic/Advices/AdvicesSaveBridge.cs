using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;

namespace _Project.Scripts.Logic.GameLogic.Advices
{
  // Конвертирует сейвы из старой версии
  public class AdvicesBridge
  {
    private readonly Dictionary<string, string> _keysData = new Dictionary<string, string>()
    {
      {"1", "4895864eace3c1d43b4118b17376a3da"},
      {"2", "b42ef090d7434a442ac4bb39b43f3fd2"},
      {"3", "fa628dbfd85525d4b8c2cb50a05c522c"},
      {"4", "f34f76d8f04f940419c90bbe0484679c"},
      {"5", "1226a7acb0ba7c1489a6aed5e8d3b4be"},
      {"6", "721222b339f1e7a4288fd0f81f9136a5"},
      {"7", "bc88d16e0ba71cd48ac0a202a4a960cf"},
      {"8", "25ae2aee5b1cdf540a918f3412cf8964"},
      {"9", "8f2bbf385102a1345843bf405a4defc6"},
      {"10", "923792ac56c7290449cc31020c8da963"},
      {"11", "4ee2ba01feeaa7740830471297899e78"},
    };
    
    public List<string> Convert(string savedAdvices)
    {
      List<Saveable> saves = Load(savedAdvices);
      return (from save in saves where _keysData.ContainsKey(save.Key) select _keysData[save.Key]).ToList();
    }

    private List<Saveable> Load(string savedAdvices) =>
      string.IsNullOrEmpty(savedAdvices) ?
        new List<Saveable>() : 
        JsonConvert.DeserializeObject<List<Saveable>>(savedAdvices);
  }

  [System.Serializable]
  public struct Saveable
  {
    public string Key;
    public string Title;
    public string Text;
    public string IconPath;

    public Saveable(string key, string title, string text, string iconPath)
    {
      Key = key;
      Title = title;
      Text = text;
      IconPath = iconPath;
    }
  }
}