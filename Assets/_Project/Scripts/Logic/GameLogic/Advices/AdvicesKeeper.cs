using System.Collections.Generic;
using System.Linq;
using _Project.Scripts.Services._Interfaces;

namespace _Project.Scripts.Logic.GameLogic.Advices
{
  public class AdvicesKeeper
  {
    private readonly IGameSaveSystem _gameSaveSystem;
    private readonly AdviceData[] _advicesData;

    public AdvicesKeeper(IGameSaveSystem gameSaveSystem, AdviceData[] advicesData)
    {
      _advicesData = advicesData;
      _gameSaveSystem = gameSaveSystem;
      
      RestoreOldTypeAdvices();
    }
    
    public void Save(string adviceId)
    {
      if (_gameSaveSystem.Get().showedAdvicesIds.Contains(adviceId)) 
        _gameSaveSystem.Get().showedAdvicesIds.Remove(adviceId);

      _gameSaveSystem.Get().showedAdvicesIds.Add(adviceId);
      _gameSaveSystem.Save();
    }

    public IEnumerable<AdviceData> Load()
    {
      List<AdviceData> data = new List<AdviceData>();
      foreach (string showedAdvicesId in _gameSaveSystem.Get().showedAdvicesIds)
      {
        AdviceData adviceData = _advicesData.FirstOrDefault(p => p.Id == showedAdvicesId);
        data.Add(adviceData);
      }
      return data;
    }

    private void RestoreOldTypeAdvices()
    {     
      var advicesBridge = new AdvicesBridge();

      List<string> ids = advicesBridge.Convert(_gameSaveSystem.Get().showedAdvices);
      foreach (string id in ids)
        Save(id);
    }
  }
}