﻿using System.Collections.Generic;
using System.Linq;
using _Project.Scripts.Logic.Factories.Interfaces;
using _Project.Scripts.Logic.UI;
using _Project.Scripts.MonoBehaviours.Game;
using _Project.Scripts.MonoBehaviours.Tutorial;
using _Project.Scripts.MonoBehaviours.UI;
using _Project.Scripts.MonoBehaviours.UI.Interfaces;
using UnityEngine;

namespace _Project.Scripts.Logic.GameLogic.Tutorial
{
  public class TutorialPreparation
  {
    private readonly IUIFactory _uiFactory;

    public TutorialPreparation(IUIFactory uiFactory)
    {
      _uiFactory = uiFactory;
    }

    public void RetrieveDragParts(out GamePart dragTarget, out Part dragOrigin)
    {
      Parts uiParts = _uiFactory.Parts;

      Part origin = uiParts.GetParts().FirstOrDefault(p => p.gameObject.activeSelf);
      GamePart target = origin?.GamePart;

      dragTarget = target;
      dragOrigin = origin;
    }

    public void PrepareBeforeTutorial(List<ITutorialDependent> tutorialDependents, Part dragOrigin)
    {
      foreach (Part part in tutorialDependents.OfType<Part>()) 
        part.TutorialStrategy = new TutorialStrategyDisable(part);

      dragOrigin.TutorialStrategy = new TutorialStrategyIgnore(dragOrigin);

      foreach (ITutorialDependent tutorialDependent in tutorialDependents)
        tutorialDependent.OnTutorialStarted();
    }

    public void CleanupAfterTutorial(List<ITutorialDependent> tutorialDependents)
    {
      foreach (ITutorialDependent tutorialDependent in tutorialDependents)
        tutorialDependent.OnTutorialFinished();
    }

    public List<ITutorialDependent> RetrieveTutorialDependents()
    {
      List<ITutorialDependent> tutorialDependents =
        Object.FindObjectsOfType<MonoBehaviour>(true).OfType<ITutorialDependent>()
          .ToList();
      return tutorialDependents;
    }
  }
}