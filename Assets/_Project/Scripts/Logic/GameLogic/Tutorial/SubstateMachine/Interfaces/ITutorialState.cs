using System.Threading.Tasks;

namespace _Project.Scripts.Logic.GameLogic.Tutorial.SubstateMachine.Interfaces
{
  public interface ITutorialExitableState
  {
    void Exit();
  }

  public interface ITutorialState : ITutorialExitableState
  {
    void Enter();
  }

  public interface ITutorialPayloadedState<TPayload> : ITutorialExitableState
  {
    void Enter(TPayload gameLogic);
  }
}