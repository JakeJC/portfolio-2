using _Project.Scripts.Logic.Factories.Interfaces;
using _Project.Scripts.Logic.GameLogic.Tutorial.SubstateMachine.Interfaces;
using _Project.Scripts.Services._Interfaces;
using AppacheRemote.Scripts.Interfaces;

namespace _Project.Scripts.Logic.GameLogic.Tutorial.SubstateMachine.States
{
  public class EndStep : ITutorialState
  {
    private readonly TutorialStateMachine _tutorialStateMachine;
    
    private readonly IGameSaveSystem _gameSaveSystem;
    
    private readonly ITutorialFactory _tutorialFactory;
    private readonly IUIFactory _uiFactory;
    
    private IRemote _remote;

    public EndStep(TutorialStateMachine tutorialStateMachine, IGameSaveSystem gameSaveSystem, ITutorialFactory tutorialFactory, IUIFactory uiFactory, IRemote remote)
    {
      _remote = remote;
      _tutorialStateMachine = tutorialStateMachine;

      _tutorialFactory = tutorialFactory;
      _gameSaveSystem = gameSaveSystem;
      _uiFactory = uiFactory;
    }
    
    public async void Enter()
    {          
      _tutorialStateMachine.SendComplete();
      
      _uiFactory.GameStatePanel.AppearGameStatePanel(true);

      _gameSaveSystem.Get().TutorialComplete = true;
      _gameSaveSystem.Save();
      
      await _tutorialFactory.Hide();
      
      _tutorialFactory.StopMotion();
    }

    public void Exit()
    {}
  }
}