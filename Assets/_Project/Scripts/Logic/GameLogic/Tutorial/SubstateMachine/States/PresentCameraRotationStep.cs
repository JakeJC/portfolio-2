using System.Threading.Tasks;
using _Project.Scripts.Logic.Factories.Interfaces;
using _Project.Scripts.Logic.GameLogic.Tutorial.SubstateMachine.Interfaces;
using _Project.Scripts.Services.LeanTouchCamera.Scripts.Interfaces;
using _Project.Scripts.Services.Localization.Logic;
using _Project.Scripts.Tools;

namespace _Project.Scripts.Logic.GameLogic.Tutorial.SubstateMachine.States
{
  public class PresentCameraRotationStep : ITutorialState
  {
    private readonly TutorialStateMachine _tutorialStateMachine;
    private readonly ILocaleSystem _localeSystem;
    
    private readonly ITutorialFactory _tutorialFactory;
    private readonly IUIFactory _uiFactory;

    private readonly ICameraController _cameraController;

    public PresentCameraRotationStep(TutorialStateMachine tutorialStateMachine, ICameraController cameraController, ILocaleSystem localeSystem, 
      ITutorialFactory tutorialFactory, IUIFactory uiFactory)
    {
      _uiFactory = uiFactory;
      _cameraController = cameraController;
      _tutorialFactory = tutorialFactory;
      _localeSystem = localeSystem;
      _tutorialStateMachine = tutorialStateMachine;
    }
    
    public async void Enter()
    {
      _uiFactory.GameStatePanel.ButtonsGrp.ignoreParentGroups = false;
      _cameraController.Rotation.ClearBeezyStatus();
      
      _tutorialStateMachine.SendAnalyticsForStep();
      await PresentCameraRotation();
    }

    public void Exit()
    {}
    
    private async Task PresentCameraRotation()
    {
      _tutorialFactory.ShowText(_localeSystem.GetText(_tutorialStateMachine.LocalizeName, "Tutorial.camera")); 
      _tutorialFactory.ShowFinger(); 
      
      _tutorialFactory.SpawnCameraPointsPanel();
      _tutorialFactory.StartAbMotion(_tutorialFactory.CameraPoints.Start, _tutorialFactory.CameraPoints.End);
      
      _tutorialStateMachine.EnableInterface(false);

      await AsyncEvents.TaskFromEvent(x =>
        _cameraController.Rotation.OnRotateStart += x, x =>
        _cameraController.Rotation.OnRotateStart -= x);

      _tutorialFactory.StopMotion();
      await _tutorialFactory.Hide();
      _tutorialFactory.RemoveCameraPointsPanel();
      
      _tutorialStateMachine.EnableInterface(true);
      
      _tutorialStateMachine.Enter<PresentCameraZoomStep>();
    }
  }
}