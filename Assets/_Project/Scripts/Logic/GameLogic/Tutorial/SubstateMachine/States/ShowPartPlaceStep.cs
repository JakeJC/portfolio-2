using System.Collections.Generic;
using System.Threading.Tasks;
using _Project.Scripts.Logic.Factories;
using _Project.Scripts.Logic.Factories.Interfaces;
using _Project.Scripts.Logic.GameLogic.Tutorial.SubstateMachine.Interfaces;
using _Project.Scripts.MonoBehaviours.Game;
using _Project.Scripts.MonoBehaviours.UI;
using _Project.Scripts.MonoBehaviours.UI.Interfaces;
using _Project.Scripts.Services.Localization.Logic;

namespace _Project.Scripts.Logic.GameLogic.Tutorial.SubstateMachine.States
{
  public class ShowPartPlaceStep : ITutorialState
  {
    private readonly TutorialStateMachine _tutorialStateMachine;
    
    private readonly TutorialPreparation _tutorialPreparation;
    private readonly TutorialTasks _tutorialTasks;
    
    private readonly IUIFactory _uiFactory;
    private readonly ITutorialFactory _tutorialFactory;
    
    private readonly ILocaleSystem _localeSystem;
    private List<ITutorialDependent> _tutorialDependents;

    public ShowPartPlaceStep(TutorialStateMachine tutorialStateMachine, ILocaleSystem localeSystem, TutorialPreparation tutorialPreparation, 
      TutorialTasks tutorialTasks, ITutorialFactory tutorialFactory, IUIFactory uiFactory)
    {
      _tutorialStateMachine = tutorialStateMachine;
      
      _localeSystem = localeSystem;
      
      _tutorialPreparation = tutorialPreparation;
      _tutorialTasks = tutorialTasks;

      _tutorialFactory = tutorialFactory;
      _uiFactory = uiFactory;
    }

    public void Exit()
    {  
      _tutorialPreparation.CleanupAfterTutorial(_tutorialDependents);
    }

    public async void Enter()
    {
      _tutorialDependents = _tutorialPreparation.RetrieveTutorialDependents();
      await LaunchStep();
    }

    private async Task LaunchStep()
    {  
      _uiFactory.GameStatePanel.AppearGameStatePanel(false);

      for (int i = 0; i < _uiFactory.Parts.GetParts().Length; i++)
      {
        _uiFactory.GameStatePanel.CanvasGroup.interactable = false;

        _tutorialStateMachine.SendAnalyticsForStep();

        await PresentDragMotion(_tutorialDependents, i == 0);
        await _tutorialStateMachine.TransitionBetweenSteps();
      }
      
      _tutorialStateMachine.Enter<WaitForMechanismLaunchStep>();
    }

    private async Task PresentDragMotion(List<ITutorialDependent> tutorialDependents, bool withText = true)
    {
      _tutorialPreparation.RetrieveDragParts(out GamePart gamePart, out Part uiPart);
      _tutorialPreparation.PrepareBeforeTutorial(tutorialDependents, uiPart);

      gamePart.AppearWithOutline();

      Task taskOnPinEvent = _tutorialTasks.GetTaskForOnPinEvent(uiPart);

      _tutorialFactory.ShowFinger();
      if(withText)
        _tutorialFactory.ShowText(_localeSystem.GetText(_tutorialStateMachine.LocalizeName, "Tutorial.ball"));
      
      _tutorialFactory.StartAbMotion(uiPart.transform, gamePart.PinPoint == null? 
        gamePart.transform :
        gamePart.PinPoint.transform);

      await taskOnPinEvent;

      _tutorialFactory.StopMotion();
      await _tutorialFactory.Hide();
    }
  }
}