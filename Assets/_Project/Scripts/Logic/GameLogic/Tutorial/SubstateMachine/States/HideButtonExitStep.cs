using System.Threading.Tasks;
using _Project.Scripts.Features.HideUIButton;
using _Project.Scripts.Logic.Factories.Interfaces;
using _Project.Scripts.Logic.GameLogic.Tutorial.SubstateMachine.Interfaces;

namespace _Project.Scripts.Logic.GameLogic.Tutorial.SubstateMachine.States
{
  public class HideButtonExitStep : ITutorialState
  {
    private readonly TutorialStateMachine _tutorialStateMachine;
    
    private readonly TutorialTasks _tutorialTasks;
    
    private readonly IUIFactory _uiFactory;

    public HideButtonExitStep(TutorialStateMachine tutorialStateMachine, TutorialTasks tutorialTasks, IUIFactory uiFactory)
    {
      _uiFactory = uiFactory;
      _tutorialTasks = tutorialTasks;
      _tutorialStateMachine = tutorialStateMachine;
    }

    public async void Enter()
    {
      _tutorialStateMachine.SendAnalyticsForStep();
      await ClickHideButtonForExit(_uiFactory.HideUIButton.HandleClick);
    }

    public void Exit()
    {}
    
    private async Task ClickHideButtonForExit(HandleClick button)
    {
      _tutorialStateMachine.EnableInterface(false);
      
      await _tutorialTasks.GetButtonClickEvent(button);
      
      _tutorialStateMachine.EnableInterface(true);
      
      _uiFactory.HideUIButton.CanvasGroup.ignoreParentGroups = false;

      _tutorialStateMachine.Enter<CongratulationsStep>();
    }
  }
}