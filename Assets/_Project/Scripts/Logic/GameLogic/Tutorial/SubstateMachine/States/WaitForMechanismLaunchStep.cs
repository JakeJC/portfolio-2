using System.Threading.Tasks;
using _Project.Scripts.Logic.Factories;
using _Project.Scripts.Logic.Factories.Interfaces;
using _Project.Scripts.Logic.GameLogic.Interfaces;
using _Project.Scripts.Logic.GameLogic.Tutorial.SubstateMachine.Interfaces;
using AppacheRemote.Scripts.Interfaces;
using DG.Tweening;

namespace _Project.Scripts.Logic.GameLogic.Tutorial.SubstateMachine.States
{
  public class WaitForMechanismLaunchStep : ITutorialState
  {
    private readonly TutorialStateMachine _tutorialStateMachine;
    private readonly TutorialTasks _tutorialTasks;
    
    private readonly IGameLogic _gameLogic;
    private readonly IUIFactory _uiFactory;
    private readonly IRemote _remote;

    public WaitForMechanismLaunchStep(TutorialStateMachine tutorialStateMachine, IGameLogic gameLogic, TutorialTasks tutorialTasks, IUIFactory uiFactory, IRemote remote)
    {
      _remote = remote;
      _tutorialStateMachine = tutorialStateMachine;
      _gameLogic = gameLogic;
      _tutorialTasks = tutorialTasks;
      _uiFactory = uiFactory;
    }
    
    public async void Enter()
    {      
      _uiFactory.GameStatePanel.ButtonsGrp.ignoreParentGroups = true;
      
      _uiFactory.GameStatePanel.ButtonsGrp.alpha = 0.1f;
      _uiFactory.GameStatePanel.ButtonsGrp.DOFade(1, 0.25f);
      
      _tutorialStateMachine.SendAnalyticsForStep();
      await LaunchStep();
    }

    public void Exit() => 
      _uiFactory.GameStatePanel.AppearGameStatePanel(false);

    private async Task LaunchStep()
    {
      await _tutorialTasks.WaitForMechanismLaunch(_gameLogic);
     
      _uiFactory.GameStatePanel.ButtonsGrp.ignoreParentGroups = false;
      _tutorialStateMachine.Enter<CongratulationsStep>();
    }
  }
}