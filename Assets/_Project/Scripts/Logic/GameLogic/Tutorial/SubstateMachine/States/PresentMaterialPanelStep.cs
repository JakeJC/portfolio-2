using System.Linq;
using System.Threading.Tasks;
using _Project.Scripts.Logic.Factories;
using _Project.Scripts.Logic.Factories.Interfaces;
using _Project.Scripts.Logic.GameLogic.Tutorial.SubstateMachine.Interfaces;
using _Project.Scripts.MonoBehaviours.UI;
using _Project.Scripts.Services.Localization.Logic;
using UnityEngine;

namespace _Project.Scripts.Logic.GameLogic.Tutorial.SubstateMachine.States
{
  public class PresentMaterialPanelStep : ITutorialState
  {
    private readonly TutorialStateMachine _tutorialStateMachine;
    
    private readonly ILocaleSystem _localeSystem;
    
    private readonly TutorialTasks _tutorialTasks;
    
    private readonly ITutorialFactory _tutorialFactory;
    private readonly IUIFactory _uiFactory;

    public PresentMaterialPanelStep(TutorialStateMachine tutorialStateMachine, ILocaleSystem localeSystem, TutorialTasks tutorialTasks,
      ITutorialFactory tutorialFactory, IUIFactory uiFactory)
    {
      _uiFactory = uiFactory;
      _tutorialFactory = tutorialFactory;
      _tutorialTasks = tutorialTasks;
      _localeSystem = localeSystem;
      _tutorialStateMachine = tutorialStateMachine;      
    }
    
    public async void Enter()
    {
      _tutorialStateMachine.SendAnalyticsForStep();
      await PresentMaterialPanel(_uiFactory.GameStatePanel.MaterialButtons.ToArray());
    }

    public void Exit()
    {} 
    
    private async Task PresentMaterialPanel(MaterialButton[] materialButtons)
    {
      _tutorialFactory.ShowFinger(true);
      _tutorialFactory.ShowText(_localeSystem.GetText(_tutorialStateMachine.LocalizeName, "Tutorial.color"));
      MaterialButton targetButton = materialButtons.FirstOrDefault(p => !p.IsActive);
      
      _tutorialFactory.StartAbPointing(targetButton.transform.position, new Vector3(25, -50, 0));

      foreach (MaterialButton materialButton in materialButtons) 
        materialButton.CanvasGroup.ignoreParentGroups = true;
      
      _tutorialStateMachine.EnableInterface(false);

      await _tutorialTasks.GetTaskForMaterialChangeEvent(targetButton);
      
      _tutorialFactory.StopMotion();
      await _tutorialFactory.Hide();  
      
      foreach (MaterialButton materialButton in materialButtons) 
        materialButton.CanvasGroup.ignoreParentGroups = false;
      
      _tutorialStateMachine.EnableInterface(true);
      
      _tutorialStateMachine.Enter<PresentWatchingMechanismStep>();
    }
  }
}