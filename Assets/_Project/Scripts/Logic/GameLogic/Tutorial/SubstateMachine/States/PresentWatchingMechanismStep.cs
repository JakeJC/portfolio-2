using System.Threading.Tasks;
using _Project.Scripts.Logic.Factories;
using _Project.Scripts.Logic.Factories.Interfaces;
using _Project.Scripts.Logic.GameLogic.Tutorial.SubstateMachine.Interfaces;
using _Project.Scripts.Services.Localization.Logic;

namespace _Project.Scripts.Logic.GameLogic.Tutorial.SubstateMachine.States
{
  public class PresentWatchingMechanismStep : ITutorialState
  {
    private readonly TutorialStateMachine _tutorialStateMachine;
    
    private readonly ILocaleSystem _localeSystem;
    private readonly ITutorialFactory _tutorialFactory;

    public PresentWatchingMechanismStep(TutorialStateMachine tutorialStateMachine, ILocaleSystem localeSystem,
      ITutorialFactory tutorialFactory)
    {
      _tutorialStateMachine = tutorialStateMachine;
      
      _tutorialFactory = tutorialFactory;
      _localeSystem = localeSystem;
    }
    
    public async void Enter()
    {
      _tutorialStateMachine.SendAnalyticsForStep();
      await PresentWatchingMechanism();
    }

    public void Exit()
    {}
    
    private async Task PresentWatchingMechanism()
    {    
      _tutorialFactory.ShowText(_localeSystem.GetText(_tutorialStateMachine.LocalizeName, "Tutorial.watchingMechanism")); 
      _tutorialStateMachine.EnableInterface(false);

      await Task.Delay(5000);
      await _tutorialFactory.Hide(); 
      
      _tutorialStateMachine.EnableInterface(true);
      
      _tutorialStateMachine.Enter<PresentHideButtonStep>();
    }
  }
}