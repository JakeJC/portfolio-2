using System.Threading.Tasks;
using _Project.Scripts.Logic.Factories;
using _Project.Scripts.Logic.Factories.Interfaces;
using _Project.Scripts.Logic.GameLogic.Tutorial.SubstateMachine.Interfaces;
using _Project.Scripts.MonoBehaviours.Tutorial;
using _Project.Scripts.Services.Localization.Logic;
using AppacheRemote.Scripts.Interfaces;
using UnityEngine;

namespace _Project.Scripts.Logic.GameLogic.Tutorial.SubstateMachine.States
{
  public class CongratulationsStep : ITutorialState
  {
    private readonly TutorialStateMachine _tutorialStateMachine;

    private readonly ILocaleSystem _localeSystem;
    
    private readonly ITutorialFactory _tutorialFactory;
    private readonly IUIFactory _uiFactory;

    private readonly IRemote _remote;
    
    private readonly TutorialTasks _tutorialTasks;

    public CongratulationsStep(TutorialStateMachine tutorialStateMachine, ILocaleSystem localeSystem,  ITutorialFactory tutorialFactory, 
      TutorialTasks tutorialTasks, IUIFactory uiFactory, IRemote remote)
    {
      _tutorialStateMachine = tutorialStateMachine;

      _localeSystem = localeSystem;
      _tutorialFactory = tutorialFactory;
      _tutorialTasks = tutorialTasks;
      _uiFactory = uiFactory;
      
      _remote = remote;
    }
    
    public void Exit()
    {}

    public async void Enter()
    {    
      _tutorialStateMachine.SendAnalyticsForStep();
      await PresentCongratulations();
    }
    
    private async Task PresentCongratulations()
    {
      _tutorialStateMachine.EnableInterface(false);
      
      _uiFactory.GameStatePanel.AppearGameStatePanel(false);
      
      _tutorialFactory.ShowText(_localeSystem.GetText(_tutorialStateMachine.LocalizeName, "Tutorial.complete"));
      
      var tutorialClickListener = new GameObject("Tutorial Click Listener").AddComponent<TutorialClickListener>();
      await _tutorialTasks.WaitForScreenClick(tutorialClickListener); 
      Object.Destroy(tutorialClickListener.gameObject);
      
      await _tutorialFactory.Hide();  
      _tutorialStateMachine.EnableInterface(true);    
      
      _tutorialStateMachine.Enter<EndStep>();
    }
  }
}