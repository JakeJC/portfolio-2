using System.Threading.Tasks;
using _Project.Scripts.Features.HideUIButton;
using _Project.Scripts.Logic.Factories.Interfaces;
using _Project.Scripts.Logic.GameLogic.Tutorial.SubstateMachine.Interfaces;
using _Project.Scripts.Services.Localization.Logic;

namespace _Project.Scripts.Logic.GameLogic.Tutorial.SubstateMachine.States
{
  public class PresentHideButtonStep : ITutorialState
  {
    private readonly TutorialStateMachine _tutorialStateMachine;
    
    private readonly ILocaleSystem _localeSystem;
    private readonly TutorialTasks _tutorialTasks;
    
    private readonly ITutorialFactory _tutorialFactory;
    private readonly IUIFactory _uiFactory;

    public PresentHideButtonStep(TutorialStateMachine tutorialStateMachine, ILocaleSystem localeSystem,
      ITutorialFactory tutorialFactory, TutorialTasks tutorialTasks, IUIFactory uiFactory)
    {
      _uiFactory = uiFactory;
      _tutorialTasks = tutorialTasks;
      _tutorialFactory = tutorialFactory;
      _localeSystem = localeSystem;
      _tutorialStateMachine = tutorialStateMachine;
    }
    
    public async void Enter()
    {
      _tutorialStateMachine.SendAnalyticsForStep();
      await PresentHideButton(_uiFactory.HideUIButton.HandleClick);
    }

    public void Exit()
    {}
    
    private async Task PresentHideButton(HandleClick button)
    {      
      _uiFactory.HideUIButton.CanvasGroup.ignoreParentGroups = true;

      _tutorialFactory.ShowText(_localeSystem.GetText(_tutorialStateMachine.LocalizeName, "Tutorial.hideButton")); 
      _tutorialStateMachine.EnableInterface(false);
      
      await _tutorialTasks.GetButtonClickEvent(button);
      await _tutorialFactory.Hide();  
      
      _tutorialStateMachine.EnableInterface(true);
      
      _tutorialStateMachine.Enter<HideButtonExitStep>();
    }
  }
}