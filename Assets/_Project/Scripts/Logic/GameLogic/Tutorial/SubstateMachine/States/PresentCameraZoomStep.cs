using System.Threading.Tasks;
using _Project.Scripts.Logic.Factories;
using _Project.Scripts.Logic.Factories.Interfaces;
using _Project.Scripts.Logic.GameLogic.Tutorial.SubstateMachine.Interfaces;
using _Project.Scripts.Services.LeanTouchCamera.Scripts.Interfaces;
using _Project.Scripts.Services.Localization.Logic;
using _Project.Scripts.Tools;

namespace _Project.Scripts.Logic.GameLogic.Tutorial.SubstateMachine.States
{
  public class PresentCameraZoomStep : ITutorialState
  {
    private readonly TutorialStateMachine _tutorialStateMachine;
    
    private readonly ICameraController _cameraController;
    private readonly ILocaleSystem _localeSystem;
    
    private readonly ITutorialFactory _tutorialFactory;

    public PresentCameraZoomStep(TutorialStateMachine tutorialStateMachine, ICameraController cameraController, ILocaleSystem localeSystem, 
      ITutorialFactory tutorialFactory)
    {
      _localeSystem = localeSystem;
      _tutorialFactory = tutorialFactory;
      _cameraController = cameraController;
      _tutorialStateMachine = tutorialStateMachine;
    }
    
    public async void Enter()
    {   
      _cameraController.Zoom.ClearBeezyStatus();
      _tutorialStateMachine.SendAnalyticsForStep();
      
      await PresentCameraZoom();
    }

    public void Exit()
    {}
    
    private async Task PresentCameraZoom()
    {
      _tutorialFactory.ShowText(_localeSystem.GetText(_tutorialStateMachine.LocalizeName, "Tutorial.zoom")); 
      _tutorialFactory.TextPanel.ZoomPointer.SetActive(true);
      
      _tutorialStateMachine.EnableInterface(false);

      await AsyncEvents.TaskFromEvent(x =>
        _cameraController.Zoom.OnZoomStart += x, x =>
        _cameraController.Zoom.OnZoomStart -= x);
     
      _tutorialFactory.TextPanel.ZoomPointer.SetActive(false);
      await _tutorialFactory.Hide();

      _tutorialStateMachine.EnableInterface(true);
      
      _tutorialStateMachine.Enter<PresentMaterialPanelStep>();
    }
  }
}