using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using _Project.Scripts.Logic.Factories;
using _Project.Scripts.Logic.Factories.Interfaces;
using _Project.Scripts.Logic.GameLogic.Interfaces;
using _Project.Scripts.Logic.GameLogic.Tutorial.SubstateMachine.Interfaces;
using _Project.Scripts.Logic.GameLogic.Tutorial.SubstateMachine.States;
using _Project.Scripts.MonoBehaviours.Game.Interfaces;
using _Project.Scripts.Services._Interfaces;
using _Project.Scripts.Services.LeanTouchCamera.Scripts.Interfaces;
using _Project.Scripts.Services.Localization.Logic;
using AppacheAnalytics.Scripts.Interfaces;
using AppacheRemote.Scripts.Interfaces;

namespace _Project.Scripts.Logic.GameLogic.Tutorial.SubstateMachine
{
  public class TutorialStateMachine
  {   
    public string LocalizeName = "Tutorial";

    private readonly Dictionary<Type, ITutorialExitableState> _states;
    private ITutorialExitableState _activeExitableState;

    private readonly IAnalytics _analytics;
    private readonly IUIFactory _uiFactory;

    private readonly IMechanism _mechanism;

    private readonly IRemote _remote;
    
    private int _step = 1;

    public TutorialStateMachine(IGameLogic gameLogic, IGameSaveSystem gameSaveSystem, IAnalytics analytics, 
      ILocaleSystem localeSystem, ICameraController cameraController, IUIFactory uiFactory, ITutorialFactory tutorialFactory,
      IMechanism mechanism, IRemote remote)
    {
      _remote = remote;
      _mechanism = mechanism;
      _uiFactory = uiFactory;
      _analytics = analytics;
      
      TutorialTasks tutorialTasks = new TutorialTasks();
      TutorialPreparation tutorialPreparation = new TutorialPreparation(uiFactory);
      
      _states = new Dictionary<Type, ITutorialExitableState>()
      {
        [typeof(ShowPartPlaceStep)] = new ShowPartPlaceStep(this, localeSystem, tutorialPreparation, tutorialTasks, tutorialFactory, uiFactory),
        [typeof(WaitForMechanismLaunchStep)] = new WaitForMechanismLaunchStep(this, gameLogic, tutorialTasks, uiFactory, _remote),
        [typeof(PresentCameraRotationStep)] = new PresentCameraRotationStep(this, cameraController, localeSystem, tutorialFactory, uiFactory),
        [typeof(PresentCameraZoomStep)] = new PresentCameraZoomStep(this, cameraController, localeSystem, tutorialFactory),
        [typeof(PresentMaterialPanelStep)] = new PresentMaterialPanelStep(this, localeSystem, tutorialTasks, tutorialFactory, uiFactory),
        [typeof(PresentWatchingMechanismStep)] = new PresentWatchingMechanismStep(this, localeSystem, tutorialFactory),
        [typeof(PresentHideButtonStep)] = new PresentHideButtonStep(this, localeSystem, tutorialFactory, tutorialTasks, uiFactory),
        [typeof(HideButtonExitStep)] = new HideButtonExitStep(this, tutorialTasks, uiFactory),
        [typeof(CongratulationsStep)] = new CongratulationsStep(this, localeSystem, tutorialFactory, tutorialTasks, uiFactory, _remote),
        [typeof(EndStep)] = new EndStep(this, gameSaveSystem, tutorialFactory, uiFactory, _remote)
      };
    }

    public void Launch()
    {
#if TEST
      Enter<EndStep>();
#else
      Enter<ShowPartPlaceStep>();
#endif
    }

    public void Enter<TState>() where TState : class, ITutorialState
    {
      ITutorialState exitableState = ChangeState<TState>();
      exitableState.Enter();
    }

    public async Task TransitionBetweenSteps(int delay = 500) =>
      await Task.Delay(delay);

    public void EnableInterface(bool value)
    {
      _uiFactory.GameStatePanel.CanvasGroup.interactable = value;
      _uiFactory.PartsPanel.CanvasGroup.interactable = value;
    }

    public void SendAnalyticsForStep()
    {
      _analytics?.Send("ev_tut_start",
        new Dictionary<string, object>() {{"step", _step}, {"level_id", _mechanism.GetId()}});
      _step++;
    }
    
    public void SendComplete() => 
      _analytics?.Send("ev_tut_complete", new Dictionary<string, object>() {{"level_id", _mechanism.GetId()}});

    private TState ChangeState<TState>() where TState : class, ITutorialExitableState
    {
      _activeExitableState?.Exit();
      TState state = GetState<TState>();
      _activeExitableState = state;
      return state;
    }

    private TState GetState<TState>() where TState : class, ITutorialExitableState =>
      _states[typeof(TState)] as TState;
  }
}