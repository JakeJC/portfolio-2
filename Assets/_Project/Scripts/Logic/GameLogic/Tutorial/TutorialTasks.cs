﻿using System.Threading.Tasks;
using _Project.Scripts.Features.HideUIButton;
using _Project.Scripts.Logic.GameLogic.Interfaces;
using _Project.Scripts.MonoBehaviours.Game;
using _Project.Scripts.MonoBehaviours.Tutorial;
using _Project.Scripts.MonoBehaviours.UI;
using _Project.Scripts.Tools;

namespace _Project.Scripts.Logic.GameLogic.Tutorial
{
  public class TutorialTasks
  {
    public Task GetButtonClickEvent(HandleClick button)
    {
      Task clickEvent = AsyncEvents.TaskFromEvent(
        h => button.OnClick += h,
        h => button.OnClick -= h);
      return clickEvent;
    }

    public Task GetTaskForOnPinEvent(Part uiPart)
    {
      var dragablePart = uiPart.GetComponentInChildren<DragablePart>(true);
      Task<GamePart> taskOnPinEvent = AsyncEvents.TaskFromEvent<GamePart>(
        h => dragablePart.OnPin += h,
        h => dragablePart.OnPin -= h);
      return taskOnPinEvent;
    }

    public Task GetTaskForMaterialChangeEvent(MaterialButton button)
    {
      Task taskOnChangeMaterial = AsyncEvents.TaskFromEvent(
        h => button.OnClickEvent += h,
        h => button.OnClickEvent -= h);

      return taskOnChangeMaterial;
    }

    private Task GetTaskForMechanismLaunch(IGameLogic gameLogic)
    {
      Task onGameComplete = AsyncEvents.TaskFromEvent(
        h => gameLogic.OnLaunch += h,
        h => gameLogic.OnLaunch -= h);

      return onGameComplete;
    }

    public async Task WaitForMechanismLaunch(IGameLogic gameLogic) => 
      await this.GetTaskForMechanismLaunch(gameLogic);

    public Task WaitForScreenClick(TutorialClickListener tutorialClickListener)
    {
      Task waitClick = AsyncEvents.TaskFromEvent(
        h => tutorialClickListener.OnClick += h,
        h => tutorialClickListener.OnClick -= h);

      return waitClick;
    }
  }
}