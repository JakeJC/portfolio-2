using _Project.Scripts.Data.Scriptables;

namespace _Project.Scripts.Logic.GameLogic.Meta.Interfaces
{
  public interface IWorldInfo
  {
    WorldData GetWorldDataForMechanism(string mechanismId);
    WorldData GetWorldDataById(string worldId);
    WorldData[] GetWorldsData();
    void SetUsersChoice(string worldId);
    string GetUsersChoiceWorldId();
    void WorldsOrder();
    bool WorldIsAvailable();
    bool WorldsIsEnded();
    bool WorldIsOutOfMechanisms(string worldId);
    WorldData[] GetSortedWorldsData();
  }
}