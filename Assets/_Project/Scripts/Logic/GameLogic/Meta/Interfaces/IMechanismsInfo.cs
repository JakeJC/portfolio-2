using _Project.Scripts.Data.Scriptables;

namespace _Project.Scripts.Logic.GameLogic.Meta.Interfaces
{
  public interface IMechanismsInfo
  {
    MechanismData GetMechanismDataById(string mechanismId);
    MechanismData[] GetMechanisms();
    string GetCurrentMechanismAddress(string worldId);
    string GetWorldIdForMechanism(string mechanismId);
    string GetMechanismAddressById(string mechanismId);
    int GetLevelOrderNumber();
    bool MarkedAsTutorial { get; set; }
    MechanismData[] GetUnlockedMechanisms();
    bool IsSomethingLocked();
    MechanismData GetAvailableMechanismForWorld(string worldId);
    bool IsAllNotComplete();
  }
}