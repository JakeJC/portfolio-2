namespace _Project.Scripts.Logic.GameLogic.Meta.Interfaces
{
  public interface ITextsInfo
  {
    string GetUniqueIndex();
    void ClearUsedIndexes();
  }
}