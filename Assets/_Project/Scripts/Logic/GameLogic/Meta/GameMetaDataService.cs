﻿using System.Linq;
using _Project.Scripts.Data.Scriptables;
using _Project.Scripts.Logic.GameLogic.Interfaces;
using _Project.Scripts.Logic.GameLogic.Main.FirstSession;
using _Project.Scripts.Logic.GameLogic.Meta.Interfaces;
using _Project.Scripts.Logic.GameLogic.Meta.Meta_Subgroups;
using _Project.Scripts.Services._Interfaces;

namespace _Project.Scripts.Logic.GameLogic.Meta
{
  public class GameMetaDataService : IGameDataProvider
  {
    private const string WorldConfigsPath = "_World Configs";
    private const string MechanismConfigsPath = "_Mechanism Configs";

    public IWorldInfo WorldsInfo { get; private set; }
    public IMechanismsInfo MechanismsInfo { get; private set;}
    public ITextsInfo EndStateTextInfo { get; private set;}
    
    public ITextsInfo LikeStateTextInfo { get; private set;}
    
    public ITextsInfo RelaxStateTextInfo { get; private set;}

    public IFirstSession FirstSession { get; }

    private readonly IGameSaveSystem _gameSaveSystem;
    private readonly IAssetProvider _assetProvider;
    
    public GameMetaDataService(IGameSaveSystem gameSaveSystem, IAssetProvider assetProvider)
    {
      _assetProvider = assetProvider;
      _gameSaveSystem = gameSaveSystem;
      
      FirstSession = new FirstSession(gameSaveSystem);
    }

    public void Initialize()
    {
      MechanismData[] mechanismData = _assetProvider.GetAllResources<MechanismData>(MechanismConfigsPath);

      WorldData[] worldsData = _assetProvider.GetAllResources<WorldData>(WorldConfigsPath);

      WorldsInfo = new WorldInfo(_gameSaveSystem, mechanismData, worldsData);
      MechanismsInfo = new MechanismsInfo(_gameSaveSystem, mechanismData);
      
      EndStateTextInfo = new TextsInfo(1, 7);
      LikeStateTextInfo = new TextsInfo(1, 6);
      RelaxStateTextInfo = new TextsInfo(1, 6);
    }

    public bool IsAllAdvicesLocked() => 
      _gameSaveSystem.Get().showedAdvicesIds.Count == 0;
  }
}