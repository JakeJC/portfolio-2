using System.Collections.Generic;
using System.Linq;
using _Project.Scripts.Logic.GameLogic.Meta.Interfaces;

namespace _Project.Scripts.Logic.GameLogic.Meta.Meta_Subgroups
{
  public class TextsInfo : ITextsInfo
  {   
    private readonly List<int> _phrasesIndexes = new List<int>();
    private readonly List<int> _usedPhrasesIndexes = new List<int>();

    public TextsInfo(int min, int max) => 
      InitializeFavorPhrasesIndexes(min, max);

    public string GetUniqueIndex()
    {
      List<int> phrasesForRandom = _phrasesIndexes.Where(phrasesIndex => !_phrasesIndexes.Contains(phrasesIndex)).ToList();

      int index;
      
      if (phrasesForRandom.Count > 0)
      {
        index = phrasesForRandom[UnityEngine.Random.Range(0, phrasesForRandom.Count)];
        _usedPhrasesIndexes.Add(index);
      }
      else
        index = _phrasesIndexes[UnityEngine.Random.Range(0, _phrasesIndexes.Count)];

      return index.ToString();
    }

    public void ClearUsedIndexes() => 
      _usedPhrasesIndexes.Clear();

    private void InitializeFavorPhrasesIndexes(int min, int max)
    {
      for (var i = min; i < max; i++)
        _phrasesIndexes.Add(i);
    }
  }
}