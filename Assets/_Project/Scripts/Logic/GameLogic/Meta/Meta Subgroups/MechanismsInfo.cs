using System;
using System.Linq;
using _Project.Scripts.Data.Scriptables;
using _Project.Scripts.Data.Serializable;
using _Project.Scripts.Logic.GameLogic.Meta.Interfaces;
using _Project.Scripts.Services._Interfaces;
using UnityEngine;

namespace _Project.Scripts.Logic.GameLogic.Meta.Meta_Subgroups
{
  public class MechanismsInfo : IMechanismsInfo
  {
    private readonly MechanismData[] _mechanismData;

    private readonly IGameSaveSystem _gameSaveSystem;

    public bool MarkedAsTutorial { get; set; }

    public MechanismsInfo(IGameSaveSystem gameSaveSystem, MechanismData[] mechanismData)
    {
      _gameSaveSystem = gameSaveSystem;
      _mechanismData = mechanismData;
    }

    public MechanismData GetMechanismDataById(string mechanismId) =>
      _mechanismData.FirstOrDefault(p => p.MechanismId == mechanismId);

    public MechanismData[] GetMechanisms() =>
      _mechanismData;

    public MechanismData[] GetUnlockedMechanisms()
    {
      return _mechanismData.Where(IsMechanismLaunched).ToArray();

      bool IsMechanismLaunched(MechanismData mechanism) =>
        _gameSaveSystem.Get().GetState(mechanism.MechanismId) == State.Launched;
    }

    public bool IsSomethingLocked() =>
      _mechanismData.Any(IsMechanismIncomplete);

    public bool IsAllNotComplete() =>
      _mechanismData.All(IsMechanismIncomplete);

    public MechanismData GetAvailableMechanismForWorld(string worldId) =>
      _mechanismData.FirstOrDefault(p => p.WorldId == worldId
                                         && IsMechanismIncomplete(p));

    private bool IsMechanismIncomplete(MechanismData mechanism) =>
      _gameSaveSystem.Get().GetState(mechanism.MechanismId) == State.Incomplete;

    public string GetCurrentMechanismAddress(string worldId)
    {
      var orderMechanisms = _mechanismData
        .OrderByDescending(p => p.WorldId == worldId).ToList();

      foreach (MechanismData data in orderMechanisms)
      {
        if (_gameSaveSystem.Get().GetState(data.MechanismId) == State.Incomplete)
          return data.AssetAddress;
      }

      throw new Exception("Больше нет механизмов");
    }

    public string GetWorldIdForMechanism(string mechanismId) =>
      _mechanismData.FirstOrDefault(p => p.MechanismId == mechanismId)?.WorldId;

    public string GetMechanismAddressById(string mechanismId) =>
      _mechanismData.FirstOrDefault(p => p.MechanismId == mechanismId)?.AssetAddress;

    public int GetLevelOrderNumber()
    {
      int count = 1;
      count -= _gameSaveSystem.Get().GroupWithTutorialComplete ? 1 : 0;
      count += _mechanismData.Count(mechanismData => _gameSaveSystem.Get().GetState(mechanismData.MechanismId) == State.Launched);

      return count;
    }
  }
}