using System.Collections.Generic;
using System.Linq;
using _Project.Scripts.Data.Scriptables;
using _Project.Scripts.Data.Serializable;
using _Project.Scripts.Logic.GameLogic.Meta.Interfaces;
using _Project.Scripts.Services._Interfaces;
using UnityEngine;

namespace _Project.Scripts.Logic.GameLogic.Meta.Meta_Subgroups
{
  public class WorldInfo : IWorldInfo
  {
    private readonly MechanismData[] _mechanismData;
    
    private readonly WorldData[] _worldsData;
    private WorldData[] _sortedWorldsData;

    private readonly IGameSaveSystem _gameSaveSystem;
    
    private string _worldId;

    public WorldInfo(IGameSaveSystem gameSaveSystem, MechanismData[] mechanismData, WorldData[] worldsData)
    {
      _gameSaveSystem = gameSaveSystem;
      _worldsData = worldsData;
      _mechanismData = mechanismData;

      SortingWorlds(_worldsData);
    }
    
    public void SetUsersChoice(string worldId) => 
      _worldId = worldId;

    public string GetUsersChoiceWorldId() => 
      _worldId;
    
    public WorldData GetWorldDataForMechanism(string mechanismId)
    {
      MechanismData mechanismData = _mechanismData.FirstOrDefault(p => p.MechanismId == mechanismId);
      return mechanismData ? _worldsData.FirstOrDefault(p => p.WorldId == mechanismData?.WorldId) : null;
    }

    public WorldData GetWorldDataById(string worldId) =>
      _worldsData.FirstOrDefault(p => p.WorldId == worldId);
    
    public WorldData[] GetWorldsData() => 
      _worldsData;
    
    public WorldData[] GetSortedWorldsData() => 
      _sortedWorldsData;
    
    public void WorldsOrder() => 
      SortingWorlds(_worldsData);

    public bool WorldIsAvailable() => 
      _mechanismData.Any(p => _gameSaveSystem.Get().GetState(p.MechanismId) == State.Incomplete);

    public bool WorldsIsEnded() => 
      _mechanismData.All(p => _gameSaveSystem.Get().GetState(p.MechanismId) == State.Launched);

    public bool WorldIsOutOfMechanisms(string worldId)
    {
      return _mechanismData.Where(p => p.WorldId == worldId)
        .All(p => _gameSaveSystem.Get().GetState(p.MechanismId) == State.Launched);
    }

    private void SortingWorlds(WorldData[] worldData)
    {
      List<string> openWorlds = _gameSaveSystem.Get().GetOpenWorlds();

      WorldData[] openWorldsData = worldData.Where(p => openWorlds.Contains(p.WorldId)).ToArray();
      WorldData[] closedWorldsData = worldData.Where(p => !openWorlds.Contains(p.WorldId)).ToArray();

      _sortedWorldsData = openWorldsData.Concat(closedWorldsData).ToArray();
      
      Debug.Log("Sorted");
    }
  }
}