﻿using _Project.Scripts.Services._Interfaces;
using UnityEngine;

namespace _Project.Scripts.Logic.GameLogic.Main.FirstSession
{
  public class FirstSession : IFirstSession
  {
    private const string SubscriptionDenyKey = "FirstSession.IsSubscriptionDeny";
    private readonly IGameSaveSystem _gameSaveSystem;
    
    public FirstSession(IGameSaveSystem gameSaveSystem) => 
      _gameSaveSystem = gameSaveSystem;

    public bool IsSubscriptionDeny
    {
      get => PlayerPrefs.GetInt(SubscriptionDenyKey, 0) == 1;
      set => PlayerPrefs.SetInt(SubscriptionDenyKey, value? 1 : 0);
    }

    public void FirstSessionEnd()
    {
      _gameSaveSystem.Get().IsFirstSession = false;
      _gameSaveSystem.Save();
    }
    
    public bool IsFirstSession()
    {
      bool isFirstSession = _gameSaveSystem.Get().IsFirstSession;
      return isFirstSession;
    }
  }
}