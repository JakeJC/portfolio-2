﻿namespace _Project.Scripts.Logic.GameLogic.Main.FirstSession
{
  public interface IFirstSession
  {
    void FirstSessionEnd();
    bool IsFirstSession(); 
    bool IsSubscriptionDeny { get; set; }
  }
}