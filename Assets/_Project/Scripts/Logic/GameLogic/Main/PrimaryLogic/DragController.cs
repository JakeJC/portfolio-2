﻿using System;
using System.Collections.Generic;
using System.Linq;
using _Project.Scripts.Logic.GameLogic.Interfaces;
using _Project.Scripts.Logic.UI;
using _Project.Scripts.MonoBehaviours.Game;
using _Project.Scripts.MonoBehaviours.Game.Interfaces;
using _Project.Scripts.MonoBehaviours.UI;
using _Project.Scripts.Services.MaterialService.Interfaces;
using UnityEngine;
using Object = UnityEngine.Object;

namespace _Project.Scripts.Logic.GameLogic.Main.PrimaryLogic
{
  public class DragController : IDragableController
  {
    private readonly IDragDrop _dragDrop;
    private readonly IMaterialService _materialService;
    private readonly IMechanism _mechanism;

    private DragablePart[] _dragParts;

    private readonly Action<GamePart> _onPin;
    private readonly Action<GamePart> _onStartDrag;
    private readonly Action<GamePart> _onRelease;
    private readonly Action<GamePart> _onReturn;

    private Parts _parts;

    public DragController(IMechanism mechanism, IDragDrop dragDrop, IMaterialService materialService, Action<GamePart> onPin, Action<GamePart> onStartDrag, Action<GamePart> onReturn)
    {
      _mechanism = mechanism;
      _materialService = materialService;
      _dragDrop = dragDrop;
      _onPin = onPin;
      _onStartDrag = onStartDrag;
      _onReturn = onReturn;
    }

    public void PrepareDragableParts(Parts parts, GamePart[] gameParts)
    {
      _parts = parts;
      _dragParts = new DragablePart[parts.GetParts().Count()];

      for (var i = 0; i < parts.GetParts().Length; i++)
        _dragParts[i] = CreateDragableFromGameIconPrefab(parts.GetParts()[i], gameParts[i]);

      Sorting();
    }

    private void Sorting()
    {
      if(_parts.GetParts().All(p => p.GamePart.SortingPriorityInPanel == 0))
        return;
      
      List<Part> parts = _parts.GetParts().OrderByDescending(p => p.GamePart.SortingPriorityInPanel).ToList();
      _parts.SetParts(parts.ToArray());
      
      foreach (Part part in parts) 
        part.transform.SetSiblingIndex(parts.IndexOf(part));
    }

    private DragablePart CreateDragableFromGameIconPrefab(Part part, GamePart gamePart)
    {
      var transform = part.transform;
      GameObject dragableObject = Object.Instantiate(gamePart.GetIconPrefab(), transform);

      var gamePartIcon = dragableObject.GetComponent<GamePartIcon>();
      gamePartIcon.Construct(_mechanism);

      DragablePart dragablePart = dragableObject.AddComponent<DragablePart>();
      dragablePart.Construct(_dragDrop, _materialService, part, gamePart, _onStartDrag, _onPin, _onReturn);

      gamePart.DragablePart = dragablePart;
      
      return dragablePart;
    }
  }
}