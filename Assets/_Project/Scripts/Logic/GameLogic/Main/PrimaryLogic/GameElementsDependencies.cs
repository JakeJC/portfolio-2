﻿using _Project.FMOD_AudioService.Logic;
using _Project.Scripts.Animations.UI;
using _Project.Scripts.Features.Disable_Ads;
using _Project.Scripts.InApp.Interfaces;
using _Project.Scripts.Logic.Factories.Interfaces;
using _Project.Scripts.Logic.GameLogic.Interfaces;
using _Project.Scripts.Logic.StatesMachine;
using _Project.Scripts.Services._Interfaces;
using _Project.Scripts.Services.FX;
using _Project.Scripts.Services.InputResolver;
using _Project.Scripts.Services.LeanTouchCamera.Scripts.Interfaces;
using _Project.Scripts.Services.LoadScreen.Logic;
using _Project.Scripts.Services.Localization.Logic;
using AppacheAds.Scripts.Interfaces;
using AppacheAnalytics.Scripts.Interfaces;
using AppacheRemote.Scripts.Interfaces;

namespace _Project.Scripts.Logic.GameLogic.Main.PrimaryLogic
{
  public class GameElementsDependencies : IGameElementsDependencies
  {
    private readonly IUIFactory _uiFactory;
    private readonly IInGameTransitions _inGameTransitions;

    public GameElementsDependencies(GameStateMachine gameStateMachine, IUIFactory uiFactory, IPrivacyPolicyFactory privacyPolicyFactory,
      IGameLevelFactory gameLevelFactory, IGameServicesFactory gameServicesFactory,
      IInGameTransitions inGameTransitions, IInputResolver inputResolver, ILocaleSystem localeSystem, IGameSaveSystem gameSaveSystem,
      IAudioService audioService, ILoadScreenService loadScreenService, IAnalytics analytics,
      ICameraController cameraController, IFXController fxController, IGameDataProvider gameDataProvider, ICurrentScreen screenManager, IInAppAccessor inAppAccessor,
      IAdsService ads, IRemote remote, IDisableAdsLogic disableAdsLogic)
    {
      _uiFactory = uiFactory;
      _inGameTransitions = inGameTransitions;

      _uiFactory.SpawnSettingsWindow();
      _uiFactory.SpawnAttentionWindow();
      _uiFactory.SettingsWindow.Construct(gameStateMachine, uiFactory, gameLevelFactory, gameServicesFactory, gameSaveSystem,
        audioService, inputResolver, localeSystem, loadScreenService, analytics, cameraController, fxController, gameDataProvider,
        privacyPolicyFactory, screenManager, inAppAccessor, ads, remote, disableAdsLogic);
    }

    public void SetupDependencies()
    {
      LinkMechanismWithUI();
      AssignAnimations();
    }

    private void LinkMechanismWithUI()
    {
      _uiFactory.GameStatePanel.Settings.onClick.AddListener(OpenSettingsWindow);
      _uiFactory.GameStatePanel.Play.onClick.AddListener(_inGameTransitions.ClickPlay);
      _uiFactory.GameStatePanel.Done.onClick.AddListener(_inGameTransitions.Next);
    }

    private void OpenSettingsWindow() =>
      _uiFactory.SettingsWindow.OpenWindow();

    private void AssignAnimations()
    {
      new ButtonAnimation(_uiFactory.GameStatePanel.Destruct.transform);
      new ButtonAnimation(_uiFactory.GameStatePanel.Done.transform);
      new ButtonAnimation(_uiFactory.GameStatePanel.Play.transform);
      new ButtonAnimation(_uiFactory.GameStatePanel.Settings.transform);
    }
  }
}