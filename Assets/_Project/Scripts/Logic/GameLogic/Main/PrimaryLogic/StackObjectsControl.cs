using System.Collections.Generic;
using System.Linq;
using _Project.Scripts.Logic.Factories.Interfaces;
using _Project.Scripts.Logic.GameLogic.Interfaces;
using _Project.Scripts.MonoBehaviours.Game;
using _Project.Scripts.MonoBehaviours.Game.Stackable;
using _Project.Scripts.MonoBehaviours.UI;
using UnityEngine;

namespace _Project.Scripts.Logic.GameLogic.Main.PrimaryLogic
{
  public class StackObjectsControl
  {
    private readonly IGameLogic _gameLogic;
    private readonly IUIFactory _uiFactory;

    private readonly Part[] _stackableParts;
    private readonly Dictionary<int, StackPart> _uniqueCounters = new Dictionary<int, StackPart>();

    private Vector2 _anchoredPosition;
    private DragablePart _fakeObject;

    public StackObjectsControl(IGameLogic gameLogic, IUIFactory uiFactory)
    {
      _uiFactory = uiFactory;
      _gameLogic = gameLogic;
      
      _stackableParts = Object
        .FindObjectsOfType<Part>()
        .Where(p => p.GamePart.GetComponent<StackableGamePart>() != null)
        .ToArray();
      
      foreach (Part stackablePart in _stackableParts)
      {
        int stackId = stackablePart.GamePart.GetComponent<StackableGamePart>().StackId;
        if (!_uniqueCounters.ContainsKey(stackId)) 
          _uniqueCounters.Add(stackId, new StackPart() {StackIndex = 0});
      }

      if(_stackableParts.Length != 0)
        CreateCounters();
      
      _gameLogic.OnPinEvent += OnDragComplete;
      _gameLogic.OnDragStart += OnDragStart;
      _gameLogic.OnReleaseEvent += OnDragRelease;
    }

    ~StackObjectsControl()
    {
      _gameLogic.OnPinEvent -= OnDragComplete;
      _gameLogic.OnDragStart -= OnDragStart;
      _gameLogic.OnReleaseEvent -= OnDragRelease;
    }

    public void SetupStartStack()
    {
      foreach (KeyValuePair<int, StackPart> stackPart in _uniqueCounters)
      {
        Stack(stackPart.Key);
        
        stackPart.Value.StackIndex = 0;
        stackPart.Value.StackPartsCount = _stackableParts.Count(p => p.GamePart.GetComponent<StackableGamePart>().StackId == stackPart.Key);
        
        SetCount(stackPart.Value.StackPartsCount - stackPart.Value.StackIndex, stackPart.Key);
      }
    }

    private void CreateCounters()
    {
      foreach (KeyValuePair<int,StackPart> stackPart in _uniqueCounters)
      {     
        StackCounter stackCounter = _uiFactory.CreateCounter(GetPartGroup(stackPart.Key)[stackPart.Value.StackIndex].transform);
        stackPart.Value.StackCounter = stackCounter;
      }
      
      _anchoredPosition = _uniqueCounters.FirstOrDefault().Value.StackCounter
        .GetComponent<RectTransform>().anchoredPosition;
    }

    private void SetCount(int count, int stackId) => 
      _uniqueCounters[stackId].StackCounter.SetCounter(count);

    private void Stack(int stackId)
    {
      Part[] parts = GetPartGroup(stackId);
      
      _uniqueCounters[stackId].StackCounter.transform.SetParent(GetPartGroup(stackId)[_uniqueCounters[stackId].StackIndex].transform);
      _uniqueCounters[stackId].StackCounter.GetComponent<RectTransform>().anchoredPosition = _anchoredPosition;
      
      for (int i = 0; i < parts.Length; i++) 
        parts[i].gameObject.SetActive(i == _uniqueCounters[stackId].StackIndex);
    }

    private Part[] GetPartGroup(int stackId) => 
      _stackableParts.Where(p => p.GamePart.GetComponent<StackableGamePart>().StackId == stackId).ToArray();

    private void OnDragComplete(GamePart gamePart)
    {
      StackableGamePart stackableGamePart = gamePart.GetComponent<StackableGamePart>();

      if(stackableGamePart == null)
        return;
      
      _uniqueCounters[stackableGamePart.StackId].StackIndex++;

      if (_uniqueCounters[stackableGamePart.StackId].StackIndex >= _uniqueCounters[stackableGamePart.StackId].StackPartsCount)
        return;

      Stack(stackableGamePart.StackId);
      
      foreach (KeyValuePair<int, StackPart> stackPart in _uniqueCounters) 
        SetCount(stackPart.Value.StackPartsCount - stackPart.Value.StackIndex, stackPart.Key);
      
      if(_fakeObject)
        Object.Destroy(_fakeObject.gameObject);
    }

    private void OnDragStart(GamePart gamePart)
    {
      Part part = _stackableParts.FirstOrDefault(p => p.GamePart == gamePart);
      if (part != null)
      {
        StackableGamePart stackableGamePart = gamePart.GetComponent<StackableGamePart>();
        if(_uniqueCounters[stackableGamePart.StackId].StackIndex < _uniqueCounters[stackableGamePart.StackId].StackPartsCount - 1)
          _fakeObject = Object.Instantiate(gamePart.DragablePart, part.transform);
      }
    }

    private void OnDragRelease(GamePart gamePart)
    {
      if(_fakeObject)
        Object.Destroy(_fakeObject.gameObject);
    }
  }
}