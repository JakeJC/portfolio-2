using System.Collections;
using _Project.Scripts.Logic.GameLogic.Interfaces;
using _Project.Scripts.MonoBehaviours.Game;
using _Project.Scripts.MonoBehaviours.UI.Interfaces;
using UnityEngine;

namespace _Project.Scripts.Logic.GameLogic.Main.PrimaryLogic
{
  public class DragDrop : MonoBehaviour, IDragDrop
  {
    private const float DragCameraOffset = 20f;
    private const float SmoothTime = 0.1f;

    private Camera _camera;
    private IDragDependent[] _dragDependents;

    private DragablePart CurrentActiveDragPart { get; set; }

    private Vector3 _smoothDampVelocity;
    private Coroutine _follow;
    
    private bool _isInDrag;

    private Vector3 PartPosition
    {
      set
      {
        if (CurrentActiveDragPart.PinPoint == null)
          CurrentActiveDragPart.transform.position = value;
        else
        {
          Vector3 delta = CurrentActiveDragPart.transform.position - CurrentActiveDragPart.PinPoint.position;
          CurrentActiveDragPart.transform.position = value + delta; 
        }
      }
      get => CurrentActiveDragPart.PinPoint == null ?
        CurrentActiveDragPart.transform.position :
        CurrentActiveDragPart.PinPoint.position;
    }

    private void Start() =>
      _camera = Camera.main;

    private void LateUpdate()
    {
      if (Input.GetMouseButtonUp(0))
        ReleasePart();
    }

    public void SetDependentModules(IDragDependent[] dragDependents) =>
      _dragDependents = dragDependents;

    public void BindPartToCursor(DragablePart dragablePart)
    {
      dragablePart.ToGameWorld();

      CurrentActiveDragPart = dragablePart;

      AllDragDependentDown();
      
      _isInDrag = true;
      _follow = StartCoroutine(FollowCursor());
    }

    private void ReleasePart()
    {
      if (_follow != null)
      {
        StopCoroutine(_follow);
        _follow = null;
      }

      if (CurrentActiveDragPart == null)
        return;

      if (CurrentActiveDragPart.IsPinned) 
        return;
      
      CurrentActiveDragPart.ReturnToBaseState();
      ResetAfterRelease();
    }

    private IEnumerator FollowCursor()
    {        
      var waitForEndOfFrame = new WaitForEndOfFrame();

      Vector3 targetPoint = Input.mousePosition;
      targetPoint = _camera.ScreenPointToRay(targetPoint).origin;
      targetPoint += _camera.transform.forward * DragCameraOffset;      
      
      PartPosition = targetPoint;
        
      while (_isInDrag)
      {
        targetPoint = Input.mousePosition;
        targetPoint = _camera.ScreenPointToRay(targetPoint).origin;
        targetPoint += _camera.transform.forward * DragCameraOffset;

        if (CurrentActiveDragPart == null)
        {
          _isInDrag = false;
          yield break;
        }

        PartPosition = Vector3.SmoothDamp(PartPosition, targetPoint, ref _smoothDampVelocity, SmoothTime);
          
        yield return waitForEndOfFrame;

        if (!CurrentActiveDragPart.CanPin() || CurrentActiveDragPart.IsPinned)
          continue;

        CurrentActiveDragPart.PinPart();
        ResetAfterRelease();
      }
    }

    private void ResetAfterRelease()
    {
      _isInDrag = false;
      CurrentActiveDragPart = null;
      AllDragDependentUp();
    }

    private void AllDragDependentDown()
    {
      foreach (IDragDependent dragDependent in _dragDependents)
        dragDependent.OnExternalDragDown();
    }

    private void AllDragDependentUp()
    {
      foreach (IDragDependent dragDependent in _dragDependents)
        dragDependent.OnExternalDragUp();
    }
  }
}