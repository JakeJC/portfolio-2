﻿using System.Linq;
using _Project.Scripts.Data.Serializable;
using _Project.Scripts.Features.Meditation.Scripts.Scriptables;
using _Project.Scripts.Logic.GameLogic.Interfaces;
using _Project.Scripts.MonoBehaviours.Game;
using _Project.Scripts.MonoBehaviours.Game.Interfaces;
using _Project.Scripts.Services._Interfaces;
using _Project.Scripts.Services.MaterialService.Interfaces;

namespace _Project.Scripts.Logic.GameLogic.Main.PrimaryLogic
{
  public class GameSavePointsLogic : IGameSavePointsLogic
  {
    private readonly IGameSaveSystem _gameSaveSystem;
    private readonly IMechanism _mechanism;

    private readonly GameLogic _gameLogic;
    private readonly GamePart[] _gameParts;

    private readonly IGameDataProvider _gameMetaDataService;
    private readonly IMaterialService _materialService;
    
    private bool _alreadyBeenCompleted;

    public GameSavePointsLogic(IGameDataProvider gameMetaDataService, GameLogic gameLogic, IGameSaveSystem gameSaveSystem,
      IMechanism mechanism, IMaterialService materialService)
    {
      _materialService = materialService;
      _gameMetaDataService = gameMetaDataService;
      _gameLogic = gameLogic;
      _gameSaveSystem = gameSaveSystem;
      _mechanism = mechanism;
      _gameParts = gameLogic.GameParts;
    }
    
    public void SetCompletedState(bool fromBuildingMode)
    {
      foreach (GamePart gamePart in _gameParts)
        gamePart.AppearAfterInitialize();
      
      if(fromBuildingMode)
        _gameLogic.ShowPlayButton();
    }

    public void SaveLaunchedMechanism()
    {
      if (IsMechanismCompleted())
        _alreadyBeenCompleted = true;
      
      _gameSaveSystem.Get().SetState(_mechanism.GetId(), State.Launched);

      if (_gameSaveSystem.Get().CompleteMechanismsInfo.All(p => p.MechanismId != _mechanism.GetId()))
      {
        _gameSaveSystem.Get().CompleteMechanismsInfo.Add(new CompleteMechanismData()
        {
          MechanismId = _mechanism.GetId(),
          ColorId = _materialService.Color.GetCurrentSchemeGuid(),
          MaterialId = _materialService.Material.GetCurrentSchemeGuid()
        });
      }

      _gameSaveSystem.Save();
    }

    public bool IsMechanismAlreadyBeenCompleted() => 
      _alreadyBeenCompleted;

    public bool IsMechanismCompleted() =>
      _gameSaveSystem.Get().GetState(_mechanism.GetId()) != State.Incomplete;
  }
}