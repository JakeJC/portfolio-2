﻿using _Project.Scripts.MonoBehaviours.Game.Stackable;

namespace _Project.Scripts.Logic.GameLogic.Main.PrimaryLogic
{
  public class StackPart
  {
    public int StackIndex;
    public int StackPartsCount;
    public StackCounter StackCounter;
  }
}