﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using _Project.FMOD_AudioService.Logic;
using _Project.FMOD_AudioService.Logic.Enums;
using _Project.FMOD_AudioService.Logic.Events;
using _Project.Scripts.Animations.UI;
using _Project.Scripts.Features.Disable_Ads;
using _Project.Scripts.Features.Stages;
using _Project.Scripts.Features.Stages.Interfaces;
using _Project.Scripts.InApp.Interfaces;
using _Project.Scripts.Logic.Factories.Interfaces;
using _Project.Scripts.Logic.GameLogic.Advices.Interfaces;
using _Project.Scripts.Logic.GameLogic.Interfaces;
using _Project.Scripts.Logic.StatesMachine;
using _Project.Scripts.Logic.StatesMachine.States;
using _Project.Scripts.Logic.UI;
using _Project.Scripts.MonoBehaviours.Game;
using _Project.Scripts.MonoBehaviours.Game.Interfaces;
using _Project.Scripts.Services._Interfaces;
using _Project.Scripts.Services.FX;
using _Project.Scripts.Services.InputResolver;
using _Project.Scripts.Services.LeanTouchCamera.Scripts.Interfaces;
using _Project.Scripts.Services.LoadScreen.Logic;
using _Project.Scripts.Services.Localization.Logic;
using _Project.Scripts.Services.MaterialService.Interfaces;
using AppacheAnalytics.Scripts.Interfaces;
using AppacheRemote.Scripts.Interfaces;
using UnityEngine;

namespace _Project.Scripts.Logic.GameLogic.Main.PrimaryLogic
{
  public class InGameTransitions : IInGameTransitions
  {
    private const string FirstGameSessionString = "firstGameSession";

    private readonly GameStateMachine _gameStateMachine;
    private readonly IGameLevelFactory _gameLevelFactory;
    private readonly GameLogic _gameLogic;
    private readonly IUIFactory _uiFactory;
    private readonly ICameraController _cameraController;

    private readonly IDragableController _dragableController;
    private readonly IAudioService _audioService;
    private readonly IInputResolver _inputResolver;
    private readonly IGameSavePointsLogic _gameSavePointsLogic;
    private readonly IGameDataProvider _gameDataProvider;
    private readonly IGameSaveSystem _gameSaveSystem;
    private readonly IAnalytics _analytics;

    private readonly Parts _parts;
    private readonly GamePart[] _gameParts;

    private readonly IGameServicesFactory _gameServicesFactory;
    private readonly StackObjectsControl _stackObjectsControl;

    private readonly IMechanism _mechanism;
    private readonly IMaterialService _materialService;
    private readonly IStagesService _stagesService;

    private AudioEventLink _audioEventLink;
    private readonly ILoadScreenService _loadScreenService;
    private readonly IFXController _fxController;

    private bool _isInTransition;
    private readonly IAssetProvider _assetProvider;
    private readonly ILocaleSystem _localeSystem;

    private IAdvices _advices;

    private bool _shouldSendEvents;
    private readonly IDisableAdsLogic _disableAdsLogic;
    private IInAppAccessor _inAppAccessor;
    private IRemote _remote;

    public InGameTransitions(GameStateMachine gameStateMachine,
      IGameLevelFactory gameLevelFactory,
      ICameraController cameraController,
      GameLogic gameLogic,
      IUIFactory uiFactory,
      IGameServicesFactory gameServicesFactory,
      IDragableController dragableController,
      IGameSavePointsLogic gameSavePointsLogic,
      IAudioService audioService,
      IInputResolver inputResolver,
      IAssetProvider assetProvider,
      IGameDataProvider gameDataProvider,
      IMechanism mechanism,
      IGameSaveSystem gameSaveSystem,
      IMaterialService materialService,
      IAnalytics analytics,
      ILoadScreenService loadScreenService,
      IFXController fxController,
      ILocaleSystem localeSystem,
      IStagesService stagesService,
      IDisableAdsLogic disableAdsLogic,
      IInAppAccessor inAppAccessor,
      IRemote remote)
    {
      _remote = remote;
      _inAppAccessor = inAppAccessor;
      _disableAdsLogic = disableAdsLogic;
      _stagesService = stagesService;
      _localeSystem = localeSystem;
      _assetProvider = assetProvider;
      _fxController = fxController;
      _loadScreenService = loadScreenService;
      _materialService = materialService;
      _analytics = analytics;
      _gameSaveSystem = gameSaveSystem;
      _mechanism = mechanism;
      _gameDataProvider = gameDataProvider;
      _gameServicesFactory = gameServicesFactory;
      _gameStateMachine = gameStateMachine;
      _gameLevelFactory = gameLevelFactory;
      _cameraController = cameraController;
      _gameLogic = gameLogic;
      _uiFactory = uiFactory;
      _dragableController = dragableController;
      _parts = gameLogic.Parts;
      _gameParts = gameLogic.GameParts;
      _gameSavePointsLogic = gameSavePointsLogic;
      _audioService = audioService;
      _inputResolver = inputResolver;

      _stackObjectsControl = new StackObjectsControl(gameLogic, uiFactory);
    }

    public void LaunchGame(bool shouldSendEvents)
    {
      _shouldSendEvents = shouldSendEvents;
      _fxController.EnableFingerTrail();

      _inputResolver.Enable();
      DisableUIElementsOnStart();

      if (!_gameSavePointsLogic.IsMechanismCompleted())
      {
        _gameLevelFactory.Mechanism.DisableAnimatorAtGameStart();
        _gameLevelFactory.Mechanism.ToStartState();
      }

      _cameraController.EnableRotation();
      _cameraController.SwitchToGame();

      _cameraController.ToBaseState();
      _cameraController.SaveOriginalPosition();

      _dragableController.PrepareDragableParts(_parts, _gameParts);

      _gameLogic.ActivateUIParts();
      _gameLogic.ChangeToSilhouette();

      _stackObjectsControl.SetupStartStack();
    }

    public void Next()
    {
      if (_inAppAccessor.IsSubscriptionBought())
        NextTransition();
      else
        _disableAdsLogic.ShowOnceInSession(NextTransition);
    }

    private void NextTransition()
    {
      if (_isInTransition)
        return;

      _isInTransition = true;

      _fxController.DisableTrail();

      StopSound();

      if (_gameSavePointsLogic.IsMechanismAlreadyBeenCompleted())
        ToGallery();
      else
      {
        if (_gameDataProvider.MechanismsInfo.IsSomethingLocked())
          Test2TransitionFeature();
        else
        {
          _loadScreenService.Launch<BlackTransition>(null, async () =>
          {
            ToEndContent();
            await Task.CompletedTask;
          });
        }
      }

      void Test2TransitionFeature()
      {
        _loadScreenService.Launch<BlackTransition>(null, async () =>
        {
          ToEndDecision();
          await Task.CompletedTask;
        });
      }
    }

    public void StopSound()
    {
      _audioService.Stop(_audioEventLink);
      _audioService.Cleanup(_audioEventLink);
      _audioService.SetMechanismInLaunch(false);
    }

    public void ToPlayMode()
    {
      _uiFactory.GameStatePanel.ToPlay();
      _gameLogic.OnComplete?.Invoke();
    }

    public void ClickPlay()
    {
      PlayGame();

      if (_gameSaveSystem.Get().TutorialComplete)
        _cameraController.FinishingRotation();

      _gameLogic.OnLaunch?.Invoke();
    }

    public void Skip()
    {
      ToMechanismLaunchAndSave();
      _uiFactory.GameStatePanel.Done.gameObject.SetActive(true);
      ActivateAllRenderers();
    }

    public void ClearBetweenTransition()
    {
      _stagesService.CompleteStage();
      _stagesService.HideStageCounterView();

      _cameraController.DisableRotation();

      _uiFactory.Clear();

      _gameLevelFactory.Clear();
      _gameServicesFactory.Clear();

      _inputResolver.Enable();
    }

    private void PlayGame()
    {
      if (_gameParts.Any(gamePart => !gamePart.IsPinned))
        return;

      ToMechanismLaunchAndSave();
    }

    private async void ToEndDecision()
    {
      _uiFactory.GameStatePanel.AppearGameStatePanel(false);

      await LoadEndDecision().Invoke();

      LoadScreenService.ActionReturnTask LoadEndDecision()
      {
        return Load(async () =>
        {
          await Task.CompletedTask;
          _gameStateMachine.Enter<LoadEndDecisionState>();
        });
      }
    }

    private async void ToEndContent()
    {
      _uiFactory.GameStatePanel.AppearGameStatePanel(false);

      await LoadEndContent().Invoke();

      LoadScreenService.ActionReturnTask LoadEndContent()
      {
        return Load(async () =>
        {
          await Task.CompletedTask;
          _gameStateMachine.Enter<LoadEndContentState>();
        });
      }
    }

    private void ToGallery() =>
      _loadScreenService.Launch<BlackTransition>(null, async () => { await _gameStateMachine.AsyncEnter<LoadGalleryState, ButtonType>(ButtonType.Worlds); });

    private LoadScreenService.ActionReturnTask Load(LoadScreenService.ActionReturnTask targetTask)
    {
      ChangeMusic();
      return targetTask;
    }

    private void ChangeMusic()
    {
      AudioEventLink audioEventLink = _audioService.GetLinkByEvent(FMOD_EventType.Music);
      _audioService.SetParameter(audioEventLink, FMOD_ParameterType.current_music, 0);
    }

    private void ToMechanismLaunchAndSave()
    {
      if (_gameDataProvider.MechanismsInfo.MarkedAsTutorial)
      {
        _gameSaveSystem.Get().GroupWithTutorialComplete = true;
        _gameSaveSystem.Save();
      }
      else
      {
        if (_shouldSendEvents)
        {
          _analytics.Send("ev_level_complete", new Dictionary<string, object>
          {
            {"level", _gameDataProvider.MechanismsInfo.GetLevelOrderNumber()},
            {"level_id", _mechanism.GetId()}
          });
        }
      }

      _gameDataProvider.MechanismsInfo.MarkedAsTutorial = false;

      _audioService.CreateEvent(out AudioEventLink musicLink, _mechanism.GetSoundEvent(), _mechanism.GetGO());
      _audioEventLink = musicLink;

      _audioService.Play(_audioEventLink);
      _audioService.SetParameter(musicLink, FMOD_ParameterType.current_skin, _materialService.Material.GetCurrentSchemeIndex());
      _audioService.SetMechanismInLaunch(true);

      _gameLogic.DeactivateOnLaunch();
      _gameSavePointsLogic.SaveLaunchedMechanism();

      _gameLevelFactory.Mechanism.FinalLaunch();

      if (_gameSavePointsLogic.IsMechanismAlreadyBeenCompleted())
        return;

      AddWorldToGallery();
    }

    private void AddWorldToGallery()
    {
      _gameSaveSystem.Get().AddOpenWorld(_gameDataProvider.WorldsInfo.GetWorldDataForMechanism(_mechanism.GetId()).WorldId);
      _gameSaveSystem.Save();

      _gameDataProvider.WorldsInfo.WorldsOrder();
    }

    private void DisableUIElementsOnStart() =>
      _uiFactory.GameStatePanel.ToBaseState();

    private void ActivateAllRenderers()
    {
      foreach (GamePart mechanismPart in _mechanism.GetMechanismParts())
      {
        mechanismPart.gameObject.SetActive(true);
        mechanismPart.gameObject.layer = LayerMask.NameToLayer("Default");
        mechanismPart.Renderer.enabled = true;

        foreach (Renderer renderer in mechanismPart.AdditionalRenderers)
        {
          renderer.gameObject.SetActive(true);
          renderer.gameObject.layer = LayerMask.NameToLayer("Default");
          renderer.enabled = true;
        }
      }
    }
  }
}