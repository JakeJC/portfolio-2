using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using _Project.FMOD_AudioService.Logic;
using _Project.FMOD_AudioService.Logic.Enums;
using _Project.FMOD_AudioService.Logic.Events;
using _Project.Scripts.Animations.UI;
using _Project.Scripts.Features.Disable_Ads;
using _Project.Scripts.Features.Stages.Interfaces;
using _Project.Scripts.InApp.Interfaces;
using _Project.Scripts.Logic.Factories.Interfaces;
using _Project.Scripts.Logic.GameLogic.Interfaces;
using _Project.Scripts.Logic.GameLogic.Tutorial.SubstateMachine;
using _Project.Scripts.Logic.StatesMachine;
using _Project.Scripts.Logic.StatesMachine.States;
using _Project.Scripts.Logic.UI;
using _Project.Scripts.MonoBehaviours.Game;
using _Project.Scripts.MonoBehaviours.TestBehaviour;
using _Project.Scripts.MonoBehaviours.UI;
using _Project.Scripts.Services._Interfaces;
using _Project.Scripts.Services.FX;
using _Project.Scripts.Services.InputResolver;
using _Project.Scripts.Services.LeanTouchCamera.Scripts.Interfaces;
using _Project.Scripts.Services.LoadScreen.Logic;
using _Project.Scripts.Services.Localization.Logic;
using _Project.Scripts.Services.MaterialService.Interfaces;
using AppacheAds.Scripts.Interfaces;
using AppacheAnalytics.Scripts.Interfaces;
using AppacheRemote.Scripts.Interfaces;
using DG.Tweening;
using JetBrains.Annotations;
using UnityEngine;
using Object = UnityEngine.Object;

namespace _Project.Scripts.Logic.GameLogic.Main.PrimaryLogic
{
  public class GameLogic : IGameLogic
  {
    private readonly IGameSaveSystem _gameSaveSystem;
    private readonly IGameElementsDependencies _gameElementsDependencies;
    private readonly IAudioService _audioService;
    private readonly IInGameTransitions _inGameTransitions;
    private readonly IGameSavePointsLogic _gameSavePointsLogic;
    private readonly IFXController _fxController;
    private readonly IAnalytics _analytics;
    private readonly ICameraController _cameraController;
    private readonly ILocaleSystem _localeSystem;
    private readonly IStagesService _stagesService;
    
    private readonly IUIFactory _uiFactory;
    private readonly IGameLevelFactory _gameLevelFactory;
    private readonly ITutorialFactory _tutorialFactory;

    private AudioEventLink _audioEventLink;

    public readonly Parts Parts;
    public readonly GamePart[] GameParts;

    private GameObject _tester;
    private IRemote _remote;
    private IInAppAccessor _inAppAccessor;
    public event Action<GamePart> OnDragStart;
    public event Action<GamePart> OnReleaseEvent;
    public event Action<GamePart> OnPinEvent;

    public Action OnComplete { get; set; }
    public Action OnLaunch { get; set; }

    public GameLogic(GameStateMachine gameStateMachine,
      IUIFactory uiFactory,
      IPrivacyPolicyFactory privacyPolicyFactory,
      IGameLevelFactory gameLevelFactory,
      IGameServicesFactory gameServicesFactory,
      ITutorialFactory tutorialFactory,
      IDragDrop dragDrop,
      IGameSaveSystem gameSaveSystem,
      ICameraController cameraController,
      IInputResolver inputResolver,
      IAudioService audioService,
      IFXController fxController,
      IMaterialService materialService,
      IGameDataProvider gameMetaDataService,
      IAssetProvider assetProvider,
      ILocaleSystem localeSystem,
      IAnalytics analytics,
      ILoadScreenService loadScreenService,
      ICurrentScreen currentScreen,
      IRemote remote,
      IStagesService stagesService,
      IInAppAccessor inAppAccessor,
      IAdsService ads,
      IDisableAdsLogic disableAdsLogic)
    {
      _inAppAccessor = inAppAccessor;
      _remote = remote;
      _tutorialFactory = tutorialFactory;
      _localeSystem = localeSystem;
      _cameraController = cameraController;
      _analytics = analytics;
      _uiFactory = uiFactory;
      _gameSaveSystem = gameSaveSystem;
      _audioService = audioService;
      _fxController = fxController;
      _stagesService = stagesService;
      
      _gameLevelFactory = gameLevelFactory;
      GameParts = _gameLevelFactory.Mechanism.GetMechanismParts();
      Parts = _uiFactory.Parts;

      IDragableController dragableController = new DragController(_gameLevelFactory.Mechanism, dragDrop, materialService, OnPin, DragStart, PartRelease);
      _gameSavePointsLogic = new GameSavePointsLogic(gameMetaDataService, this, _gameSaveSystem, _gameLevelFactory.Mechanism, materialService);
      _inGameTransitions = new InGameTransitions(gameStateMachine, _gameLevelFactory, cameraController, this, uiFactory,
        gameServicesFactory, dragableController, _gameSavePointsLogic, audioService, inputResolver, assetProvider, gameMetaDataService,
        _gameLevelFactory.Mechanism, _gameSaveSystem, materialService, analytics, loadScreenService, _fxController, localeSystem, stagesService,
        disableAdsLogic, inAppAccessor, remote);
      _gameElementsDependencies = new GameElementsDependencies(gameStateMachine, uiFactory, privacyPolicyFactory, gameLevelFactory, gameServicesFactory,
        _inGameTransitions, inputResolver, localeSystem, _gameSaveSystem, _audioService, loadScreenService, _analytics, cameraController, fxController,
        gameMetaDataService, currentScreen, _inAppAccessor, ads, remote, disableAdsLogic);

      InitializeWorldIcon(_gameLevelFactory, gameMetaDataService);
#if TEST || COMPLETE_GAME || SOUND_DESIGNER
      AddTestSkipLevel();
#endif
      CreateSoundEvent();
    }

    public void LaunchGame(GameDataStruct gameDataStruct)
    {
      _gameElementsDependencies.SetupDependencies();
      _inGameTransitions.LaunchGame(gameDataStruct.ShouldSendEvents);

      ValidateMaterialButtonsState();
    }

    public void ShowPlayButton() =>
      _inGameTransitions.ToPlayMode();

    public async void ActivateUIParts()
    {
      var tasks = new List<Task>();
      foreach (Part part in Parts.GetParts())
      {
        Parts.SetActive(part, true);
        var offStateAnimation = new OnOffStateAnimation(part.transform);
        var task = new Task(() => { });
        tasks.Add(task);
        void OnEndTask() => task.Start();
        offStateAnimation.Enable(OnEndTask);
      }

      await Task.WhenAll(tasks);

      if (!_gameSaveSystem.Get().TutorialComplete)
      {
        TutorialStateMachine tutorialStateMachine = new TutorialStateMachine(this, _gameSaveSystem, _analytics, _localeSystem,
          _cameraController, _uiFactory, _tutorialFactory, _gameLevelFactory.Mechanism, _remote);
        
        _stagesService.HideStageCounterView();
        tutorialStateMachine.Launch();
      }
    }
    
    public void DeactivateOnLaunch()
    {
      for (var i = 0; i < Parts.GetParts().Length; i++)
      {
        if (!GameParts[i].IsPinned)
          GameParts[i].gameObject.SetActive(false);

        Parts.SetActive(Parts.GetParts()[i], false);
      }
    }

    public void ChangeToSilhouette()
    {
      foreach (var gamePart in GameParts)
        gamePart.Fade();
    }

    public void Clear()
    {
      _inGameTransitions.ClearBetweenTransition();
      
      if (_tester)
        Object.Destroy(_tester);
    }

    private void CreateSoundEvent()
    {
      _audioService.CreateEvent(out AudioEventLink soundLink, FMOD_EventType.UI, _tester);
      _audioEventLink = soundLink;
    }

    private void ValidateMaterialButtonsState()
    {
      bool isDisabled = _gameLevelFactory.Mechanism.GetMechanismParts().All(k => !k.IsPinned);

      _uiFactory.GameStatePanel.MaterialsButtons.interactable = !isDisabled;
      
      _uiFactory.GameStatePanel.MaterialButtons.ForEach(p =>
        p.SetDisabled(isDisabled));

      _uiFactory.GameStatePanel.GetMaterialSoundButtons().ForEach(p =>
        p.MuteButtonSound(isDisabled));

      _uiFactory.GameStatePanel.MaterialsButtons.DOFade(isDisabled ? 0.2f : 1, 0.2f);
    }

    private void InitializeWorldIcon(IGameLevelFactory gameLevelFactory, IGameDataProvider gameMetaDataService) =>
      _uiFactory.GameStatePanel.WorldIcon.sprite = gameMetaDataService.WorldsInfo.GetWorldDataForMechanism(gameLevelFactory.Mechanism.GetId()).WorldIcon;

    private void OnPin(GamePart gamePart)
    {
      _audioService.Play(_audioEventLink);
      _audioService.SetParameter(_audioEventLink, FMOD_ParameterType.current_ui, 6);

      _fxController.PlayPlaceParticle(gamePart.Renderer);

      gamePart.ReturnBaseLayer();

      OnPinEvent?.Invoke(gamePart);
      gamePart.GetComponent<OnPinTrigger>()?.OnPin?.Invoke();

      ValidateMaterialButtonsState();
      
      _cameraController.EnableRotation();

      _uiFactory.GameBar.AddStep();
      
      if (!GameParts.All(p => p.IsPinned))
        return;

      Parts.SetEndConstruction();
      _gameSavePointsLogic.SetCompletedState(true);
      
      _uiFactory.HideUIButton.Appear();
    }

    private void DragStart(GamePart part)
    {
      _uiFactory.PartsPanel.CanvasGroup.interactable = false;
      OnDragStart?.Invoke(part);
      
      _cameraController.DisableRotation();
    }

    private void PartRelease(GamePart part)
    {
      OnReleaseEvent?.Invoke(part);
      _uiFactory.PartsPanel.CanvasGroup.interactable = true;
        
      _cameraController.EnableRotation();
    }
    
    [UsedImplicitly]
    private void AddTestSkipLevel()
    {
      _tester = new GameObject("[Test] Skip Level");
      var skipLevelTestFeature = _tester.AddComponent<SkipLevelTestFeature>();
      skipLevelTestFeature.OnClick = () =>
      {
        _inGameTransitions.Skip();
      };
    }
  }
}