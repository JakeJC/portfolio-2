﻿using _Project.Scripts.MonoBehaviours.Game;

namespace _Project.Scripts.Logic.GameLogic.Interfaces
{
  public interface IDragDrop
  {
    void BindPartToCursor(DragablePart dragablePart);
  }
}