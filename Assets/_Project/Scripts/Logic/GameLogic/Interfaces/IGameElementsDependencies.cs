﻿namespace _Project.Scripts.Logic.GameLogic.Interfaces
{
  public interface IGameElementsDependencies
  {
    void SetupDependencies();
  }
}