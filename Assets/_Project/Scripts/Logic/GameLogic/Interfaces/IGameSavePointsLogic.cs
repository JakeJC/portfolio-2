﻿namespace _Project.Scripts.Logic.GameLogic.Interfaces
{
  public interface IGameSavePointsLogic
  {
    void SetCompletedState(bool fromBuildingMode);
    void SaveLaunchedMechanism();
    bool IsMechanismCompleted();
    bool IsMechanismAlreadyBeenCompleted();
  }
}