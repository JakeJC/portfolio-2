﻿namespace _Project.Scripts.Logic.GameLogic.Interfaces
{
  public interface IInGameTransitions
  {
    void LaunchGame(bool shouldSendEvents);
    void ClickPlay();
    void ToPlayMode();
    void Next();
    void Skip();
    void ClearBetweenTransition();
    void StopSound();
  }
}