using _Project.Scripts.Logic.GameLogic.Main.FirstSession;
using _Project.Scripts.Logic.GameLogic.Meta.Interfaces;
using _Project.Scripts.Service_Base;

namespace _Project.Scripts.Logic.GameLogic.Interfaces
{
  public interface IGameDataProvider : IService
  {
    IWorldInfo WorldsInfo { get; }
    IMechanismsInfo MechanismsInfo { get; }
    ITextsInfo EndStateTextInfo { get; }
    ITextsInfo LikeStateTextInfo { get; }
    ITextsInfo RelaxStateTextInfo { get; }
    IFirstSession FirstSession { get; }
    void Initialize();
    bool IsAllAdvicesLocked();
  }
}