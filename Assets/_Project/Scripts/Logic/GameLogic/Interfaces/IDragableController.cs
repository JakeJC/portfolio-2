﻿using _Project.Scripts.Logic.UI;
using _Project.Scripts.MonoBehaviours.Game;

namespace _Project.Scripts.Logic.GameLogic.Interfaces
{
  public interface IDragableController
  {
    void PrepareDragableParts(Parts parts, GamePart[] gameParts);
  }
}