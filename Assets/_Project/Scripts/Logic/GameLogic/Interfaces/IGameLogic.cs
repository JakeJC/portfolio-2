﻿using System;
using _Project.Scripts.Logic.StatesMachine.States;
using _Project.Scripts.MonoBehaviours.Game;

namespace _Project.Scripts.Logic.GameLogic.Interfaces
{
  public interface IGameLogic
  {
    void LaunchGame(GameDataStruct gameDataStruct);
    event Action<GamePart> OnDragStart;
    event Action<GamePart> OnReleaseEvent;
    event Action<GamePart> OnPinEvent;
    Action OnLaunch { get; set; }
  }
}