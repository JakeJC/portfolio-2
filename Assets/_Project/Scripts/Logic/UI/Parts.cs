using System.Linq;
using _Project.Scripts.MonoBehaviours.UI;
using UnityEngine.Events;

namespace _Project.Scripts.Logic.UI
{
  public class Parts
  {
    private readonly UnityAction _showLine;
    private readonly UnityAction _hideLine;

    private Part[] _parts;

    public Parts(Part[] parts, UnityAction showLine, UnityAction hideLine)
    {
      _parts = parts;
      _showLine = showLine;
      _hideLine = hideLine;
    }

    public Part[] GetParts() =>
      _parts;
    
    public void SetParts(Part[] parts) =>
      _parts = parts;
    
    public void EnableColliderForAll(bool value)
    {
      foreach (Part part in _parts) 
        part.BoxCollider.enabled = value;
    }
    
    public void SetActive(Part part, bool value)
    {
      _parts.FirstOrDefault(p => p == part)?.gameObject.SetActive(value);
      
      if(_parts.All(p => !p.gameObject.activeSelf))
        _hideLine?.Invoke();
      else
        _showLine?.Invoke();
    }
    
    public void SetEndConstruction() => 
      _hideLine?.Invoke();
  }
}