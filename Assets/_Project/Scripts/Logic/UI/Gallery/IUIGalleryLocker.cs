namespace _Project.Scripts.Logic.UI.Gallery
{
  public interface IUIGalleryLocker
  {
    bool IsInTransition { get; }
  }
}