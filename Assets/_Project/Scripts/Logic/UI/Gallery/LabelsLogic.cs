using System.Collections.Generic;
using System.Linq;
using _Project.Scripts.Data.Scriptables;
using _Project.Scripts.Data.Serializable;
using _Project.Scripts.Logic.GameLogic.Interfaces;
using _Project.Scripts.Logic.UI.Gallery.Interfaces;
using _Project.Scripts.Services._Interfaces;
using UnityEngine;

namespace _Project.Scripts.Logic.UI.Gallery
{
  public class LabelsLogic : ILabelsLogic
  {
    private readonly IGameSaveSystem _gameSaveSystem;
    private readonly MechanismData[] _mechanisms;

    private readonly List<string> _recentLabels = new List<string>();

    public LabelsLogic(IGameDataProvider gameDataProvider, IGameSaveSystem gameSaveSystem)
    {
      _mechanisms = gameDataProvider.MechanismsInfo.GetMechanisms();

      _gameSaveSystem = gameSaveSystem;
    }

    public bool IsWorldLabeledAsRecent(string worldId) =>
      GetRecentWorldsId().Contains(worldId);

    public bool IsMechanismLabeledAsRecent(string mechanismId) =>
      GetRecentMechanismsId().Contains(mechanismId);

    public void WriteMechanismsShownLabels(string mechanismId)
    {
      if (!_mechanisms.FirstOrDefault(p => p.MechanismId == mechanismId))
        return;

      _gameSaveSystem.Get().AddLabelsToPersistentStorage(new List<string>() {mechanismId});
      _gameSaveSystem.Save();

      _recentLabels.Remove(mechanismId);
    }

    private List<string> GetRecentWorldsId()
    {
      List<string> recentWorldsId = _mechanisms
        .Where(data => GetRecentMechanismsId().Contains(data.MechanismId))
        .Select(p => p.WorldId)
        .ToList();
      
      Debug.Log("Point");
      
      return recentWorldsId;
    }

    private List<string> GetRecentMechanismsId() =>
      _recentLabels;

    public void FindRecentMechanisms()
    {
      var completeMechanisms = _mechanisms.Where(IsMechanismComplete);

      foreach (MechanismData mechanismData in completeMechanisms)
      {
        if (LabelIsAlreadyShown(mechanismData))
          continue;

        if(!_recentLabels.Contains(mechanismData.MechanismId))
          _recentLabels.Add(mechanismData.MechanismId);
      }

      bool LabelIsAlreadyShown(MechanismData mechanismData) =>
        _gameSaveSystem.Get().LabelIsAlreadyShown(mechanismData.MechanismId);

      bool IsMechanismComplete(MechanismData mechanismData) => 
        _gameSaveSystem.Get().GetState(mechanismData.MechanismId) == State.Launched;
    }
  }
}