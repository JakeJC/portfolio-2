using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using _Modules._Analytics;
using _Modules._InApps.Interfaces;
using _Project.FMOD_AudioService.Logic;
using _Project.Scripts.Animations.UI;
using _Project.Scripts.Data.Scriptables;
using _Project.Scripts.Features.Disable_Ads;
using _Project.Scripts.Features.Meditation.Scripts.Logic;
using _Project.Scripts.Features.Stages;
using _Project.Scripts.Features.Stages.Interfaces;
using _Project.Scripts.InApp.Interfaces;
using _Project.Scripts.Logic.Factories.Interfaces;
using _Project.Scripts.Logic.GameLogic.Advices;
using _Project.Scripts.Logic.GameLogic.Interfaces;
using _Project.Scripts.Logic.StatesMachine;
using _Project.Scripts.Logic.StatesMachine.States;
using _Project.Scripts.Logic.UI.Gallery.Data;
using _Project.Scripts.Logic.UI.Gallery.Interfaces;
using _Project.Scripts.MonoBehaviours.UI.Windows.Gallery;
using _Project.Scripts.Services._Interfaces;
using _Project.Scripts.Services.Environment;
using _Project.Scripts.Services.FX;
using _Project.Scripts.Services.InputResolver;
using _Project.Scripts.Services.LeanTouchCamera.Scripts.Interfaces;
using _Project.Scripts.Services.LeanTouchCamera.Scripts.Logic;
using _Project.Scripts.Services.LoadScreen.Logic;
using _Project.Scripts.Services.Localization.Logic;
using _Project.Scripts.Services.MaterialService.Interfaces;
using AppacheAds.Scripts.Interfaces;
using AppacheAnalytics.Scripts.Interfaces;
using AppacheRemote.Scripts.Interfaces;
using DG.Tweening;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI.Extensions;
using Object = UnityEngine.Object;

namespace _Project.Scripts.Logic.UI.Gallery
{
  public class GalleryLogic
    : IUIGalleryLocker
  {
    private readonly GameStateMachine _gameStateMachine;
    
    private readonly IGalleryFactory _galleryFactory;
    private readonly IUIFactory _uiFactory;
    private readonly IPrivacyPolicyFactory _privacyPolicyFactory;
    private readonly IGameLevelFactory _gameLevelFactory;
    private readonly IGameServicesFactory _gameServicesFactory;

    private readonly IAssetProvider _assetProvider;
    private readonly IAudioService _audioService;
    private readonly IInputResolver _inputResolver;
    private readonly IGameSaveSystem _gameSaveSystem;
    private readonly ILocaleSystem _localeSystem;
    private readonly ILoadScreenService _loadScreenService;
    private readonly IGameDataProvider _gameDataProvider;
    private readonly ICameraController _cameraController;
    private readonly IAnalytics _analytics;
    private readonly IFXController _fxController;
    private readonly ICurrentScreen _screenManager;
    private readonly IStagesService _stagesService;
    private readonly IRemote _remote;
    private readonly IAdsService _ads;
    private readonly IInAppAccessor _inAppAccessor;

    private GalleryWindowWorlds _galleryWindowWorlds;
    private GalleryWindowMechanisms _galleryWindowMechanisms;
    private GalleryUI _galleryUI;
    private GalleryWindowAdvices _galleryWindowAdvices;

    private readonly ILabelsLogic _labelsLogic;
    private int _targetMechanismIndex;

    private bool _isAdvicesSnapped;

    public bool IsInTransition { get; private set; }

    private readonly List<string> _addresables = new List<string>();
    private GalleryPlay _playButton;

    private readonly IDisableAdsLogic _disableAdsLogic;

    private readonly IEnvironmentController _environmentController;
    private IMaterialService _materialService;
    
    private IMeditation _meditation;
    private IInAppService _inAppService;

    public GalleryLogic(
      GameStateMachine gameStateMachine,
      IGalleryFactory galleryFactory,
      IUIFactory uiFactory,
      IPrivacyPolicyFactory privacyPolicyFactory,
      IGameLevelFactory gameLevelFactory,
      IGameServicesFactory gameServicesFactory,
      IAssetProvider assetProvider,
      IAudioService audioService,
      IInputResolver inputResolver,
      IGameSaveSystem gameSaveSystem,
      ILocaleSystem localeSystem,
      IGameDataProvider gameDataProvider,
      IAnalytics analytics,
      ICameraController cameraController,
      ILoadScreenService loadScreenService,
      IFXController fxController,
      ICurrentScreen screenManager,
      IRemote remote,
      IStagesService stagesService, 
      IInAppAccessor inAppAccessor,
      IAdsService ads,
      IDisableAdsLogic disableAdsLogic,
      IEnvironmentController environmentController,
      IMaterialService materialService,
      IInAppService inAppService)
    {
      _inAppService = inAppService;
      _materialService = materialService;
      _environmentController = environmentController;
      _disableAdsLogic = disableAdsLogic;
      _inAppAccessor = inAppAccessor;
      _remote = remote;
      _ads = ads;
      _stagesService = stagesService;
      _screenManager = screenManager;
      _fxController = fxController;
      _loadScreenService = loadScreenService;
      _gameStateMachine = gameStateMachine;
      _galleryFactory = galleryFactory;
      _uiFactory = uiFactory;
      _privacyPolicyFactory = privacyPolicyFactory;
      _gameLevelFactory = gameLevelFactory;
      _gameServicesFactory = gameServicesFactory;
      _assetProvider = assetProvider;
      _audioService = audioService;
      _inputResolver = inputResolver;
      _gameSaveSystem = gameSaveSystem;
      _localeSystem = localeSystem;
      _analytics = analytics;
      _gameDataProvider = gameDataProvider;
      _cameraController = cameraController;

      _labelsLogic = new LabelsLogic(_gameDataProvider, _gameSaveSystem);
      _labelsLogic.FindRecentMechanisms();
    }

    public void Cleanup()
    {
      _uiFactory.RemoveSettingsWindow();

      foreach (string addresable in _addresables)
        _assetProvider.Release(addresable);
      
      CloseMeditationWindow();
      
      if (_galleryWindowWorlds)
        Object.Destroy(_galleryWindowWorlds.gameObject);
      if (_galleryWindowMechanisms)
        Object.Destroy(_galleryWindowMechanisms.gameObject);
      if (_galleryWindowAdvices)
        Object.Destroy(_galleryWindowAdvices.gameObject);
      if (_galleryUI)
        Object.Destroy(_galleryUI.gameObject);
    }

    public void SpawnGalleryWorldsWindow(ButtonType screenType)
    {
      CreateGalleryWorldsWindow();
      CreateGalleryWorldsItems();

      CreateAdvicesWindow();
      CreateAdvicesItems();

      CreateSettings();
      CreateGalleryUI();

      switch (screenType)
      {
        case ButtonType.Worlds: 
          CloseMeditationWindow();
          OpenGalleryWorldsWindow();
          break;
        case ButtonType.Advices:    
          CloseMeditationWindow();
          OpenAdvicesWindow();
          _galleryUI.ToAdvices();
          break;
        case ButtonType.Meditation:
          OpenMeditationWindow();
          _galleryUI.ToMeditation();
          break;
      }

      UnlockMeditation();
    }

    private void UnlockMeditation() => 
      _galleryUI.UnlockMeditation();

    private void OpenGalleryWorldsWindow()
    {    
      CloseMeditationWindow();

      IsInTransition = true;

      _galleryWindowAdvices.CloseWindow(() => IsInTransition = false);
      _galleryWindowWorlds.OpenWindow();
    }

    private async void OpenAdvicesWindow()
    {
      if(_galleryUI.Advices.IsOn)
        return;
      
      CloseMeditationWindow();
      
      IsInTransition = true;
      
      CloseGalleryMechanismsWindow();

      _galleryWindowWorlds.CloseWindow(() => IsInTransition = false);
      _galleryWindowAdvices.OpenWindow();

      await SnapToLastAdvice();
    }

    private void CreateGalleryWorldsWindow()
    {
      _galleryWindowWorlds = _galleryFactory.CreateGalleryWindowWorlds();
      _galleryWindowWorlds.Construct(_audioService, _localeSystem);
      _galleryWindowWorlds.Settings.onClick.AddListener(OpenSettingsWindow);
      
      _playButton = _galleryFactory.CreateGalleryPlayButton();
    }

    private void CreateGalleryUI()
    {
      _galleryUI = _galleryFactory.CreateGalleryUI();
      _galleryUI.Construct(_gameDataProvider, this, _localeSystem, _audioService, OpenGalleryWorldWithButton, OpenAdvicesWindow, OpenMeditationWindow);
    }

    private void OpenMeditationWindow()
    {       
      if(_galleryUI.Meditation.IsOn)
        return;
      
      _ads.CurrentInterstitial.ShowInterstitial(ScreenNames.Gallery, null, null);

      if (_galleryWindowMechanisms)
        _galleryWindowMechanisms.CloseWindow();
      
      if (_galleryWindowWorlds)
        _galleryWindowWorlds.CloseWindow();
      
      if(_galleryWindowAdvices)
        _galleryWindowAdvices.CloseWindow();
      
      _meditation = new Meditation(_inAppService, _assetProvider, _gameSaveSystem, _gameDataProvider, _loadScreenService, 
        _localeSystem, _audioService, _cameraController, _ads, _environmentController, _materialService);

      _meditation.AddOnHideUIEvent(HideGalleryUI);
      _meditation.Launch(_galleryUI.transform.SetAsLastSibling);
    }

    private void HideGalleryUI(bool isHide)
    {
      var offset = -Screen.height / 2;
      var duration = .5f;
      
      _galleryUI.transform.DOLocalMove(
        _galleryUI.transform.localPosition + offset * (isHide ? Vector3.up : Vector3.down), duration).SetEase(Ease.Linear);
    }
    
    private void CloseMeditationWindow()
    {
      _meditation?.CloseMeditation();
      _meditation = null;
    }

    private void OpenGalleryWorldWithButton()
    {
      if(_galleryUI.Worlds.IsOn)
        return;
      
      _ads.CurrentInterstitial.ShowInterstitial(ScreenNames.Gallery, null, null);
      OpenGalleryWorldsWindow();
    }
    
    private void CreateAdvicesWindow()
    {
      _galleryWindowAdvices = _galleryFactory.CreateAdvicesWindow();
      _galleryWindowAdvices.Construct(_audioService, _localeSystem);
      _galleryWindowAdvices.Settings.onClick.AddListener(OpenSettingsWindow);
    }

    private void CreateAdvicesItems()
    {
      var advicesData = new Advices(_assetProvider, _gameSaveSystem);
      List<AdviceData> loadAdvicesData = advicesData.GetOpenedAdvices();

      foreach (AdviceData adviceData in loadAdvicesData)
      {
        GalleryItemAdvice galleryItemAdvice = _galleryFactory.CreateAdvicesItem(_galleryWindowAdvices);
        galleryItemAdvice.Construct(_assetProvider, _localeSystem, adviceData);
      }
    }

    private void CreateSettings()
    {
      _uiFactory.SpawnSettingsWindow();
      _uiFactory.SettingsWindow.Construct(_gameStateMachine, _uiFactory, _gameLevelFactory, _gameServicesFactory,
        _gameSaveSystem, _audioService, _inputResolver, _localeSystem, _loadScreenService, _analytics, _cameraController,
        _fxController, _gameDataProvider, _privacyPolicyFactory, _screenManager, _inAppAccessor, _ads, _remote, _disableAdsLogic, true);
    }

    private void OpenSettingsWindow() =>
      _uiFactory.SettingsWindow.OpenWindow();

    private async void CreateGalleryWorldsItems()
    {
      WorldData[] worldsData = _gameDataProvider.WorldsInfo.GetSortedWorldsData();
      WorldData[] openWorlds = worldsData
        .Where(p => _gameSaveSystem.Get().GetOpenWorlds().Contains(p.WorldId)).ToArray();

      MechanismData[] mechanisms = _gameDataProvider.MechanismsInfo.GetMechanisms();

      SpawnUnlockedWorldCards(openWorlds);
      SetupPlayButton();
      SpawnLockedWorldCards(worldsData, openWorlds);

      await SnapToWorld(openWorlds.Length);
    }

    private void SpawnUnlockedWorldCards(WorldData[] openWorlds)
    {
      foreach (var worldData in openWorlds)
      {
        GalleryItemWorlds galleryItem = _galleryFactory.CreateGalleryItemWorlds(_galleryWindowWorlds);

        galleryItem.Construct(_localeSystem, _audioService, worldData);
        galleryItem.Unlock();
        galleryItem.SetOnClickEvent(_ => { OnWorldItemClick(worldData); });
        galleryItem.ShowLabel(_labelsLogic.IsWorldLabeledAsRecent(worldData.WorldId));
      }
    }

    private void SetupPlayButton()
    {
      if (!_gameDataProvider.MechanismsInfo.IsSomethingLocked())
      {
        _playButton.Hide();
        return;
      }

      _playButton.Construct(_audioService, _localeSystem, _analytics, ExecuteWhenComplete);
      
      _playButton.SetAvailable(true);
      _playButton.SetOnClickEvent(OnWorldPlayableItemClick);
    }

    private void SpawnLockedWorldCards(WorldData[] worldsData, WorldData[] openWorlds)
    {
      int cardsCount = worldsData.Length - openWorlds.Length;
      
      if (worldsData.Length == openWorlds.Length)
        SpawnLockedCards(cardsCount);
      else
      {
        SpawnPlusCard();
        SpawnLockedCards(cardsCount - 1);
      }

      void SpawnPlusCard()
      {
        GalleryItemPlay playCard = _galleryFactory.CreateGalleryItemPlay(_galleryWindowWorlds);
        playCard.Construct(_audioService, OnWorldPlayableItemClick);
        playCard.ReadyForPlay();
      }

      void SpawnLockedCards(int count)
      {
        for (int i = 0; i < count; i++)
        {
          GalleryItemPlay lockItem = _galleryFactory.CreateGalleryItemPlay(_galleryWindowWorlds);
          lockItem.LockWithoutTimer();
        }
      }
    }
    
    private void ExecuteWhenComplete(GalleryPlay button)
    {
      button.SetAvailable(true);
      button.SetOnClickEvent(OnWorldPlayableItemClick);
    }

    private void OnWorldItemClick(WorldData worldData)
    {
      _ads.CurrentInterstitial.ShowInterstitial(ScreenNames.Gallery, null, null);
      SpawnGalleryMechanismsWindow(worldData);
    }
    
    private void OnWorldPlayableItemClick()
    {
      _ads.CurrentInterstitial.ShowInterstitial(ScreenNames.Interview, OnClick,OnClick);

      void OnClick() => 
        _stagesService.OpenStageDescription(Stages.Interview, () => _gameStateMachine.Enter<LoadInterviewState>(), 0, _analytics);
    }

    private void OnMechanismItemClick(MechanismData mechanismData)
    {
      _ads.CurrentInterstitial.ShowInterstitial(ScreenNames.Interview, OnClick,OnClick);

      void OnClick()
      {
        _labelsLogic.WriteMechanismsShownLabels(mechanismData.MechanismId);
        _loadScreenService.Launch<BlackTransition>(null, () => ToGameScreen(mechanismData));
      }
    }

    private async Task ToGameScreen(MechanismData mechanismData)
    {
      await _gameStateMachine.AsyncEnter<LoadGameEnvironmentState, LaunchInfo>(new LaunchInfo()
      {
        MechanismId = mechanismData.MechanismId
      });
    }

    private void SpawnGalleryMechanismsWindow(WorldData worldData)
    {
      _labelsLogic.FindRecentMechanisms();

      CreateGalleryMechanismsWindow(_localeSystem.GetText("Main", worldData.KeyName));
      CreateGalleryMechanismsItems(worldData);
      OpenGalleryMechanismsWindow();

      _galleryUI.transform.SetAsLastSibling();
    }

    private void CreateGalleryMechanismsWindow(string worldName)
    {
      _galleryWindowMechanisms = _galleryFactory.CreateGalleryWindowMechanisms();
      _galleryWindowMechanisms.Construct(_audioService, CloseGalleryMechanismsWindow,
        worldName,
        onDisappearFinished: () =>
        {
          if (_galleryWindowMechanisms)
            Object.Destroy(_galleryWindowMechanisms.gameObject);
        });
    }
    
    private void CreateGalleryMechanismsItems(WorldData worldData)
    {
      var mechanisms = _gameDataProvider.MechanismsInfo.GetUnlockedMechanisms();
      var mechanismsOfCurrentWorld = mechanisms.Where(m => m.WorldId == worldData.WorldId);

      _targetMechanismIndex = 0;

      foreach (var mechanismData in mechanismsOfCurrentWorld)
      {
        GalleryItemMechanisms galleryItem = _galleryFactory.CreateGalleryItemMechanisms(_galleryWindowMechanisms);
        var texture = _galleryFactory.GetTexture(mechanismData.CoverAddress);

        if (!_addresables.Contains(mechanismData.CoverAddress))
          _addresables.Add(mechanismData.CoverAddress);

        galleryItem.Construct(_localeSystem, _audioService,
          onClick: () => OnMechanismItemClick(mechanismData),
          texture, worldData);

        galleryItem.Unlock();

        bool isMechanismLabeledAsRecent = _labelsLogic.IsMechanismLabeledAsRecent(mechanismData.MechanismId);
        galleryItem.ShowLabel(isMechanismLabeledAsRecent);
      }
    }

    private async Task SnapToWorld(int targetScrollIndex)
    {
      await Task.Delay(100);

      var horizontalScrollSnap = _galleryWindowWorlds.GetComponentInChildren<HorizontalScrollSnap>();
      if (horizontalScrollSnap)
        horizontalScrollSnap.GoToScreen(targetScrollIndex);
    }

    private async Task SnapToMechanism(int targetScrollIndex)
    {
      await Task.Delay(100);

      var horizontalScrollSnap = _galleryWindowMechanisms.GetComponentInChildren<HorizontalScrollSnap>();
      if (horizontalScrollSnap)
        horizontalScrollSnap.GoToScreen(targetScrollIndex);
    }

    private async Task SnapToLastAdvice()
    {
      if (_isAdvicesSnapped)
        return;

      await Task.Delay(100);

      var horizontalScrollSnap = _galleryWindowAdvices.GetComponentInChildren<HorizontalScrollSnap>();
      if (horizontalScrollSnap)
        horizontalScrollSnap.GoToScreen(_galleryWindowAdvices.galleryItemsParent.childCount - 1);

      _isAdvicesSnapped = true;
    }

    private async void OpenGalleryMechanismsWindow()
    {
      IsInTransition = true;
      _galleryWindowMechanisms.OpenWindow(() => IsInTransition = false);
      await SnapToMechanism(_targetMechanismIndex);
    }

    private void CloseGalleryMechanismsWindow()
    {
      _ads.CurrentInterstitial.ShowInterstitial(ScreenNames.Gallery, null, null);
      
      if (_galleryWindowMechanisms)
        _galleryWindowMechanisms.CloseWindow();
    }
  }
}