namespace _Project.Scripts.Logic.UI.Gallery.Interfaces
{
  public interface ILabelsLogic
  {
    bool IsWorldLabeledAsRecent(string worldId);
    bool IsMechanismLabeledAsRecent(string mechanismId);
    void WriteMechanismsShownLabels(string mechanismId);
    void FindRecentMechanisms();
  }
}