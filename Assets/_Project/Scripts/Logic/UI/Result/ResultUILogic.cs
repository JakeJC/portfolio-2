using System;
using System.Threading.Tasks;
using _Modules._Analytics;
using _Project.FMOD_AudioService.Logic;
using _Project.Scripts.Animations.UI;
using _Project.Scripts.Features.Rate_Feature;
using _Project.Scripts.Features.Stages;
using _Project.Scripts.Features.Stages.Interfaces;
using _Project.Scripts.Logic.Factories.Interfaces;
using _Project.Scripts.Logic.GameLogic.Advices;
using _Project.Scripts.Logic.GameLogic.Advices.Interfaces;
using _Project.Scripts.Logic.GameLogic.Interfaces;
using _Project.Scripts.Logic.StatesMachine;
using _Project.Scripts.Logic.StatesMachine.States;
using _Project.Scripts.Logic.UI.Gallery;
using _Project.Scripts.Logic.UI.Result.Interfaces;
using _Project.Scripts.MonoBehaviours.UI.Windows;
using _Project.Scripts.MonoBehaviours.UI.Windows.Gallery;
using _Project.Scripts.Services._Interfaces;
using _Project.Scripts.Services.LoadScreen.Logic;
using _Project.Scripts.Services.Localization.Logic;
using AppacheAds.Scripts.Interfaces;
using AppacheAnalytics.Scripts.Interfaces;
using AppacheRemote.Scripts.Interfaces;
using UnityEngine;
using Object = UnityEngine.Object;

namespace _Project.Scripts.Logic.UI.Result
{
  public class ResultUILogic : IResultUILogic, IUIGalleryLocker
  {
    private readonly GameStateMachine _gameStateMachine;
    private readonly IAssetProvider _assetProvider;

    private ResultWindow _resultWindow;

    private readonly IRemote _remote;
    private readonly IAnalytics _analytics;
    private readonly IAdsService _adsService;
    private readonly IGameDataProvider _gameDataProvider;
    private readonly ILoadScreenService _loadScreenService;

    private readonly IAdvices _advices;
    private ILocaleSystem _localeSystem;

    private IStagesService _stagesService;

    private GalleryUI _galleryUI;

    public ResultUILogic(GameStateMachine gameStateMachine, IResultUIFactory resultUIFactory, IAssetProvider assetProvider,
      ILocaleSystem localeSystem, IGameDataProvider gameDataProvider,
      IAudioService audioService, IAnalytics analytics,
      ILoadScreenService loadScreenService, IAdvices advices, IStagesService stagesService, IRemote remote, IAdsService adsService)
    {
      _remote = remote;
      _advices = advices;
      _analytics = analytics;
      _adsService = adsService;
      _loadScreenService = loadScreenService;
      _gameDataProvider = gameDataProvider;
      _assetProvider = assetProvider;
      _gameStateMachine = gameStateMachine;

      SpawnWindow(resultUIFactory, localeSystem, audioService, analytics, stagesService);
    }

    public void Cleanup() =>
      Object.Destroy(_resultWindow.gameObject);

    private void SpawnWindow(IResultUIFactory resultUIFactory, ILocaleSystem localeSystem, IAudioService audioService,
      IAnalytics analytics, IStagesService stagesService)
    {
      _stagesService = stagesService;
      _localeSystem = localeSystem;
      _resultWindow = resultUIFactory.CreateResultWindow(_gameDataProvider.FirstSession.IsFirstSession());


      _resultWindow.Construct
      (localeSystem,
        audioService,
        _gameDataProvider,
        analytics,
        ToGallery,
        Quit,
        Rate,
        ToMeditation,
        ToNextMechanism,
        _gameDataProvider.FirstSession.IsFirstSession(),
        _remote
      );

      _galleryUI = resultUIFactory.GalleryUI;

      if (_galleryUI != null)
      {
        _galleryUI.Construct(_gameDataProvider, this, localeSystem, audioService,
          ToGallery, ToAdvices, ToMeditation);

        _galleryUI.UnlockMeditation();
        _galleryUI.ToAdvices();
      }

      SetNextAdvice();

      _resultWindow.OpenWindow();

      _stagesService.ShowStageCounterView(Stages.Advice, 0);
    }

    private void ToAdvices() =>
      Debug.Log("Already Choosen");

    private void Rate()
    {
      Rate rate = new Rate();
      rate.RequestReview(rate.LaunchReview);
    }

    private void Quit() =>
      ToQuit();

    private void ToQuit() =>
      _loadScreenService.ToQuit();

    private void ToGallery()
    {
      if (_adsService.AdsEnabled)
        _adsService.CurrentInterstitial.ShowInterstitial(ScreenNames.Game, TransitToGallery, TransitToGallery);
      else
        TransitToGallery();

      void TransitToGallery()
      {
        _stagesService.HideStageCounterView();
        _loadScreenService.Launch<BlackTransition>(null, ToLoadGalleryState);
      }
    }

    private void ToMeditation()
    {
      _stagesService.HideStageCounterView();
      _loadScreenService.Launch<BlackTransition>(null, ToLoadGalleryStateMeditation);
    }

    private async Task ToLoadGalleryState() =>
      await _gameStateMachine.AsyncEnter<LoadGalleryState, ButtonType>(ButtonType.Worlds);

    private async Task ToLoadGalleryStateMeditation() =>
      await _gameStateMachine.AsyncEnter<LoadGalleryState, ButtonType>(ButtonType.Meditation);


    private void SetNextAdvice()
    {
      AdviceData adviceData = _advices.GetAdvice();
      var icon = _assetProvider.GetResource<Sprite>(adviceData.IconPath);

      string title = _localeSystem.GetText("UI/Advices/AdviceTitles", adviceData.TitleKey);
      string text = _localeSystem.GetText("UI/Advices/Advices", adviceData.TextKey);

      _resultWindow.SetAdvice(title, text, icon);
    }

    private void ToNextMechanism()
    {
      _analytics.Send("btn_stage_two");

      if (_gameDataProvider.FirstSession.IsFirstSession())
      {
        if (_adsService.AdsEnabled)
          _adsService.CurrentInterstitial.ShowInterstitial(ScreenNames.Game, Next, Next);
      }
      else
        Next();

      void Next()
      {
        _loadScreenService.Launch<BlackTransition>(null, async ()
          => await _gameStateMachine.AsyncEnter<LoadGameEnvironmentState>());
      }
    }

    public bool IsInTransition { get; }
  }
}