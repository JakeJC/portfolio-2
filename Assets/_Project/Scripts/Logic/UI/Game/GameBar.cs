using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace _Project.Scripts.Logic.UI.Game
{
  public class GameBar : MonoBehaviour
  {
    private static readonly int Fill = Shader.PropertyToID("_Fill");
    
    [SerializeField] private CanvasGroup Group;
    [SerializeField] private Image Foreground;
    [SerializeField] private CanvasGroup White;
    [SerializeField] private CanvasGroup Blink;

    private int _counter;
    private int _maxCounter;

    private Material _foregroundMaterial;

    public void Construct(int maxCounter)
    {
      _maxCounter = maxCounter;

      PrepareMaterial();
      InitializeAnimatedObjects();

      void PrepareMaterial()
      {
        _foregroundMaterial = new Material(Foreground.material);
        Foreground.material = _foregroundMaterial;
        _foregroundMaterial.SetFloat(Fill, 0);
      }

      void InitializeAnimatedObjects()
      {
        Foreground.gameObject.SetActive(true);
        White.gameObject.SetActive(false);
        Blink.gameObject.SetActive(false);
      }
    }
    
    public void AddStep()
    {
      if(_counter == _maxCounter)
        return;
      
      _counter++;
      float fill = (float) _counter / _maxCounter;
      
      AnimateStep(fill);
    }

    private void Complete()
    {
      GlowBar();
      AnimateBlinkAroundBar();
    }

    private void GlowBar()
    {
      White.gameObject.SetActive(true);
      White.alpha = 0;
      White.DOFade(1, 0.25f);
    }

    private void AnimateBlinkAroundBar()
    {
      Blink.gameObject.SetActive(true);
      Blink.alpha = 0;
      Blink.DOFade(0.5f, 0.25f).SetLoops(2, LoopType.Yoyo).onComplete += () =>
        Group.DOFade(0, 0.25f);
      Blink.transform.DOScale(new Vector2(1.1f, 3f), 0.5f);
    }

    private void AnimateStep(float fill)
    {     
      float progress = 0;

      Tweener animate = DOTween.To(x => progress = x, _foregroundMaterial.GetFloat(Fill), fill, 1f)
        .SetEase(Ease.OutExpo);
      animate.onUpdate += () => _foregroundMaterial.SetFloat(Fill, progress);
      animate.onComplete += () =>
      {
        if (_counter == _maxCounter)
          Complete();
      };
    }
  }
}