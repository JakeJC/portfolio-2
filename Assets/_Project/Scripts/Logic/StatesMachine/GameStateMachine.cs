using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using _Modules._InApps.Interfaces;
using _Project.FMOD_AudioService.Logic;
using _Project.Scripts.Features.Disable_Ads;
using _Project.Scripts.Features.Stages.Interfaces;
using _Project.Scripts.InApp.Interfaces;
using _Project.Scripts.Logic.GameLogic.Interfaces;
using _Project.Scripts.Logic.StatesMachine.Interfaces;
using _Project.Scripts.Logic.StatesMachine.States;
using _Project.Scripts.Services._Interfaces;
using _Project.Scripts.Services.Environment;
using _Project.Scripts.Services.FX;
using _Project.Scripts.Services.InputResolver;
using _Project.Scripts.Services.LeanTouchCamera.Scripts.Interfaces;
using _Project.Scripts.Services.LoadScreen.Logic;
using _Project.Scripts.Services.Localization.Logic;
using _Project.Scripts.Services.MaterialService.Interfaces;
using AppacheAds.Scripts.Interfaces;
using AppacheAnalytics.Scripts.Interfaces;
using AppacheRemote.Scripts.Interfaces;

namespace _Project.Scripts.Logic.StatesMachine
{
  public class GameStateMachine
  {
    private readonly Dictionary<Type, IExitableState> _states;
    private IExitableState _activeExitableState;
    
    public GameStateMachine(Factories.Factories factories,
      IGameSaveSystem gameSaveSystem,
      IAssetProvider assetProvider, 
      ILocaleSystem localeSystem, 
      ICameraController cameraController, 
      IInputResolver inputResolver, 
      IAudioService audioService, 
      IFXController fxController,
      IMaterialService materialService,
      IGameDataProvider gameMetaDataService,
      IAnalytics analytics,
      ILoadScreenService loadScreenService, 
      ICurrentScreen screenManager,
      IRemote remote,
      IAdsService ads,
      IStagesService stagesService,
      IInAppAccessor inAppAccessor,
      IDisableAdsLogic disableAdsLogic,
      IEnvironmentController environmentController,
      IInAppService inAppService)
    {
      _states = new Dictionary<Type, IExitableState>()
      {
        [typeof(LoadSoundState)] = new LoadSoundState(this, audioService, remote, assetProvider, localeSystem, gameMetaDataService, ads, inputResolver),
        [typeof(LoadEntryState)] = new LoadEntryState(this, factories.UIFactory, gameMetaDataService, loadScreenService,
          screenManager, stagesService, inAppAccessor, ads, analytics, remote, gameSaveSystem, factories.PrivacyPolicyFactory,
          audioService, inputResolver, localeSystem),
        [typeof(LoadGameEnvironmentState)] = new LoadGameEnvironmentState(this, factories.GameLevelFactory, cameraController,
          inputResolver, screenManager, gameMetaDataService, analytics, stagesService, remote, environmentController),
        [typeof(LoadGameUIState)] = new LoadGameUIState(this,
          factories.UIFactory, materialService),
        [typeof(LoadGameServicesState)] = new LoadGameServicesState(),
        [typeof(LoadGameState)] = new LoadGameState( this, inputResolver, gameSaveSystem, cameraController, 
          factories.GameServicesFactory, factories.TutorialFactory, factories.UIFactory, 
          factories.PrivacyPolicyFactory, factories.GameLevelFactory, audioService, fxController, materialService, assetProvider, 
          localeSystem, gameMetaDataService, analytics, loadScreenService, screenManager, remote, inAppAccessor, ads, stagesService, disableAdsLogic),
        [typeof(LoadInterviewState)] = new LoadInterviewState(this, factories.InterviewFactory, materialService, 
          analytics, loadScreenService, gameMetaDataService, screenManager, gameSaveSystem, remote, ads, stagesService, inAppAccessor),
        [typeof(LoadEndContentState)] = new LoadEndContentState(this, assetProvider, localeSystem, audioService, loadScreenService, gameMetaDataService),
        [typeof(LoadResultUIState)] = new LoadResultUIState(this, factories.ResultUIFactory, assetProvider, 
          localeSystem, gameMetaDataService, materialService, audioService, analytics, loadScreenService,
          screenManager, stagesService, remote, ads),
        [typeof(LoadGalleryState)] = new LoadGalleryState(this, factories.GalleryFactory, factories.UIFactory, 
          factories.PrivacyPolicyFactory, factories.GameLevelFactory, factories.GameServicesFactory,
          assetProvider, audioService, inputResolver, gameSaveSystem, localeSystem, gameMetaDataService,
          analytics, cameraController, loadScreenService, fxController, screenManager, remote, ads, stagesService, inAppAccessor, 
          disableAdsLogic, environmentController, materialService, inAppService),
        [typeof(LoadPolicyState)] = new LoadPolicyState(this, factories.PrivacyPolicyFactory, gameSaveSystem,
          audioService, inputResolver, localeSystem, screenManager, analytics, remote),
        [typeof(LoadEndDecisionState)] = new LoadEndDecisionState(this, assetProvider, gameSaveSystem, localeSystem, audioService,
          loadScreenService, remote, stagesService, analytics, gameMetaDataService, ads),
        [typeof(LoadFinishTutorialState)] = new LoadFinishTutorialState(this, assetProvider, localeSystem, audioService, loadScreenService,
          inAppAccessor, disableAdsLogic, stagesService, analytics, ads)
      };
    }

    public void Enter<TState>() where TState : class, IState
    {
      IState exitableState = ChangeState<TState>();
      exitableState.Enter();
    }

    public void Enter<TState, TPayload>(TPayload payload) where TState : class, IPayloadedState<TPayload>
    {
      IPayloadedState<TPayload> state = ChangeState<TState>();
      state.Enter(payload);
    }

    public async Task AsyncEnter<TState, TPayload>(TPayload payload) where TState : class, IPayloadedState<TPayload>
    {
      IPayloadedState<TPayload> state = ChangeState<TState>();
      await state.AsyncEnter(payload);
    }
    
    public async Task AsyncEnter<TState>() where TState : class, IState
    {
      IState exitableState = ChangeState<TState>();
      await exitableState.AsyncEnter();
    }
    
    private TState ChangeState<TState>() where TState : class, IExitableState
    {
      _activeExitableState?.Exit();
      TState state = GetState<TState>();
      _activeExitableState = state;
      return state;
    }

    private TState GetState<TState>() where TState : class, IExitableState =>
      _states[typeof(TState)] as TState;
  }
}
