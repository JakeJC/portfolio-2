using System.Threading.Tasks;
using _Project.Scripts.Logic.StatesMachine.Interfaces;
using _Project.Scripts.Services.InputResolver;

namespace _Project.Scripts.Logic.StatesMachine.States
{
  public class LoadGameServicesState : IPayloadedState<IInputResolver>
  {
    public Task AsyncEnter(IInputResolver gameLogic) => 
      throw new System.NotImplementedException();

    public void Enter(IInputResolver inputResolver)
    {}
    
    public void Exit() { }
  }
}