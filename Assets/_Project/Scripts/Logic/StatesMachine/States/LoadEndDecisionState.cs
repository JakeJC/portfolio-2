using System.Threading.Tasks;
using _Project.FMOD_AudioService.Logic;
using _Project.Scripts.Features.End_Decision_Window;
using _Project.Scripts.Features.Stages.Interfaces;
using _Project.Scripts.Logic.GameLogic.Interfaces;
using _Project.Scripts.Logic.StatesMachine.Interfaces;
using _Project.Scripts.Services._Interfaces;
using _Project.Scripts.Services.LoadScreen.Logic;
using _Project.Scripts.Services.Localization.Logic;
using AppacheAds.Scripts.Interfaces;
using AppacheAnalytics.Scripts.Interfaces;
using AppacheRemote.Scripts.Interfaces;

namespace _Project.Scripts.Logic.StatesMachine.States
{
  public class LoadEndDecisionState : IState
  {
    private readonly GameStateMachine _gameStateMachine;
    
    private readonly IAssetProvider _assetProvider;
    private readonly IGameSaveSystem _gameSaveSystem;
    private readonly ILocaleSystem _localeSystem;
    private readonly IAudioService _audioService;
    private readonly ILoadScreenService _loadScreenService;

    private readonly IStagesService _stagesService;
    private readonly IRemote _remote;
    private readonly IAnalytics _analytics;
    private readonly IGameDataProvider _gameDataProvider;
    private readonly IAdsService _ads;
    
    private EndDecision _endDecision;

    public LoadEndDecisionState(GameStateMachine gameStateMachine, IAssetProvider assetProvider, 
      IGameSaveSystem gameSaveSystem, ILocaleSystem localeSystem, IAudioService audioService, ILoadScreenService loadScreenService,
      IRemote remote, IStagesService stagesService, IAnalytics analytics, IGameDataProvider gameDataProvider, IAdsService ads)
    {
      _gameDataProvider = gameDataProvider;
      _analytics = analytics;
      _remote = remote;
      _stagesService = stagesService;
      _loadScreenService = loadScreenService;
      _audioService = audioService;
      _localeSystem = localeSystem;
      _gameSaveSystem = gameSaveSystem;
      _assetProvider = assetProvider;
      _gameStateMachine = gameStateMachine;
      _ads = ads;
    }
    
    public void Enter()
    {
      _endDecision = new EndDecision(_gameStateMachine, _assetProvider, _gameSaveSystem, _localeSystem, 
        _audioService, _loadScreenService, _stagesService, _analytics, _gameDataProvider, _remote, _ads);
      _endDecision.Open();
    }

    public void Exit()
    {}

    public Task AsyncEnter() => 
      throw new System.NotImplementedException();
  }
}