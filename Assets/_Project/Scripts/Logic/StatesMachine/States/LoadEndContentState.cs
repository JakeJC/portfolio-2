using System.Threading.Tasks;
using _Project.FMOD_AudioService.Logic;
using _Project.Scripts.Features.End_Content;
using _Project.Scripts.Logic.GameLogic.Interfaces;
using _Project.Scripts.Logic.StatesMachine.Interfaces;
using _Project.Scripts.Services._Interfaces;
using _Project.Scripts.Services.LoadScreen.Logic;
using _Project.Scripts.Services.Localization.Logic;

namespace _Project.Scripts.Logic.StatesMachine.States
{
  public class LoadEndContentState : IState
  {
    private readonly GameStateMachine _gameStateMachine;
    
    private readonly IAssetProvider _assetProvider;
    private readonly ILocaleSystem _localeSystem;
    private readonly IAudioService _audioService;
    private readonly ILoadScreenService _loadScreenService;
    private readonly IGameDataProvider _gameDataProvider;

    private EndContent _endContent;

    public LoadEndContentState(GameStateMachine gameStateMachine, IAssetProvider assetProvider,
      ILocaleSystem localeSystem, IAudioService audioService, ILoadScreenService loadScreenService, IGameDataProvider gameDataProvider)
    {
      _gameDataProvider = gameDataProvider;
      _loadScreenService = loadScreenService;
      _audioService = audioService;
      _localeSystem = localeSystem;
      _assetProvider = assetProvider;
      _gameStateMachine = gameStateMachine;
    }
    
    public void Enter()
    {
      _endContent = new EndContent(_gameStateMachine, _assetProvider, _localeSystem, _audioService, _loadScreenService, _gameDataProvider);
      _endContent.Open();
    }

    public void Exit()
    {}

    public Task AsyncEnter() => 
      throw new System.NotImplementedException();
  }
}