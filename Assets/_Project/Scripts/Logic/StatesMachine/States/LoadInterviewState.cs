﻿using System.Linq;
using System.Threading.Tasks;
using _Modules._Analytics;
using _Project.Scripts.Animations.UI;
using _Project.Scripts.Features.Interview.Scripts;
using _Project.Scripts.Features.Stages;
using _Project.Scripts.Features.Stages.Interfaces;
using _Project.Scripts.InApp.Interfaces;
#if TEST
using _Project.Scripts.Data.Scriptables;
using _Project.Scripts.Features.End_Interview.Scripts.Logic.Test;
using UnityEngine;
#endif
using _Project.Scripts.Logic.Factories.Interfaces;
using _Project.Scripts.Logic.GameLogic.Interfaces;
using _Project.Scripts.Logic.StatesMachine.Interfaces;
using _Project.Scripts.Services._Interfaces;
using _Project.Scripts.Services.LoadScreen.Logic;
using _Project.Scripts.Services.MaterialService.Interfaces;
using AppacheAds.Scripts.Interfaces;
using AppacheAnalytics.Scripts.Interfaces;
using AppacheRemote.Scripts.Interfaces;

namespace _Project.Scripts.Logic.StatesMachine.States
{
  public class LoadInterviewState : IState
  {
    private readonly GameStateMachine _gameStateMachine;
    private readonly IInterviewFactory _interviewFactory;

    private readonly IMaterialService _materialService;
    private readonly IAnalytics _analytics;
    private readonly ILoadScreenService _loadScreenService;
    private readonly IGameDataProvider _gameDataProvider;
    private IInterview _interviewLogic;
    private readonly ICurrentScreen _screenManager;
    private readonly IGameSaveSystem _gameSaveSystem;
    private readonly IRemote _remote;
    private readonly IAdsService _ads;
    private readonly IStagesService _stagesService;
    private readonly IInAppAccessor _inAppAccessor;

#if TEST
    private GameObject _tester;
#endif

    public LoadInterviewState(GameStateMachine gameStateMachine, IInterviewFactory interviewFactory,
      IMaterialService materialService, IAnalytics analytics, ILoadScreenService loadScreenService, IGameDataProvider gameDataProvider,
      ICurrentScreen screenManager, IGameSaveSystem gameSaveSystem, IRemote remote, IAdsService ads, IStagesService stagesService, IInAppAccessor inAppAccessor)
    {
      _inAppAccessor = inAppAccessor;
      _stagesService = stagesService;
      _remote = remote;
      _ads = ads;
      _gameSaveSystem = gameSaveSystem;
      _gameDataProvider = gameDataProvider;
      _loadScreenService = loadScreenService;
      _materialService = materialService;
      _analytics = analytics;
      _gameStateMachine = gameStateMachine;
      _interviewFactory = interviewFactory;
      _screenManager = screenManager;
    }

    public void Exit()
    {
    }

    public Task AsyncEnter() =>
      throw new System.NotImplementedException();

    public void Enter()
    {
      _screenManager.SetCurrentScreen(ScreenNames.Interview);

      SpawnInterview();
#if TEST
      AddSkip();
#endif
      _stagesService.ShowStageCounterView(Stages.Interview, 0);
    }

    private void SpawnInterview() =>
      _interviewLogic = new InterviewLogic(_screenManager, _inAppAccessor, _gameDataProvider, _loadScreenService, _analytics, _remote, _ads, _interviewFactory, Next);

    private async Task Next()
    {
#if TEST
      if (_tester)
        Object.Destroy(_tester.gameObject);
#endif
      _interviewFactory.Clear();

      ApplyChoicesResults();

      _gameDataProvider.MechanismsInfo.MarkedAsTutorial = !_gameSaveSystem.Get().TutorialComplete;

      _stagesService.CompleteStage();
      _stagesService.HideStageCounterView();

      if (_stagesService.IsStageAlreadyComplete(Stages.Construction))
      {
        _stagesService.OpenStageDescription(Stages.Construction, null, 1);
        await _gameStateMachine.AsyncEnter<LoadGameEnvironmentState>();
      }
      else
        _stagesService.OpenStageDescription(Stages.Construction, LoadGameThroughBlackScreen, 1);

      return;

      void LoadGameThroughBlackScreen() =>
        _loadScreenService.Launch<BlackTransition>(null, _gameStateMachine.AsyncEnter<LoadGameEnvironmentState>);
    }

    private void ApplyChoicesResults()
    {
      _gameDataProvider.WorldsInfo.SetUsersChoice(_interviewLogic.GetWorldId());
      _materialService.SetData(_interviewLogic.GetColorGuids().First());
    }
    
#if TEST
    private void AddSkip()
    {
      _tester = new GameObject("[Test] Skip Interview");

      var skipLevelTestFeature = _tester.AddComponent<SkipEndInterview>();
      skipLevelTestFeature.OnClick = NextForSkip;
    }

    private async void NextForSkip()
    {
      if (_tester)
        Object.Destroy(_tester.gameObject);

      _interviewFactory.Clear();

      WorldData availableWorld = _gameDataProvider.WorldsInfo.GetWorldsData().FirstOrDefault(p =>
        !_gameDataProvider.WorldsInfo.WorldIsOutOfMechanisms(p.WorldId));

      if (availableWorld != null)
        _gameDataProvider.WorldsInfo.SetUsersChoice(availableWorld.WorldId);

      _materialService.SetData("5eb6aaf86faf4c24f8cb59b237c11e01");

      await _gameStateMachine.AsyncEnter<LoadGameEnvironmentState>();
    }
#endif
  }
}