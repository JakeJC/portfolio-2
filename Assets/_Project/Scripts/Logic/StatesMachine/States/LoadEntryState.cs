using System.Threading.Tasks;
using _Modules._Analytics;
using _Project.FMOD_AudioService.Logic;
using _Project.Scripts.Animations.UI;
using _Project.Scripts.Features.Stages;
using _Project.Scripts.Features.Stages.Interfaces;
using _Project.Scripts.InApp.Interfaces;
using _Project.Scripts.Logic.Factories.Interfaces;
using _Project.Scripts.Logic.GameLogic.Interfaces;
using _Project.Scripts.Logic.StatesMachine.Interfaces;
using _Project.Scripts.MonoBehaviours.UI;
using _Project.Scripts.Services._Interfaces;
using _Project.Scripts.Services.InputResolver;
using _Project.Scripts.Services.LoadScreen.Logic;
using _Project.Scripts.Services.Localization.Logic;
using AppacheAds.Scripts.Interfaces;
using AppacheAnalytics.Scripts.Interfaces;
using AppacheRemote.Scripts.Interfaces;
using UnityEngine;

namespace _Project.Scripts.Logic.StatesMachine.States
{
  public class LoadEntryState : IState
  {
    private readonly GameStateMachine _gameStateMachine;
    
    private readonly IAnalytics _analytics;
    private readonly IRemote _remote;
    private readonly IStagesService _stagesService;
    private readonly IAudioService _audioService;
    private readonly IInputResolver _inputResolver; 
    private readonly ILocaleSystem _localeSystem;
    private readonly IGameSaveSystem _gameSaveSystem;
    private readonly IGameDataProvider _gameDataProvider;
    private readonly ILoadScreenService _loadScreenService;
    private readonly IPrivacyPolicyFactory _privacyPolicyFactory;

    private readonly IUIFactory _uiFactory;
    private readonly ICurrentScreen _screenManager;

    private readonly IInAppAccessor _inAppAccessor;
    private readonly IAdsService _adsService;

    private PrivacyPolicy _privacyPolicy;
    
    private string _cachedScreen;
    
    public LoadEntryState(GameStateMachine gameStateMachine, IUIFactory uiFactory,
      IGameDataProvider gameDataProvider, ILoadScreenService loadScreenService, ICurrentScreen screenManager, IStagesService stagesService,
      IInAppAccessor inAppAccessor, IAdsService adsService, IAnalytics analytics, IRemote remote, IGameSaveSystem gameSaveSystem,
      IPrivacyPolicyFactory privacyPolicyFactory, IAudioService audioService, IInputResolver inputResolver, ILocaleSystem localeSystem)
    {
      _analytics = analytics;
      _remote = remote;
      _gameSaveSystem = gameSaveSystem;
      _adsService = adsService;
      _audioService = audioService;
      _inputResolver = inputResolver;
      _localeSystem = localeSystem;
      _inAppAccessor = inAppAccessor;
      _stagesService = stagesService;
      _screenManager = screenManager;
      _uiFactory = uiFactory;
      _loadScreenService = loadScreenService;
      _gameDataProvider = gameDataProvider;
      _gameStateMachine = gameStateMachine;
      _privacyPolicyFactory = privacyPolicyFactory;
    }

    public Task AsyncEnter() => 
      throw new System.NotImplementedException();

    public void Enter() => 
      SetupServices();

    public void Exit()
    {}

    private void SetupServices() => ShowHeadphonesMessage();

    private void ShowHeadphonesMessage()
    {
      _screenManager.SetCurrentScreen(ScreenNames.Phones);
      
      _privacyPolicy = _privacyPolicyFactory.SpawnPrivacyPolicy();
      _privacyPolicy.Construct(_gameSaveSystem, _audioService, _inputResolver, _localeSystem, _analytics, _remote);
      
      _uiFactory.CreateHeadphone(Next, PrivacyPolicy);

      LoadSound();
      
      void LoadSound()
      {
        _audioService.MuteMusic(_gameSaveSystem.Get().IsMusicDisabled);
        _audioService.MuteSounds(_gameSaveSystem.Get().IsSoundDisabled);
      }
    }
    
    private void PrivacyPolicy()
    {
      _privacyPolicy.ClosingStarted += OnClosingStarted;
      _privacyPolicy.OpenWindow(true);
      
      _cachedScreen = _screenManager.GetCurrentScreen();
      _screenManager.SetCurrentScreen(ScreenNames.Privacy);

      void OnClosingStarted()
      {
        _privacyPolicy.ClosingStarted -= OnClosingStarted;
        _screenManager.SetCurrentScreen(_cachedScreen);
      }
    }
    
    private void ShowSubscription()
    {
      _screenManager.SetCurrentScreen(ScreenNames.Subscription);
      _inAppAccessor.Subscription.ShowInterviewSubscription(OnBuySubscription, ToInterview);
    }
    
    private void OnBuySubscription()
    {
      _adsService.DisableAds();
      ToInterview();
    }

    private void Next()
    {
      if(!_gameSaveSystem.Get().IsPolicyAccepted)
        _privacyPolicy.AcceptPrivacyPolicy(true);
      
      if (_inAppAccessor.IsSubscriptionBought())
        ToInterview();
      else
        ShowSubscription();
    }

    private bool IsFirstGameSession() => 
      PlayerPrefs.GetInt("firstGameSession", 1) == 1;

    private void ToInterview()
    {
      _adsService.CurrentInterstitial.ShowInterstitial(ScreenNames.Interview, null, null);

      bool isContentAvailable = _gameDataProvider.MechanismsInfo.IsSomethingLocked();

      if (isContentAvailable)
      {
        _stagesService.OpenStageDescription(Stages.Interview, () =>
          _gameStateMachine.Enter<LoadInterviewState>(), 0, _analytics);
      }
      else
        ToGallery();
    }

    private void ToGallery() => 
      _loadScreenService.Launch<BlackTransition>(null, ToLoadGalleryState);

    private async Task ToLoadGalleryState() => 
      await _gameStateMachine.AsyncEnter<LoadGalleryState, ButtonType>(ButtonType.Worlds);
  }
}