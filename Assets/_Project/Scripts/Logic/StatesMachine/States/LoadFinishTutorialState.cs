using System.Threading.Tasks;
using _Project.FMOD_AudioService.Logic;
using _Project.Scripts.Features.Disable_Ads;
using _Project.Scripts.Features.Stages.Interfaces;
using _Project.Scripts.InApp.Interfaces;
using _Project.Scripts.Logic.GameLogic.Interfaces;
using _Project.Scripts.Logic.StatesMachine.Interfaces;
using _Project.Scripts.MonoBehaviours.UI.Windows.Mono;
using _Project.Scripts.Services._Interfaces;
using _Project.Scripts.Services.LoadScreen.Logic;
using _Project.Scripts.Services.Localization.Logic;
using AppacheAds.Scripts.Interfaces;
using AppacheAnalytics.Scripts.Interfaces;

namespace _Project.Scripts.Logic.StatesMachine.States
{
    public class LoadFinishTutorialState : IState
    {
        private readonly GameStateMachine _gameStateMachine;
        private FinishTutorial _finishTutorial;
        
        private readonly IAssetProvider _assetProvider;
        private readonly ILocaleSystem _localeSystem;
        private readonly IAudioService _audioService;
        private readonly ILoadScreenService _loadScreenService;
        private readonly IInAppAccessor _inAppAccessor;
        private readonly IDisableAdsLogic _disableAdsLogic;
        private readonly IStagesService _stagesService;
        private readonly IAnalytics _analytics;
        private readonly IAdsService _ads;
        
        public LoadFinishTutorialState(
            GameStateMachine gameStateMachine,
            IAssetProvider assetProvider,
            ILocaleSystem localeSystem,
            IAudioService audioService,
            ILoadScreenService loadScreenService,
            IInAppAccessor inAppAccessor,
            IDisableAdsLogic disableAdsLogic,
            IStagesService stagesService,
            IAnalytics analytics,
            IAdsService ads)
        {
            _gameStateMachine = gameStateMachine;
            _assetProvider = assetProvider;
            _localeSystem = localeSystem;
            _audioService = audioService;
            _loadScreenService = loadScreenService;
            _inAppAccessor = inAppAccessor;
            _disableAdsLogic = disableAdsLogic;
            _stagesService = stagesService;
            _analytics = analytics;
            _ads = ads;
        }
        
        public void Enter()
        {
            _finishTutorial = new FinishTutorial(_gameStateMachine, _assetProvider, _localeSystem, _audioService, _loadScreenService, _inAppAccessor, _disableAdsLogic, _stagesService, _analytics, _ads);
            _finishTutorial.Open();
        }

        public void Exit()
        {
            
        }

        public Task AsyncEnter() => 
            throw new System.NotImplementedException();
    }
}
