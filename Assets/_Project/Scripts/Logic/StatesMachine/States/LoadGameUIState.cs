using System.Threading.Tasks;
using _Project.FMOD_AudioService.Logic;
using _Project.FMOD_AudioService.Logic.Enums;
using _Project.FMOD_AudioService.Logic.Events;
using _Project.Scripts.Logic.Factories.Interfaces;
using _Project.Scripts.Logic.Factories;
using _Project.Scripts.Logic.GameLogic.Interfaces;
using _Project.Scripts.Logic.StatesMachine.Interfaces;
using _Project.Scripts.Services.MaterialService.Interfaces;

namespace _Project.Scripts.Logic.StatesMachine.States
{
  public class LoadGameUIState : IPayloadedState<GameDataStruct>
  {
    private readonly GameStateMachine _gameStateMachine;
    
    private readonly IMaterialService _materialService;
    private readonly IUIFactory _uiFactory;
    
    private IGameLevelFactory _gameLevelFactory;
    
    public LoadGameUIState(GameStateMachine gameStateMachine,
      IUIFactory uiFactory,
      IMaterialService materialService)
    {
      _materialService = materialService;
      _gameStateMachine = gameStateMachine;
      _uiFactory = uiFactory;
    }

    public void Enter(GameDataStruct gameData) =>
      SetupUI(gameData);
    
    public async Task AsyncEnter(GameDataStruct gameData) =>
      throw new System.NotImplementedException();

    public void Exit()
    {}

    private void SetupUI(GameDataStruct gameData)
    {
      _gameLevelFactory = gameData.GameLevelFactory;
      
      _materialService.Initialize(_gameLevelFactory.MainMaterialProvider, _gameLevelFactory.CurrentWorldId);

      _uiFactory.Spawn(_gameLevelFactory.Mechanism);
      
      _gameStateMachine.Enter<LoadGameState, GameDataStruct>(gameData);
    }
  }
}