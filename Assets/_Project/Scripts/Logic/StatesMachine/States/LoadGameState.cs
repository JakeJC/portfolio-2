using System.Collections.Generic;
using System.Threading.Tasks;
using _Modules._Analytics;
using _Project.FMOD_AudioService.Logic;
using _Project.FMOD_AudioService.Logic.Enums;
using _Project.FMOD_AudioService.Logic.Events;
using _Project.Scripts.Features.Disable_Ads;
using _Project.Scripts.Features.Stages.Interfaces;
using _Project.Scripts.InApp.Interfaces;
using _Project.Scripts.Logic.Factories.Interfaces;
using _Project.Scripts.Logic.GameLogic.Interfaces;
using _Project.Scripts.Logic.StatesMachine.Interfaces;
using _Project.Scripts.MonoBehaviours.Game;
using _Project.Scripts.MonoBehaviours.UI;
using _Project.Scripts.MonoBehaviours.Visual;
using _Project.Scripts.Services._Interfaces;
using _Project.Scripts.Services.FX;
using _Project.Scripts.Services.InputResolver;
using _Project.Scripts.Services.LeanTouchCamera.Scripts.Interfaces;
using _Project.Scripts.Services.LoadScreen.Logic;
using _Project.Scripts.Services.Localization.Logic;
using _Project.Scripts.Services.MaterialService.Interfaces;
using AppacheAds.Scripts.Interfaces;
using AppacheAnalytics.Scripts.Interfaces;
using AppacheRemote.Scripts.Interfaces;
using UnityEngine;

namespace _Project.Scripts.Logic.StatesMachine.States
{
  public class LoadGameState : IPayloadedState<GameDataStruct>
  {
    private readonly IInputResolver _inputResolver;
    private readonly IGameSaveSystem _gameSaveSystem;
    private readonly ICameraController _cameraController;
    private readonly IAudioService _audioService;
    private readonly ILocaleSystem _localeSystem;
    private readonly IAnalytics _analytics;
    private readonly IAdsService _ads;

    private readonly IFXController _fxController;
    private readonly IMaterialService _materialService;
    private readonly IAssetProvider _assetProvider;

    private readonly IGameDataProvider _gameMetaDataService;
    private readonly ILoadScreenService _loadScreenService;
    private readonly ICurrentScreen _screenManager;

    private readonly IPrivacyPolicyFactory _privacyPolicyFactory;
    private readonly IGameServicesFactory _gameServicesFactory;
    private readonly ITutorialFactory _tutorialFactory;
    private readonly IGameLevelFactory _gameLevelFactory;
    private readonly IUIFactory _uiFactory;
    private readonly IStagesService _stagesService;
    private readonly IRemote _remote;
    private IInAppAccessor _inAppAccessor;
    private IDisableAdsLogic _disableAdsLogic;

    private readonly GameStateMachine _gameStateMachine;
    private GameLogic.Main.PrimaryLogic.GameLogic _gameLogic;

    public LoadGameState(GameStateMachine gameStateMachine,
      IInputResolver inputResolver,
      IGameSaveSystem gameSaveSystem,
      ICameraController cameraController,
      IGameServicesFactory gameServicesFactory,
      ITutorialFactory tutorialFactory,
      IUIFactory uiFactory,
      IPrivacyPolicyFactory privacyPolicyFactory,
      IGameLevelFactory gameLevelFactory,
      IAudioService audioService,
      IFXController fxController,
      IMaterialService materialService,
      IAssetProvider assetProvider,
      ILocaleSystem localeSystem,
      IGameDataProvider gameMetaDataService,
      IAnalytics analytics,
      ILoadScreenService loadScreenService,
      ICurrentScreen screenManager,
      IRemote remote,
      IInAppAccessor inAppAccessor,
      IAdsService ads,
      IStagesService stagesService,
      IDisableAdsLogic disableAdsLogic)
    {
      _disableAdsLogic = disableAdsLogic;
      _inAppAccessor = inAppAccessor;
      _stagesService = stagesService;
      _remote = remote;
      _ads = ads;
      _gameLevelFactory = gameLevelFactory;
      _screenManager = screenManager;
      _loadScreenService = loadScreenService;
      _analytics = analytics;
      _assetProvider = assetProvider;
      _materialService = materialService;
      _gameMetaDataService = gameMetaDataService;
      _gameStateMachine = gameStateMachine;
      _inputResolver = inputResolver;
      _gameSaveSystem = gameSaveSystem;
      _cameraController = cameraController;
      _gameServicesFactory = gameServicesFactory;
      _tutorialFactory = tutorialFactory;
      _uiFactory = uiFactory;
      _privacyPolicyFactory = privacyPolicyFactory;
      _audioService = audioService;
      _fxController = fxController;
      _localeSystem = localeSystem;
    }

    public Task AsyncEnter(GameDataStruct gameLogic) =>
      throw new System.NotImplementedException();

    public void Enter(GameDataStruct gameData)
    {
      LaunchGame(gameData);

      InitializeMaterials();
      ChangeMusic();

      _materialService.BaseMaterialsState();
    }

    public void Exit()
    {
      if(_ads.AdsEnabled)
        _ads.CurrentInterstitial.ShowInterstitial(ScreenNames.Result, null, null);
      
      _gameLogic.Clear();
    }

    private void LaunchGame(GameDataStruct gameData)
    {
      IDragDrop dragDropControl = _gameServicesFactory.SpawnDragDropControl(_uiFactory.PartsPanel);

      _gameLogic = new GameLogic.Main.PrimaryLogic.GameLogic(_gameStateMachine, _uiFactory, _privacyPolicyFactory, _gameLevelFactory, _gameServicesFactory, _tutorialFactory, dragDropControl,
        _gameSaveSystem, _cameraController, _inputResolver, _audioService, _fxController, _materialService, _gameMetaDataService, _assetProvider,
        _localeSystem, _analytics, _loadScreenService, _screenManager, _remote, _stagesService, _inAppAccessor, _ads, _disableAdsLogic);
      _gameLogic.LaunchGame(gameData);
    }

    private void InitializeMaterials()
    {
      IMaterialProvider[] materialParts = FindComponents<IMaterialProvider>();
      IColorProvider[] colorParts = FindComponents<IColorProvider>();

      var skyboxColor = Object.FindObjectOfType<SkyboxColor>();
      ColumnColorProvider[] columnColor = Object.FindObjectsOfType<ColumnColorProvider>();
      var environmentColor = Object.FindObjectOfType<EnvironmentColorProvider>();

      _materialService.SetMechanism(_gameLevelFactory.Mechanism);

      _materialService.Material.AddRange(materialParts);
      _materialService.Color.AddRange(colorParts);
      _materialService.Color.AddObject(skyboxColor);

      foreach (ColumnColorProvider t in columnColor)
        _materialService.Color.AddObject(t);

      _materialService.Color.AddObject(environmentColor);
    }

    private void ChangeMusic()
    {
      AudioEventLink audioEventLink = _audioService.GetLinkByEvent(FMOD_EventType.Music);
      int currentWorldMusicIndex = CurrentWorldMusicIndex();
      _audioService.SetParameter(audioEventLink, FMOD_ParameterType.current_music, currentWorldMusicIndex);

      int CurrentWorldMusicIndex() =>
        _gameMetaDataService.WorldsInfo.GetWorldDataById(_gameLevelFactory.CurrentWorldId).MusicIndex;
    }

    private T[] FindComponents<T>()
    {
      List<T> parts = new List<T>();

      parts.Add(_gameLevelFactory.Mechanism.GetGO().GetComponent<T>());

      foreach (PartMaterial partMaterial in _gameLevelFactory.Mechanism.GetMaterialParts())
        parts.Add(partMaterial.GetComponentInChildren<T>(true));

      foreach (Part part in _uiFactory.Parts.GetParts())
        parts.Add(part.GetComponentInChildren<T>(true));

      return parts.ToArray();
    }
  }
}