using System.Threading.Tasks;
using _Modules._Analytics;
using _Project.FMOD_AudioService.Logic;
using _Project.Scripts.Logic.Factories.Interfaces;
using _Project.Scripts.Logic.StatesMachine.Interfaces;
using _Project.Scripts.Services._Interfaces;
using _Project.Scripts.Services.InputResolver;
using _Project.Scripts.Services.Localization.Logic;
using AppacheAnalytics.Scripts.Interfaces;
using AppacheRemote.Scripts.Interfaces;

namespace _Project.Scripts.Logic.StatesMachine.States
{
  public class LoadPolicyState : IState
  {
    private readonly GameStateMachine _gameStateMachine;
    private readonly IPrivacyPolicyFactory _privacyPolicyFactory;
    private readonly IGameSaveSystem _gameSaveSystem;
    private readonly IAudioService _audioService;
    private readonly IInputResolver _inputResolver;
    private readonly ILocaleSystem _localeSystem;
    private readonly ICurrentScreen _screenManager;
    private IAnalytics _analytics;
    private readonly IRemote _remote;

    public LoadPolicyState(GameStateMachine gameStateMachine,
      IPrivacyPolicyFactory privacyPolicyFactory,
      IGameSaveSystem gameSaveSystem,
      IAudioService audioService,
      IInputResolver inputResolver,
      ILocaleSystem localeSystem,
      ICurrentScreen screenManager,
      IAnalytics analytics,
      IRemote remote)
    {
      _analytics = analytics;
      _remote = remote;
      _gameStateMachine = gameStateMachine;
      _privacyPolicyFactory = privacyPolicyFactory;
      _gameSaveSystem = gameSaveSystem;
      _audioService = audioService;
      _inputResolver = inputResolver;
      _localeSystem = localeSystem;
      _screenManager = screenManager;
    }

    public Task AsyncEnter() =>
      throw new System.NotImplementedException();

    public void Enter()
    {
      _screenManager.SetCurrentScreen(ScreenNames.Privacy);

      if (!_gameSaveSystem.Get().IsPolicyAccepted)
      {
        var privacyPolicy = _privacyPolicyFactory.SpawnPrivacyPolicy();
        privacyPolicy.Construct(_gameSaveSystem, _audioService, _inputResolver, _localeSystem, _analytics, _remote);
        privacyPolicy.ClosingFinished += OnClosingFinished;
        privacyPolicy.OpenWindow();

        void OnClosingFinished()
        {
          Next();
          privacyPolicy.ClosingFinished -= OnClosingFinished;
        }
      }
      else
        Next();
      
      LoadSound();
    }

    private void LoadSound()
    {
      _audioService.MuteMusic(_gameSaveSystem.Get().IsMusicDisabled);
      _audioService.MuteSounds(_gameSaveSystem.Get().IsSoundDisabled);
    }

    private void Next()
    {
      _gameStateMachine.Enter<LoadEntryState>();
      _inputResolver.Enable();
    }

    public void Exit() =>
      _privacyPolicyFactory.Clear();
  }
}