using System;
using System.Threading.Tasks;
using _Modules._Analytics;
using _Project.FMOD_AudioService.Logic;
using _Project.Scripts.Features.Stages;
using _Project.Scripts.Features.Stages.Interfaces;
using _Project.Scripts.Logic.Factories.Interfaces;
using _Project.Scripts.Logic.GameLogic.Advices.Interfaces;
using _Project.Scripts.Logic.GameLogic.Interfaces;
using _Project.Scripts.Logic.StatesMachine.Interfaces;
using _Project.Scripts.Logic.UI.Result;
using _Project.Scripts.Logic.UI.Result.Interfaces;
using _Project.Scripts.Services._Interfaces;
using _Project.Scripts.Services.LoadScreen.Logic;
using _Project.Scripts.Services.Localization.Logic;
using _Project.Scripts.Services.MaterialService.Interfaces;
using AppacheAds.Scripts.Interfaces;
using AppacheAnalytics.Scripts.Interfaces;
using AppacheRemote.Scripts.Interfaces;

namespace _Project.Scripts.Logic.StatesMachine.States
{
  public class LoadResultUIState : IPayloadedState<IAdvices>
  {
    private readonly GameStateMachine _gameStateMachine;
    private readonly IResultUIFactory _resultUIFactory;
    private readonly ILocaleSystem _localeSystem;
    private readonly IAudioService _audioService;
    private readonly IAnalytics _analytics;

    private readonly IAssetProvider _assetProvider;
    private IResultUILogic _resultUI;
    private readonly IGameDataProvider _gameDataProvider;
    private readonly IMaterialService _materialService;
    private readonly ILoadScreenService _loadScreenService;
    private readonly ICurrentScreen _screenManager;
    private readonly IStagesService _stagesService;
    private readonly IAdsService _adsService;

    private readonly IRemote _remote;

    public LoadResultUIState(GameStateMachine gameStateMachine, IResultUIFactory resultUIFactory, IAssetProvider assetProvider, ILocaleSystem localeSystem,
      IGameDataProvider gameDataProvider, IMaterialService materialService, IAudioService audioService, IAnalytics analytics,
      ILoadScreenService loadScreenService, ICurrentScreen screenManager, IStagesService stagesService, IRemote remote, IAdsService adsService)
    {
      _adsService = adsService;
      _remote = remote;
      _stagesService = stagesService;
      _screenManager = screenManager;
      _loadScreenService = loadScreenService;
      _materialService = materialService;
      _audioService = audioService;
      _gameDataProvider = gameDataProvider;
      _localeSystem = localeSystem;
      _assetProvider = assetProvider;
      _gameStateMachine = gameStateMachine;
      _resultUIFactory = resultUIFactory;
      _analytics = analytics;
    }

    public Task AsyncEnter(IAdvices advices) =>
      throw new NotImplementedException();

    public void Enter(IAdvices advices)
    {
      _screenManager.SetCurrentScreen(ScreenNames.Result);
      _materialService.Clear();
      
      _stagesService.ShowHideCounter(false);
      _stagesService.OpenStageDescription(Stages.Advice, () =>
      {
        _stagesService.CompleteStage();
        CreateResultWindow(advices);
      }, 0);
    }

    private void CreateResultWindow(IAdvices advices)
    {
      _resultUI = new ResultUILogic(_gameStateMachine, _resultUIFactory,
        _assetProvider, _localeSystem, _gameDataProvider, _audioService, _analytics,
        _loadScreenService, advices, _stagesService, _remote, _adsService);
    }

    public void Exit()
    {
      _resultUI.Cleanup();
      _resultUI = null;

      if (_gameDataProvider.FirstSession.IsFirstSession())
        _gameDataProvider.FirstSession.FirstSessionEnd();
    }
  }
}