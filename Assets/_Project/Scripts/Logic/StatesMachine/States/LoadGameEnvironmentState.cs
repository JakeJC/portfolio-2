using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using _Modules._Analytics;
using _Project.Scripts.Data.Serializable;
using _Project.Scripts.Features.Stages;
using _Project.Scripts.Features.Stages.Interfaces;
using _Project.Scripts.Logic.Factories.Interfaces;
using _Project.Scripts.Logic.GameLogic.Interfaces;
using _Project.Scripts.Logic.StatesMachine.Interfaces;
using _Project.Scripts.Logic.UI.Gallery.Data;
using _Project.Scripts.MonoBehaviours.Game;
using _Project.Scripts.Services.Environment;
using _Project.Scripts.Services.InputResolver;
using _Project.Scripts.Services.LeanTouchCamera.Scripts.Interfaces;
using AppacheAnalytics.Scripts.Interfaces;
using AppacheRemote.Scripts.Interfaces;
using UnityEngine;

namespace _Project.Scripts.Logic.StatesMachine.States
{
  public class LoadGameEnvironmentState : IPayloadedState<LaunchInfo>, IState
  {
    private readonly GameStateMachine _gameStateMachine;
    private readonly IGameLevelFactory _gameLevelFactory;

    private readonly ICameraController _cameraController;
    private readonly IInputResolver _inputResolver;
    private readonly ICurrentScreen _screenManager;
    private readonly IGameDataProvider _gameDataProvider;
    private readonly IAnalytics _analytics;
    private readonly IEnvironmentController _environmentController;
    private IRemote _remote;
    private IStagesService _stagesService;

    private bool _shouldSendEvents;

    public LoadGameEnvironmentState(GameStateMachine gameStateMachine,
      IGameLevelFactory gameLevelFactory,
      ICameraController cameraController,
      IInputResolver inputResolver,
      ICurrentScreen screenManager,
      IGameDataProvider gameDataProvider,
      IAnalytics analytics,
      IStagesService stagesService,
      IRemote remote,
      IEnvironmentController environmentController)
    {
      _stagesService = stagesService;
      _remote = remote;
      _environmentController = environmentController;
      _analytics = analytics;
      _gameDataProvider = gameDataProvider;
      _screenManager = screenManager;
      _gameStateMachine = gameStateMachine;
      _gameLevelFactory = gameLevelFactory;
      _cameraController = cameraController;
      _inputResolver = inputResolver;
    }

    public void Enter(LaunchInfo launchInfo) =>
      throw new NotImplementedException();

    public void Enter() =>
      throw new NotImplementedException();

    public async Task AsyncEnter()
    {
      await SetupGameEnvironment();

      _stagesService.ShowStageCounterView(Stages.Construction, 1);
    }

    public async Task AsyncEnter(LaunchInfo launchInfo) =>
      await SetupGameEnvironment(launchInfo);

    public void Exit()
    {
    }

    private async Task SetupGameEnvironment()
    {
      _screenManager.SetCurrentScreen(ScreenNames.Game);
      _shouldSendEvents = true;

      await _gameLevelFactory.SpawnLevel();
      
      _environmentController.SetEnvironment(_gameLevelFactory.Mechanism.GetId());
      PrepareGameEnvironment();

      if (!_gameDataProvider.MechanismsInfo.MarkedAsTutorial)
      {
        _analytics.Send("ev_level_start", new Dictionary<string, object>
        {
          {"level", _gameDataProvider.MechanismsInfo.GetLevelOrderNumber()},
          {"level_id", _gameLevelFactory.Mechanism.GetId()}
        });
      }
    }

    private async Task SetupGameEnvironment(LaunchInfo launchInfo)
    {     
      _environmentController.SetEnvironment(launchInfo.MechanismId);
      
      _screenManager.SetCurrentScreen(ScreenNames.Game);
      _shouldSendEvents = false;

      await _gameLevelFactory.SpawnLevel(launchInfo);
      PrepareGameEnvironment();
    }

    private void PrepareGameEnvironment()
    {
      _cameraController.PrepareToGameSwitch();

      _cameraController.SetCameraData(new CameraData()
      {
        Position = _gameLevelFactory.Mechanism.GetBaseAnchorPosition
      });

      PrepareGameParts();
      LaunchTransition(EnterInGame);
    }

    private void EnterInGame()
    {
      var factoryStruct = new GameDataStruct {GameLevelFactory = _gameLevelFactory, ShouldSendEvents = _shouldSendEvents};
      _gameStateMachine.Enter<LoadGameUIState, GameDataStruct>(factoryStruct);
    }

    private void PrepareGameParts()
    {
      Transform cameraTransform = Camera.main.transform;

      foreach (GamePart mechanismPart in _gameLevelFactory.Mechanism.GetMechanismParts())
      {
        mechanismPart.Construct(cameraTransform, _inputResolver);
        mechanismPart.Fade();
      }
    }

    private void LaunchTransition(Action onEnd) =>
      onEnd?.Invoke();
  }
}