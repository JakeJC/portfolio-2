#define TEST

using System.Threading.Tasks;
using _Modules._Analytics;
using _Modules._InApps.Interfaces;
using _Project.FMOD_AudioService.Logic;
using _Project.Scripts.Features.Disable_Ads;
using _Project.Scripts.Features.Stages.Interfaces;
using _Project.Scripts.InApp.Interfaces;
using _Project.Scripts.Logic.Factories.Interfaces;
using _Project.Scripts.Logic.GameLogic.Interfaces;
using _Project.Scripts.Logic.StatesMachine.Interfaces;
using _Project.Scripts.Logic.UI.Gallery;
using _Project.Scripts.Services._Interfaces;
using _Project.Scripts.Services.Environment;
using _Project.Scripts.Services.FX;
using _Project.Scripts.Services.InputResolver;
using _Project.Scripts.Services.LeanTouchCamera.Scripts.Interfaces;
using _Project.Scripts.Services.LoadScreen.Logic;
using _Project.Scripts.Services.Localization.Logic;
using _Project.Scripts.Services.MaterialService.Interfaces;
using AppacheAds.Scripts.Interfaces;
using AppacheAnalytics.Scripts.Interfaces;
using AppacheRemote.Scripts.Interfaces;

namespace _Project.Scripts.Logic.StatesMachine.States
{
  public class LoadGalleryState : IPayloadedState<ButtonType>
  {
    private readonly GameStateMachine _gameStateMachine;
    private readonly IGalleryFactory _galleryFactory;
    private readonly IUIFactory _uiFactory;
    private readonly IPrivacyPolicyFactory _privacyPolicyFactory;
    private readonly IGameLevelFactory _gameLevelFactory;
    private readonly IGameServicesFactory _gameServicesFactory;

    private readonly IAssetProvider _assetProvider;
    private readonly IAudioService _audioService;
    private readonly IInputResolver _inputResolver;
    private readonly IGameSaveSystem _gameSaveSystem;
    private readonly ILocaleSystem _localeSystem;
    private readonly IGameDataProvider _gameDataProvider;
    private readonly ILoadScreenService _loadScreenService;
    private readonly ICameraController _cameraController;
    private readonly IAnalytics _analytics;
    private readonly IAdsService _ads;
    private readonly IFXController _fxController;
    private readonly ICurrentScreen _screenManager;
    private readonly IStagesService _stagesService;
    private readonly IInAppAccessor _inAppAccessor;
    private readonly IRemote _remote;

    private GalleryLogic _galleryLogic;
    private IDisableAdsLogic _disableAdsLogic;
    private IEnvironmentController _environmentController;
    private IMaterialService _materialService;
    private IInAppService _inAppService;

    public LoadGalleryState(
      GameStateMachine gameStateMachine,
      IGalleryFactory galleryFactory,
      IUIFactory uiFactory,
      IPrivacyPolicyFactory privacyPolicyFactory,
      IGameLevelFactory gameLevelFactory,
      IGameServicesFactory gameServicesFactory,
      IAssetProvider assetProvider,
      IAudioService audioService,
      IInputResolver inputResolver,
      IGameSaveSystem gameSaveSystem,
      ILocaleSystem localeSystem,
      IGameDataProvider gameDataProvider,
      IAnalytics analytics,
      ICameraController cameraController,
      ILoadScreenService loadScreenService,
      IFXController fxController,
      ICurrentScreen screenManager,
      IRemote remote,
      IAdsService ads,
      IStagesService stagesService,
      IInAppAccessor inAppAccessor,
      IDisableAdsLogic disableAdsLogic,
      IEnvironmentController environmentController,
      IMaterialService materialService,
      IInAppService inAppService)
    {
      _inAppService = inAppService;
      _materialService = materialService;
      _environmentController = environmentController;
      _disableAdsLogic = disableAdsLogic;
      _inAppAccessor = inAppAccessor;
      _stagesService = stagesService;
      _remote = remote;
      _ads = ads;
      _screenManager = screenManager;
      _fxController = fxController;
      _loadScreenService = loadScreenService;
      _gameDataProvider = gameDataProvider;
      _gameStateMachine = gameStateMachine;
      _galleryFactory = galleryFactory;
      _uiFactory = uiFactory;
      _privacyPolicyFactory = privacyPolicyFactory;
      _gameLevelFactory = gameLevelFactory;
      _gameServicesFactory = gameServicesFactory;
      _assetProvider = assetProvider;
      _audioService = audioService;
      _inputResolver = inputResolver;
      _gameSaveSystem = gameSaveSystem;
      _localeSystem = localeSystem;
      _analytics = analytics;
      _cameraController = cameraController;
    }

    public async Task AsyncEnter(ButtonType screenType)
    {
      await _galleryFactory.LoadTextures();
      Enter(screenType);
    }

    public void Enter(ButtonType screenType)
    {
      _screenManager.SetCurrentScreen(ScreenNames.Gallery);

      _galleryLogic = new GalleryLogic(
        _gameStateMachine, _galleryFactory, _uiFactory, _privacyPolicyFactory, _gameLevelFactory, _gameServicesFactory, _assetProvider, _audioService,
        _inputResolver, _gameSaveSystem, _localeSystem, _gameDataProvider, _analytics, _cameraController,
        _loadScreenService, _fxController, _screenManager, _remote, _stagesService, _inAppAccessor, _ads, _disableAdsLogic, _environmentController, _materialService, _inAppService);

      _galleryLogic.SpawnGalleryWorldsWindow(screenType);
    }

    public void Exit()
    {
      _galleryFactory.CleanTextures();

      _galleryLogic.Cleanup();
      _galleryLogic = null;
    }
  }
}