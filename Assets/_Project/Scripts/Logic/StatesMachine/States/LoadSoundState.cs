using System.Threading.Tasks;
using _Project.FMOD_AudioService.Logic;
using _Project.FMOD_AudioService.Logic.Enums;
using _Project.FMOD_AudioService.Logic.Events;
using _Project.Scripts.Logic.GameLogic.Interfaces;
using _Project.Scripts.Logic.StatesMachine.Interfaces;
using _Project.Scripts.Services._Interfaces;
using _Project.Scripts.Services.InputResolver;
using _Project.Scripts.Services.Localization.Logic;
using AppacheAds.Scripts.Interfaces;
using AppacheRemote.Scripts.Interfaces;
using UnityEngine;

namespace _Project.Scripts.Logic.StatesMachine.States
{
  public class LoadSoundState : IState
  {
    private readonly GameStateMachine _gameStateMachine;

    private readonly IAudioService _audioService;
    private readonly ILocaleSystem _localeSystem;
    private readonly IRemote _remote;
    private readonly IAdsService _adsService;
    private readonly IInputResolver _inputResolver;

    private readonly IGameDataProvider _gameDataProvider;

    public LoadSoundState(GameStateMachine gameStateMachine, IAudioService audioService,
      IRemote remote, IAssetProvider assetProvider, ILocaleSystem localeSystem, IGameDataProvider gameDataProvider, 
      IAdsService adsService, IInputResolver inputResolver)
    {
      _adsService = adsService;
      _inputResolver = inputResolver;
      _gameDataProvider = gameDataProvider;
      _gameStateMachine = gameStateMachine;

      _localeSystem = localeSystem;
      _audioService = audioService;
      _remote = remote;
    }

    public void Exit()
    {
    }

    public Task AsyncEnter() =>
      throw new System.NotImplementedException();

    public void Enter()
    {
      CreateMusicEvent();
      ShowEnterAppWindow();
    }

    private void ShowEnterAppWindow()
    {
      _gameStateMachine.Enter<LoadEntryState>();
      _inputResolver.Enable();
    }

    private void CreateMusicEvent()
    {
      GameObject musicEmitter = new GameObject("[] Music");
      _audioService.CreateEvent(out AudioEventLink musicLink, FMOD_EventType.Music, musicEmitter, true);
      _audioService.Play(musicLink);

      if (_audioService.MusicIsMute())
        _audioService.Pause(musicLink);
    }
  }
}