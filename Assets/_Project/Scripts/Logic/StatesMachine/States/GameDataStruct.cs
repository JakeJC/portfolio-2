﻿using System;
using _Project.Scripts.Logic.Factories.Interfaces;

namespace _Project.Scripts.Logic.StatesMachine.States
{
  [Serializable]
  public struct GameDataStruct
  {
    public IGameLevelFactory GameLevelFactory;
    public bool ShouldSendEvents;
  }
}