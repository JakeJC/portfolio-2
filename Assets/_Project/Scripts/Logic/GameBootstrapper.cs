using System;
using System.Collections.Generic;
using _Modules._Analytics;
using _Modules._InApps.Interfaces;
using _Project.FMOD_AudioService.Logic;
using _Project.Scripts.Features.Disable_Ads;
using _Project.Scripts.Features.Splashscreen;
using _Project.Scripts.Features.Stages.Interfaces;
using _Project.Scripts.InApp;
using _Project.Scripts.InApp.Interfaces;
using _Project.Scripts.Logic.Factories;
using _Project.Scripts.Logic.Factories.Interfaces;
using _Project.Scripts.Logic.GameLogic.Interfaces;
using _Project.Scripts.Logic.StatesMachine;
using _Project.Scripts.Logic.StatesMachine.States;
using _Project.Scripts.Services._Interfaces;
using _Project.Scripts.Services.Environment;
using _Project.Scripts.Services.FX;
using _Project.Scripts.Services.InputResolver;
using _Project.Scripts.Services.LeanTouchCamera.Scripts.Interfaces;
using _Project.Scripts.Services.LoadScreen.Logic;
using _Project.Scripts.Services.Localization.Logic;
using _Project.Scripts.Services.MaterialService.Interfaces;
using _Project.Scripts.Timer;
using _Project.Scripts.Timer.Interfaces;
using AppacheAds;
using AppacheAds.Scripts.Interfaces;
using AppacheAds.Scripts.Logger;
using AppacheAnalytics;
using AppacheAnalytics.Scripts.Interfaces;
using AppacheAnalytics.Scripts.Logic;
using AppacheRemote;
using AppacheRemote.Scripts.Interfaces;
#if UNITY_ANDROID
using Backtrace.Unity;
#endif
using FlurrySDK;
using GoogleMobileAds.Api;
using Unity.Services.Core;
using Unity.Services.Core.Environments;
using UnityEngine;
using Zenject;

#if COMPLETE_GAME || SOUND_DESIGNER
using _Project.Scripts.Logic.GameLogic.Advices;
using _Project.Scripts.Logic.GameLogic.Advices.Interfaces;
using _Project.Scripts.Data.Scriptables;
using _Project.Scripts.Logic.UI.Result;
using _Project.Scripts.Logic.UI.Result.Interfaces;
using State = _Project.Scripts.Data.Serializable.State;
#endif

#if UNITY_IOS
using Balaso;
#endif

namespace _Project.Scripts.Logic
{
  public class GameBootstrapper : MonoBehaviour
  {
    public string environment = "production";

    private IGameSaveSystem _gameSaveSystem;
    private IAssetProvider _assetProvider;
    private IAudioService _audioService;
    private IInputResolver _inputResolver;
    private IFXController _fxController;
    private ILocaleSystem _localeSystem;
    private IMaterialService _materialService;
    private ILoadScreenService _loadScreenService;
    private ICurrentScreen _screenManager;
    private IAnalytics _analytics;
    private IAdsService _ads;

    private IEnvironmentController _environmentController;

    private IGameDataProvider _gameDataProvider;
    private ICameraController _cameraController;

    private IFailConnectionFactory _failConnectionFactory;

    private IRemote _remote;
    private IStagesService _stages;

    private IInAppService _inAppService;
    private IInAppAccessor _inAppLauncher;

    private GameStateMachine _gameStateMachine;
    private SplashLogic _splashLogic;
    private AdsSafeCallback _adsInterstitialLoadCallback;
    private IDisableAdsLogic _disableAdsLogic;
    private ITimerController _timerController;
    private INotificationService _notificationService;

    private async void Awake()
    {
      try
      {
        var options = new InitializationOptions()
          .SetEnvironmentName(environment);

        await UnityServices.InitializeAsync(options);

        Launch();
      }
      catch (Exception)
      {
        Launch();
      }
    }

    private void Launch()
    {
      Application.targetFrameRate = 120;
#if UNITY_IOS
      AppTrackingTransparency.RequestTrackingAuthorization();
#endif
      LaunchWithSplash(LaunchGameStateMachine);
    }

    [Inject]
    public void Construct(IInputResolver inputResolver,
      IGameSaveSystem gameSaveSystem,
      IAssetProvider assetProvider,
      IFXController fxController,
      IAudioService audioService,
      IMaterialService materialService,
      ILocaleSystem localeSystem,
      ILoadScreenService loadScreenService,
      IGameDataProvider gameDataProvider,
      ICameraController cameraController,
      IInAppService inAppService,
      INotificationService notificationService,
      IRemote remote,
      IStagesService stages,
      IEnvironmentController environmentController)
    {
      _notificationService = notificationService;
      _inAppService = inAppService;
      _stages = stages;
      _remote = remote;
      _cameraController = cameraController;
      _fxController = fxController;
      _inputResolver = inputResolver;
      _gameSaveSystem = gameSaveSystem;
      _assetProvider = assetProvider;
      _audioService = audioService;
      _materialService = materialService;
      _localeSystem = localeSystem;
      _loadScreenService = loadScreenService;
      _gameDataProvider = gameDataProvider;
      _environmentController = environmentController;

      _failConnectionFactory = new FailConnectionFactory(assetProvider, localeSystem, audioService);
    }

#if TEST
    public IAudioService GetAudioService()
    {
      return _audioService;
    }
#endif

    private void LaunchWithSplash(Action initializeGame)
    {
      _splashLogic = new SplashLogic(_assetProvider, _remote, _localeSystem);
      _splashLogic.Launch(initializeGame);

      _remote.OnFetch += InitializeServices;
      _remote.OnFetch += delegate { _splashLogic.OnFetchRemote(); };
      _remote.Initialize();
    }

    private void InitializeServices()
    {
      PrewarmServices();

      _screenManager.SetCurrentScreen(ScreenNames.Splash);

      InitializeAnalytics();
      InitializeCrashlytics();
#if UNITY_ANDROID
      InitializeBacktrace();
#endif
    }

    private void InitializeAds()
    {
      _adsInterstitialLoadCallback = new GameObject().AddComponent<AdsSafeCallback>();
      _adsInterstitialLoadCallback.name = "Callback";

      _adsInterstitialLoadCallback.Construct(OnInterstitialLoaded);

      _ads.Initialize(_remote.GetPlacementType(), () =>
      {
        _ads.AddListenerOnPaidEvent(TrackOnPaidEvent);

        _inAppLauncher.SetCallback(() =>
        {
          _adsInterstitialLoadCallback.Complete();

          if (_inAppLauncher.IsSubscriptionBought())
          {
            _timerController.Clean();
            _ads.DisableAds();
          }
          else
            _disableAdsLogic.SwitchAds();
        });

        _inAppLauncher.Initialize();
      });
    }

    private void OnInterstitialLoaded() =>
      _splashLogic.Complete();

    private void PrewarmServices()
    {
      _screenManager = new ScreenManager();

#if UNITY_STANDALONE_WIN || SOUND_DESIGNER
      _analytics = new AnalyticsDummy();
#else
      _analytics = new Analytics(_screenManager, new SessionManager());
#endif

#if IOS_FIRST_RELEASE
      _ads = new AdsDummy();
#else
      _ads = new AdsService(_analytics, new AdsLogger());
#endif
      
      var factories = new Factories.Factories
      {
        GameLevelFactory = new GameLevelFactory(_assetProvider, _gameDataProvider, _failConnectionFactory, _loadScreenService),
        UIFactory = new UIFactory(_inAppService, _assetProvider, _inputResolver, _gameSaveSystem, _materialService, _localeSystem, _audioService, _analytics, _stages, _ads, _remote),
        GameServicesFactory = new GameServicesFactory(_assetProvider),
        TutorialFactory = new TutorialFactory(_assetProvider),
        PrivacyPolicyFactory = new PrivacyPolicyFactory(_assetProvider),
        ResultUIFactory = new ResultUIFactory(_assetProvider),
        TimerPopupUIFactory = new TimerPopupUIFactory(_assetProvider),
        GalleryFactory = new GalleryFactory(_assetProvider, _gameDataProvider, _failConnectionFactory, _loadScreenService),
        InterviewFactory = new InterviewFactory(_assetProvider, _gameSaveSystem, _localeSystem, _audioService, _analytics, _gameDataProvider, _failConnectionFactory, _loadScreenService, _stages, _remote),
        FailConnectionFactory = _failConnectionFactory
      };

      _timerController = new TimerController();
      _timerController.LaunchService();

      _disableAdsLogic = new DisableAdsLogic(_localeSystem, _ads, _timerController, _audioService, _stages, _analytics);

      _inAppLauncher = new InAppLauncher(_inAppService, _assetProvider, _audioService, _localeSystem,
        _failConnectionFactory, _loadScreenService, _gameDataProvider, _analytics, _screenManager, _gameSaveSystem, _inputResolver,
        factories.PrivacyPolicyFactory, _remote, _timerController);

      _gameStateMachine = new GameStateMachine(factories, _gameSaveSystem, _assetProvider, _localeSystem, _cameraController,
        _inputResolver, _audioService, _fxController, _materialService, _gameDataProvider,
        _analytics, _loadScreenService, _screenManager, _remote, _ads, _stages, _inAppLauncher, _disableAdsLogic, _environmentController, _inAppService);
    }

    private void LoadSystems()
    {
      _gameDataProvider.Initialize();
      _audioService.InitializeEvents();

#if COMPLETE_GAME || SOUND_DESIGNER
      CompleteGame();
#endif
    }

    private void InitializeAnalytics()
    {
      _analytics.OnInitialize += InitializeAnalyticsRemoteEvents;
      _analytics.Initialize();
    }

    private void InitializeAnalyticsRemoteEvents()
    {
      Flurry.UserProperties.Add("AB_remote_config", _remote.GetPlacementType().ToString());

      _analytics.Send("AB_remote_config", new Dictionary<string, object>()
      {
        { "group", _remote.GetPlacementType() }
      }, true);

      if (_analytics.IsFirstLaunch)
        _analytics.Send($"v{Application.version.Replace(".", "_")}", true);

      InitializeAds();
    }

#if UNITY_ANDROID
    private void InitializeBacktrace()
    {
      if (Application.isEditor)
        return;

      var backtraceClient = _assetProvider.GetResource<BacktraceClient>("Services/Backtrace/Backtrace");
      Instantiate(backtraceClient);
    }
#endif

    private void InitializeCrashlytics()
    {
      Firebase.FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(task =>
      {
        var dependencyStatus = task.Result;
        if (dependencyStatus == Firebase.DependencyStatus.Available)
        {
          Firebase.FirebaseApp app = Firebase.FirebaseApp.DefaultInstance;
        }
        else
          Debug.LogError($"Could not resolve all Firebase dependencies: {dependencyStatus}");
      });
    }

    private void LaunchGameStateMachine()
    {
      if (!_gameDataProvider.FirstSession.IsFirstSession())
        _ads.CurrentInterstitial.ShowInterstitial(ScreenNames.Splash, null, null, true);

      LoadSystems();
      _gameStateMachine.Enter<LoadSoundState>();
    }

    private void TrackOnPaidEvent(AdValueEventArgs args, string placement, string unit)
    {}

    private void OnApplicationFocus(bool hasFocus)
    {
      if (hasFocus)
      {
        _audioService.ResumeAll();
        Debug.Log("[Audio] ResumeAll");
      }
      else
      {
        _audioService.PauseAll();
        Debug.Log("[Audio] ReleaseAll");
      }
    }

    private void OnApplicationPause(bool pauseStatus)
    {
      if (pauseStatus)
        SendNotificationsPack();
      else
        _notificationService.ClearAll();
    }

    private void OnApplicationQuit() =>
      SendNotificationsPack();

    private void SendNotificationsPack()
    {
      DateTime current = DateTime.Now;
#if TEST
      for (int i = 1; i < 8; i++) 
        _notificationService.Send(current.AddMinutes(i));
#else
      for (int i = 1; i < 8; i++)
        _notificationService.Send(current.AddDays(i));
#endif
    }

#if COMPLETE_GAME || SOUND_DESIGNER
    private void CompleteGame()
    {
      foreach (MechanismData mechanismData in _gameDataProvider.MechanismsInfo.GetMechanisms())
        _gameSaveSystem.Get().SetState(mechanismData.MechanismId, State.Launched);

      foreach (WorldData worldData in _gameDataProvider.WorldsInfo.GetWorldsData())
        _gameSaveSystem.Get().AddOpenWorld(worldData.WorldId);
      
      _gameSaveSystem.Get().TutorialComplete = true;
      
      IAdvices advicesSaver = new Advices(_assetProvider, _gameSaveSystem);
      advicesSaver.OpenAll();

      _gameSaveSystem.Save();
    }
#endif
  }
}