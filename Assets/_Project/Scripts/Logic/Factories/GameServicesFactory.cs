using _Project.Scripts.Logic.Factories.Interfaces;
using _Project.Scripts.Logic.GameLogic.Interfaces;
using _Project.Scripts.Logic.GameLogic.Main.PrimaryLogic;
using _Project.Scripts.MonoBehaviours.UI.Interfaces;
using _Project.Scripts.Services._Interfaces;
using UnityEngine;

namespace _Project.Scripts.Logic.Factories
{
  public class GameServicesFactory : IGameServicesFactory
  {
    private const string ServicesGameDragDropInputPath = "Services/Game/Drag & Drop Input";

    private readonly IAssetProvider _assetProvider;
    private DragDrop _drop;

    public GameServicesFactory(IAssetProvider assetProvider) =>
      _assetProvider = assetProvider;

    public IDragDrop SpawnDragDropControl(IDragDependent dragDependent)
    {
      Clear();

      var dragDrop = _assetProvider.GetResource<GameObject>(ServicesGameDragDropInputPath);
      _drop = Object.Instantiate(dragDrop).GetComponent<DragDrop>();

      _drop.SetDependentModules(new[] {dragDependent});

      return _drop;
    }

    public void Clear()
    {
      if (_drop != null)
        Object.Destroy(_drop.gameObject);
    }
  }
}