using _Project.Scripts.Logic.Factories.Interfaces;
using _Project.Scripts.MonoBehaviours.UI.Windows;
using _Project.Scripts.MonoBehaviours.UI.Windows.Gallery;
using _Project.Scripts.Services._Interfaces;
using UnityEngine;

namespace _Project.Scripts.Logic.Factories
{
  public class ResultUIFactory : IResultUIFactory
  {
    private const string CanvasId = "Canvas";

    private const string ResultWindowTestPath = "UI/Advices/Result Window Test";
    private const string ResultWindowMeditationPath = "UI/Advices/Result Window Meditation";

    private readonly IAssetProvider _assetProvider;

    public GalleryUI GalleryUI { get; set; }
    
    public ResultUIFactory(IAssetProvider assetProvider) => 
      _assetProvider = assetProvider;

    public ResultWindow CreateResultWindow(bool meditationLead)
    {
      var parent = GameObject.Find(CanvasId).transform;
      var prefabPath = meditationLead
        ? ResultWindowMeditationPath
        : ResultWindowTestPath;
      var original = _assetProvider.GetResource<GameObject>(prefabPath);
      GameObject obj = Object.Instantiate(original, parent);

      GalleryUI = obj.GetComponentInChildren<GalleryUI>();
      
      return obj.GetComponent<ResultWindow>();
    }
  }
}