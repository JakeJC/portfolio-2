using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using _Project.Scripts.Data.Scriptables;
using _Project.Scripts.InApp.Mono;
using _Project.Scripts.Logic.Factories.Interfaces;
using _Project.Scripts.Logic.GameLogic.Interfaces;
using _Project.Scripts.Logic.UI.Gallery;
using _Project.Scripts.MonoBehaviours.UI.Windows.Gallery;
using _Project.Scripts.Services._Interfaces;
using _Project.Scripts.Services.LoadScreen.Logic;
using UnityEngine;
using Object = UnityEngine.Object;

namespace _Project.Scripts.Logic.Factories
{
  public class GalleryFactory : IGalleryFactory
  {
    private const string CanvasId = "Canvas";

    private const string GalleryWindowWorldsPath = "UI/Gallery/Worlds/Gallery of Worlds Window";
    private const string GalleryItemWorldsPath = "UI/Gallery/Worlds/Gallery of Worlds Item";
    private const string GalleryPlayWorldsPath = "UI/Gallery/Worlds/Gallery Play Item";

    private const string GalleryWindowMechanismsPath = "UI/Gallery/Mechanisms/Gallery of Mechanisms Window";
    private const string GalleryItemMechanismsPath = "UI/Gallery/Mechanisms/Gallery of Mechanisms Item";

    private const string GalleryAdvicesWindowPath = "UI/Gallery/Advices/Gallery of Advices Window";
    private const string GalleryAdvicesItemPath = "UI/Gallery/Advices/Gallery of Advices Item";

    private const string GalleryUIPath = "UI/Gallery/Gallery UI";
    private const string GalleryPlayPath = "UI/Gallery/Gallery Play";

    private readonly IAssetProvider _assetProvider;
    private readonly IGameDataProvider _gameDataProvider;
    private readonly ILoadScreenService _loadScreenService;
    private readonly IFailConnectionFactory _failConnectionFactory;

    private readonly Dictionary<string, Texture2D> _texture2Ds = new Dictionary<string, Texture2D>();
    private GalleryWindowWorlds _worldsWindow;

    public GalleryFactory(IAssetProvider assetProvider, IGameDataProvider gameDataProvider, IFailConnectionFactory failConnectionFactory, ILoadScreenService loadScreenService)
    {
      _failConnectionFactory = failConnectionFactory;
      _loadScreenService = loadScreenService;
      _gameDataProvider = gameDataProvider;
      _assetProvider = assetProvider;
    }

    public Texture2D GetTexture(string address) => 
      _texture2Ds[address];

    public async Task LoadTextures()
    {
      try
      {
        foreach (MechanismData mechanismData in _gameDataProvider.MechanismsInfo.GetUnlockedMechanisms())
        {
          if (!_texture2Ds.ContainsKey(mechanismData.CoverAddress))
            _texture2Ds.Add(mechanismData.CoverAddress, await _assetProvider.Load<Texture2D>(mechanismData.CoverAddress));
        }
      }
      catch (ArgumentException)
      {
        CallNoConnectionWindow();
        throw;
      }
    }

    public void CleanTextures()
    {
      foreach (KeyValuePair<string, Texture2D> tex in _texture2Ds)
        _assetProvider.Release(tex.Key);

      _texture2Ds.Clear();
    }

    public GalleryUI CreateGalleryUI()
    {
      var original = _assetProvider.GetResource<GameObject>(GalleryUIPath);
      var parent = GameObject.Find(CanvasId).transform;
      var gameObject = Object.Instantiate(original, parent);
      return gameObject.GetComponent<GalleryUI>();
    }

    public GalleryPlay CreateGalleryPlayButton()
    {
      var original = _assetProvider.GetResource<GameObject>(GalleryPlayPath);
      var parent = _worldsWindow.ParentContainer;
      var gameObject = Object.Instantiate(original, parent);
      return gameObject.GetComponent<GalleryPlay>();
    }

    public GalleryWindowAdvices CreateAdvicesWindow()
    {
      var original = _assetProvider.GetResource<GameObject>(GalleryAdvicesWindowPath);
      var parent = GameObject.Find(CanvasId).transform;
      var gameObject = Object.Instantiate(original, parent);
      return gameObject.GetComponent<GalleryWindowAdvices>();
    }

    public GalleryItemAdvice CreateAdvicesItem(GalleryWindowAdvices galleryWindowAdvices)
    {
      var original = _assetProvider.GetResource<GameObject>(GalleryAdvicesItemPath);
      var parent = galleryWindowAdvices.galleryItemsParent.transform;
      var gameObject = Object.Instantiate(original, parent);
      return gameObject.GetComponent<GalleryItemAdvice>();
    }

    public GalleryWindowWorlds CreateGalleryWindowWorlds()
    {
      var original = _assetProvider.GetResource<GameObject>(GalleryWindowWorldsPath);
      var parent = GameObject.Find(CanvasId).transform;
      _worldsWindow = Object.Instantiate(original, parent).GetComponent<GalleryWindowWorlds>();
      return _worldsWindow;
    }

    public GalleryItemWorlds CreateGalleryItemWorlds(GalleryWindowWorlds galleryWindowWorlds)
    {
      var original = _assetProvider.GetResource<GameObject>(GalleryItemWorldsPath);
      var parent = galleryWindowWorlds.galleryItemsParent.transform;
      var gameObject = Object.Instantiate(original, parent);
      return gameObject.GetComponent<GalleryItemWorlds>();
    }

    public GalleryItemPlay CreateGalleryItemPlay(GalleryWindowWorlds galleryWindowWorlds)
    {
      var original = _assetProvider.GetResource<GameObject>(GalleryPlayWorldsPath);
      var parent = galleryWindowWorlds.galleryItemsParent.transform;
      var gameObject = Object.Instantiate(original, parent);
      return gameObject.GetComponent<GalleryItemPlay>();
    }

    public GalleryWindowMechanisms CreateGalleryWindowMechanisms()
    {
      var original = _assetProvider.GetResource<GameObject>(GalleryWindowMechanismsPath);
      var parent = GameObject.Find(CanvasId).transform;
      var gameObject = Object.Instantiate(original, parent);
      return gameObject.GetComponent<GalleryWindowMechanisms>();
    }

    public GalleryItemMechanisms CreateGalleryItemMechanisms(GalleryWindowMechanisms galleryWindowMechanisms)
    {
      var original = _assetProvider.GetResource<GameObject>(GalleryItemMechanismsPath);
      var parent = galleryWindowMechanisms.galleryItemsParent.transform;
      var gameObject = Object.Instantiate(original, parent);
      return gameObject.GetComponent<GalleryItemMechanisms>();
    }

    private void CallNoConnectionWindow()
    {
      Debug.Log("[Addressables Remote Host] Connection Lost");

      _failConnectionFactory.CreateNotAvailableWindow();
      NotAvailableWindow notAvailableWindow = _failConnectionFactory.NotAvailableWindow;

      notAvailableWindow.SetCloseAction(_loadScreenService.ToQuit);
      notAvailableWindow.Open();

      _loadScreenService.ForceRemoveScreens();
    }
  }
}