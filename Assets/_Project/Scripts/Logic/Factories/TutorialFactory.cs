using System.Threading.Tasks;
using _Project.Scripts.Logic.Factories.Interfaces;
using _Project.Scripts.MonoBehaviours.Game;
using _Project.Scripts.MonoBehaviours.Tutorial;
using _Project.Scripts.Services._Interfaces;
using _Project.Scripts.Services.LeanTouchCamera.Scripts.Logic;
using _Project.Scripts.Tools;
using DG.Tweening;
using DG.Tweening.Core;
using DG.Tweening.Plugins.Options;
using UnityEngine;
using UnityEngine.UI;

namespace _Project.Scripts.Logic.Factories
{
  public class TutorialFactory : ITutorialFactory
  {
    private const string Canvas = "Canvas";

    private const string UIFinger = "UI/Tutorial/Arrow";
    private const string UICameraSwipePanel = "UI/Tutorial/Camera Swipe Panel";
    private const string UITextPanel = "UI/Tutorial/Tutorial Text Panel";

    private readonly IAssetProvider _assetProvider;
    private GameObject Finger { get; set; }
    public CameraPoints CameraPoints { get; private set; }
    public TutorialTextPanel TextPanel { get; private set; }
    
    private Tween _move;

    public TutorialFactory(IAssetProvider assetProvider) => 
      _assetProvider = assetProvider;

    public void ShowFinger(bool overlay = false)
    {
      Transform parent = GameObject.Find(Canvas).transform;
      SpawnFinger(parent);
    }
    
    public void ShowText(string tutorialText) => 
      SpawnTextPanel(tutorialText);

    public async Task Hide()
    {
      _move?.Kill();
      
      RemoveFinger();
      await RemoveTextPanel();
    }

    public void SpawnCameraPointsPanel()
    {
      CameraPoints = Object.Instantiate(_assetProvider.GetResource<GameObject>(UICameraSwipePanel), 
        GameObject.Find(Canvas).transform).GetComponent<CameraPoints>();
    }

    public void RemoveCameraPointsPanel()
    {     
      if(CameraPoints)
        Object.Destroy(CameraPoints.gameObject);
    }

    public void StartAbMotion(Transform pointA, Transform pointB)
    {
      PointsProjection(pointA, pointB, out Vector3 pointAProjection, out Vector3 _);

      Finger.transform.position = pointAProjection;

      float lerpPosition = 0;

      Sequence sequence = DOTween.Sequence();
      sequence.Append(DOTween
        .To(() => lerpPosition, x => lerpPosition = x, 1, 1f)
        .SetEase(Ease.Linear));
      sequence.Append(Finger.GetComponent<Image>().DOFade(0, 0.4f));
      sequence.SetLoops(-1, LoopType.Restart);
      
      sequence.onUpdate += () =>
      {
        PointsProjection(pointA, pointB, out Vector3 pointAProjection1, out Vector3 pointBProjection2);
        Finger.transform.position = Vector3.Lerp(pointAProjection1, pointBProjection2, lerpPosition);
      };

      _move = sequence;
    }

    public void StartAbPointing(Vector3 targetPoint, Vector3 offset)
    {
      Finger.transform.position = targetPoint + offset;

      float lerpPosition = 0;
      
      Sequence sequence = DOTween.Sequence();
      sequence.Append(DOTween
        .To(() => lerpPosition, x => lerpPosition = x, 1, 1f)
        .SetEase(Ease.Linear));
      sequence.Append(Finger.GetComponent<Image>().DOFade(0, 0.4f));
      sequence.SetLoops(-1, LoopType.Restart);

      sequence.onUpdate += () =>
      {
        Finger.transform.position = Vector3.Lerp(targetPoint + offset, targetPoint, lerpPosition);
      };
      
      _move = sequence;
    }

    public void StopMotion()
    {
      if(Finger)
        Finger.transform.DOKill();
    }

    private void SpawnTextPanel(string tutorialText)
    {
      TextPanel = Object.Instantiate(_assetProvider.GetResource<GameObject>(UITextPanel), 
          GameObject.Find(Canvas).transform).
        GetComponentInChildren<TutorialTextPanel>();

      TextPanel.CanvasGroup.alpha = 0;
      TextPanel.CanvasGroup.DOFade(1, 0.5f);
      
      TextPanel.Text.text = tutorialText;
    }

    private async Task RemoveTextPanel()
    {
      if (TextPanel)
      {
        var fade = TextPanel.CanvasGroup.DOFade(0, 0.49f);
        fade.onComplete += () =>
          Object.Destroy(TextPanel.gameObject);
        await TaskFromTweenEnd(fade);
      }
    }

    private Task TaskFromTweenEnd(TweenerCore<float, float, FloatOptions> tweenCallback)
    {
      Task tweenEnd = AsyncEvents.TaskFromEvent(
        h => tweenCallback.onComplete += () => h?.Invoke(),
        h => tweenCallback.onComplete = null);
      
      return tweenEnd;
    }

    private void PointsProjection(Transform pointA, Transform pointB, out Vector3 pointAProjection, out Vector3 pointBProjection)
    {
      Transform mainTransform = Camera.main.transform;
      Vector3 transformForward = mainTransform.forward;

      Plane plane = new Plane(transformForward, mainTransform.position);
      
      pointAProjection = plane.ClosestPointOnPlane(pointA.position) + transformForward;
      pointBProjection = plane.ClosestPointOnPlane(pointB.position) + transformForward;
    }

    private void SpawnFinger(Transform parent)
    {
      var finger = _assetProvider.GetResource<GameObject>(UIFinger);
      Finger = Object.Instantiate(finger.gameObject, parent);
    }

    private void RemoveFinger()
    {
      if (Finger)
        Object.Destroy(Finger.gameObject);
    }
  }
}