using _Project.Scripts.MonoBehaviours.UI;

namespace _Project.Scripts.Logic.Factories.Interfaces
{
  public interface IPrivacyPolicyFactory
  {
    PrivacyPolicy SpawnPrivacyPolicy();
    void Clear();
  }
}