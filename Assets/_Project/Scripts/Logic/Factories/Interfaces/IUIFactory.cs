using System;
using _Project.Scripts.Features.HideUIButton;
using _Project.Scripts.Logic.UI;
using _Project.Scripts.Logic.UI.Game;
using _Project.Scripts.MonoBehaviours.Game.Interfaces;
using _Project.Scripts.MonoBehaviours.Game.Stackable;
using _Project.Scripts.MonoBehaviours.UI;
using UnityEngine;

namespace _Project.Scripts.Logic.Factories.Interfaces
{
  public interface IUIFactory
  {
    ExitAttention ExitAttentionWindow { get; }
    Settings SettingsWindow { get; }
    GameStatePanel GameStatePanel { get; }
    PartsPanel PartsPanel { get; }
    Parts Parts { get; }
    HideUIButton HideUIButton { get; }
    GameBar GameBar { get; }
    void Spawn(IMechanism mechanism);
    void SpawnSettingsWindow();
    void SpawnAttentionWindow();
    void RemoveSettingsWindow();
    MaterialButton CreateMaterialButton(RectTransform parent);
    MaterialButton CreateMaterialColorButton(RectTransform parent);
    void CreateHeadphone(Action onHeadphoneEnd, Action onClickPrivacy);
    StackCounter CreateCounter(Transform parent);
    void Clear();
  }
}