using System.Threading.Tasks;
using _Project.Scripts.Logic.UI.Gallery.Data;
using _Project.Scripts.MonoBehaviours.Game.Interfaces;
using _Project.Scripts.Services.MaterialService.Interfaces;

namespace _Project.Scripts.Logic.Factories.Interfaces
{
  public interface IGameLevelFactory
  {
    IMechanism Mechanism { get; }
    IMainMaterialProvider MainMaterialProvider { get; }
    string CurrentWorldId { get; }
    Task SpawnLevel();
    Task SpawnLevel(LaunchInfo launchInfo);
    void Clear();
  }
}