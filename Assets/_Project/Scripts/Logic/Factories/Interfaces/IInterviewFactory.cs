using _Project.Scripts.Features.Interview.Scripts;
using _Project.Scripts.Features.Interview.Scripts.Bar;
using _Project.Scripts.Features.Interview.Scripts.Interviews;

namespace _Project.Scripts.Logic.Factories.Interfaces
{
  public interface IInterviewFactory
  {
    InterviewUI SpawnInterviewUi();
    InterviewBar SpawnInterviewBar(int stepsCount);
    InterviewName CreateNamePart();
    InterviewHello CreateHelloPart();
    InterviewReason CreateReasonPart();
    InterviewColor CreateColorPart();
    InterviewMechanism CreateMechanismsPart();
    InterviewLoading CreateLoadingPart();
    void Clear();
  }
}