using _Project.Scripts.MonoBehaviours.UI.Windows;

namespace _Project.Scripts.Logic.Factories.Interfaces
{
  public interface ITimerPopupUIFactory
  {
    TimerPopupWindow CreateTimerPopupWindow();
  }
}