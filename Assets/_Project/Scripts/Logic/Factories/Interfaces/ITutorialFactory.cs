using System.Threading.Tasks;
using _Project.Scripts.MonoBehaviours.Tutorial;
using _Project.Scripts.Services.LeanTouchCamera.Scripts.Logic;
using UnityEngine;

namespace _Project.Scripts.Logic.Factories.Interfaces
{
  public interface ITutorialFactory
  {
    CameraPoints CameraPoints { get; }
    TutorialTextPanel TextPanel { get; }
    void ShowFinger(bool overlay = false);
    void ShowText(string tutorialText);
    Task Hide();
    void SpawnCameraPointsPanel();
    void RemoveCameraPointsPanel();
    void StartAbMotion(Transform pointA, Transform pointB);
    void StartAbPointing(Vector3 targetPoint, Vector3 offset);
    void StopMotion();
  }
}