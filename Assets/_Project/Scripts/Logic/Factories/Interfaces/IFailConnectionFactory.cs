﻿using _Project.Scripts.InApp.Mono;

namespace _Project.Scripts.Logic.Factories.Interfaces
{
  public interface IFailConnectionFactory
  {
    NotAvailableWindow NotAvailableWindow { get; }
    void CreateNotAvailableWindow();
    void Clear();
  }
}