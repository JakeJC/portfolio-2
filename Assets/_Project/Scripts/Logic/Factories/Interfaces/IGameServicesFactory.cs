using _Project.Scripts.Logic.GameLogic.Interfaces;
using _Project.Scripts.MonoBehaviours.UI.Interfaces;

namespace _Project.Scripts.Logic.Factories.Interfaces
{
  public interface IGameServicesFactory
  {
    IDragDrop SpawnDragDropControl(IDragDependent dragDependent);
    void Clear();
  }
}