using System.Threading.Tasks;
using _Project.Scripts.MonoBehaviours.UI.Windows.Gallery;
using UnityEngine;

namespace _Project.Scripts.Logic.Factories.Interfaces
{
  public interface IGalleryFactory
  {
    Texture2D GetTexture(string address);
    Task LoadTextures();
    void CleanTextures();
    GalleryUI CreateGalleryUI();
    GalleryPlay CreateGalleryPlayButton();
    GalleryWindowAdvices CreateAdvicesWindow();
    GalleryItemAdvice CreateAdvicesItem(GalleryWindowAdvices galleryWindowAdvices);
    GalleryWindowWorlds CreateGalleryWindowWorlds();
    GalleryItemWorlds CreateGalleryItemWorlds(GalleryWindowWorlds galleryWindowWorlds);
    GalleryItemPlay CreateGalleryItemPlay(GalleryWindowWorlds galleryWindowWorlds);
    GalleryWindowMechanisms CreateGalleryWindowMechanisms();
    GalleryItemMechanisms CreateGalleryItemMechanisms(GalleryWindowMechanisms galleryWindowMechanisms);
  }
}