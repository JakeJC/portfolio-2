using _Project.Scripts.MonoBehaviours.UI.Windows;
using _Project.Scripts.MonoBehaviours.UI.Windows.Gallery;

namespace _Project.Scripts.Logic.Factories.Interfaces
{
  public interface IResultUIFactory
  {
    ResultWindow CreateResultWindow(bool meditationLead);
    GalleryUI GalleryUI { get; set; }
  }
}