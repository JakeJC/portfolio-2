﻿using _Project.FMOD_AudioService.Logic;
using _Project.Scripts.Features.Interview.Scripts;
using _Project.Scripts.Features.Interview.Scripts.Bar;
using _Project.Scripts.Features.Interview.Scripts.Interviews;
using _Project.Scripts.Features.Stages.Interfaces;
using _Project.Scripts.Features.Stages.Mono.UI_Adapters;
using _Project.Scripts.Logic.Factories.Interfaces;
using _Project.Scripts.Logic.GameLogic.Interfaces;
using _Project.Scripts.Services._Interfaces;
using _Project.Scripts.Services.LoadScreen.Logic;
using _Project.Scripts.Services.Localization.Logic;
using AppacheAnalytics.Scripts.Interfaces;
using AppacheRemote.Scripts.Interfaces;
using DG.Tweening;
using UnityEngine;
using Object = UnityEngine.Object;

namespace _Project.Scripts.Logic.Factories
{
  public class InterviewFactory : IInterviewFactory
  {
    private const string CanvasCamera = "Canvas";
    private const string Parts = "Stretch Panel/Parts";

    private const string QuestionnairePath = "UI/Interview/Interview Window";

    private const string NamePartPath = "UI/Interview/Parts/NamePart";
    private const string HelloPartPath = "UI/Interview/Parts/HelloPart";
    private const string ReasonPartPath = "UI/Interview/Parts/ReasonPart";
    private const string ColorPartPath = "UI/Interview/Parts/ColorPart";
    private const string MechanismsPartPath = "UI/Interview/Parts/MechanismsPart";
    private const string LoadingPartPath = "UI/Interview/Parts/LoadingPart";
    
    private const string InterviewBarPath = "UI/Interview/Bar/Interview Bar";
    private const string InterviewBarSectionPath = "UI/Interview/Bar/Section";

    private Transform _parentParts;
    private readonly Transform _parent;

    private readonly ILocaleSystem _localeSystem;
    private readonly IAssetProvider _assetProvider;
    private readonly IAudioService _audioService;
    private readonly IAnalytics _analytics;
    private readonly IFailConnectionFactory _failConnectionFactory;
    private readonly ILoadScreenService _loadScreenService;
    private readonly IStagesService _stagesService;
    private readonly IGameDataProvider _gameDataProvider;
    private readonly IRemote _remote;

    private InterviewUI _interviewUi;

    private InterviewName _interviewName;
    private InterviewHello _interviewHello;
    private InterviewReason _interviewReason;
    private InterviewColor _interviewColor;
    private InterviewMechanism _interviewMechanism;
    private InterviewLoading _interviewLoading;

    private readonly NameOperator _nameOperator;


    private InterviewBar _interviewBar;

    public InterviewFactory(IAssetProvider assetProvider, IGameSaveSystem gameSaveSystem, ILocaleSystem localeSystem, 
      IAudioService audioService, IAnalytics analytics, IGameDataProvider gameDataProvider, IFailConnectionFactory
        failConnectionFactory, ILoadScreenService loadScreenService, IStagesService stagesService, IRemote remote)
    {
      _remote = remote;
      _stagesService = stagesService;
      _loadScreenService = loadScreenService;
      _failConnectionFactory = failConnectionFactory;
      _gameDataProvider = gameDataProvider;
      _localeSystem = localeSystem;
      _audioService = audioService;
      _analytics = analytics;
      _assetProvider = assetProvider;

      _parent = GameObject.Find(CanvasCamera).transform;

      _nameOperator = new NameOperator(gameSaveSystem);
    }

    public InterviewUI SpawnInterviewUi()
    {
      var lockObject = _assetProvider.GetResource<InterviewUI>(QuestionnairePath);
      _interviewUi = Object.Instantiate(lockObject, _parent);
      _interviewUi.Construct(_gameDataProvider, _localeSystem, _audioService, _analytics, _remote, _nameOperator);

      foreach (ICounterLayout counterLayout in _interviewUi.GetComponentsInChildren<ICounterLayout>()) 
        counterLayout.Construct(_stagesService);

      _parentParts = _interviewUi.transform.Find(Parts).transform;

      return _interviewUi;
    }

    public InterviewBar SpawnInterviewBar(int stepsCount)
    {
      var barPrefab = _assetProvider.GetResource<InterviewBar>(InterviewBarPath);
      _interviewBar = Object.Instantiate(barPrefab, _parent);

      Section[] sections = new Section[stepsCount];
      
      for (int i = 0; i < stepsCount; i++)
      {
        var sectionPrefab = _assetProvider.GetResource<Section>(InterviewBarSectionPath);
        sections[i] = Object.Instantiate(sectionPrefab, _interviewBar.transform);
      }
      
      _interviewBar.Construct(sections);
      
      foreach (ICounterLayout counterLayout in _interviewBar.GetComponentsInChildren<ICounterLayout>()) 
        counterLayout.Construct(_stagesService);
      
      _parentParts = _interviewUi.transform.Find(Parts).transform;

      return _interviewBar;
    }
    
    public InterviewName CreateNamePart()
    {
      var lockObject = _assetProvider.GetResource<InterviewName>(NamePartPath);
      _interviewName = Object.Instantiate(lockObject, _parentParts);
      _interviewName.Construct(_analytics, _interviewUi, _remote);

      return _interviewName;
    }

    public InterviewHello CreateHelloPart()
    {
      var lockObject = _assetProvider.GetResource<InterviewHello>(HelloPartPath);
      _interviewHello = Object.Instantiate(lockObject, _parentParts);
      _interviewHello.Construct(_analytics, _interviewUi, _remote);

      return _interviewHello;
    }

    public InterviewReason CreateReasonPart()
    {
      var lockObject = _assetProvider.GetResource<InterviewReason>(ReasonPartPath);
      _interviewReason = Object.Instantiate(lockObject, _parentParts);
      _interviewReason.Construct(_analytics, _interviewUi, _remote);
      return _interviewReason;
    }

    public InterviewColor CreateColorPart()
    {
      var lockObject = _assetProvider.GetResource<InterviewColor>(ColorPartPath);
      _interviewColor = Object.Instantiate(lockObject, _parentParts);
      _interviewColor.Construct(_analytics, _interviewUi, _remote);
      return _interviewColor;
    }

    public InterviewMechanism CreateMechanismsPart()
    {
      var lockObject = _assetProvider.GetResource<InterviewMechanism>(MechanismsPartPath);
      _interviewMechanism = Object.Instantiate(lockObject, _parentParts);
      
      _interviewMechanism.SetGameDataProvider(_assetProvider, _gameDataProvider, _audioService);
      _interviewMechanism.Construct(_analytics, _interviewUi, _remote);

      return _interviewMechanism;
    }

    public InterviewLoading CreateLoadingPart()
    {
      var lockObject = _assetProvider.GetResource<InterviewLoading>(LoadingPartPath);
      _interviewLoading = Object.Instantiate(lockObject, _parentParts);
      _interviewLoading.SetServices(_assetProvider, _failConnectionFactory, _loadScreenService);
      _interviewLoading.Construct(_analytics, _interviewUi, _remote);
      return _interviewLoading;
    }
    
    public void Clear()
    {
      DOTween.KillAll();

      if (_interviewUi != null)
        Object.Destroy(_interviewUi.gameObject);
      
      if(_interviewBar != null)
        Object.Destroy(_interviewBar.gameObject);
    }
  }
}