using System;
using _Modules._InApps.Interfaces;
using _Project.FMOD_AudioService.Logic;
using _Project.Scripts.Features.Headphone;
using _Project.Scripts.Features.HideUIButton;
using _Project.Scripts.Features.Stages.Interfaces;
using _Project.Scripts.Features.Stages.Mono.UI_Adapters;
using _Project.Scripts.Logic.Factories.Interfaces;
using _Project.Scripts.Logic.UI;
using _Project.Scripts.Logic.UI.Game;
using _Project.Scripts.MonoBehaviours.Game;
using _Project.Scripts.MonoBehaviours.Game.Interfaces;
using _Project.Scripts.MonoBehaviours.Game.Stackable;
using _Project.Scripts.MonoBehaviours.UI;
using _Project.Scripts.Services._Interfaces;
using _Project.Scripts.Services.InputResolver;
using _Project.Scripts.Services.MaterialService.Interfaces;
using _Project.Scripts.Services.Localization.Logic;
using AppacheAds.Scripts.Interfaces;
using AppacheAnalytics.Scripts.Interfaces;
using AppacheRemote.Scripts.Interfaces;
using UnityEngine;
using UnityEngine.UI;
using Object = UnityEngine.Object;

namespace _Project.Scripts.Logic.Factories
{
  public class UIFactory : IUIFactory
  {
    private const string Canvas = "Canvas";

    private const string UIPartsPanel = "UI/Game/Parts Panel";
    private const string UIPart = "UI/Game/Part";

    private const string UIGameStatePanel = "UI/Game/Game State Panel";
    private const string UIGameStatePanelIOS = "UI/Game/Game State Panel IOS";

    private const string UIGameBar = "UI/Game/Game Progress Bar";
    private const string UISettingsWindow = "UI/Game/Settings Window";
    private const string UIAttentionWindow = "UI/Game/Attention Window";

    private const string UIMaterialButton = "UI/Game/Material Button";
    private const string UIMaterialColorButton = "UI/Game/Material Color Button";
    private const string CounterPath = "UI/Game/Counter";

    private const string HeadphonePath = "UI/HeadphonesAttention/HeadphoneAttention";

    private readonly ILocaleSystem _localeSystem;
    private readonly IAssetProvider _assetProvider;
    private readonly IGameSaveSystem _gameSaveSystem;
    private readonly IMaterialService _materialService;
    private readonly IInputResolver _inputResolver;
    private readonly IAudioService _audioService;
    private readonly IAnalytics _analytics;
    private readonly IStagesService _stagesService;
    private readonly IAdsService _adsService;
    private readonly IRemote _remote;
    private readonly IInAppService _inAppService;

    public ExitAttention ExitAttentionWindow { get; private set; }
    public Settings SettingsWindow { get; private set; }
    public GameStatePanel GameStatePanel { get; private set; }
    public PartsPanel PartsPanel { get; private set; }
    public Parts Parts { get; private set; }
    public GameBar GameBar { get; private set; }

    public HideUIButton HideUIButton { get; private set; }

    public UIFactory(IInAppService inAppService, IAssetProvider assetProvider, IInputResolver inputResolver, IGameSaveSystem gameSaveSystem,
      IMaterialService materialService, ILocaleSystem localeSystem, IAudioService audioService, IAnalytics analytics,
      IStagesService stagesService, IAdsService adsService, IRemote remote)
    {
      _inAppService = inAppService;
      _remote = remote;
      _adsService = adsService;
      _stagesService = stagesService;
      _localeSystem = localeSystem;
      _assetProvider = assetProvider;
      _inputResolver = inputResolver;
      _gameSaveSystem = gameSaveSystem;
      _materialService = materialService;
      _audioService = audioService;
      _analytics = analytics;
    }

    public void Spawn(IMechanism mechanism)
    {
      Transform parent = GameObject.Find(Canvas).transform;
      Transform parent2 = GameObject.Find(Canvas).transform;

      SpawnPartsPanel(parent);
      SpawnGameStatePanel(parent2);
      SpawnGameBar(parent, mechanism.GetMechanismParts().Length);

      Part[] parts = SpawnParts(mechanism.GetMechanismParts(), PartsPanel.transform);
      Parts = new Parts(parts, PartsPanel.ShowLine, PartsPanel.HideLine);
    }

    public void SpawnSettingsWindow()
    {
      ClearWindow();

      Transform parent = GameObject.Find(Canvas).transform;

      var window = _assetProvider.GetResource<Settings>(UISettingsWindow);
      SettingsWindow = Object.Instantiate(window, parent);
    }

    public void SpawnAttentionWindow()
    {
      ClearWindow();

      Transform parent = GameObject.Find(Canvas).transform;

      var window = _assetProvider.GetResource<ExitAttention>(UIAttentionWindow);
      ExitAttentionWindow = Object.Instantiate(window, parent);
    }

    public void RemoveSettingsWindow()
    {
      if (SettingsWindow)
        Object.Destroy(SettingsWindow.gameObject);
    }

    public MaterialButton CreateMaterialButton(RectTransform parent) =>
      Object.Instantiate(_assetProvider.GetResource<MaterialButton>(UIMaterialButton), parent);

    public MaterialButton CreateMaterialColorButton(RectTransform parent) =>
      Object.Instantiate(_assetProvider.GetResource<MaterialButton>(UIMaterialColorButton), parent);

    public void CreateHeadphone(Action onHeadphoneEnd, Action onClickPrivacy)
    {
      Transform parent = GameObject.Find(Canvas).transform;

      var lockObject = _assetProvider.GetResource<HeadphoneAttention>(HeadphonePath);
      lockObject = Object.Instantiate(lockObject, parent);

      onHeadphoneEnd += () => Object.Destroy(lockObject.gameObject);
      lockObject.Construct(_localeSystem, _audioService, _analytics, _gameSaveSystem, _remote, onHeadphoneEnd, onClickPrivacy);
    }

    public StackCounter CreateCounter(Transform parent)
    {
      var counterPrefab = _assetProvider.GetResource<GameObject>(CounterPath);
      var counter = Object.Instantiate(counterPrefab, parent);
      return counter.GetComponent<StackCounter>();
    }

    public void Clear()
    {
      ClearWindow();

      if (GameStatePanel)
        Object.Destroy(GameStatePanel.gameObject);
      if (PartsPanel)
        Object.Destroy(PartsPanel.gameObject);
      if (SettingsWindow)
        Object.Destroy(SettingsWindow.gameObject);
      if (ExitAttentionWindow)
        Object.Destroy(ExitAttentionWindow.gameObject);
      if (GameBar)
        Object.Destroy(GameBar.gameObject);

      HideUIButton?.Clear();
      HideUIButton = null;
    }

    private void ClearWindow()
    {
    }

    private void SpawnPartsPanel(Transform parent)
    {
      var partsPanel = _assetProvider.GetResource<PartsPanel>(UIPartsPanel);
      PartsPanel = Object.Instantiate(partsPanel.gameObject, parent).GetComponent<PartsPanel>();
    }

    private void SpawnGameStatePanel(Transform parent)
    {
#if UNITY_IOS
      string uiGameGameStatePanel = UIGameStatePanelIOS;
#else
      string uiGameGameStatePanel = UIGameStatePanel;
#endif

      var partsPanel = _assetProvider.GetResource<GameStatePanel>(uiGameGameStatePanel);
      GameStatePanel = Object.Instantiate(partsPanel, parent);
      GameStatePanel.Construct(this, _localeSystem, _materialService, _audioService, _adsService, _remote, _assetProvider, 
        _stagesService, _inAppService);

      foreach (ICounterLayout counterLayout in GameStatePanel.GetComponentsInChildren<ICounterLayout>())
        counterLayout.Construct(_stagesService);

      HideUIButton = new HideUIButton(_gameSaveSystem, _assetProvider, _audioService, GameStatePanel);
    }

    private void SpawnGameBar(Transform parent, int maxCount)
    {
      var bar = _assetProvider.GetResource<GameBar>(UIGameBar);
      GameBar = Object.Instantiate(bar, parent);
      GameBar.Construct(maxCount);
    }

    private Part[] SpawnParts(GamePart[] gameParts, Transform parent)
    {
      var parts = new Part[gameParts.Length];
      var partsPanel = _assetProvider.GetResource<Part>(UIPart);

      for (var i = 0; i < parts.Length; i++)
      {
        parts[i] = Object.Instantiate(partsPanel, parent.GetComponent<ScrollRect>().content);
        parts[i].Construct(_inputResolver, _materialService, _audioService, gameParts[i]);
        parts[i].AddColliderToUI();
      }

      return parts;
    }
  }
}