using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using _Project.Scripts.InApp.Mono;
using _Project.Scripts.Logic.Factories.Interfaces;
using _Project.Scripts.Logic.GameLogic.Interfaces;
using _Project.Scripts.Logic.UI.Gallery.Data;
using _Project.Scripts.MonoBehaviours.Game;
using _Project.Scripts.MonoBehaviours.Game.Interfaces;
using _Project.Scripts.Services._Interfaces;
using _Project.Scripts.Services.LoadScreen.Logic;
using _Project.Scripts.Services.MaterialService.Interfaces;
using UnityEngine;
using Object = UnityEngine.Object;

namespace _Project.Scripts.Logic.Factories
{
  public class GameLevelFactory : IGameLevelFactory
  {
    private const string GameEnvironment = "Game Environment";

    private readonly Transform _parent;

    private Mechanism _mechanism;

    private readonly IAssetProvider _assetProvider;
    private readonly IGameDataProvider _gameDataProvider;
    private readonly ILoadScreenService _loadScreenService;
    private readonly IFailConnectionFactory _failConnectionFactory;

    private string _currentAddress;

    public IMechanism Mechanism => 
      _mechanism;
    public IMainMaterialProvider MainMaterialProvider => 
      _mechanism;
    
    public string CurrentWorldId =>
      _gameDataProvider.MechanismsInfo.GetWorldIdForMechanism(_mechanism.Id);
    
    public GameLevelFactory(IAssetProvider assetProvider, IGameDataProvider gameDataProvider, IFailConnectionFactory failConnectionFactory, ILoadScreenService loadScreenService)
    {
      _loadScreenService = loadScreenService;
      _failConnectionFactory = failConnectionFactory;
      _gameDataProvider = gameDataProvider;
      _assetProvider = assetProvider;

      _parent = GameObject.Find(GameEnvironment).transform;
    }

    public async Task SpawnLevel()
    {
      try
      {
        _currentAddress = _gameDataProvider.MechanismsInfo.GetCurrentMechanismAddress(_gameDataProvider.WorldsInfo.GetUsersChoiceWorldId());
        var asset = await _assetProvider.Load<GameObject>(_currentAddress);
      
        _mechanism = Object.Instantiate(asset, _parent).GetComponent<Mechanism>();
        _mechanism.Construct();
      }
      catch (ArgumentException)
      {
        CallNoConnectionWindow();
        throw;
      }
    }

    public async Task SpawnLevel(LaunchInfo launchInfo)
    {
      try
      {
        _currentAddress = _gameDataProvider.MechanismsInfo.GetMechanismAddressById(launchInfo.MechanismId);
        var asset = await _assetProvider.Load<GameObject>(_currentAddress);

        _mechanism = Object.Instantiate(asset, _parent).GetComponent<Mechanism>();
        _mechanism.Construct();
      }
      catch (ArgumentException)
      {
        CallNoConnectionWindow();
        throw;
      }
    }

    public void Clear()
    {
      if (_mechanism)
      {
        Object.Destroy(_mechanism.gameObject);
        _assetProvider.Release(_currentAddress);
      }
    }

    private void CallNoConnectionWindow()
    {
      Debug.Log("[Addressables Remote Host] Connection Lost");

      _failConnectionFactory.CreateNotAvailableWindow();
      NotAvailableWindow notAvailableWindow = _failConnectionFactory.NotAvailableWindow;

      notAvailableWindow.SetCloseAction(_loadScreenService.ToQuit);
      notAvailableWindow.Open();
      
      _loadScreenService.ForceRemoveScreens();
    }
  }
}