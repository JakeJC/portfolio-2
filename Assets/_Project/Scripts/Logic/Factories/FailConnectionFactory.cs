﻿using _Project.FMOD_AudioService.Logic;
using _Project.Scripts.InApp.Mono;
using _Project.Scripts.Logic.Factories.Interfaces;
using _Project.Scripts.Services._Interfaces;
using _Project.Scripts.Services.Localization.Logic;
using UnityEngine;

namespace _Project.Scripts.Logic.Factories
{
  public class FailConnectionFactory : IFailConnectionFactory
  {
    private const string CanvasId = "Canvas";
    
    private const string NotAvailableWindowPath = "UI/Subscription Window/Not Available Window";

    private readonly IAssetProvider _assetProvider;
    private readonly IAudioService _audioService;
    private readonly ILocaleSystem _localeSystem;

    public NotAvailableWindow NotAvailableWindow { get; private set; }

    public FailConnectionFactory(IAssetProvider assetProvider, ILocaleSystem localeSystem, IAudioService audioService)
    {
      _localeSystem = localeSystem;
      _audioService = audioService;
      _assetProvider = assetProvider;
    }
    
    public void CreateNotAvailableWindow()
    { 
      if (NotAvailableWindow)
        return;
      
      var original = _assetProvider.GetResource<GameObject>(NotAvailableWindowPath);
      var parent = GameObject.Find(CanvasId).transform;
      var gameObject = Object.Instantiate(original, parent);
      NotAvailableWindow = gameObject.GetComponent<NotAvailableWindow>();
      NotAvailableWindow.Construct(_localeSystem, _audioService);
    }
    
    public void Clear()
    {
      if (NotAvailableWindow)
        Object.Destroy(NotAvailableWindow.gameObject);

      NotAvailableWindow = null;
    }
  }
}