using _Project.Scripts.Logic.Factories.Interfaces;

namespace _Project.Scripts.Logic.Factories
{
  public struct Factories
  {
    public IGameLevelFactory GameLevelFactory;
    public IUIFactory UIFactory;
    public IGameServicesFactory GameServicesFactory;
    public ITutorialFactory TutorialFactory;
    public IResultUIFactory ResultUIFactory;
    public ITimerPopupUIFactory TimerPopupUIFactory;
    public IPrivacyPolicyFactory PrivacyPolicyFactory;
    public IInterviewFactory InterviewFactory;
    public IGalleryFactory GalleryFactory;
    public IFailConnectionFactory FailConnectionFactory;
  }
}