﻿using _Project.Scripts.Logic.Factories.Interfaces;
using _Project.Scripts.MonoBehaviours.UI.Windows;
using _Project.Scripts.Services._Interfaces;
using UnityEngine;

namespace _Project.Scripts.Logic.Factories
{
  public class TimerPopupUIFactory : ITimerPopupUIFactory
  {
    private const string CanvasId = "Canvas";
    private const string TimerPopupWindowPath = "UI/Gallery/Lock Popup Window";
    
    private readonly IAssetProvider _assetProvider;

    private TimerPopupWindow TimerPopupWindow { get; set; }

    public TimerPopupUIFactory(IAssetProvider assetProvider) => 
      _assetProvider = assetProvider;

    public TimerPopupWindow CreateTimerPopupWindow()
    {
      TimerPopupWindow = Object.Instantiate(_assetProvider.GetResource<GameObject>(TimerPopupWindowPath), 
        GameObject.Find(CanvasId).transform).GetComponent<TimerPopupWindow>();
      return TimerPopupWindow;
    }
  }
}