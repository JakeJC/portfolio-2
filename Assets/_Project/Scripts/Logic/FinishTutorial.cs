using System;
using _Modules._Analytics;
using _Project.FMOD_AudioService.Logic;
using _Project.Scripts.Animations.UI;
using _Project.Scripts.Features.Disable_Ads;
using _Project.Scripts.Features.Stages;
using _Project.Scripts.Features.Stages.Interfaces;
using _Project.Scripts.InApp.Interfaces;
using _Project.Scripts.Logic.StatesMachine;
using _Project.Scripts.Logic.StatesMachine.States;
using _Project.Scripts.MonoBehaviours.Tutorial;
using _Project.Scripts.MonoBehaviours.UI.Windows.Mono;
using _Project.Scripts.Services._Interfaces;
using _Project.Scripts.Services.LoadScreen.Logic;
using _Project.Scripts.Services.Localization.Logic;
using AppacheAds.Scripts.Interfaces;
using AppacheAnalytics.Scripts.Interfaces;
using UnityEngine.Analytics;

namespace _Project.Scripts.Logic
{
    public class FinishTutorial
    {
        private readonly FinishTutorialFactory _finishTutorialFactory;

        private readonly GameStateMachine _gameStateMachine;
        private FinishTutorialWindow _finishTutorialWindow;
        
        private readonly ILocaleSystem _localeSystem;
        private readonly IAudioService _audioService;
        private readonly ILoadScreenService _loadScreenService;
        private readonly IInAppAccessor _inAppAccessor;
        private readonly IDisableAdsLogic _disableAdsLogic;
        private readonly IStagesService _stagesService;
        private readonly IAnalytics _analytics;
        private readonly IAdsService _ads;
        
        public FinishTutorial(
            GameStateMachine gameStateMachine,
            IAssetProvider assetProvider,
            ILocaleSystem localeSystem,
            IAudioService audioService,
            ILoadScreenService loadScreenService,
            IInAppAccessor inAppAccessor,
            IDisableAdsLogic disableAdsLogic,
            IStagesService stagesService,
            IAnalytics analytics,
            IAdsService ads)
        {
            _gameStateMachine = gameStateMachine;
            _localeSystem = localeSystem;
            _audioService = audioService;
            _loadScreenService = loadScreenService;
            _inAppAccessor = inAppAccessor;
            _disableAdsLogic = disableAdsLogic;
            _stagesService = stagesService;
            _analytics = analytics;
            _ads = ads;
            
            _finishTutorialFactory = new FinishTutorialFactory(assetProvider);
        }

        public void Open()
        {
            _finishTutorialFactory.CreateFinishTutorialWindow();
            _finishTutorialWindow = _finishTutorialFactory.FinishTutorialWindow;
            _finishTutorialWindow.Construct(_localeSystem, _audioService,(() => Close(Next)) );

            _finishTutorialWindow.OpenWindow();
        }

        public void Close(Action onClose)
        {
            _finishTutorialWindow.CloseWindow((() =>
            {
                onClose?.Invoke();
                _finishTutorialFactory.Clear();
            }));
        }

        private void Next()
        {
            _inAppAccessor.Subscription.ShowInterviewSubscription(OnBuySubscription, LoadInterviewWithAds);
        }

        private void OnBuySubscription()
        {
            _ads.DisableAds();
            LoadInterview();
        }

        private void LoadInterview()
        {
            _loadScreenService.Launch<BlackTransition>(null, async () =>
            {
                _stagesService.OpenStageDescriptionAfterTutorial(() =>
                    _gameStateMachine.Enter<LoadInterviewState>(), _analytics);
            });
        }
        
        private void LoadInterviewWithAds()
        {
            _ads.CurrentInterstitial.ShowInterstitial(ScreenNames.Interview, LoadInterview, LoadInterview);
        }
    }
}
