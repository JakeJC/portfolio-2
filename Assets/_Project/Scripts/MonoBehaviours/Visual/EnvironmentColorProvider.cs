using _Project.Scripts.Logic.GameLogic.Interfaces;
using _Project.Scripts.Services.Environment;
using _Project.Scripts.Services.MaterialService.Data;
using _Project.Scripts.Services.MaterialService.Interfaces;
using UnityEngine;

namespace _Project.Scripts.MonoBehaviours.Visual
{
  public class EnvironmentColorProvider : MonoBehaviour, IColorProvider
  {
    [SerializeField] private Material _floorMaterial;
    [SerializeField] private Material _objectsMaterial;
    [SerializeField] private Camera _camera;
    [SerializeField] private GameObject _floor;

    private static readonly int Tint = Shader.PropertyToID("_BaseColor");

    private IEnvironmentController _environmentController;

    public void Construct(IEnvironmentController environmentController) => 
      _environmentController = environmentController;

    public void SetColor(ColorData colorData, bool ignoreColorFlag = false)
    {
      IGameDataProvider gameDataProvider = _environmentController.GameDataProvider;
      string mechanismId = _environmentController.MechanismId;
      
      bool isLightWorld = gameDataProvider.WorldsInfo.GetWorldDataForMechanism(mechanismId).IsLightWorld;
      
      _floorMaterial.SetColor(Tint, isLightWorld == false ? colorData.FloorColor : colorData.LightWorldFloorColor);
      _objectsMaterial.SetColor(Tint, colorData.EnvironmentColor);
      _camera.backgroundColor = isLightWorld == false ? colorData.CameraSolidColor : colorData.LightWorldCameraSolidColor;
    }
  }
}