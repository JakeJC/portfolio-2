using UnityEngine;
using UnityEngine.UI;

namespace _Project.Scripts.MonoBehaviours.Visual
{
  public class RawImageAnimation : MonoBehaviour
  {
    private RawImage _rawImage;
    [SerializeField] private float speed = 10;

    private void Awake()
    {
      _rawImage = GetComponent<RawImage>();

      float rand = Random.Range(0, 100.1f);
      
      Rect uvRect = _rawImage.uvRect;
      uvRect.x += rand;
      _rawImage.uvRect = uvRect;
    }

    private void Update()
    {
      Rect uvRect = _rawImage.uvRect;
      uvRect.x += speed * Time.deltaTime;
      _rawImage.uvRect = uvRect;
    }
  }
}