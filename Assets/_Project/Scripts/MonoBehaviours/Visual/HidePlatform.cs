﻿using DG.Tweening;
using UnityEngine;

namespace _Project.Scripts.MonoBehaviours.Visual
{
  public class HidePlatform : MonoBehaviour
  {
    public float Duration = 1f;
    public float DownDistance = 2f;

    public void Hide()
    {
      transform.DOScale(Vector3.zero, Duration);
      transform.DOMove(transform.position + DownDistance * Vector3.down, Duration);
    }
  }
}