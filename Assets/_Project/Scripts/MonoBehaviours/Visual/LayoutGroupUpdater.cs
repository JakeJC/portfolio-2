using UnityEngine;
using UnityEngine.UI;

namespace _Project.Scripts.MonoBehaviours.Visual
{
  [RequireComponent(typeof(LayoutGroup))]
  public class LayoutGroupUpdater : MonoBehaviour
  {
    private LayoutGroup _layoutGroup;

    private void OnEnable() => 
      _layoutGroup = GetComponent<LayoutGroup>();

    private void Update()
    {
      _layoutGroup.enabled = false;
      _layoutGroup.enabled = true;
    }
  }
}