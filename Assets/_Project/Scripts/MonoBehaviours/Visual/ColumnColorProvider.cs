using _Project.Scripts.Services.MaterialService.Data;
using _Project.Scripts.Services.MaterialService.Interfaces;
using UnityEngine;

namespace _Project.Scripts.MonoBehaviours.Visual
{
  [RequireComponent(typeof(Renderer))]
  public class ColumnColorProvider : MonoBehaviour, IColorProvider
  {
    private static readonly int Tint = Shader.PropertyToID("_SpecialColor");
    private Renderer _rend;
      
    private Color _baseColor;

    public void SetColor(ColorData colorData, bool ignoreColorFlag)
    {
      if (_rend == null)
      {
        _rend = GetComponent<Renderer>();
        if(_rend.sharedMaterial)
          _baseColor = _rend.sharedMaterial.GetColor(Tint);
        _rend.material = new Material(_rend.material);
      }
      
      if(_rend)
        if(_rend.sharedMaterial)
          _rend.sharedMaterial.SetColor(Tint, ignoreColorFlag ? _baseColor : colorData.ColumnColor);
    }
  }
}