﻿using _Project.Scripts.Services.MaterialService.Data;
using _Project.Scripts.Services.MaterialService.Interfaces;
using UnityEngine;

namespace _Project.Scripts.MonoBehaviours.Visual
{
  public class SkyboxColor : MonoBehaviour, IColorProvider
  {
    private static readonly int Property = Shader.PropertyToID("_Tint");
    [SerializeField] private Material skyboxMaterial;

    public void SetColor(ColorData colorData, bool ignoreColorFlag = false) => 
      skyboxMaterial.SetColor(Property, colorData.EnvironmentColor);
  }
}