using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace _Project.Scripts.MonoBehaviours.Visual
{
  public class HideBySettings : MonoBehaviour
  {
    public void Hide()
    {
      var image = GetComponent<Image>();
      if(image)
        image.enabled = false;
      
      var textMeshProUGUI = GetComponent<TextMeshProUGUI>();
      if(textMeshProUGUI)
        textMeshProUGUI.enabled = false;
    }

    public void Appear()
    {
      var image = GetComponent<Image>();
      if(image)
        image.enabled = true;
      
      var textMeshProUGUI = GetComponent<TextMeshProUGUI>();
      if (textMeshProUGUI)
        textMeshProUGUI.enabled = true;
    }
  }
}