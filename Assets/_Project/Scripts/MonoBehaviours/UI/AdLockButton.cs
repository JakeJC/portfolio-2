using AppacheAds.Scripts.Interfaces;
using UnityEngine;
using UnityEngine.UI;

namespace _Project.Scripts.MonoBehaviours.UI
{
  [RequireComponent(typeof(Button))]
  public class AdLockButton : MonoBehaviour
  {
    public string Key;
    public GameObject IconObject;
    
    private bool IsOpen
    {
      get => PlayerPrefs.GetInt(Key, 0) == 1;
      set => PlayerPrefs.SetInt(Key, value? 1 : 0);
    }
    
    private IAdsService _ads;

    private Button _button;

    public void Construct(IAdsService ads)
    {
      _ads = ads;
      
      AssignButton();

      if(IsOpen)
        Deactivate();
    }

    public void HideIcon() => 
      IconObject.SetActive(false);

    private void OnClick()
    {
      _ads.Rewarded.ShowReward($"unlock_{Key}", () =>
      {
        Deactivate();
        IsOpen = true;
      });
    }

    private void AssignButton()
    {
      _button = GetComponent<Button>();
      _button.onClick.AddListener(OnClick);
      
      _button.transform.SetAsLastSibling();
    }

    private void Deactivate() => 
      gameObject.SetActive(false);
  }
}