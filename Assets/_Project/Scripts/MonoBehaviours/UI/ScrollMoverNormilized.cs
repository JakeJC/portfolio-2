﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace _Project.Scripts.MonoBehaviours.UI
{
  [RequireComponent(typeof(ScrollRect))]
  public class ScrollMoverNormilized : MonoBehaviour, IBeginDragHandler, IEndDragHandler
  {
    private ScrollRect _rect;
    private Tween _tween;

    private void Start()
    {
      _rect = GetComponent<ScrollRect>();
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
      Clear();
    }

    public void OnEndDrag(PointerEventData eventData)
    {
      Snap(_rect.horizontalNormalizedPosition);
    }

    public void Back()
    {
      Snap(0);
    }

    private void Snap(float currentPosition)
    {
      var targetPosition = ComputeTarget(currentPosition);

      _tween = DOTween.To(() => currentPosition, x => currentPosition = x, targetPosition, 0.2f);
      _tween.onUpdate += () =>
      {
        _rect.horizontalNormalizedPosition = currentPosition;
      };
    }

    private float ComputeTarget(float currentPosition)
    {
      int count = _rect.content.childCount;

      float step = count <= 1 ? 1f : 1f / (count - 1);

      float targetPosition = 0;
      float minDelta = Mathf.Infinity;

      for (float i = 0; i <= 1f; i += step)
      {
        float delta = Mathf.Abs(i - currentPosition);
        if (delta < minDelta)
        {
          targetPosition = i;
          minDelta = delta;
        }
      }

      return targetPosition;
    }

    private void Clear()
    {
      if (_tween != null)
        _tween.Kill();
      _tween = null;
    }
  }
}