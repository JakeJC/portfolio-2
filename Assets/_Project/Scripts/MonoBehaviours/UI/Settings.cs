﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using _Modules._Analytics;
using _Project.FMOD_AudioService.Logic;
using _Project.FMOD_AudioService.Logic.Enums;
using _Project.FMOD_AudioService.Logic.Events;
using _Project.Scripts.Animations.UI;
using _Project.Scripts.Features.Disable_Ads;
using _Project.Scripts.Features.Rate_Feature;
using _Project.Scripts.InApp.Interfaces;
using _Project.Scripts.Logic.Factories.Interfaces;
using _Project.Scripts.Logic.GameLogic.Interfaces;
using _Project.Scripts.Logic.StatesMachine;
using _Project.Scripts.Logic.StatesMachine.States;
using _Project.Scripts.MonoBehaviours.Visual;
using _Project.Scripts.Services._Interfaces;
using _Project.Scripts.Services.FX;
using _Project.Scripts.Services.InputResolver;
using _Project.Scripts.Services.LeanTouchCamera.Scripts.Interfaces;
using _Project.Scripts.Services.LoadScreen.Logic;
using _Project.Scripts.Services.Localization.Logic;
using AppacheAds.Scripts.Interfaces;
using AppacheAnalytics.Scripts.Interfaces;
using AppacheRemote.Scripts.Interfaces;
using Assets._Project.FMOD_AudioService.Logic;
using DG.Tweening;
using JetBrains.Annotations;
using Sirenix.Utilities;
using UnityEngine;
using UnityEngine.UI;

namespace _Project.Scripts.MonoBehaviours.UI
{
  public class Settings : MonoBehaviour
  {
    private const string IconMusicOnPath = "icon_music_on";
    private const string IconMusicOffPath = "icon_music_off";
    private const string IconSoundOnPath = "icon_sound_on";
    private const string IconSoundOffPath = "icon_sound_off";
    private const string IconVibrationOffPath = "icon_vibration_off";
    private const string IconVibrationOnPath = "icon_vibration_on";
    private const string AccountSubscriptionsLink = "https://play.google.com/store/account/subscriptions";

    private const float SwitcherTime = .25f;

    public Image Background;

    public Button ButtonBack;
    public Button ButtonPrivacyPolicy;
    public Button ButtonRate;
    public Button ButtonGallery;

    public Button ButtonMusic;
    public Button ButtonSound;

    public Button Screen;
    public Button SubscriptionBtn;
    public Button RejectSubscriptionBtn;

    public Button NoAdsButton;
#if UNITY_IOS
    public Button RestorePurchasesButton;
#endif
    public GameObject NoAdsContainer;

    public Image IconMusic;
    public Image IconSound;
    public Image IconVibration;
    public Transform SwitcherMusic;
    public Transform SwitcherSound;
    public Transform SwitcherVibration;

    public Sprite Active;
    public Sprite Inactive;
    public Color InactiveColor;

    private Sprite _musicOn;
    private Sprite _musicOff;
    private Sprite _soundOn;
    private Sprite _soundOff;
    private Sprite _switcherOn;
    private Sprite _switcherOff;

    private ICameraController _cameraController;
    private ILoadScreenService _loadScreenService;
    private IGameSaveSystem _gameSaveSystem;
    private IAudioService _audioService;
    private IInputResolver _inputResolver;
    private IAnalytics _analytics;
    private IFXController _fxController;
    private IGameDataProvider _gameDataProvider;
    private ICurrentScreen _screenManager;

    private PrivacyPolicy _privacyPolicy;
    private GameStateMachine _gameStateMachine;

    private List<FadeCanvasGroup> _fadeCanvasGroups;
    private SlideWindow _slideWindow;

    private IUIFactory _uiFactory;
    private IGameLevelFactory _gameLevelFactory;
    private IGameServicesFactory _gameServicesFactory;
    private string _cachedScreen;
    private IInAppAccessor _inAppAccessor;
    private IAdsService _adsService;
    private IRemote _remote;
    private IDisableAdsLogic _disableAdsLogic;

    private bool _checkRejectSubscription;

    public void Construct(GameStateMachine gameStateMachine,
      IUIFactory uiFactory,
      IGameLevelFactory gameLevelFactory,
      IGameServicesFactory gameServicesFactory,
      IGameSaveSystem gameSaveSystem,
      IAudioService audioService,
      IInputResolver inputResolver,
      ILocaleSystem localeSystem,
      ILoadScreenService loadScreenService,
      IAnalytics analytics,
      ICameraController cameraController,
      IFXController fxController,
      IGameDataProvider gameDataProvider,
      IPrivacyPolicyFactory privacyPolicyFactor,
      ICurrentScreen screenManager,
      IInAppAccessor inAppAccessor,
      IAdsService adsService,
      IRemote remote,
      IDisableAdsLogic disableAdsLogic,
      bool isGallery = false)
    {
      _disableAdsLogic = disableAdsLogic;
      _remote = remote;
      _adsService = adsService;
      _inAppAccessor = inAppAccessor;
      _screenManager = screenManager;
      _gameDataProvider = gameDataProvider;
      _fxController = fxController;
      _gameStateMachine = gameStateMachine;
      _uiFactory = uiFactory;
      _gameLevelFactory = gameLevelFactory;
      _gameServicesFactory = gameServicesFactory;

      _cameraController = cameraController;
      _loadScreenService = loadScreenService;
      _gameSaveSystem = gameSaveSystem;
      _audioService = audioService;
      _inputResolver = inputResolver;
      _analytics = analytics;

      _privacyPolicy = privacyPolicyFactor.SpawnPrivacyPolicy();
      _privacyPolicy.Construct(_gameSaveSystem, _audioService, _inputResolver, localeSystem, analytics, _remote);

      Localize(localeSystem);
      LoadSprites();
      AssignListeners();
      AssignFades();

      MuteMusic(_gameSaveSystem.Get().IsMusicDisabled);
      MuteSounds(_gameSaveSystem.Get().IsSoundDisabled);

      gameObject.SetActive(false);

      bool isHaveOpenWorld =
        (_gameDataProvider.WorldsInfo.GetWorldsData().Length - _gameSaveSystem.Get().GetOpenWorlds().Count) == _gameDataProvider.WorldsInfo.GetWorldsData().Length;

      if (isGallery || isHaveOpenWorld)
      {
        ButtonGallery.gameObject.SetActive(false);
        Background.rectTransform.sizeDelta = new Vector2(Background.rectTransform.sizeDelta.x, 580f);
        Background.rectTransform.localPosition = new Vector3(0, -580f, 0);
      }
      else
        uiFactory.ExitAttentionWindow.Construct(localeSystem, _audioService, ToGallery, () =>
        {
          _uiFactory.PartsPanel.gameObject.SetActive(true);
          OpenWindow();
        });

      new ButtonAnimation(ButtonBack.transform);

      _slideWindow = new SlideWindow(Background.GetComponent<RectTransform>());

      SoundCreator();
#if IOS_FIRST_RELEASE
      HideContent();
#else
      SetupSubscriptionButton();
      SetupRejectSubscriptionButton();
      SetupNoAdsButton();
#endif
    }

    [UsedImplicitly]
    private void HideContent()
    {
      SubscriptionBtn.gameObject.SetActive(false);
      RejectSubscriptionBtn.gameObject.SetActive(false);
      NoAdsButton.gameObject.SetActive(false);
#if UNITY_IOS
      RestorePurchasesButton.transform.parent.gameObject.SetActive(false);
#endif
      NoAdsContainer.SetActive(false);
    }

#if !IOS_FIRST_RELEASE
    private void SetupSubscriptionButton()
    {
      ButtonUpdateStateSubscription();
      SubscriptionBtn.onClick.AddListener(ShowSubscriptionWindow);
    }
    
    private void SetupRejectSubscriptionButton()
    {
#if UNITY_IOS
      RejectSubscriptionBtn.gameObject.SetActive(false);
#else
      ButtonUpdateStateRejectSubscription();
      RejectSubscriptionBtn.onClick.AddListener(delegate
      {
        _checkRejectSubscription = true;
        Application.OpenURL(AccountSubscriptionsLink);
      });
#endif
    }
    
    private void SetupNoAdsButton()
    {
      NoAdsContainer.SetActive(true);
      NoAdsButton.onClick.AddListener(() => _disableAdsLogic.ShowWindow(null));

      if (_inAppAccessor.IsSubscriptionBought())
        NoAdsContainer.SetActive(false);
    }
#endif
    
#if !IOS_FIRST_RELEASE
    private void ButtonUpdateStateSubscription()
    {
      SubscriptionBtn.gameObject.SetActive(!_inAppAccessor.IsSubscriptionBought());

      if (_inAppAccessor.IsSubscriptionBought())
        NoAdsContainer.SetActive(false);
    }

    private void ButtonUpdateStateRejectSubscription()
    {
#if UNITY_IOS
      RejectSubscriptionBtn.gameObject.SetActive(false);
#else
      RejectSubscriptionBtn.gameObject.SetActive(_inAppAccessor.IsSubscriptionBought());

      if (!_inAppAccessor.IsSubscriptionBought())
        NoAdsContainer.SetActive(true);
#endif
    }
    
    private void ShowSubscriptionWindow() =>
      _inAppAccessor.Subscription.ShowInterviewSubscription(OnBuySubscription, null);

    private void OnBuySubscription()
    {
      _adsService.DisableAds();
      CloseWindow();
    }
    
    private void RejectSubscriptionCheck()
    {
      if (!_checkRejectSubscription)
        return;
      _checkRejectSubscription = false;

      CloseWindow();
    }
#endif

    private void SoundCreator()
    {
      new ButtonSound(_audioService, ButtonBack.gameObject, 17);
      new ButtonSound(_audioService, ButtonMusic.gameObject, 25);
      new ButtonSound(_audioService, ButtonSound.gameObject, 25);
      new ButtonSound(_audioService, ButtonPrivacyPolicy.gameObject, 19);
      new ButtonSound(_audioService, ButtonRate.gameObject, 19);
      new ButtonSound(_audioService, NoAdsButton.gameObject, 19);
      new ButtonSound(_audioService, ButtonGallery.gameObject, 19);
      new ButtonSound(_audioService, SubscriptionBtn.gameObject, 19);
    }

    public void OpenWindow()
    {
      _slideWindow.Open();

      gameObject.SetActive(true);
      _inputResolver.Disable();

      _fadeCanvasGroups.ToArray().ForEach(x => x.Appear());

      HideBySettings();
    }

    private void Rate()
    {
      Rate rate = new Rate();
      rate.RequestReview(rate.LaunchReview);
    }

    private void Localize(ILocaleSystem localeSystem)
    {
      List<LocalizedMainText> localizedTexts = transform.GetComponentsInChildren<LocalizedMainText>(true).ToList();
      localizedTexts.ForEach(item =>
      {
        item.Construct(localeSystem);
        item.Awake();
      });
    }

    private void CloseWindow()
    {
      _slideWindow.Close(delegate
      {
#if !IOS_FIRST_RELEASE
        ButtonUpdateStateRejectSubscription();
        ButtonUpdateStateSubscription();
#endif
      }, () =>
      {
        gameObject.SetActive(false);
        _inputResolver.Enable();
      });

      _fadeCanvasGroups.ToArray().ForEach(x => x.Disappear());

      AppearBySettings();
    }

    private void Back() =>
      CloseWindow();

    private void PrivacyPolicy()
    {
      _privacyPolicy.ClosingStarted += OnClosingStarted;
      _privacyPolicy.OpenWindow();
      CloseWindow();
      _cachedScreen = _screenManager.GetCurrentScreen();
      _screenManager.SetCurrentScreen(ScreenNames.Privacy);

      void OnClosingStarted()
      {
        OpenWindow();
        _privacyPolicy.ClosingStarted -= OnClosingStarted;
        _screenManager.SetCurrentScreen(_cachedScreen);
      }
    }

    private void SwitchMusic() =>
      MuteMusic(!_audioService.MusicIsMute());

    private void SwitchSound() =>
      MuteSounds(!_audioService.SoundsIsMute());

    private void LoadSprites()
    {
      _musicOn = LoadSprite(IconMusicOnPath);
      _musicOff = LoadSprite(IconMusicOffPath);
      _soundOn = LoadSprite(IconSoundOnPath);
      _soundOff = LoadSprite(IconSoundOffPath);
    }

    private Sprite LoadSprite(string path) =>
      Resources.Load<Sprite>(path);

    private void AssignListeners()
    {
      ButtonBack.onClick.AddListener(Back);
      ButtonPrivacyPolicy.onClick.AddListener(PrivacyPolicy);
      ButtonMusic.onClick.AddListener(SwitchMusic);
      ButtonSound.onClick.AddListener(SwitchSound);
      Screen.onClick.AddListener(Back);
      ButtonRate.onClick.AddListener(Rate);
      ButtonGallery.onClick.AddListener(OpenExitAttention);

#if UNITY_IOS
      RestorePurchasesButton.onClick.AddListener(RestorePurchases);
      RestorePurchasesButton.gameObject.SetActive(true);
#endif
    }

#if UNITY_IOS
    private void RestorePurchases() =>
      _inAppAccessor.RestorePurchases();
#endif

    private void AssignFades()
    {
      _fadeCanvasGroups = new List<FadeCanvasGroup>();
      foreach (CanvasGroup canvasGroup in GetComponentsInChildren<CanvasGroup>(true))
        _fadeCanvasGroups.Add(new FadeCanvasGroup(canvasGroup));
    }

    private void MuteMusic(bool mute)
    {
      SwitchAudio(SoundType.Music, mute);

      _gameSaveSystem.Get().IsMusicDisabled = mute;
      _gameSaveSystem.Save();
    }

    private void MuteSounds(bool mute)
    {
      SwitchAudio(SoundType.Sound, mute);

      _gameSaveSystem.Get().IsSoundDisabled = mute;
      _gameSaveSystem.Save();
    }

    private void SwitchAudio(SoundType type, bool mute)
    {
      switch (type)
      {
        case SoundType.Music:
          IconMusic.sprite = mute ? _musicOff : _musicOn;
          SwitcherMusic.DOLocalMove(mute ? Vector3.left * 19 : Vector3.right * 19, SwitcherTime); //ToDo: Заменить на бахину анимацию
          var music = SwitcherMusic.parent.GetComponent<Image>();
          music.sprite = mute ? Inactive : Active;
          music.color = mute ? InactiveColor : Color.white;
          _audioService.MuteMusic(mute);
          break;
        case SoundType.Sound:
          IconSound.sprite = mute ? _soundOff : _soundOn;
          SwitcherSound.DOLocalMove(mute ? Vector3.left * 19 : Vector3.right * 19, SwitcherTime); //ToDo: Заменить на бахину анимацию
          var sound = SwitcherSound.parent.GetComponent<Image>();
          sound.sprite = mute ? Inactive : Active;
          sound.color = mute ? InactiveColor : Color.white;
          _audioService.MuteSounds(mute);
          break;
      }
    }

    private void OpenExitAttention()
    {
      CloseWindow();

      _uiFactory.PartsPanel.gameObject.SetActive(false);

      _uiFactory.ExitAttentionWindow.Open();
    }

    private void ToGallery()
    {
      _fxController.DisableTrail();

      _audioService.StopCurrentMechanism();

      _analytics?.Send("btn_gallery");

      ButtonGallery.interactable = false;

      _loadScreenService.Launch<BlackTransition>(null, ToLoadGalleryState);

      CloseWindow();
    }

    private async Task ToLoadGalleryState()
    {
      ClearBetweenTransition();
      ChangeMusic();

      await _gameStateMachine.AsyncEnter<LoadGalleryState, ButtonType>(ButtonType.Worlds);
    }

    private void ClearBetweenTransition()
    {
      _cameraController.DisableRotation();

      _uiFactory.Clear();

      _gameLevelFactory.Clear();
      _gameServicesFactory.Clear();

      _inputResolver.Enable();
    }

    private void ChangeMusic()
    {
      AudioEventLink audioEventLink = _audioService.GetLinkByEvent(FMOD_EventType.Music);
      _audioService.SetParameter(audioEventLink, FMOD_ParameterType.current_music, 0);
    }

    private void HideBySettings()
    {
      foreach (HideBySettings hideBySettings in FindObjectsOfType<HideBySettings>())
        hideBySettings.Hide();
    }

    private void AppearBySettings()
    {
      foreach (HideBySettings hideBySettings in FindObjectsOfType<HideBySettings>())
        hideBySettings.Appear();
    }

    private enum SoundType
    {
      Music,
      Sound
    }
    
#if !IOS_FIRST_RELEASE
    private void OnApplicationFocus(bool hasFocus)
    {
      if (hasFocus)
        RejectSubscriptionCheck();
    }

    private void OnApplicationPause(bool pauseStatus)
    {
      if (!pauseStatus)
        RejectSubscriptionCheck();
    }
#endif
  }
}