using System;
using _Project.FMOD_AudioService.Logic;
using _Project.FMOD_AudioService.Logic.Enums;
using _Project.FMOD_AudioService.Logic.Events;
using _Project.Scripts.MonoBehaviours.Game;
using _Project.Scripts.MonoBehaviours.Tutorial;
using _Project.Scripts.MonoBehaviours.UI.Interfaces;
using _Project.Scripts.Services.InputResolver;
using _Project.Scripts.Services.MaterialService.Interfaces;
using UnityEngine;

namespace _Project.Scripts.MonoBehaviours.UI
{
  public class Part : MonoBehaviour, ITutorialDependent, INonUIInput
  {
    private const float CollinearTolerance = 0.75f;
    private const float ColliderFillPartPercent = 1f;

    public TutorialStrategyBase TutorialStrategy;

    public Action OnPress { get; set; }
    public GamePart GamePart { get; set; }
    
    public bool IsInDrag { get; set; }

    private BoxCollider _boxCollider;
    private Vector3 _previousValue = Vector3.zero;

    public BoxCollider BoxCollider =>
      _boxCollider;
    
    public IInputResolver InputResolver { get; set; }
    private IMaterialService MaterialService { get; set; }
    private IAudioService AudioService { get; set; }
    
    public void Construct(IInputResolver inputResolver, IMaterialService materialService, IAudioService audioService, GamePart gamePart)
    {
      GamePart = gamePart;
      InputResolver = inputResolver;
      MaterialService = materialService;
      AudioService = audioService;
    }
    
    public void AddColliderToUI()
    {
      _boxCollider = gameObject.AddComponent<BoxCollider>();
      Vector2 sizeDelta = GetComponent<RectTransform>().sizeDelta;
      _boxCollider.size = new Vector3(sizeDelta.x * ColliderFillPartPercent, sizeDelta.y * ColliderFillPartPercent, 10);
      _boxCollider.center = Vector3.zero;
    }

    public void OnTutorialStarted() => 
      TutorialStrategy.OnTutorialStarted();

    public void OnTutorialFinished() =>
      TutorialStrategy.OnTutorialFinished();

    private void OnMouseDrag()
    {
      if (InputResolver.IsLocked || MaterialService.Material.IsRematerialActive())
        return;

      if(IsInDrag)
        return;
      
      if (IsDragDirectionUp())
      {
        OnPress?.Invoke();

        AudioService.CreateEvent(out AudioEventLink link, FMOD_EventType.UI, gameObject);
        AudioService.Play(link);
        AudioService.SetParameter(link, FMOD_ParameterType.current_ui, 5);

        IsInDrag = true;
      }
      
      _previousValue = Input.mousePosition;

      bool IsDragDirectionUp()
      {
        if (_previousValue == Vector3.zero)
          return false;
        
        float dot = Vector2.Dot((Input.mousePosition - _previousValue), Vector2.up) / ((Input.mousePosition - _previousValue).magnitude);
        dot = Mathf.Clamp(dot, -1, 1);
        return dot > CollinearTolerance;
      }
    }
  }
}
