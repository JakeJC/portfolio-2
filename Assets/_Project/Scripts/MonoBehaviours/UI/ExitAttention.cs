using System;
using System.Collections.Generic;
using System.Linq;
using _Project.FMOD_AudioService.Logic;
using _Project.Scripts.Animations.UI;
using _Project.Scripts.Services.Localization.Logic;
using Assets._Project.FMOD_AudioService.Logic;
using UnityEngine;
using UnityEngine.UI;

namespace _Project.Scripts.MonoBehaviours.UI
{
  public class ExitAttention : MonoBehaviour
  {
    [SerializeField] private Button _buttonNo;
    [SerializeField] private Button _buttonYes;

    public void Construct(ILocaleSystem localeSystem, IAudioService audioService, Action onClickYes, Action onClickNo)
    {
      Localize(localeSystem);
      
      _buttonYes.onClick.AddListener(() => onClickYes?.Invoke());
      _buttonNo.onClick.AddListener(() =>
      {
        onClickNo?.Invoke();
        Close();
      });

      new ButtonAnimation(_buttonYes.transform);
      new ButtonAnimation(_buttonNo.transform);
      
      new ButtonSound(audioService, _buttonYes.gameObject, 19);
      new ButtonSound(audioService, _buttonNo.gameObject, 19);
    }

    public void Open() => 
      gameObject.SetActive(true);

    private void Close() => 
      gameObject.SetActive(false);
    
    private void Localize(ILocaleSystem localeSystem)
    {
      List<LocalizedMainText> localizedTexts = transform.GetComponentsInChildren<LocalizedMainText>().ToList();
      localizedTexts.ForEach(item =>
      {
        item.Construct(localeSystem);
        item.Awake();
      });
    }
  }
}