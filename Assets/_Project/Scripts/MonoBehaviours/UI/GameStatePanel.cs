using System;
using System.Collections.Generic;
using System.Linq;
using _Modules._InApps.Interfaces;
using _Project.FMOD_AudioService.Logic;
using _Project.Scripts.Animations.UI;
using _Project.Scripts.Features.Open_Ad_Panels_Windows;
using _Project.Scripts.Features.Stages.Interfaces;
using _Project.Scripts.Features.Stages.Mono;
using _Project.Scripts.Logic.Factories.Interfaces;
using _Project.Scripts.Services._Interfaces;
using _Project.Scripts.Services.Localization.Logic;
using _Project.Scripts.Services.MaterialService.Data;
using _Project.Scripts.Services.MaterialService.Interfaces;
using AppacheAds.Scripts.Interfaces;
using AppacheRemote.Scripts.Interfaces;
using Assets._Project.FMOD_AudioService.Logic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace _Project.Scripts.MonoBehaviours.UI
{
  public class GameStatePanel : MonoBehaviour
  {
    private const float ColorsPanelStepYDuration = 0.6f;

    [Space(10)] public RectTransform ColorsPanel;
    public RectTransform MaterialsButtonsPanel;
    public RectTransform ColorsButtonsPanel;
    public Image CurrentColorIcon;

    public GameObject ButtonBackgroundWithColor;
    public GameObject ButtonBackgroundSimple;

    public CanvasGroup CanvasGroup;

    [Header("Buttons:")] public Button Settings;
    public Button Play;
    public Button Done;
    public Button Destruct;
    public Button ShowColors;
    public Button HideColors;

    public RectTransform ShowHideBtnParent;

    [Header("CanvasGroups:")] public CanvasGroup PlayGrp;
    public CanvasGroup DoneGrp;
    public CanvasGroup SettingsGrp;
    public CanvasGroup TopPanelGrp;
    public CanvasGroup ButtonsGrp;
    public CanvasGroup ColorsGrp;
    public CanvasGroup ColorsButtons;
    public CanvasGroup MaterialsButtons;

    [Header("World Icon:")] public Image WorldIcon;

    [Header("Ads Buttons")] public AdLockButton MaterialsLockButton;
    public AdLockButton ColorsLockButton;

    private FadeCanvasGroup _playBtnGrpFade;
    private FadeCanvasGroup _doneBtnGrpFade;
    private FadeCanvasGroup _settingsBtnGrpFade;
    private FadeCanvasGroup _topPanelGrpFade;
    private FadeCanvasGroup _buttonsGrpGrpFade;
    private FadeCanvasGroup _colorsGrpFade;

    private IUIFactory _uiFactory;

    private List<MaterialButton> _materialButtons;
    private List<MaterialButton> _materialColorButtons;

    private IAudioService _audioService;
    private ILocaleSystem _localeSystem;
    private IMaterialService _materialService;
    private IStagesService _stagesService;
    private IInAppService _inAppService;

    private FadeCanvasGroup _gameStatePanelFade;

    private readonly Vector3 _colorButtonsStartAnchoredPos = new Vector2(0, -200);


    public List<MaterialButton> MaterialButtons =>
      _materialButtons;

    private ButtonSound _soundContinue;

    private readonly List<ButtonSound> _materialSoundButtons = new List<ButtonSound>();

    public void Construct(IUIFactory uiFactory,
      ILocaleSystem localeSystem, IMaterialService materialService, IAudioService audioService, IAdsService ads,
      IRemote remote, IAssetProvider assetProvider, IStagesService stagesService, IInAppService inAppService)
    {
      _inAppService = inAppService;
      _stagesService = stagesService;
      _uiFactory = uiFactory;
      _localeSystem = localeSystem;
      _materialService = materialService;
      _audioService = audioService;

      Localize();

      Done.onClick.AddListener(() => { _doneBtnGrpFade.Disappear(); });
      Play.onClick.AddListener(ToEditMode);
      ShowColors.onClick.AddListener(DoShowColors);
      HideColors.onClick.AddListener(DoHideColors);

      _playBtnGrpFade = new FadeCanvasGroup(PlayGrp);
      _settingsBtnGrpFade = new FadeCanvasGroup(SettingsGrp);
      _doneBtnGrpFade = new FadeCanvasGroup(DoneGrp);
      _topPanelGrpFade = new FadeCanvasGroup(TopPanelGrp);
      _buttonsGrpGrpFade = new FadeCanvasGroup(ButtonsGrp);
      _colorsGrpFade = new FadeCanvasGroup(ColorsGrp);
      _gameStatePanelFade = new FadeCanvasGroup(CanvasGroup, 0, 0.5f);

      _gameStatePanelFade.Appear();
      _settingsBtnGrpFade.Appear();
      _playBtnGrpFade.Appear();
      _doneBtnGrpFade.Appear();
      _topPanelGrpFade.Appear();
      _buttonsGrpGrpFade.Appear();
      _colorsGrpFade.Appear();

      _materialButtons = new List<MaterialButton>();
      _materialColorButtons = new List<MaterialButton>();

      SetMaterialButtons();
      SetMaterialColorButtons();
      SetDefaultColorAndMaterial();

      _soundContinue = new ButtonSound(_audioService, Done.gameObject, 9);

      new ButtonSound(_audioService, Play.gameObject, 8);

      new ButtonSound(_audioService, Settings.gameObject, 20);
      new ButtonSound(_audioService, ShowColors.gameObject, 17);
      new ButtonSound(_audioService, HideColors.gameObject, 17);

      foreach (MaterialButton materialButton in _materialButtons)
        _materialSoundButtons.Add(new ButtonSound(_audioService, materialButton.gameObject, 7));

      foreach (MaterialButton materialColorButton in _materialColorButtons)
        new ButtonSound(_audioService, materialColorButton.gameObject, 2);

      InitializeAdButtons(ads, remote, assetProvider);
    }

    private void InitializeAdButtons(IAdsService ads, IRemote remote, IAssetProvider assetProvider)
    {
#if IOS_FIRST_RELEASE
      ColorsLockButton.gameObject.SetActive(false);
      MaterialsLockButton.gameObject.SetActive(false);
      return;
#endif

      if (_inAppService.IsSubscriptionPurchased())
      {
        ColorsLockButton.gameObject.SetActive(false);
        MaterialsLockButton.gameObject.SetActive(false);
        return;
      }
      
      var adOpenLogic = new AdOpenLogic(_localeSystem, _audioService, ads, assetProvider,
        () => ColorsLockButton.gameObject.SetActive(false),
        () => MaterialsLockButton.gameObject.SetActive(false));

      SetupMaterialAdButton(adOpenLogic);
      SetupColorAdButton(adOpenLogic);

      MaterialsLockButton.HideIcon();
      ColorsLockButton.HideIcon();

      void SetupMaterialAdButton(AdOpenLogic adOpenLogic)
      {
        if (!adOpenLogic.IsOpenMaterial)
        {
          MaterialsLockButton.GetComponent<Button>().onClick.AddListener(adOpenLogic.OpenMaterialWindow);
          MaterialsLockButton.gameObject.SetActive(true);
          MaterialsLockButton.transform.SetAsLastSibling();
        }
        else
          MaterialsLockButton.gameObject.SetActive(false);
      }

      void SetupColorAdButton(AdOpenLogic adOpenLogic)
      {
        if (!adOpenLogic.IsOpenColor)
        {
          ColorsLockButton.GetComponent<Button>().onClick.AddListener(adOpenLogic.OpenColorWindow);
          ColorsLockButton.gameObject.SetActive(true);
          ColorsLockButton.transform.SetAsLastSibling();
        }
        else
          ColorsLockButton.gameObject.SetActive(false);
      }
    }

    public List<ButtonSound> GetMaterialSoundButtons() =>
      _materialSoundButtons;

    public void ToBaseState()
    {
      Play.gameObject.SetActive(false);
      Done.gameObject.SetActive(false);
    }

    public void ToPlay()
    {
      Done.interactable = false;
      Play.interactable = false;

      _doneBtnGrpFade.Disappear(() => Done.interactable = false, () =>
      {
        if (Done)
          Done.gameObject.SetActive(false);
      });

      _playBtnGrpFade.Appear(() => { Play.gameObject.SetActive(true); }, () => { Play.interactable = true; });
    }

    public void AppearGameStatePanel(bool show)
    {
      if (show)
        _gameStatePanelFade.Appear(null, null, 0.1f);
      else
        _gameStatePanelFade.Disappear(null, null, 0.1f);
    }

    public void DoShowColors()
    {
      if (!ColorsButtons.interactable)
        return;

      foreach (MaterialButton colorsButton in _materialColorButtons)
      {
        if (colorsButton.AnimationIsFinished == false)
          Debug.Log("isAnim");

        if (colorsButton.AnimationIsFinished == false)
          return;
      }

      ShowColors.gameObject.SetActive(false);
      HideColors.gameObject.SetActive(true);

      ButtonBackgroundWithColor.SetActive(false);
      ButtonBackgroundSimple.SetActive(true);

      ColorsButtons.interactable = false;

      ShowHideBtnParent.DOScale(new Vector3(1, -1, 1), ColorsPanelStepYDuration).onComplete += () => { ColorsButtons.interactable = true; };

      MaterialButton[] materialButtons = _materialColorButtons.ToArray();
      for (int i = 0; i < materialButtons.Length; i++)
        materialButtons[i].Show(i);
    }

    public void ShiftGamePanel(bool isShowed, Action onEnd, float targetAlpha = 1f)
    {
      var offset = -Screen.width / 2;
      var duration = .5f;

      DoneGrp.transform.DOLocalMove(DoneGrp.transform.localPosition + offset * (isShowed ? Vector3.up : Vector3.down), duration).SetEase(Ease.Linear);
      TopPanelGrp.transform.DOLocalMove(TopPanelGrp.transform.localPosition + offset * (isShowed ? Vector3.down : Vector3.up), duration).SetEase(Ease.Linear);
      ButtonsGrp.transform.DOLocalMove(ButtonsGrp.transform.localPosition + offset * (isShowed ? Vector3.up : Vector3.down), duration).SetEase(Ease.Linear);
      SettingsGrp.transform.DOLocalMove(SettingsGrp.transform.localPosition + offset * (isShowed ? Vector3.down : Vector3.up), duration).SetEase(Ease.Linear);

      ColorsGrp.transform.DOLocalMove(ColorsGrp.transform.localPosition + offset * (isShowed ? Vector3.right : Vector3.left), duration).SetEase(Ease.Linear).onComplete += () => { onEnd?.Invoke(); };

      if (!isShowed)
        _gameStatePanelFade.Appear(ShowStageView, null, 0, targetAlpha);
      else
        _gameStatePanelFade.Disappear(HideStageView);
    }

    private void ShowStageView() =>
      _stagesService.ShowHideCounter(true);

    private void HideStageView() => 
      _stagesService.ShowHideCounter(false);

    private void SetMaterialButtons()
    {
      MaterialSchemeData[] materials = _materialService.Material.GetAllMaterialsScheme();

      foreach (MaterialSchemeData material in materials)
      {
        MaterialButton materialButton = _uiFactory.CreateMaterialButton(MaterialsButtonsPanel);

        materialButton.SetIcon(material.GetIcon());
        materialButton.SetGuid(material.GetGuid());
        materialButton.SetOnClickEvent(OnMaterialButtonClick);

        _materialButtons.Add(materialButton);
      }

      MaterialsButtons.interactable = true;
    }

    private void SetMaterialColorButtons()
    {
      ColorSchemeData[] colors = _materialService.Color.GetAllColorScheme();

      Vector3 anchoredPosition = _colorButtonsStartAnchoredPos;

      foreach (ColorSchemeData color in colors)
      {
        MaterialButton colorButton = _uiFactory.CreateMaterialColorButton(ColorsButtonsPanel);

        colorButton.SetIcon(color.GetIcon());
        colorButton.SetGuid(color.GetGuid());
        colorButton.SetColor(color.ColorData.Color);
        colorButton.SetOnClickEvent(OnMaterialButtonColorClick);
        colorButton.SetAnchoredPosition(anchoredPosition);
        colorButton.gameObject.SetActive(false);

        _materialColorButtons.Add(colorButton);

        anchoredPosition.y += MaterialButton.OffsetStep;
      }
    }

    private void SetDefaultColorAndMaterial()
    {
      SetDefaultMaterial();
      SetDefaultColor();

      void SetDefaultMaterial()
      {
        if (_materialButtons.Count > 0)
        {
          string materialGuid = _materialService.Material.GetCurrentSchemeGuid();

          bool materialFound = false;
          foreach (MaterialButton materialButton in _materialButtons)
          {
            if (materialButton.GetGuid().Equals(materialGuid))
            {
              materialButton.OnClick(true);
              materialFound = true;
            }
          }

          if (!materialFound)
            _materialButtons[0].OnClick(true);
        }
      }

      void SetDefaultColor()
      {
        string colorGuid = _materialService.Color.GetCurrentSchemeGuid();

        bool colorFound = false;
        foreach (MaterialButton colorButton in _materialColorButtons)
        {
          if (colorButton.GetGuid().Equals(colorGuid))
          {
            colorButton.OnClick();
            colorFound = true;
          }
        }

        if (!colorFound)
          _materialColorButtons[0].OnClick();
      }
    }

    private void Localize()
    {
      List<LocalizedMainText> localizedTexts = transform.GetComponentsInChildren<LocalizedMainText>().ToList();
      localizedTexts.ForEach(item =>
      {
        item.Construct(_localeSystem);
        item.Awake();
      });
    }

    private void OnDestroy()
    {
      Play.onClick.RemoveListener(ToEditMode);
      ShowColors.onClick.RemoveListener(DoShowColors);
      HideColors.onClick.RemoveListener(DoHideColors);
    }

    private void ToEditMode()
    {
      CheckDoneLocalize();

      Done.interactable = false;
      Play.interactable = false;

      _doneBtnGrpFade.Appear(() =>
      {
        Done.gameObject.SetActive(true);
        Localize();
      }, () => Done.interactable = true);
      _playBtnGrpFade.Disappear(null, () =>
      {
        if (Play) Play.gameObject.SetActive(false);
      });
    }

    private void CheckDoneLocalize() =>
      _soundContinue.SetNewSound(10);

    private void DoHideColors()
    {
      if (!ColorsButtons.interactable)
        return;

      foreach (MaterialButton colorsButton in _materialColorButtons)
      {
        if (colorsButton.AnimationIsFinished == false)
          Debug.Log("isAnim");

        if (colorsButton.AnimationIsFinished == false)
          return;
      }

      ShowColors.gameObject.SetActive(true);
      HideColors.gameObject.SetActive(false);

      ButtonBackgroundWithColor.SetActive(true);
      ButtonBackgroundSimple.SetActive(false);

      ColorsButtons.interactable = false;

      ShowHideBtnParent.DOScale(new Vector3(1, 1, 1), ColorsPanelStepYDuration).onComplete += () => { ColorsButtons.interactable = true; };

      MaterialButton[] materialButtons = _materialColorButtons.ToArray();
      for (int i = materialButtons.Length - 1; i >= 0; i--)
        materialButtons[i].Hide((materialButtons.Length - 1) - i);
    }

    private void OnMaterialButtonClick(MaterialButton clickedButton, bool isInitialize)
    {
      foreach (MaterialButton materialButton in MaterialButtons)
      {
        materialButton.Deactivate();
        materialButton.CanvasGroup.interactable = true;
      }

      clickedButton.Activate();
      clickedButton.CanvasGroup.interactable = false;

      if (!isInitialize)
        LockPanels();

      _materialService.Material.SetScheme(clickedButton.GetGuid());
      _materialService.Color.SetScheme(_materialService.Color.GetCurrentSchemeGuid());
    }

    private void OnMaterialButtonColorClick(MaterialButton clickedButton, bool isInitialize)
    {
      foreach (MaterialButton materialButton in _materialColorButtons)
        materialButton.Deactivate();

      clickedButton.Activate();

      _materialService.SetData(clickedButton.GetGuid());
      _materialService.Color.SetScheme(clickedButton.GetGuid());

      CurrentColorIcon.color = clickedButton.GetColor();
    }

    private void LockPanels()
    {
      MaterialsButtons.interactable = false;
      _uiFactory.Parts.EnableColliderForAll(false);

      _materialService.Material.OnChangeMaterialEnd += OnChangeMaterialEnd;
    }

    private void OnChangeMaterialEnd()
    {
      _uiFactory.Parts.EnableColliderForAll(true);
      MaterialsButtons.interactable = true;

      _materialService.Material.OnChangeMaterialEnd -= OnChangeMaterialEnd;
    }
  }
}