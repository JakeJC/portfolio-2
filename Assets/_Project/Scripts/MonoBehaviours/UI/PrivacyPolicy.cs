using System;
using System.Collections.Generic;
using System.Linq;
using _Project.FMOD_AudioService.Logic;
using _Project.Scripts.Animations.UI;
using _Project.Scripts.Logic.StatesMachine.States;
using _Project.Scripts.Services._Interfaces;
using _Project.Scripts.Services.InputResolver;
using _Project.Scripts.Services.Localization.Logic;
using AppacheAnalytics.Scripts.Interfaces;
using AppacheRemote.Scripts.Interfaces;
using Assets._Project.FMOD_AudioService.Logic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace _Project.Scripts.MonoBehaviours.UI
{
  public class PrivacyPolicy : MonoBehaviour
  {
    [Header("AcceptScreen")] [SerializeField]
    private GameObject acceptScreen;

    [SerializeField] private Button linkButton;
    [SerializeField] private Button acceptButton1;

    [Header("ScrollScreen")] [SerializeField]
    private ScrollRect scrollRect;

    [SerializeField] private GameObject scrollScreen;
    [SerializeField] private Button acceptButton2;
    [SerializeField] private Button backButton;

    [SerializeField] private Button denyButton;
    [SerializeField] private CanvasGroup denyButtonCanvasGroup;

    public event Action ClosingStarted;
    public event Action ClosingFinished;

    private IAudioService _audioService;
    private IInputResolver _inputResolver;
    private IGameSaveSystem _gameSaveSystem;
    private IAnalytics _analytics;
    private IRemote _remote;

    private FadeImage _fadeBlurPanel;
    private List<FadeCanvasGroup> _fadeCanvasGroups;

    public void Construct(IGameSaveSystem gameSaveSystem, IAudioService audioService, IInputResolver inputResolver,
      ILocaleSystem localeSystem, IAnalytics analytics, IRemote remote)
    {
      _remote = remote;
      _analytics = analytics;
      _audioService = audioService;
      _inputResolver = inputResolver;
      _gameSaveSystem = gameSaveSystem;

      AssignFades();

      gameObject.SetActive(false);

      denyButton.gameObject.SetActive(_gameSaveSystem.Get().IsPolicyAccepted);
      
      denyButtonCanvasGroup.interactable = false;
      denyButtonCanvasGroup.alpha = 0.5f;

      Localize(localeSystem);

      AssignAnimations();

      scrollRect.onValueChanged.AddListener(OnDrag);
    }

    private void OnDrag(Vector2 arg0)
    {
      float normalizedPosition = scrollRect.verticalNormalizedPosition;

      if (normalizedPosition <= 0.01f)
      {
        denyButtonCanvasGroup.interactable = true;
        denyButtonCanvasGroup.alpha = 1f;
        
        new ButtonAnimation(denyButton.transform);
        new ButtonSound(_audioService, denyButton.gameObject, 23);
      }
    }

    public void OpenWindow(bool readOnly = false)
    {
      gameObject.SetActive(true);

      if (readOnly)
        ShowReadOnlyScreen();
      else
      {
        if (_gameSaveSystem.Get().IsPolicyAccepted)
          ShowScrollScreen();
        else
          ShowAcceptScreen(); 
      }

      acceptButton2.gameObject.SetActive(!_gameSaveSystem.Get().IsPolicyAccepted);
      backButton.gameObject.SetActive(_gameSaveSystem.Get().IsPolicyAccepted);
      
      AssignListeners();
      _inputResolver.Disable();
      _fadeBlurPanel.Appear();
      _fadeCanvasGroups.ForEach(x => x.Appear());
    }

    public void Clear()
    {
      _fadeBlurPanel.Clear();
      _fadeCanvasGroups.ForEach(p => p.Clear());
    }
    
    public void AcceptPrivacyPolicy(bool value)
    {
      _gameSaveSystem.Get().IsPolicyAccepted = value;
      _gameSaveSystem.Save();
    }

    private void AssignAnimations()
    {
      new ButtonAnimation(acceptButton1.transform);
      new ButtonAnimation(acceptButton2.transform);
      new ButtonAnimation(backButton.transform);

      new ButtonSound(_audioService, acceptButton1.gameObject, 0);
      new ButtonSound(_audioService, acceptButton2.gameObject, 0);
      new ButtonSound(_audioService, backButton.gameObject, 21);
      new ButtonSound(_audioService, linkButton.gameObject, 19);
    }

    private void Localize(ILocaleSystem localeSystem)
    {
      List<LocalizedMainText> localizedTexts = transform.GetComponentsInChildren<LocalizedMainText>(true).ToList();
      localizedTexts.ForEach(item =>
      {
        item.Construct(localeSystem);
        item.Awake();
      });
    }

    private void Accept()
    {
      _analytics.Send("btn_policy");

      AcceptPrivacyPolicy(true);

      CloseWindow();
    }

    private void Deny()
    {
      AcceptPrivacyPolicy(false);

      _audioService.Clean();

      SceneManager.LoadScene("Main");
    }

    private void Back() =>
      CloseWindow();

    private void CloseWindow()
    {
      _fadeBlurPanel.Disappear(null, OnClosingFinished);
      _fadeCanvasGroups.ForEach(x => x.Disappear());
      ClosingStarted?.Invoke();
      RemoveListeners();
      gameObject.SetActive(false);
      ClosingFinished?.Invoke();

      void OnClosingFinished()
      {
        gameObject.SetActive(false);
        ClosingFinished?.Invoke();
      }
    }

    private void ShowAcceptScreen()
    {
      acceptScreen.gameObject.SetActive(true);
      scrollScreen.gameObject.SetActive(false);
      denyButton.gameObject.SetActive(_gameSaveSystem.Get().IsPolicyAccepted);
    }

    private void ShowScrollScreen()
    {
      acceptScreen.gameObject.SetActive(false);
      scrollScreen.gameObject.SetActive(true);
      denyButton.gameObject.SetActive(_gameSaveSystem.Get().IsPolicyAccepted);
    }

    private void ShowReadOnlyScreen()
    {
      acceptScreen.gameObject.SetActive(false);
      scrollScreen.gameObject.SetActive(true);
      denyButton.gameObject.SetActive(false);
    }

    private void AssignListeners()
    {
      if (!_gameSaveSystem.Get().IsPolicyAccepted)
      {
        linkButton.onClick.AddListener(ShowScrollScreen);
        acceptButton1.onClick.AddListener(Accept);
        acceptButton2.onClick.AddListener(Accept);
      }
      else
      {
        denyButton.onClick.AddListener(Deny);
        backButton.onClick.AddListener(Back);
      }
    }

    private void RemoveListeners()
    {
      linkButton.onClick.RemoveListener(ShowScrollScreen);
      acceptButton1.onClick.RemoveListener(Accept);
      acceptButton2.onClick.RemoveListener(Accept);

      denyButton.onClick.RemoveListener(Deny);
      backButton.onClick.RemoveListener(Back);
    }

    private void AssignFades()
    {
      _fadeBlurPanel = new FadeImage(GetComponent<Image>());
      _fadeCanvasGroups = new List<FadeCanvasGroup>();

      var groups = GetComponentsInChildren<CanvasGroup>(true).ToList();
      groups.Remove(denyButtonCanvasGroup);
      
      foreach (CanvasGroup canvasGroup in groups)
        _fadeCanvasGroups.Add(new FadeCanvasGroup(canvasGroup));
    }
  }
}