﻿using System.Collections.Generic;
using _Project.FMOD_AudioService.Logic;
using _Project.Scripts.Animations.UI;
using _Project.Scripts.Services.InputResolver;
using Sirenix.Utilities;
using UnityEngine;
using UnityEngine.UI;

namespace _Project.Scripts.MonoBehaviours.UI
{
  public class Credits : MonoBehaviour
  {
    public Button ButtonBack;

    private IAudioService _audioService;
    private IInputResolver _inputResolver;
    
    private Settings _settings;
    private FadeImage _fadeBlurPanel;
    private List<FadeCanvasGroup> _fadeCanvasGroups;

    public void Construct(Settings settings, IAudioService audioService, IInputResolver inputResolver)
    {
      _audioService = audioService;
      _inputResolver = inputResolver;
      _settings = settings;
      
      AssignListeners();
      AssingFades();
      
      gameObject.SetActive(false);
    }

    public void OpenWindow()
    {
      gameObject.SetActive(true);
      _inputResolver.Disable();
      _fadeBlurPanel.Appear();
      _fadeCanvasGroups.ToArray().ForEach(x => x.Appear());
    }

    private void CloseWindow()
    {        
      _settings.OpenWindow();

      _fadeBlurPanel.Disappear(null, () =>
      {
        gameObject.SetActive(false);
      });
      _fadeCanvasGroups.ToArray().ForEach(x => x.Disappear());
    }

    private void Back()
    {
      CloseWindow();
      //_audioService.Play(AudioType.ButtonOther);
    }

    private void AssignListeners() =>
      ButtonBack.onClick.AddListener(Back);

    private void AssingFades()
    {
      _fadeBlurPanel = new FadeImage(GetComponent<Image>());
      _fadeCanvasGroups = new List<FadeCanvasGroup>();
      foreach (CanvasGroup canvasGroup in GetComponentsInChildren<CanvasGroup>(true))
        _fadeCanvasGroups.Add(new FadeCanvasGroup(canvasGroup));
    }
  }
}