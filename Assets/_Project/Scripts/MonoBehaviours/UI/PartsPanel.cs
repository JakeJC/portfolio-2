using _Project.Scripts.MonoBehaviours.UI.Interfaces;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace _Project.Scripts.MonoBehaviours.UI
{
  public class PartsPanel : MonoBehaviour, IDragDependent, ITutorialDependent
  {
    [SerializeField] private Image _line;
    [SerializeField] private Color _lineShowColor;
    [SerializeField] private Color _lineHideColor; 
    
    public CanvasGroup CanvasGroup;

    private ScrollRect _scrollRect;
    private RectTransform _rectTransform;

    private bool _isTutorialActive;

    private void OnValidate()
    {
      if (_line)
        _line.color = _lineShowColor;
    }

    private void Start()
    {
      _scrollRect = GetComponent<ScrollRect>();
      _rectTransform = GetComponent<RectTransform>();
    }

    public void OnExternalDragUp()
    {
      if (_isTutorialActive) return;
      _scrollRect.enabled = true;
    }

    public void OnExternalDragDown()
    {
      if (_isTutorialActive) return;
      _scrollRect.enabled = false;
    }

    public void OnTutorialStarted()
    {
      _isTutorialActive = true;
      _scrollRect.enabled = false;
    }

    public void OnTutorialFinished()
    {
      _isTutorialActive = false;
      _scrollRect.enabled = true;
    }

    public void ShowLine()
    {
      _line.color = _lineHideColor;
      _line.DOColor(_lineShowColor, 0.2f);
    }

    public void HideLine() => 
      _line.DOColor(_lineHideColor, 0.15f);
  }
}