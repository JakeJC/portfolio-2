﻿using System;
using System.Threading.Tasks;
using _Modules._Analytics;
using _Project.FMOD_AudioService.Logic;
using _Project.FMOD_AudioService.Logic.Enums;
using _Project.FMOD_AudioService.Logic.Events;
using _Project.Scripts.Animations.UI;
using _Project.Scripts.InApp.Interfaces;
using _Project.Scripts.Logic.StatesMachine.States;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace _Project.Scripts.MonoBehaviours.UI.Windows.Gallery
{
  public class GalleryItemPlay : MonoBehaviour
  {
    [SerializeField] private Button button;

    [SerializeField] private TextMeshProUGUI timerText;

    [Header("States")] [SerializeField] private GameObject _lock;
    [SerializeField] private GameObject _lockWithTimer;
    [SerializeField] private GameObject _readyForPlay;

    private IAudioService _audioService;

    private AudioEventLink _audioEventLink;
    private Action _onClick;

    private State CurrentState { get; set; } = State.Opened;

    public void Construct(IAudioService audioService, Action onClick)
    {
      _onClick = onClick;
      _audioService = audioService;

      CreateSoundEvent();
      AssignButtons();

      void CreateSoundEvent()
      {
        _audioService.CreateEvent(out AudioEventLink soundLink, FMOD_EventType.UI, button.gameObject);
        _audioEventLink = soundLink;
      }
    }

    private void Awake()
    {
      if (CurrentState == State.Closed)
        Lock();
      else if (CurrentState == State.Opened)
        Unlock();
      else if (CurrentState == State.ClosedWithoutTimer)
        LockWithoutTimer();
    }

    private void OnDestroy() => 
      RemoveButtons();

    public void LockWithoutTimer()
    {
      CurrentState = State.ClosedWithoutTimer;
      SwitchUIToState(CurrentState);
    }

    public void ReadyForPlay()
    {
      CurrentState = State.ReadyForPlay;
      SwitchUIToState(CurrentState);
    }

    private void Lock()
    {
      CurrentState = State.Closed;
      SwitchUIToState(CurrentState);
    }

    private void Unlock()
    {
      CurrentState = State.Opened;
      SwitchUIToState(CurrentState);
    }

    private void AssignButtons() =>
      button.onClick.AddListener(Click);

    private void RemoveButtons() =>
      button.onClick.RemoveListener(Click);

    private void Click()
    {
      if(!button.interactable)
        return;
      
      Debug.Log("Gallery Item Play Click");
      
      button.interactable = false;
      
      _audioService.Play(_audioEventLink);
      _audioService.SetParameter(_audioEventLink, FMOD_ParameterType.current_ui, 15);

      _onClick?.Invoke();
      _onClick = null;
    }

    private void SwitchUIToState(State state)
    {
      _lock.SetActive(state == State.ClosedWithoutTimer);
      _lockWithTimer.SetActive(state == State.Closed);
      _readyForPlay.SetActive(state == State.ReadyForPlay);
    }
  }
}