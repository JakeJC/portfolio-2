using _Project.Scripts.Logic.GameLogic.Advices;
using _Project.Scripts.Logic.UI.Result;
using _Project.Scripts.Services._Interfaces;
using _Project.Scripts.Services.Localization.Logic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace _Project.Scripts.MonoBehaviours.UI.Windows.Gallery
{
  public class GalleryItemAdvice : MonoBehaviour
  {
    [SerializeField] private Button button;
    
    [SerializeField] private Image adviceImage;
    [SerializeField] private TextMeshProUGUI adviceTitle;
    [SerializeField] private TextMeshProUGUI adviceDescription;
    
    public void Construct(IAssetProvider assetProvider, ILocaleSystem localeSystem, AdviceData adviceData) => 
      SetupAdviceData(assetProvider, localeSystem, adviceData);

    private void SetupAdviceData(IAssetProvider assetProvider, ILocaleSystem localeSystem, AdviceData adviceData)
    {
      adviceImage.sprite = assetProvider.GetResource<Sprite>(adviceData.IconPath);
      adviceTitle.text = localeSystem.GetText("UI/Advices/AdviceTitles", adviceData.TitleKey);
      adviceDescription.text = localeSystem.GetText("UI/Advices/Advices", adviceData.TextKey);
    }
  }
}