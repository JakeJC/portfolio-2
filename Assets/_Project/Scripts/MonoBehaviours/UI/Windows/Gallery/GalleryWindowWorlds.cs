using System;
using System.Collections.Generic;
using System.Linq;
using _Project.FMOD_AudioService.Logic;
using _Project.Scripts.Animations.UI;
using _Project.Scripts.Services.Localization.Logic;
using Assets._Project.FMOD_AudioService.Logic;
using UnityEngine;
using UnityEngine.UI;

namespace _Project.Scripts.MonoBehaviours.UI.Windows.Gallery
{
  public class GalleryWindowWorlds : MonoBehaviour
  {
    public Transform galleryItemsParent;

    private Action _onAppearStarted;
    private Action _onDisappearFinished;

    private FadeCanvasGroup _fadeCanvasGroupAnimation;
    
    [Header("Buttons:")]
    public Button Settings;

    public Transform ParentContainer;

    private bool _isOpen;

    public void Construct(IAudioService audioService, ILocaleSystem localeSystem,
      Action onAppearStarted = null,
      Action onDisappearFinished = null)
    {
      gameObject.SetActive(false);

      InitializeEvents();
      SubscribeButtons();
      InitializeAnimations();

      Localize(localeSystem);
      
      new ButtonSound(audioService, Settings.gameObject, 20);

      void InitializeEvents()
      {
        _onAppearStarted = onAppearStarted;
        _onDisappearFinished = onDisappearFinished;
      }

      void InitializeAnimations()
      {
        _fadeCanvasGroupAnimation = new FadeCanvasGroup(GetComponent<CanvasGroup>());
        new ButtonAnimation(Settings.transform);
      }
    }

    private void OnDestroy()
    {
      UnsubscribeButtons();
    }

    public void OpenWindow(Action onEnd = null)
    {    
      if(_isOpen)
        return;
      
      _isOpen = true;

      _fadeCanvasGroupAnimation.Appear(onStart: () =>
      {
        _onAppearStarted?.Invoke();
        gameObject.SetActive(true);
        
        onEnd?.Invoke();
      });
    }

    public void CloseWindow(Action onEnd = null)
    {
      _fadeCanvasGroupAnimation.Disappear(onEnd: () =>
      {
        _onDisappearFinished?.Invoke();
        gameObject.SetActive(false);
        
        onEnd?.Invoke();
        
        _isOpen = false;
      });
    }

    private void Localize(ILocaleSystem localeSystem)
    {
      List<LocalizedMainText> localizedTexts = transform.GetComponentsInChildren<LocalizedMainText>(true).ToList();
      localizedTexts.ForEach(item =>
      {
        item.Construct(localeSystem);
        item.Awake();
      });
    }

    private void SubscribeButtons()
    {
    }

    private void UnsubscribeButtons()
    {
    }
  }
}