using System;
using _Project.FMOD_AudioService.Logic;
using _Project.Scripts.Animations.UI;
using _Project.Scripts.Services.Localization.Logic;
using AppacheAnalytics.Scripts.Interfaces;
using Assets._Project.FMOD_AudioService.Logic;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace _Project.Scripts.MonoBehaviours.UI.Windows.Gallery
{
  public class GalleryPlay : MonoBehaviour
  {
    public TextMeshProUGUI Text;
    public CanvasGroup CanvasGroup;
    public Button Button;
    public GameObject Icon;
    
    private Action<GalleryPlay> _onComplete;
    private Action _onClick;
    
    private ILocaleSystem _localeSystem;
    private IAudioService _audioService;
    private IAnalytics _analytics;

    public void Construct(IAudioService audioService, ILocaleSystem localeSystem, IAnalytics analytics, Action<GalleryPlay> onComplete)
    {
      _analytics = analytics;
      _audioService = audioService;
      _localeSystem = localeSystem;
      _onComplete = onComplete;
      
      Button.onClick.AddListener(Click);
      
      SetAvailable(false);
    }

    public void SetAvailable(bool isEnabled)
    {
      CanvasGroup.DOFade(isEnabled? 1 : 0.5f, 0.25f);
      CanvasGroup.interactable = isEnabled;

      if (isEnabled)
      {    
        var transform1 = transform;

        new ButtonAnimation(transform1);
        new ButtonSound(_audioService, transform1.gameObject, 0);

        Text.text = _localeSystem.GetText("Main", "Stages.Name.Interview");
        Icon.SetActive(false);
      }
    }

    public void Hide() => 
      gameObject.SetActive(false);

    public void SetOnClickEvent(Action onClick) => 
      _onClick = onClick;

    private void Click()
    {    
      if(!Button.interactable)
        return;
      
      Button.interactable = false;
      
      _onClick?.Invoke();
      _onClick = null;
      
      _analytics.Send("btn_seance");
    }
  }
}