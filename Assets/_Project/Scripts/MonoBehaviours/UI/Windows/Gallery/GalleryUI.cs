using System;
using _Project.FMOD_AudioService.Logic;
using _Project.Scripts.Logic.GameLogic.Advices;
using _Project.Scripts.Logic.GameLogic.Interfaces;
using _Project.Scripts.Logic.UI.Gallery;
using _Project.Scripts.Services.Localization.Logic;
using UnityEngine;

namespace _Project.Scripts.MonoBehaviours.UI.Windows.Gallery
{
  public class GalleryUI : MonoBehaviour
  {
    public GalleryButtonUI Worlds;
    public GalleryButtonUI Advices;
    public GameObject MeditationLocked;
    public GalleryButtonUI Meditation;

    public CanvasGroup CanvasGroupAdvices;

    private Action _onWorldsClick;
    private Action _onAdvicesClick;
    private Action _onMediationClick;

    private ILocaleSystem _localeSystem;
    private IUIGalleryLocker _uiGalleryLocker;

    public void Construct(IGameDataProvider gameDataProvider, IUIGalleryLocker uiGalleryLocker, ILocaleSystem localeSystem, IAudioService audioService,
      Action onWorldsClick, Action onAdvicesClick, Action onMediationClick)
    {
      _uiGalleryLocker = uiGalleryLocker;
      _localeSystem = localeSystem;

      ToDefault();

      _onMediationClick = onMediationClick;
      _onAdvicesClick = onAdvicesClick;
      _onWorldsClick = onWorldsClick;

      if(Worlds)
        Worlds.Construct(audioService, OnWorldsClick);
      Advices.Construct(audioService, OnAdvicesClick);
      if (Meditation)
        Meditation.Construct(audioService, OnMeditationClick);

      Localize();

      if (gameDataProvider.MechanismsInfo.IsAllNotComplete() || gameDataProvider.IsAllAdvicesLocked())
        DisableAdvicesButton();
    }

    public void UnlockMeditation()
    {
      if(!Meditation)
        return;
      
      MeditationLocked.SetActive(false);
      Meditation.gameObject.SetActive(true);
    }

    public void ToAdvices() =>
      EnableWorldsOffAdvices(ButtonType.Advices);

    private void ToDefault() =>
      EnableWorldsOffAdvices(ButtonType.Worlds);

    public void ToMeditation() =>
      EnableWorldsOffAdvices(ButtonType.Meditation);


    private void OnWorldsClick()
    {
      if (_uiGalleryLocker.IsInTransition)
        return;

      _onWorldsClick?.Invoke();
      EnableWorldsOffAdvices(ButtonType.Worlds);
    }

    private void OnAdvicesClick()
    {
      if (_uiGalleryLocker.IsInTransition)
        return;

      _onAdvicesClick?.Invoke();
      EnableWorldsOffAdvices(ButtonType.Advices);
    }

    private void OnMeditationClick()
    {
      if (_uiGalleryLocker.IsInTransition)
        return;

      _onMediationClick?.Invoke();
      EnableWorldsOffAdvices(ButtonType.Meditation);
    }

    private void EnableWorldsOffAdvices(ButtonType value)
    {
      switch (value)
      {
        case ButtonType.Worlds:
          if (Worlds)
            Worlds.On(true);
          Advices.On(false);
          if (Meditation)
            Meditation.On(false);
          break;
        case ButtonType.Advices:
          if (Worlds)
            Worlds.On(false);
          Advices.On(true);
          if (Meditation)
            Meditation.On(false);
          break;
        case ButtonType.Meditation:
          if(Worlds)
            Worlds.On(false);
          Advices.On(false);
          if (Meditation)
            Meditation.On(true);
          break;
      }
    }

    private void DisableAdvicesButton()
    {
      CanvasGroupAdvices.interactable = false;
      CanvasGroupAdvices.alpha = 0.2f;
    }

    private void Localize()
    {
      var localizedTexts = transform.GetComponentsInChildren<LocalizedMainText>(true);
      foreach (LocalizedMainText localizedMainText in localizedTexts)
      {
        localizedMainText.Construct(_localeSystem);
        localizedMainText.Awake();
      }
    }
  }
}

public enum ButtonType
{
  Worlds,
  Advices,
  Meditation
}