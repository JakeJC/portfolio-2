using System;
using _Project.FMOD_AudioService.Logic;
using _Project.Scripts.Animations.UI;
using Assets._Project.FMOD_AudioService.Logic;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UI.Extensions;

namespace _Project.Scripts.MonoBehaviours.UI.Windows.Gallery
{
  public class GalleryWindowMechanisms : MonoBehaviour
  {
    public Transform galleryItemsParent;
    public Button backButton;
    public TextMeshProUGUI WorldTitle;

    private Action _onAppearStarted;
    private Action _onDisappearFinished;
    private Action _onBackButtonClicked;

    private FadeCanvasGroup _fadeCanvasGroupAnimation;

    public void Construct(
      IAudioService audioService,
      Action onBackButtonClicked,
      string worldName,
      Action onAppearStarted = null,
      Action onDisappearFinished = null)
    {
      gameObject.SetActive(false);

      InitializeEvents();
      SubscribeButtons();
      InitializeAnimations(audioService);

      WorldTitle.text = worldName;
      
      void InitializeEvents()
      {
        _onAppearStarted = onAppearStarted;
        _onDisappearFinished = onDisappearFinished;
        _onBackButtonClicked = onBackButtonClicked;
      }

      void InitializeAnimations(IAudioService audioService)
      {
        _fadeCanvasGroupAnimation = new FadeCanvasGroup(GetComponent<CanvasGroup>());
        new ButtonAnimation(backButton.transform);
        new ButtonSound(audioService, backButton.gameObject, 16);
      }
    }

    private void OnDestroy()
    {
      UnsubscribeButtons();
    }

    public void OpenWindow(Action onEnd = null)
    {
      _fadeCanvasGroupAnimation.Appear(onStart: () =>
      {
        _onAppearStarted?.Invoke();
        gameObject.SetActive(true);
        
        onEnd?.Invoke();
      });

      var horizontalScrollSnap = galleryItemsParent.GetComponentInParent<HorizontalScrollSnap>();
      var pageStep = horizontalScrollSnap.PageStep;
      Setter(0);
      DOTween.To(Getter, Setter, pageStep, 0.5f);

      float Getter()
      {
        return horizontalScrollSnap.PageStep;
      }

      void Setter(float value)
      {
        horizontalScrollSnap.PageStep = value;
        horizontalScrollSnap.UpdateLayout();
      }
    }

    public void CloseWindow(Action onEnd = null)
    {
      _fadeCanvasGroupAnimation.Disappear(onEnd: () =>
      {
        _onDisappearFinished?.Invoke();
        gameObject.SetActive(false);

        onEnd?.Invoke();
      });
    }

    private void SubscribeButtons()
    {
      backButton.onClick.AddListener(OnBackButtonClick);
    }

    private void UnsubscribeButtons()
    {
      backButton.onClick.RemoveListener(OnBackButtonClick);
    }

    private void OnBackButtonClick()
    {
      _onBackButtonClicked?.Invoke();
    }
  }
}