using System;
using _Project.FMOD_AudioService.Logic;
using _Project.FMOD_AudioService.Logic.Enums;
using _Project.FMOD_AudioService.Logic.Events;
using _Project.Scripts.Animations.UI;
using _Project.Scripts.Data.Scriptables;
using _Project.Scripts.Services.Localization.Logic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace _Project.Scripts.MonoBehaviours.UI.Windows.Gallery
{
  public class GalleryItemWorlds : MonoBehaviour
  {
    private const string LocalizeName = "Main";

    [SerializeField] private Button button;

    [SerializeField] private Image iconImage;
    
    [SerializeField] private Animator iconImageAnimator;

    [SerializeField] private TextMeshProUGUI titleText;

    [SerializeField] private Transform labelContainer;
    
    [SerializeField] private Animator animator;
    
    [Header("States")]
    [SerializeField] private GameObject _unlock;
    [SerializeField] private GameObject _lock;
    [SerializeField] private GameObject _lockWithTimer;
    [SerializeField] private GameObject _readyForPlay;

    private IAudioService _audioService;
    private AudioEventLink _audioEventLink;
    
    private State CurrentState { get; set; } = State.Opened;

    private static readonly int LockAnimatorParameter = Animator.StringToHash("Lock");
    private static readonly int UnlockAnimatorParameter = Animator.StringToHash("Unlock");

    private Action<WorldData> _onClick;
    private WorldData _worldData;
    
    public void Construct(ILocaleSystem localeSystem, IAudioService audioService, WorldData worldData)
    {
      _audioService = audioService;
      
      _worldData = worldData;
      
      titleText.text = localeSystem.GetText(LocalizeName, worldData.KeyName);

      new CardAnimation(button, transform as RectTransform);

      SubscribeButtons();

      SetupCardVisual(worldData.WorldIcon);
      SetupCardAnimation(worldData.WorldIconAnimatorController);

      CreateSoundEvent();
      
      void CreateSoundEvent()
      {
        _audioService.CreateEvent(out AudioEventLink soundLink, FMOD_EventType.UI, button.gameObject);
        _audioEventLink = soundLink;
      }
    }

    private void Awake()
    {
      animator.ResetTrigger(LockAnimatorParameter);
      animator.ResetTrigger(UnlockAnimatorParameter);

      if (CurrentState == State.Closed)
        Lock();
      else if (CurrentState == State.Opened)
        Unlock();
      else if (CurrentState == State.ClosedWithoutTimer)
        LockWithoutTimer();
      else if (CurrentState == State.ReadyForPlay)
        ReadyForPlay();
    }

    private void OnDestroy() =>
      UnsubscribeButtons();

    public void SetOnClickEvent(Action<WorldData> onClick) => 
      _onClick = onClick;

    public void ShowLabel(bool value) =>
      labelContainer.transform.gameObject.SetActive(value);

    public void Unlock()
    {
      CurrentState = State.Opened;
      SwitchUIToState(CurrentState);
      
      animator.SetTrigger(UnlockAnimatorParameter);
    }

    private void LockWithoutTimer()
    {
      CurrentState = State.ClosedWithoutTimer;
      SwitchUIToState(CurrentState);
      
      animator.SetTrigger(LockAnimatorParameter);
    }

    private void ReadyForPlay()
    {
      CurrentState = State.ReadyForPlay;
      SwitchUIToState(CurrentState);
      
      if(animator)
        animator.SetTrigger(UnlockAnimatorParameter);
    }

    private void Lock()
    {
      CurrentState = State.Closed;
      SwitchUIToState(CurrentState);
      
      if(animator)
        animator.SetTrigger(LockAnimatorParameter);
    }

    private void SetupCardVisual(Sprite icon) =>
      iconImage.sprite = icon;

    private void SetupCardAnimation(RuntimeAnimatorController controller) => 
      iconImageAnimator.runtimeAnimatorController = controller;

    private void SubscribeButtons() => 
      button.onClick.AddListener(OnClick);

    private void UnsubscribeButtons() => 
      button.onClick.RemoveListener(OnClick);

    private void OnClick()
    {
      _audioService.Play(_audioEventLink);
      _audioService.SetParameter(_audioEventLink, FMOD_ParameterType.current_ui, 15);
      
      _onClick?.Invoke(_worldData);
    }

    private void SwitchUIToState(State state)
    {
      _unlock.SetActive(state == State.Opened);
      _lock.SetActive(state == State.ClosedWithoutTimer);
      _lockWithTimer.SetActive(state == State.Closed);
      _readyForPlay.SetActive(state == State.ReadyForPlay);
    }
  }

  public enum State
  {
    Opened,
    ReadyForPlay,
    Closed,
    ClosedWithoutTimer
  }
}