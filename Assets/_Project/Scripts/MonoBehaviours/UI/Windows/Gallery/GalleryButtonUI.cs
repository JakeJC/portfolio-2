using _Project.FMOD_AudioService.Logic;
using _Project.Scripts.Animations.UI;
using Assets._Project.FMOD_AudioService.Logic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace _Project.Scripts.MonoBehaviours.UI.Windows.Gallery
{
  public class GalleryButtonUI : MonoBehaviour
  {
    public Button Button;

    public Image Icon;
    public TextMeshProUGUI Text;

    public Color Active;
    public Color Disabled;
    
    public bool IsOn { get; set; }
    
    public void Construct(IAudioService audioService, UnityAction onClick)
    {
      Button.onClick.AddListener(onClick);
      new ButtonAnimation(transform);
      new ButtonSound(audioService, gameObject, 14);
    }

    public void On(bool value)
    {
      IsOn = value;
      Icon.color = value ? Active : Disabled;
      Text.color = value ? Active : Disabled;
    }
  }
}