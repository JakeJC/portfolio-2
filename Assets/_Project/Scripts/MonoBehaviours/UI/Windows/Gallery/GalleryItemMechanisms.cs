using System;
using System.Collections.Generic;
using System.Linq;
using _Project.FMOD_AudioService.Logic;
using _Project.FMOD_AudioService.Logic.Enums;
using _Project.FMOD_AudioService.Logic.Events;
using _Project.Scripts.Animations.UI;
using _Project.Scripts.Data.Scriptables;
using _Project.Scripts.Services.Localization.Logic;
using UnityEngine;
using UnityEngine.UI;

namespace _Project.Scripts.MonoBehaviours.UI.Windows.Gallery
{
  public class GalleryItemMechanisms : MonoBehaviour
  {
    [SerializeField] private Button button;

    [SerializeField] private RawImage rawImage;

    [SerializeField] private Animator animator;
    
    [SerializeField] private GameObject labelContainer;

    [SerializeField] private GameObject background;
    [SerializeField] private GameObject backgroundLightWorld;

    private IAudioService _audioService;
    private AudioEventLink _audioEventLink;

    private State CurrentState { get; set; } = State.Opened;

    private static readonly int LockAnimatorParameter = Animator.StringToHash("Lock");
    private static readonly int UnlockAnimatorParameter = Animator.StringToHash("Unlock");

    private Action _onClick;

    public void Construct(ILocaleSystem localeSystem, IAudioService audioService, Action onClick, Texture renderTexture, WorldData worldData)
    {
      _audioService = audioService;
      rawImage.texture = renderTexture;

      if (worldData.IsLightWorld)
      {
        background.SetActive(false);
        backgroundLightWorld.SetActive(true);
      }

      InitializeEvents();
      SubscribeButtons();
      InitializeAnimations();
      CreateSoundEvent();

      Localize(localeSystem);
      
      void InitializeEvents()
      {
        _onClick = onClick;
      }

      void InitializeAnimations()
      { 
        new CardAnimation(button, transform as RectTransform);
      }
      
      void CreateSoundEvent()
      {
        _audioService.CreateEvent(out AudioEventLink soundLink, FMOD_EventType.UI, button.gameObject);
        _audioEventLink = soundLink;
      }
    }

    public void Unlock()
    {
      CurrentState = State.Opened;
      if(animator)
        animator.SetTrigger(UnlockAnimatorParameter);
    }

    public void ShowLabel(bool value)
    {
      if(labelContainer)
        labelContainer.SetActive(value);
    }

    private void Awake()
    {
      animator.ResetTrigger(LockAnimatorParameter);
      animator.ResetTrigger(UnlockAnimatorParameter);

      if (CurrentState == State.Closed)
        animator.SetTrigger(LockAnimatorParameter);
      else if (CurrentState == State.Opened)
        animator.SetTrigger(UnlockAnimatorParameter);

      ExecuteWhenComplete();
    }

    private void OnDestroy()
    {
      UnsubscribeButtons();
    }

    private void SubscribeButtons()
    {
      button.onClick.AddListener(OnClick);
    }

    private void UnsubscribeButtons()
    {
      button.onClick.RemoveListener(OnClick);
    }

    private void OnClick()
    {
      _audioService.Play(_audioEventLink);
      _audioService.SetParameter(_audioEventLink, FMOD_ParameterType.current_ui, 18);
      
      _onClick?.Invoke();
    }

    private enum State
    {
      Opened,
      Closed,
    }

    private void ExecuteWhenComplete() => 
      Unlock();

    private void Localize(ILocaleSystem localeSystem)
    {
      List<LocalizedMainText> localizedTexts = transform.GetComponentsInChildren<LocalizedMainText>(true).ToList();
      localizedTexts.ForEach(item =>
      {
        item.Construct(localeSystem);
        item.Awake();
      });
    }
  }
}