﻿using System;
using System.Collections.Generic;
using System.Linq;
using _Project.Scripts.Animations.UI;
using _Project.Scripts.Services.Localization.Logic;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace _Project.Scripts.MonoBehaviours.UI.Windows
{
  public class TimerPopupWindow : MonoBehaviour
  {
    private const string LocaleKey = "Main";

    private const float AnimationGalleryButtonDuration = 0.3f;

    public TextMeshProUGUI TimerText;
    
    public Button GalleryBtn;
    public Button QuitBtn;

    public CanvasGroup TextContainer;
    public CanvasGroup ButtonContainer;

    private Action _onGallery;
    private Action _onQuit;
    private Action _onClose;
    private Action _onOpen;

    private FadeImage _fadeCanvasGroupAnimation;

    private FadeCanvasGroup _fadeCanvasGroupGallery;
    private FadeCanvasGroup _fadeCanvasGroupQuit;
    
    private FadeCanvasGroup _fadeCanvasGroupTextContainer;
    private FadeCanvasGroup _fadeCanvasGroupButtonContainer;

    public RectTransform AnimationGalleryButtonRect;
    
    private Image _animationGalleryButtonImage;
    private Vector2 _animationGalleryButtonDefaultSize;
    private Color _animationGalleryButtonDefaultColor;
    private Color _animationGalleryButtonHideColor => 
      new Color(_animationGalleryButtonDefaultColor.r, _animationGalleryButtonDefaultColor.g, _animationGalleryButtonDefaultColor.b, 0);

    public void Construct(ILocaleSystem localeSystem,
      Action onGallery,
      Action onQuit,
      Action onClose = null,
      Action onOpen = null)
    {
      gameObject.SetActive(false);

      Localize(localeSystem);
      
      InitializeEvents();
      InitializeButtons();
      InitializeAnimations();
      
      void InitializeEvents()
      {
        _onGallery = onGallery;
        _onQuit = onQuit;
        _onClose = onClose;
        _onOpen = onOpen;
      }

      void InitializeButtons()
      {
        GalleryBtn.onClick.AddListener(ToGallery);
        QuitBtn.onClick.AddListener(ToQuit);
      }

      void InitializeAnimations()
      {
        _fadeCanvasGroupAnimation = new FadeImage(GetComponent<Image>());

        _fadeCanvasGroupGallery = new FadeCanvasGroup(GalleryBtn.GetComponent<CanvasGroup>(), 0.5f);
        _fadeCanvasGroupQuit = new FadeCanvasGroup(QuitBtn.GetComponent<CanvasGroup>(), 0.5f);
        
        _fadeCanvasGroupTextContainer = new FadeCanvasGroup(TextContainer, 1, 1);
        _fadeCanvasGroupButtonContainer = new FadeCanvasGroup(ButtonContainer, 1.5f, 1);

        InitializeAnimationGalleryButton();
      }
    }

    private void OnDestroy()
    {
      GalleryBtn.onClick.RemoveListener(ToGallery);
      QuitBtn.onClick.RemoveListener(ToQuit);
    }

    public void OpenWindow()
    {
      _onOpen?.Invoke();
      gameObject.SetActive(true);
      
      AppearAnimation();
    }

    private void Localize(ILocaleSystem localeSystem)
    {
      List<LocalizedMainText> localizedTexts = transform.GetComponentsInChildren<LocalizedMainText>(true).ToList();
      localizedTexts.ForEach(item =>
      {
        item.Construct(localeSystem);
        item.Awake();
      });
    }
    
    private void CloseWindow()
    {
      _fadeCanvasGroupAnimation.Disappear(null, () =>
      {
        _onClose?.Invoke();
        gameObject.SetActive(false);
      });

      DisappearAnimation();
    }

    private void ToGallery()
    {
      GalleryBtn.interactable = false;
      _onGallery?.Invoke();
      CloseWindow();
    }

    private void ToQuit()
    {
      QuitBtn.interactable = false;
      _onQuit?.Invoke();
      CloseWindow();
    }
    
    private void AppearAnimation()
    {
      _fadeCanvasGroupTextContainer.Appear();
      _fadeCanvasGroupButtonContainer.Appear();

      _fadeCanvasGroupAnimation.Appear();

      _fadeCanvasGroupGallery.Appear();
      _fadeCanvasGroupQuit.Appear();
      
      Canvas.ForceUpdateCanvases();
    }

    private void DisappearAnimation()
    {
      _fadeCanvasGroupTextContainer.Disappear();
      _fadeCanvasGroupButtonContainer.Disappear();

      _fadeCanvasGroupGallery.Disappear();
      _fadeCanvasGroupQuit.Disappear();
    }
    
    void InitializeAnimationGalleryButton()
    {
      _animationGalleryButtonImage = AnimationGalleryButtonRect.GetComponent<Image>();
      _animationGalleryButtonDefaultSize = AnimationGalleryButtonRect.sizeDelta;
      _animationGalleryButtonDefaultColor = _animationGalleryButtonImage.color;
      _animationGalleryButtonImage.color = _animationGalleryButtonHideColor;
      
      EventTrigger trigger = GalleryBtn.gameObject.AddComponent<EventTrigger>();
      EventTrigger.Entry entry = new EventTrigger.Entry();
      entry.eventID = EventTriggerType.PointerDown;
      entry.callback.AddListener((eventData) =>
      {
        _animationGalleryButtonImage.color = _animationGalleryButtonHideColor;
        _animationGalleryButtonImage.DOColor(_animationGalleryButtonDefaultColor, AnimationGalleryButtonDuration);

        AnimationGalleryButtonRect.sizeDelta = new Vector2(_animationGalleryButtonDefaultSize.x / 2, _animationGalleryButtonDefaultSize.y);
        AnimationGalleryButtonRect.DOSizeDelta(_animationGalleryButtonDefaultSize, AnimationGalleryButtonDuration);
      });
      trigger.triggers.Add(entry);

      entry = new EventTrigger.Entry();
      entry.eventID = EventTriggerType.PointerExit;
      entry.callback.AddListener((eventData) =>
      {
        _animationGalleryButtonImage.DOColor(_animationGalleryButtonHideColor, AnimationGalleryButtonDuration);
      });
      trigger.triggers.Add(entry);
    }
  }
}