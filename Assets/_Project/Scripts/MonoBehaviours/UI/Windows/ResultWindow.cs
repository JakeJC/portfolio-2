using System;
using System.Collections.Generic;
using System.Linq;
using _Modules._Analytics;
using _Project.FMOD_AudioService.Logic;
using _Project.Scripts.Animations.UI;
using _Project.Scripts.Features.Stages;
using _Project.Scripts.Logic.GameLogic.Interfaces;
using _Project.Scripts.Logic.StatesMachine.States;
using _Project.Scripts.Services.LoadScreen.Logic;
using _Project.Scripts.Services.Localization.Logic;
using AppacheAds.Scripts.Interfaces;
using AppacheAnalytics.Scripts.Interfaces;
using AppacheRemote.Scripts.Interfaces;
using Assets._Project.FMOD_AudioService.Logic;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace _Project.Scripts.MonoBehaviours.UI.Windows
{
  public class ResultWindow : MonoBehaviour
  {
    public TextMeshProUGUI Title;
    public TextMeshProUGUI Advice;
    public Image Picture;

    public Button GalleryBtn;
    public Button QuitBtn;
    public Button RateBtn;

    public CanvasGroup TextContainer;
    public CanvasGroup MiddleContainer;
    public CanvasGroup ButtonContainer;

    public VerticalLayoutGroup ButtonsLayout;

    private Action _onGallery;
    private Action _onQuit;
    private Action _onClose;
    private Action _onOpen;

    private IAudioService _audioService;
    private IAnalytics _analytics;
    private IRemote _remote;

    private FadeCanvasGroup _fadeCanvasGroupQuit;
    private FadeCanvasGroup _fadeCanvasGroupTextContainer;
    private FadeCanvasGroup _fadeCanvasGroupMiddleContainer;
    private FadeCanvasGroup _fadeCanvasGroupButtonContainer;

    private IGameDataProvider _gameDataProvider;

    private Action _onRate;
    private Action _onMeditation;

    public void Construct(
      ILocaleSystem localeSystem,
      IAudioService audioService,
      IGameDataProvider gameDataProvider,
      IAnalytics analytics,
      Action onGallery,
      Action onQuit,
      Action onRate,
      Action onMeditation,
      Action toNextMechanism,
      bool meditationLead,
      IRemote remote,
      Action onClose = null,
      Action onOpen = null)
    {
      _onRate = onRate;
      _gameDataProvider = gameDataProvider;
      _audioService = audioService;
      _analytics = analytics;
      _remote = remote;

      gameObject.SetActive(false);

      Localize(localeSystem);

      InitializeEvents();
      InitializeButtons();
      InitializeAnimations();

      void InitializeEvents()
      {
        _onMeditation = onMeditation;
        _onGallery = onGallery;
        _onQuit = onQuit;
        _onClose = onClose;
        _onOpen = onOpen;
      }

      void InitializeButtons()
      {
        RateBtn.onClick.AddListener(Rate);
        
        if (meditationLead)
          GalleryBtn.onClick.AddListener(ToMeditation);
        else
          GalleryBtn.onClick.AddListener(ToGallery);
        
        QuitBtn.GetComponentInChildren<LocalizedMainText>().SetNewKey("Stages.Name.Construction");
        QuitBtn.onClick.AddListener(() => toNextMechanism?.Invoke());

        var transformQuit = QuitBtn.transform;
        var transformGallery = GalleryBtn.transform.parent;
        (transformQuit.localPosition, transformGallery.localPosition) =
          (transformGallery.localPosition, transformQuit.localPosition);

        var imageQuit = QuitBtn.GetComponent<Image>();
        var imageGallery = GalleryBtn.GetComponent<Image>();
        (imageQuit.material, imageGallery.material) =
          (imageGallery.material, imageQuit.material);
        (imageQuit.color, imageGallery.color) =
          (imageGallery.color, imageQuit.color);
      }

      void InitializeAnimations()
      {
        _fadeCanvasGroupQuit = new FadeCanvasGroup(QuitBtn.GetComponent<CanvasGroup>(), 0.5f);

        _fadeCanvasGroupTextContainer = new FadeCanvasGroup(TextContainer, 1, 1);
        _fadeCanvasGroupMiddleContainer = new FadeCanvasGroup(MiddleContainer, 1.25f, 1);
        _fadeCanvasGroupButtonContainer = new FadeCanvasGroup(ButtonContainer, 1.5f, 1);

        new ButtonSound(_audioService, QuitBtn.gameObject, 13);
        new ButtonSound(_audioService, RateBtn.gameObject, 13);
        new ButtonSound(_audioService, GalleryBtn.gameObject, 13);

        new ButtonAnimation(QuitBtn.transform);
        new ButtonAnimation(GalleryBtn.transform);
      }

      EnableQuitButton();
    }

    private void EnableQuitButton() =>
      QuitBtn.gameObject.SetActive(true);

    private void OnDestroy()
    {
      GalleryBtn.onClick.RemoveListener(ToGallery);
      QuitBtn.onClick.RemoveListener(ToQuit);
    }

    public void SetAdvice(string title, string text, Sprite icon)
    {
      Title.text = title;
      Advice.text = text;

      Picture.sprite = icon;
      Picture.SetNativeSize();
    }

    public void OpenWindow()
    {
      _onOpen?.Invoke();
      gameObject.SetActive(true);

      AppearAnimation();
    }

    private void CloseWindow()
    {
      _fadeCanvasGroupButtonContainer.Disappear(null, () =>
      {
        _onClose?.Invoke();
        gameObject.SetActive(false);
      });

      DisappearAnimation();
    }

    private void ToGallery()
    {
      _analytics?.Send("btn_gallery");

      GalleryBtn.interactable = false;

      _onGallery?.Invoke();
      _onGallery = null;

      CloseWindow();
    }

    private void ToMeditation()
    {
      _analytics?.Send("btn_gallery");

      GalleryBtn.interactable = false;

      _onMeditation?.Invoke();
      _onMeditation = null;

      CloseWindow();
    }

    private void ToQuit()
    {
      _analytics?.Send("btn_close");
      _onQuit?.Invoke();
    }

    private void Rate() =>
      _onRate?.Invoke();

    private void AppearAnimation()
    {
      _fadeCanvasGroupTextContainer.Appear();
      _fadeCanvasGroupMiddleContainer.Appear();
      _fadeCanvasGroupButtonContainer.Appear();

      _fadeCanvasGroupQuit.Appear();

      Canvas.ForceUpdateCanvases();
    }

    private void DisappearAnimation()
    {
      _fadeCanvasGroupTextContainer.Disappear();
      _fadeCanvasGroupMiddleContainer.Disappear();

      _fadeCanvasGroupQuit.Disappear();
    }

    private void Localize(ILocaleSystem localeSystem)
    {
      List<LocalizedMainText> localizedTexts = transform.GetComponentsInChildren<LocalizedMainText>(true).ToList();
      localizedTexts.ForEach(item =>
      {
        item.Construct(localeSystem);
        item.Awake();
      });
    }
  }
}