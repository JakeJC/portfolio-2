using System;
using System.Collections.Generic;
using System.Linq;
using _Project.FMOD_AudioService.Logic;
using _Project.Scripts.Animations.UI;
using _Project.Scripts.Services.Localization.Logic;
using Assets._Project.FMOD_AudioService.Logic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace _Project.Scripts.MonoBehaviours.UI.Windows.Mono
{
    public class FinishTutorialWindow : MonoBehaviour
    {
        [SerializeField] private TMP_Text Header;
        [SerializeField] private TMP_Text Description;
        
        [SerializeField] private Text NextButtonText;
        
        [SerializeField] private Button NextButton;

        [SerializeField] private CanvasGroup TopGroup;
        [SerializeField] private CanvasGroup MiddleGroup1;
        [SerializeField] private CanvasGroup MiddleGroup2;
        [SerializeField] private CanvasGroup BottomGroup;
        
        private FadeCanvasGroup _fadeTop;
        private FadeCanvasGroup _fadeMiddle1;
        private FadeCanvasGroup _fadeMiddle2;
        private FadeCanvasGroup _fadeBottom;
        
        private ILocaleSystem _localeSystem;
        
        public void Construct(ILocaleSystem localeSystem, IAudioService audioService, Action next)
        {
            _localeSystem = localeSystem;
            
            AssignAnimations();
            
            AssignSounds(audioService);
            AssignButtonsListeners(next);
            AssignButtonsAnimations();
            
            Localize(localeSystem);
        }

        
        public void OpenWindow()
        {
            gameObject.SetActive(true);
            AppearAnimation();
        }

        public void CloseWindow(Action onClose)
        {
            _fadeBottom.Disappear(null, (() =>
            {
                onClose?.Invoke();
                gameObject.SetActive(false);
            }));
            
            DisappearAnimation();
        }
        
        private void AssignAnimations()
        {
            _fadeTop = new FadeCanvasGroup(TopGroup, 1, 1);
            _fadeMiddle1 = new FadeCanvasGroup(MiddleGroup1, 1.25f, 1);
            _fadeMiddle2 = new FadeCanvasGroup(MiddleGroup2, 1.5f, 1);
            _fadeBottom = new FadeCanvasGroup(BottomGroup, 1.75f, 1);
        }
        
        private void AssignButtonsAnimations()
        {
            new ButtonAnimation(NextButton.transform);
        }
        
        private void AssignSounds(IAudioService audioService)
        {
            new ButtonSound(audioService, NextButton.gameObject, 13);
        }
        
        private void AppearAnimation()
        {
            _fadeTop.Appear();
            _fadeMiddle1.Appear();
            _fadeMiddle2.Appear();
            _fadeBottom.Appear();
        }
        
        private void DisappearAnimation()
        {
            _fadeTop.Disappear();
            _fadeMiddle1.Disappear();
            _fadeMiddle2.Disappear();
        }
        
        private void AssignButtonsListeners(Action next)
        {
            NextButton.onClick.AddListener(next.Invoke);
        }
        
        private void Localize(ILocaleSystem localeSystem)
        {
            List<LocalizedMainText> localizedTexts = transform.GetComponentsInChildren<LocalizedMainText>(true).ToList();
            localizedTexts.ForEach(item =>
            {
                item.Construct(localeSystem);
                item.Awake();
            });
        }
    }
}
