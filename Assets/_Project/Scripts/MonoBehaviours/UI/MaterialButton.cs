using System;
using DG.Tweening;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace _Project.Scripts.MonoBehaviours.UI
{
  public class MaterialButton : MonoBehaviour
  {
    public const float OffsetStep = 76;
    private const float DefaultScale = 1;
    private const float Duration = 0.35f;
    private const float Delay = 0.1f;

    [SerializeField] Image _backgroundImg;
    [SerializeField] Image _iconImg;

    [SerializeField] private float _activeBackgroundSize = 0.5f;
    [SerializeField] private float _inactiveBackgroundSize = 1;

    [SerializeField] private RectTransform _iconRect;
    [SerializeField] private float _activeButtonSize = 0.5f;
    [SerializeField] private float _inactiveButtonSize = 1;

    [SerializeField] private Color _inactiveBackgroundColor;
    [SerializeField] private Color _activeBackgroundColor;

    public CanvasGroup CanvasGroup;

    private RectTransform _rectTransform;

    private string _guid;

    private UnityAction<MaterialButton, bool> _onClick;
    private bool _isDisabled;

    public event Action OnClickEvent;

    public bool IsDisabled =>
      _isDisabled;

    public bool IsActive { get; private set; }

    public bool AnimationIsFinished { get; private set; }

    private void Awake()
    {
      GetComponent<Button>().onClick.AddListener(() => OnClick());

      _rectTransform = GetComponent<RectTransform>();

      AnimationIsFinished = true;
    }

    public void SetOnClickEvent(UnityAction<MaterialButton, bool> onClick)
    {
      _onClick = onClick;
    }

    public void SetDisabled(bool isDisabled) =>
      _isDisabled = isDisabled;

    public void Activate()
    {
      _backgroundImg.DOColor(_activeBackgroundColor, 0.2f);
      _backgroundImg.rectTransform.DOScale(new Vector3(_activeBackgroundSize, _activeBackgroundSize, _activeBackgroundSize), 0.2f);

      _iconRect.DOScale(new Vector3(_activeButtonSize, _activeButtonSize, _activeButtonSize), 0.2f);

      IsActive = true;
    }

    public void Deactivate()
    {
      _backgroundImg.DOColor(_inactiveBackgroundColor, 0.2f);
      _backgroundImg.rectTransform.DOScale(new Vector3(_inactiveBackgroundSize, _inactiveBackgroundSize, _inactiveBackgroundSize), 0.2f);

      _iconRect.DOScale(new Vector3(_inactiveButtonSize, _inactiveButtonSize, _inactiveButtonSize), 0.2f);

      IsActive = false;
    }

    public void OnClick(bool onInitialize = false)
    {
      _onClick?.Invoke(this, onInitialize);
      OnClickEvent?.Invoke();
    }

    public void SetIcon(Sprite icon) =>
      _iconRect.GetComponent<Image>().sprite = icon;

    public void SetGuid(string guid) =>
      _guid = guid;

    public void SetColor(Color color) =>
      _iconImg.color = color;

    public Color GetColor() =>
      _iconImg.color;

    public void SetAnchoredPosition(Vector2 anchoredPosition) =>
      _rectTransform.anchoredPosition = anchoredPosition;

    public string GetGuid() =>
      _guid;

    public void Show(float delayCoeff)
    {
      AnimationIsFinished = false;

      gameObject.SetActive(true);
      _rectTransform.localScale = Vector3.zero;

      _rectTransform.DOScale(new Vector3(DefaultScale, DefaultScale, DefaultScale), Duration).SetEase(Ease.InCubic)
        .SetDelay(Delay * delayCoeff)
        .OnComplete(delegate { AnimationIsFinished = true; });
    }

    public void Hide(float delayCoeff)
    {
      AnimationIsFinished = false;

      gameObject.SetActive(true);
      _rectTransform.localScale = new Vector3(DefaultScale, DefaultScale, DefaultScale);

      _rectTransform.DOScale(Vector3.zero, Duration).SetEase(Ease.OutCubic)
        .SetDelay(Delay * delayCoeff)
        .OnComplete(delegate
        {
          AnimationIsFinished = true;
          gameObject.SetActive(false);
        });
    }
  }
}