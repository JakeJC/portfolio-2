namespace _Project.Scripts.MonoBehaviours.UI.Interfaces
{
  public interface IDragDependent
  {
    void OnExternalDragUp();
    void OnExternalDragDown();
  }
}