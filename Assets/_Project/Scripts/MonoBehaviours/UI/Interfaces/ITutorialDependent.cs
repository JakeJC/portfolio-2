namespace _Project.Scripts.MonoBehaviours.UI.Interfaces
{
  public interface ITutorialDependent
  {
    void OnTutorialStarted();
    void OnTutorialFinished();
  }
}
