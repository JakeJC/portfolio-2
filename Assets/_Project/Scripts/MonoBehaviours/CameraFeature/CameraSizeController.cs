﻿using UnityEngine;

namespace _Project.Scripts.MonoBehaviours.CameraFeature
{
  public class CameraSizeController : MonoBehaviour
  {
    [SerializeField] private Camera _targetCamera;
    private Camera _camera;

    [SerializeField] private AnimationCurve _animationCurve;
    [SerializeField] private Vector2 minMaxOrthoSize;
    
    private void Start() => 
      _camera = GetComponent<Camera>();

    private void Update()
    {
      float normalizedOrtho = (_targetCamera.orthographicSize - minMaxOrthoSize.x)/(minMaxOrthoSize.y - minMaxOrthoSize.x);
      _camera.fieldOfView = _animationCurve.Evaluate(normalizedOrtho);
    }
  }
}