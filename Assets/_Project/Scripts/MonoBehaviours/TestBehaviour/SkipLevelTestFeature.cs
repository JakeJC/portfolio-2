using System;
using UnityEngine;

namespace _Project.Scripts.MonoBehaviours.TestBehaviour
{
  public class SkipLevelTestFeature : MonoBehaviour
  {
    public Action OnClick;

    public void OnGUI()
    {
      if (GUI.Button(new Rect(20, 120, 200, 60), "Complete"))
        OnClick?.Invoke();
    }
  }
}