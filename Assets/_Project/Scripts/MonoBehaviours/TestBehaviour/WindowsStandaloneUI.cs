using UnityEngine;
using UnityEngine.UI;

namespace _Project.Scripts.MonoBehaviours.TestBehaviour
{
  [RequireComponent(typeof(CanvasScaler))]
  public class WindowsStandaloneUI : MonoBehaviour
  {
    void Start()
    {
#if UNITY_STANDALONE_WIN
       GetComponent<CanvasScaler>().matchWidthOrHeight = 1;
#endif
    }
  }
}