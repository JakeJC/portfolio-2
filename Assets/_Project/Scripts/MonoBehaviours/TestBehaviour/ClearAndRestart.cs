﻿#if TEST
using _Project.Scripts.Logic;
using _Project.Scripts.MonoBehaviours.Game;
using UnityEngine.SceneManagement;
#endif
using UnityEngine;

namespace _Project.Scripts.MonoBehaviours.TestBehaviour
{
  public class ClearAndRestart : MonoBehaviour
  {
#if TEST
    public void OnGUI()
    {
      if (GUI.Button(new Rect(Screen.width - 20 - 200, 120, 200, 60), "Reset Progress & Restart"))
      {
        Clear();
        Restart();
      }

      if (GUI.Button(new Rect(Screen.width - 20 - 200, 120 + 20 + 60, 200, 60), "Restart App"))
        Restart();
    }
    private void Clear()
    {
      PlayerPrefs.DeleteAll();
    }
    
    private void Restart()
    {
      FindObjectOfType<GameBootstrapper>().GetAudioService().Clean();
      SceneManager.LoadScene("Main");
    }
#endif
  }
}