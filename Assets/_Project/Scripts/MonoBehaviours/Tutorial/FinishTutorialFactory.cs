using _Project.Scripts.MonoBehaviours.UI.Windows.Mono;
using _Project.Scripts.Services._Interfaces;
using UnityEngine;

namespace _Project.Scripts.MonoBehaviours.Tutorial
{
    public class FinishTutorialFactory
    {
        private const string FinishTutorialWindowPath = "UI/Tutorial/Finish Tutor Window";
        private const string CanvasName = "Canvas";

        private readonly IAssetProvider _assetProvider;
        
        public FinishTutorialWindow FinishTutorialWindow { get; private set; }

        public FinishTutorialFactory(IAssetProvider assetProvider)
        {
            _assetProvider = assetProvider;
        }

        public void CreateFinishTutorialWindow()
        {
            if(FinishTutorialWindow)
                return;

            var prefab = _assetProvider.GetResource<GameObject>(FinishTutorialWindowPath);
            var parent = GameObject.Find(CanvasName).transform;

            FinishTutorialWindow = Object.Instantiate(prefab, parent).GetComponent<FinishTutorialWindow>();
        }

        public void Clear()
        {
            if(FinishTutorialWindow == null)
                return;
            
            Object.Destroy(FinishTutorialWindow.gameObject);
        }
    }
}
