using _Project.Scripts.MonoBehaviours.UI.Interfaces;

namespace _Project.Scripts.MonoBehaviours.Tutorial
{
  public abstract class TutorialStrategyBase : ITutorialDependent
  {
    public abstract void OnTutorialStarted();
    public abstract void OnTutorialFinished();
  }
}