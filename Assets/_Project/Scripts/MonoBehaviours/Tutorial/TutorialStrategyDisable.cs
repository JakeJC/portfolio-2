using _Project.Scripts.MonoBehaviours.UI;

namespace _Project.Scripts.MonoBehaviours.Tutorial
{
  public class TutorialStrategyDisable : TutorialStrategyBase
  {
    private readonly Part _part;

    public TutorialStrategyDisable(Part part) => _part = part;

    public override void OnTutorialStarted() =>
      _part.BoxCollider.enabled = false;

    public override void OnTutorialFinished() =>
      _part.BoxCollider.enabled = true;
  }
}