﻿using System;
using UnityEngine;

namespace _Project.Scripts.MonoBehaviours.Tutorial
{
  public class TutorialClickListener : MonoBehaviour
  {
    private bool _press;
    public event Action OnClick;

    private void Update()
    {
      if (Input.GetMouseButtonDown(0))
        _press = true;

      if (Input.GetMouseButtonUp(0))
      {
        if(_press)
          OnClick?.Invoke();
        _press = false;
      }
    }
  }
}