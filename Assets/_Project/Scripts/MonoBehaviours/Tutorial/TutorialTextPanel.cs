using TMPro;
using UnityEngine;

namespace _Project.Scripts.MonoBehaviours.Tutorial
{
  public class TutorialTextPanel : MonoBehaviour
  {
    public TextMeshProUGUI Text;
    public GameObject ZoomPointer;
    public CanvasGroup CanvasGroup;
  }
}