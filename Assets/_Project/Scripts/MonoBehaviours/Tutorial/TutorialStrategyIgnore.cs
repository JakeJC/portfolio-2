using _Project.Scripts.MonoBehaviours.UI;

namespace _Project.Scripts.MonoBehaviours.Tutorial
{
  public class TutorialStrategyIgnore : TutorialStrategyBase
  {  
    private readonly Part _part;

    public TutorialStrategyIgnore(Part part) => _part = part;
    
    public override void OnTutorialStarted() =>
      _part.BoxCollider.enabled = true;

    public override void OnTutorialFinished() =>
      _part.BoxCollider.enabled = false;
  }
}