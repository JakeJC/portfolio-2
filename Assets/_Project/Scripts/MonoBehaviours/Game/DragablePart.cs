using System;
using _Project.Scripts.Animations.InGame;
using _Project.Scripts.Animations.UI;
using _Project.Scripts.Data.Serializable;
using _Project.Scripts.Logic.GameLogic.Interfaces;
using _Project.Scripts.MonoBehaviours.UI;
using _Project.Scripts.MonoBehaviours.Visual;
using _Project.Scripts.Services.MaterialService.Interfaces;
using _Project.Scripts.Tools;
using UnityEngine;

namespace _Project.Scripts.MonoBehaviours.Game
{
  public class DragablePart : MonoBehaviour
  {
    private const float MinimalConnectDistance = 0.5f;

    private Transform _baseParent;
    private PlaceData _originalPlaceData;
    private IDragDrop _dragDrop;
    private IMaterialService _materialService;
    private MagnetPartAnimation _magnetPartAnimation;

    private GamePart GamePart { get; set; }
    private Part UIPart { get; set; }

    public bool IsPinned { get; private set; }

    public event Action<GamePart> OnPin;
    public event Action<GamePart> OnDragStart;
    public event Action<GamePart> OnReturnToBaseState;

    public Transform PinPoint;
    
    public void Construct(IDragDrop dragDrop, IMaterialService materialService, Part uiPart, GamePart gamePart, Action<GamePart> onDragStart, Action<GamePart> onPin, Action<GamePart> onReturnToBaseState)
    {
      _materialService = materialService;
      _dragDrop = dragDrop;
      
      GamePart = gamePart;
      UIPart = uiPart;

      AddActionToUI();
      
      _originalPlaceData = GetPlaceData();

      OnPin = onPin;
      OnDragStart = onDragStart;
      OnReturnToBaseState = onReturnToBaseState;

      if(GamePart.PinPoint != null)
        PinPoint = Instantiate(GamePart.PinPoint, transform);
    }

    public void ToGameWorld()
    {
      OnDragStart?.Invoke(GamePart);
      
      _baseParent = transform.parent;

      transform.SetParent(GamePart.transform.parent);

      SetPlaceData(GamePart.GetPlaceData());

      GamePart.AppearWithOutline();
    }

    public void ReturnToBaseState()
    {  
      UIPart.IsInDrag = false;
      IsPinned = false;
      transform.SetParent(_baseParent);
      SetPlaceData(_originalPlaceData);
      
      GamePart.FadeAfterInitialize();
      
      OnReturnToBaseState?.Invoke(GamePart);
    }

    public void PinPart(bool withoutAnimation = false)
    {
      if (withoutAnimation)
        PinWithoutAnimation();
      else
        PinWithAnimation();
    }

    public virtual bool CanPin()
    {
      if (GamePart.PinPoint != null)
        return Vector3.Distance(GetProjectionOnScreenPoint(GamePart.PinPoint.position), GetProjectionOnScreenPoint(PinPoint.position)) <= MinimalConnectDistance;

      return Vector3.Distance(GamePart.PointForCompare, GetProjectionOnScreenPoint(GetComponentInChildren<Renderer>(true).bounds.center)) <= MinimalConnectDistance;
    }

    private void PinWithAnimation()
    {
      if (_magnetPartAnimation != null) 
        return;

      IsPinned = true;

      _magnetPartAnimation = new MagnetPartAnimation(transform, GamePart.transform.position);
      _magnetPartAnimation.Magnet(OnEndPin);
    }

    private void PinWithoutAnimation()
    {
      IsPinned = true;
      OnEndPin(false);
    }

    private Vector3 GetProjectionOnScreenPoint(Vector3 position)
    {
      Plane plane = new Plane(Camera.main.transform.forward, Camera.main.transform.position);
      return plane.ClosestPointOnPlane(position);
    }

    private void OnEndPin(bool shouldExecuteCallback = true)
    {
      var renderer = GetComponentInChildren<Renderer>(true);
      GamePart.GetComponent<PartMaterial>().SetMaterial(renderer.sharedMaterial);
      GamePart.IsPinned = true;

      OnOffStateAnimation offStateAnimation = new OnOffStateAnimation(UIPart.transform);
      offStateAnimation.Disable(() => UIPart.gameObject.SetActive(false));

      _magnetPartAnimation = null;
      
      if(shouldExecuteCallback)
        OnPin?.Invoke(GamePart);

      _materialService.RemoveFromBothModules(this);
      
      Destroy(gameObject);
    }

    private void SetPlaceData(PlaceData placeData) => 
      transform.SetPlaceData(placeData);

    private PlaceData GetPlaceData()
    {
      Transform partTransform = transform;
      _originalPlaceData = partTransform.GetPlaceData();

      return _originalPlaceData;
    }

    private void AddActionToUI() => 
      UIPart.OnPress = Bind;

    private void Bind() => 
      _dragDrop.BindPartToCursor(this);
  }
}