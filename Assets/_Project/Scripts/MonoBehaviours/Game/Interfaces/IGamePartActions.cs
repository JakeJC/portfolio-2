﻿using System;
using UnityEngine;

namespace _Project.Scripts.MonoBehaviours.Game.Interfaces
{
  public interface IGamePartActions
  {
    event Action<Material> OnMaterialSet;
    event Action<bool> OnRendererEnabled;
  }
}