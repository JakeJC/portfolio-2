using System.Collections.Generic;
using _Project.FMOD_AudioService.Logic.Enums;
using UnityEngine;

namespace _Project.Scripts.MonoBehaviours.Game.Interfaces
{
  public interface IMechanism
  {
    string GetId();
    void FinalLaunch();
    GamePart[] GetMechanismParts();
    PartMaterial[] GetMaterialParts();
    void ToStartState();
    void DisableAnimatorAtGameStart();
    GameObject GetGO();
    FMOD_EventType GetSoundEvent();
    Material GetMaterial(string materialId);
    Vector3 GetBaseAnchorPosition { get; }
    int GetMaterialsIndex(Material material);
    bool GetIsLight();
  }
}