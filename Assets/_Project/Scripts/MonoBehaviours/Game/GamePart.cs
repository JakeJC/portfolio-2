using System;
using System.Collections.Generic;
using _Project.Scripts.Data.Serializable;
using _Project.Scripts.MonoBehaviours.Game.Interfaces;
using _Project.Scripts.Services.InputResolver;
using _Project.Scripts.Tools;
using UnityEngine;

namespace _Project.Scripts.MonoBehaviours.Game
{
  public class GamePart : MonoBehaviour, INonUIInput, IGamePartActions
  {
    private const string LayerName = "Fade";
    private const string DefaultLayer = "Default";
    
    private Renderer _renderer;

    private Transform _cameraTransform;
    private PlaceData _placeData;

    public GameObject IconPrefab;

    public Transform PinPoint;
    
    public List<Renderer> AdditionalRenderers;

    public int SortingPriorityInPanel;
    
    public  DragablePart DragablePart { get; set; }
    
    public bool IsPinned
    {
      get => _isPinned;
      set => _isPinned = value;
    }

    public IInputResolver InputResolver { get; set; }
    
    public Renderer Renderer =>
      _renderer;
    
    private bool _isPinned;
    
    public Vector3 PointForCompare
      => GetProjectionPosition();

    public event Action<Material> OnMaterialSet;
    public event Action<bool> OnRendererEnabled;

    public void Construct(Transform cameraTransform, 
      IInputResolver inputResolver)
    {
      InputResolver = inputResolver;
      
      _cameraTransform = cameraTransform;

      _renderer = GetComponentInChildren<Renderer>();

      WriteStartPlaceData();
      FadeAfterInitialize();
    }

    public GameObject GetIconPrefab() => 
      IconPrefab;
    
    public void Fade()
    {
      _renderer.gameObject.layer = LayerMask.NameToLayer(LayerName);
      AdditionalRenderers.ForEach(p => p.gameObject.layer = LayerMask.NameToLayer(LayerName));

      OnMaterialSet?.Invoke(_renderer.sharedMaterial);
    }
    
    public PlaceData GetPlaceData() => 
      _placeData;

    public void AppearAfterInitialize()
    {
      if (_renderer == null)
        _renderer = GetComponentInChildren<Renderer>();
      
      _renderer.enabled = true;
      AdditionalRenderers.ForEach(p => p.enabled = true);

      OnRendererEnabled?.Invoke(_renderer.enabled);
    }

    public void AppearWithOutline()
    {
      AppearAfterInitialize();
      
      _renderer.gameObject.layer = LayerMask.NameToLayer(LayerName);
      AdditionalRenderers.ForEach(p => p.gameObject.layer = LayerMask.NameToLayer(LayerName));
    }

    public void FadeAfterInitialize()
    {
      if (_renderer == null)
        _renderer = GetComponentInChildren<Renderer>();
      
      _renderer.enabled = false;
      AdditionalRenderers.ForEach(p => p.enabled = false);
      
      OnRendererEnabled?.Invoke(_renderer.enabled);
    }

    public void ReturnBaseLayer()
    {
      _renderer.gameObject.layer = LayerMask.NameToLayer(DefaultLayer);
      AdditionalRenderers.ForEach(p => p.gameObject.layer = LayerMask.NameToLayer(DefaultLayer));
    }

    private void WriteStartPlaceData() => 
      _placeData = transform.GetPlaceData();

    private Vector3 GetProjectionPosition()
    {
      Plane plane = new Plane(_cameraTransform.forward, _cameraTransform.position);
      return plane.ClosestPointOnPlane(_renderer.bounds.center);
    }
  }
}