﻿using UnityEngine;
using UnityEngine.Events;

namespace _Project.Scripts.MonoBehaviours.Game
{
  public class OnPinTrigger : MonoBehaviour
  {
    public UnityEvent OnPin;
  }
}