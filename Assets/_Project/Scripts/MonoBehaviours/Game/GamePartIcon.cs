﻿using System;
using _Project.Scripts.MonoBehaviours.Game.Interfaces;
using _Project.Scripts.Services.MaterialService.Data;
using _Project.Scripts.Services.MaterialService.Interfaces;
using UnityEngine;

namespace _Project.Scripts.MonoBehaviours.Game
{
  public class GamePartIcon : MonoBehaviour, IMaterialProvider, IColorProvider
  {
    private static readonly int SpecialColor = Shader.PropertyToID("_SpecialColor");

    [NonSerialized] public RectTransform Rect;
    public bool setProgrammaticaly;
    public float WeightedScaleInsideBox;

    private Renderer[] _renderers;

    private IMechanism _mechanism;

    public void Construct(IMechanism mechanism)
    {
      _mechanism = mechanism;
      SetVertexColorMask();
    }

    public Material GetMaterial(string schemeGuid) =>
      _mechanism.GetMaterial(schemeGuid);

    public void SetMaterial(Material material)
    {
      _renderers ??= GetComponentsInChildren<Renderer>();

      foreach (Renderer renderer1 in _renderers)
      {
        if (renderer1.sharedMaterial != null)
          renderer1.sharedMaterial = material;
      }
    }

    public Renderer[] GetRenders()
    {
      _renderers ??= GetComponentsInChildren<Renderer>();
      return _renderers;
    }

    public void SetColor(ColorData colorData, bool ignoreColorFlag = false)
    {
      _renderers ??= GetComponentsInChildren<Renderer>();

      foreach (Renderer renderer1 in _renderers)
      {
        if (renderer1.sharedMaterial != null)
          renderer1.sharedMaterial.SetColor(SpecialColor, colorData.MechanismColor);
      }
    }

    public bool IsMechanism()
      => false;

    private void SetVertexColorMask()
    {
      MeshFilter[] meshFilters = GetComponentsInChildren<MeshFilter>();

      foreach (MeshFilter meshFilter in meshFilters)
      {
        Color[] colors = new Color[meshFilter.mesh.vertices.Length];
        for (int i = 0; i < colors.Length; i++)
          colors[i] = Color.black;

        meshFilter.mesh.SetColors(colors);
      }
    }
  }
}