using System;
using System.Collections.Generic;
using System.Linq;
using _Project.FMOD_AudioService.Logic.Enums;
using _Project.Scripts.MonoBehaviours.Game.Interfaces;
using _Project.Scripts.Services.MaterialService.Data;
using _Project.Scripts.Services.MaterialService.Interfaces;
using Sirenix.OdinInspector;
using UnityEditor;
using UnityEngine;
using UnityEngine.Events;

namespace _Project.Scripts.MonoBehaviours.Game
{
  public class Mechanism : MonoBehaviour, IMechanism, IMaterialProvider, IMainMaterialProvider
  {
    public Transform BaseLevelAnchor;
    
    private const string Final = "Final";
    private static readonly int StateName = Animator.StringToHash(Final);

    private const string StartValue = "Start";
    private static readonly int BaseStateName = Animator.StringToHash(StartValue);

    private Animator[] _animators;

    public string Id;
    public FMOD_EventType Event;
    
    public List<MaterialInfo> Materials;

    [SerializeField] private UnityEvent OnLaunch; //Use with caution
    
    private PartMaterial[] _materialParts;

    public bool IsLight;
    
    public void Construct()
    {
      _animators = GetComponentsInChildren<Animator>(true);
      _materialParts ??= GetComponentsInChildren<PartMaterial>();
      
      foreach (PartMaterial materialPart in _materialParts) 
        materialPart.Construct();
    }

    public bool GetIsLight() => IsLight;
    
    public string GetId() => 
      Id;

    public FMOD_EventType GetSoundEvent() => 
      Event;

    public Material GetMaterial(string materialId) => 
      Materials.FirstOrDefault(p => p.Scheme.GetGuid() == materialId).Material;

    public void SetMaterial(Material material)
    {
      _materialParts ??= GetComponentsInChildren<PartMaterial>();

      foreach (PartMaterial materialPart in _materialParts) 
        materialPart.SetMaterial(material);
    }

    public Renderer[] GetRenders()
    {     
      _materialParts ??= GetComponentsInChildren<PartMaterial>();

      List<Renderer> renderers = new List<Renderer>();
      
      foreach (PartMaterial materialPart in _materialParts) 
        renderers.AddRange(materialPart.GetRenders());
      
      return renderers.ToArray();
    }

    public bool IsMechanism()
      => true;

    public Vector3 GetBaseAnchorPosition => 
      BaseLevelAnchor.position;
    
    public void DisableAnimatorAtGameStart()
    {
      foreach (Animator animator in _animators) 
        animator.enabled = false;
    }

    public GameObject GetGO() => 
      gameObject;

    public void ToStartState()
    {
      foreach (Animator animator in _animators)
      {
        animator.Play(BaseStateName);
        animator.enabled = false;
      }
    }

    public void FinalLaunch()
    {
      foreach (Animator animator in _animators) 
        animator.enabled = true;

      foreach (Animator animator in _animators)
        animator.Play(StateName);
      
      OnLaunch?.Invoke();
    }

    public void OnLaunchAddListener(UnityAction action) => 
      OnLaunch.AddListener(action);
    
    public void OnLaunchRemoveListener(UnityAction action) => 
      OnLaunch.AddListener(action);


    public GamePart[] GetMechanismParts() =>
      GetComponentsInChildren<GamePart>(true);

    public PartMaterial[] GetMaterialParts() => 
      GetComponentsInChildren<PartMaterial>(true);

    public int GetMaterialsIndex(Material material) => 
      Materials.IndexOf(Materials.FirstOrDefault(p => p.Material == material));

#if UNITY_EDITOR
    [Button]
    public void GenerateGuid() => 
      Id = GUID.Generate().ToString();
#endif
  }
  
  [Serializable]
  public struct MaterialInfo
  {
    public MaterialSchemeData Scheme;
    public Material Material;
  }
}