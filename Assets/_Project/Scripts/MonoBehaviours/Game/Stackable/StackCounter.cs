using TMPro;
using UnityEngine;

namespace _Project.Scripts.MonoBehaviours.Game.Stackable
{
  public class StackCounter : MonoBehaviour
  {
    [SerializeField] private TextMeshProUGUI count;

    public void SetCounter(int counter) => 
      count.text = counter.ToString();
  }
}