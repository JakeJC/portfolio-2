using UnityEngine;

namespace _Project.Scripts.MonoBehaviours.Game.Stackable
{
  [RequireComponent(typeof(GamePart))]
  public class StackableGamePart : MonoBehaviour
  {
    public int StackId;
  }
}