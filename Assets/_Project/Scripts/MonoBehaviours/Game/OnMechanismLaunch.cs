using JetBrains.Annotations;
using UnityEngine;

namespace _Project.Scripts.MonoBehaviours.Game
{
  public class OnMechanismLaunch : MonoBehaviour
  {
    public GameObject[] PartsForAnimation;
    public GameObject[] PartsForHide;

    [UsedImplicitly]
    public void ActivateAnimationParts()
    {
      foreach (GameObject obj in PartsForAnimation) 
        obj.SetActive(true);
      
      foreach (GameObject obj in PartsForHide) 
        obj.SetActive(false);
    }
  }
}