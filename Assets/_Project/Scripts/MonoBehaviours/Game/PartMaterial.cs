using System.Collections.Generic;
using _Project.Scripts.Services.MaterialService.Data;
using _Project.Scripts.Services.MaterialService.Interfaces;
using Sirenix.OdinInspector;
using UnityEditor;
using UnityEngine;

namespace _Project.Scripts.MonoBehaviours.Game
{
  public class PartMaterial : MonoBehaviour, IColorProvider
  {
    private static readonly int SpecialColor = Shader.PropertyToID("_SpecialColor");
    
    public string UniqueId;

    public Material UnchangableMaterial;

#if UNITY_EDITOR
    [Button]
    public void GenerateGuid() => 
      UniqueId = GUID.Generate().ToString();
#endif

    public void Construct()
    {}
    
    public void SetMaterial(Material material)
    {
      if (GetComponent<Renderer>().sharedMaterial.Equals(UnchangableMaterial))
        return;
      
      GetComponent<Renderer>().sharedMaterial = material;
      GetComponent<GamePart>().AdditionalRenderers.ForEach(p => p.sharedMaterial = material);
    }
    
    public Renderer[] GetRenders()
    {
      List<Renderer> list = GetComponent<GamePart>().AdditionalRenderers;
      list.Add(GetComponent<Renderer>());

      return list.ToArray();
    }

    public void SetColor(ColorData colorData, bool ignoreColorFlag = false)
    {
      var rend = GetComponent<Renderer>();
      if(rend)
        if(rend.sharedMaterial != null)
          rend.sharedMaterial.SetColor(SpecialColor, colorData.MechanismColor);

      var gamePart = GetComponent<GamePart>();
      if(gamePart)
        gamePart.AdditionalRenderers.ForEach(p =>
        {
          if(p.sharedMaterial != null)
            p.sharedMaterial.SetColor(SpecialColor, colorData.MechanismColor);
        });
    }
  }
}