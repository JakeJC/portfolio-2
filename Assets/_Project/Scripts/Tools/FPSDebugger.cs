using UnityEngine;

namespace _Project.Scripts.Tools
{
  public class FPSDebugger : MonoBehaviour
  {
#if TEST
    private int FontSize = 75;
    
    private void Start() => 
      Application.targetFrameRate = 120;

    private void OnGUI()
    {
      GUIStyle style = new GUIStyle();
      style.fontSize = Mathf.Abs(FontSize);
      style.normal.textColor = Color.green;
      GUI.TextField(new Rect(Screen.width / 2 - 100, 0, 200, 50), $"FPS: {(int) (1.0f / Time.deltaTime)}", style);
    }
#endif
  }
}