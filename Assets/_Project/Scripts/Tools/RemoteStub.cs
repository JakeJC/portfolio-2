using System;
using System.Threading.Tasks;
using AppacheRemote.Scripts.Data;
using AppacheRemote.Scripts.Interfaces;

namespace _Project.Scripts.Tools
{
  public class RemoteStub : IRemote
  {
    public bool ShouldEnableFeatureTest2()
      => false;

    public event Action OnFetch;

    public Task Initialize()
    {
      OnFetch?.Invoke();
      return Task.CompletedTask;
    }

    public PlacementType GetPlacementType() => 
      PlacementType.test;

    public string GetPrivacyText() => 
      "Privacy";

    public bool ShouldEnableFeatureTest()
      => true;
  }
}