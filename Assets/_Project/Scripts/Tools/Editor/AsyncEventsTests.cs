using System;
using System.Threading.Tasks;
using NUnit.Framework;

namespace _Project.Scripts.Tools.Editor
{
  public class AsyncEventsTests
  {
    private class Dice
    {
      public event Action<int> Rolled;
      public void OnRolled(int arg) => Rolled?.Invoke(arg);
    }

    private class Button
    {
      public event Action Clicked;
      public void OnClicked() => Clicked?.Invoke();
    }

    private static void RunAsyncTest(Func<Task> asyncTest)
    {
      // https://forum.unity.com/threads/async-await-in-unittests.513857/
      // NUnit included in Unity does not support async tests.
      // This is a workaround.
      Task.Run(asyncTest).GetAwaiter().GetResult();
    }

    [Test]
    public void TaskCompletesOnlyAfterEvent()
    {
      RunAsyncTest(async () =>
      {
        var button = new Button();

        var taskClicked = AsyncEvents.TaskFromEvent(
          h => button.Clicked += h,
          h => button.Clicked -= h);

        await Task.Delay(TimeSpan.FromSeconds(0.2f));

        Assert.IsFalse(taskClicked.IsCompleted,
          "Task should not complete before the event");

        // Raise event after delay. Do not wait.
        Task.Run(async () =>
        {
          await Task.Delay(TimeSpan.FromSeconds(0.2f));
          button.OnClicked();
        });

        await taskClicked;

        Assert.IsTrue(taskClicked.IsCompleted,
          "Task should be completed after the event");

        taskClicked.Dispose();
      });
    }

    [Test]
    public void TaskCompletesOnlyAfterEventGeneric()
    {
      RunAsyncTest(async () =>
      {
        var dice = new Dice();

        var taskRolled = AsyncEvents.TaskFromEvent<int>(
          h => dice.Rolled += h,
          h => dice.Rolled -= h);

        await Task.Delay(TimeSpan.FromSeconds(0.2f));

        Assert.IsFalse(taskRolled.IsCompleted,
          "Task should not complete before the event");

        // Raise event after delay. Do not wait.
        Task.Run(async () =>
        {
          await Task.Delay(TimeSpan.FromSeconds(0.2f));
          dice.OnRolled(42);
        });

        var result = await taskRolled;

        Assert.IsTrue(taskRolled.IsCompleted,
          "Task should be completed after the event");

        // Check event argument.
        Assert.AreEqual(42, result);

        taskRolled.Dispose();
      });
    }
  }
}
