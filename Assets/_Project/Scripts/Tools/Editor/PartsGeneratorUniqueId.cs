using System.Collections.Generic;
using System.Linq;
using _Project.Scripts.MonoBehaviours.Game;
using _Project.Scripts.MonoBehaviours.Visual;
using Sirenix.OdinInspector;
using UnityEditor;
using UnityEngine;

namespace _Project.Scripts.Tools.Editor
{
  [CreateAssetMenu(fileName = "Unique Ids Generator", menuName = "Generators/Create Unique Ids Generator", order = 0)]
  public class PartsGeneratorUniqueId : ScriptableObject
  {
    [Button]
    public void GenerateGUIDsForAllGameParts()
    {
      List<GameObject> objects = GetListOfGameObjectsWithComponent<GamePart>().ToList();

      foreach (var obj in objects)
      {
        AddPartMaterial(obj);
        FillGUID(obj);
      }
      
      AssetDatabase.SaveAssets();
    }

    private void AddPartMaterial(GameObject gameObject)
    {
      var partsMaterial = gameObject.GetComponentsInChildren<GamePart>();
      
      foreach (GamePart partMaterial in partsMaterial)
      {
        if (partMaterial.GetComponent<PartMaterial>() == null)
        {
          partMaterial.gameObject.AddComponent<PartMaterial>();
          PrefabUtility.SavePrefabAsset(gameObject);
        }
      }
    }

    private void FillGUID(GameObject gameObject)
    {
      var partsMaterial = gameObject.GetComponentsInChildren<PartMaterial>();
      
      foreach (PartMaterial partMaterial in partsMaterial)
      {
        if (partMaterial.UniqueId == "")
        {
          partMaterial.UniqueId = GUID.Generate().ToString();
          PrefabUtility.SavePrefabAsset(gameObject);
        }
      }
    }

    private static List<GameObject> GetListOfGameObjectsWithComponent<T>() where T : Component
    {
      List<string> paths = AssetDatabase
        .FindAssets("t:Prefab")
        .Select(AssetDatabase.GUIDToAssetPath).ToList();

      List<GameObject> components = paths
        .Select(AssetDatabase.LoadAssetAtPath<GameObject>)
        .Where(p => p.GetComponentInChildren<T>())
        .ToList();

      return components;
    }
  }
}