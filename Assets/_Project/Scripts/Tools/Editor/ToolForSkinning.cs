﻿using Sirenix.OdinInspector;
using UnityEngine;

namespace _Project.Scripts.Tools.Editor
{
  [CreateAssetMenu(fileName = "Skin Tool", menuName = "Create/Skin Tool", order = 0)]
  public class ToolForSkinning : ScriptableObject
  {
    private const string DataAssetPath = "Services/Radial Window/Radial Window Data Container";

    [Button]
    public void SetSkinPack(int index)
    {
      // List<GamePart> gameParts = new List<GamePart>();
      // foreach (WorldMechanism worldMechanism in FindObjectsOfType<WorldMechanism>()) 
      //   gameParts.AddRange(worldMechanism.GetComponentsInChildren<GamePart>(true));
      //
      // var container = Resources.Load<RadialWindowDataContainer>(DataAssetPath);
      //
      // foreach (GamePart gamePart in gameParts)
      // {
      //   string uniqueId = gamePart.GetComponent<PartMaterial>().UniqueId;
      //   RadialWindowData[] data = container.GetData(uniqueId);
      //   
      //   int clampedIndex = Mathf.Clamp(index, 0, data.Length - 1);
      //   
      //   try
      //   {
      //     gamePart.SetMaterial(data[clampedIndex].WindowData.Material);
      //   }
      //   catch (NullReferenceException e)
      //   {
      //     Debug.LogError($"{gamePart.name} has problems with materials setup");
      //   }
      // }
    }
  }
}