using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using _Project.Scripts.Data.Scriptables;
using _Project.Scripts.MonoBehaviours.Game;
using _Project.Scripts.Services._Interfaces;
using _Project.Scripts.Services.AssetProvider;
using Lean.Touch;
using Sirenix.OdinInspector;
using UnityEditor;
using UnityEditor.AddressableAssets;
using UnityEditor.AddressableAssets.Settings;
using UnityEditor.SceneManagement;
using UnityEngine;

namespace _Project.Scripts.Tools.Editor
{
  [CreateAssetMenu(fileName = "MechanismsRenderer", menuName = "MechanismsRenderer", order = 0)]
  public class MechanismsRenderer : ScriptableObject
  {
    private const string CameraPivot = "Camera Pivot";
    private const string GameEnvironment = "Game Environment";

    [Sirenix.OdinInspector.FilePath(Extensions = "unity")] [SerializeField]
    private string scenePath = "Assets/_Project/Scenes/Main.unity";

    [FolderPath] [SerializeField] private string assetsPath = "Assets/_Project/Resources/Gallery Covers/Generated";

    public string mechanismId;

    [SerializeField] private TextureImporterType textureType = TextureImporterType.Sprite;
    [SerializeField] private float yPosition = -2.88f;
      
    [Button]
    [InfoBox("Will open and then close a scene.", InfoMessageType.Warning)]
    public void StartRender()
    {
      // Remember loaded scenes.
      var loadedSceneCount = EditorSceneManager.loadedSceneCount;
      var loadedScenePaths = new string[loadedSceneCount];
      for (var i = 0; i < loadedSceneCount; i++)
      {
        loadedScenePaths[i] = EditorSceneManager.GetSceneAt(i).path;
      }

      EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo();
      EditorSceneManager.OpenScene(scenePath, OpenSceneMode.Single);

      RenderCovers();

      // Reopen previously loaded scenes.
      EditorSceneManager.OpenScene(loadedScenePaths[0], OpenSceneMode.Single);
      for (var i = 0; i < loadedSceneCount; i++)
      {
        EditorSceneManager.OpenScene(loadedScenePaths[i], OpenSceneMode.Additive);
      }
    }

    private void RenderCovers()
    {
      IAssetProvider assetProvider = new AssetProvider();

      string SelectorPrefab(MechanismData m) => m.AssetAddress;

      var mechanisms = assetProvider.GetAllResources<MechanismData>("_Mechanism Configs").Where(p =>
        p.MechanismId == mechanismId);

      var renderSize = new Vector2Int(460, 740);

      var environment = GameObject.Find("Environment");
      if (environment != null) environment.SetActive(false);

      // Render covers for mechanisms.
      foreach (var mechanismData in mechanisms)
      {
        var mechanismPrefabs = new[] {SelectorPrefab(mechanismData)};

        var pathMechanism = $"{assetsPath}/M{mechanismData.MechanismId}.png";
        RenderToFile(mechanismPrefabs, renderSize, pathMechanism);
        AdjustImportSettings(pathMechanism);

        MarkAsAddressable(pathMechanism, mechanismData.MechanismId);
      }

      if (environment != null)
        environment.SetActive(true);
    }

    private void MarkAsAddressable(string assetPath, string id)
    {
      var settings = AddressableAssetSettingsDefaultObject.Settings;
      var group = settings.DefaultGroup;

      GUID guid = AssetDatabase.GUIDFromAssetPath(assetPath);

      var entry = settings.CreateOrMoveEntry(guid.ToString(), group, readOnly: false, postEvent: false);
      entry.address = $"M{id}";
      entry.labels.Add("Cover");

      settings.SetDirty(AddressableAssetSettings.ModificationEvent.EntryMoved, entry, true);
    }

    private void RenderToFile(IEnumerable<string> prefabs, Vector2Int renderSize, string path)
    {
      List<Mechanism> mechanisms = new List<Mechanism>();

      foreach (string assetAddress in prefabs)
      {
        var asset = GetMechanismAsset(assetAddress);
        mechanisms.Add(asset);
      }

      var texture2D = RenderToTexture(mechanisms, renderSize);
      var bytes = texture2D.EncodeToPNG();
      File.WriteAllBytes(path, bytes);

      AssetDatabase.ImportAsset(path);
    }

    private static Mechanism GetMechanismAsset(string assetAddress)
    {
      string[] findAssets = AssetDatabase.FindAssets("t:Prefab");

      foreach (var guid in findAssets)
      {
        string guidToAssetPath = AssetDatabase.GUIDToAssetPath(guid);
        Mechanism mechanism = AssetDatabase.LoadAssetAtPath<Mechanism>(guidToAssetPath);

        if (mechanism == null)
          continue;

        if (mechanism.name == assetAddress)
          return mechanism;
      }

      return null;
    }

    private void AdjustImportSettings(string path)
    {
      if (AssetImporter.GetAtPath(path) is TextureImporter textureImporter)
      {
        textureImporter.textureType = textureType;
        textureImporter.spriteImportMode = SpriteImportMode.Single;
        textureImporter.alphaIsTransparency = true;
        textureImporter.mipmapEnabled = false;
        textureImporter.wrapMode = TextureWrapMode.Clamp;

        textureImporter.SaveAndReimport();
      }
    }

    public Texture2D RenderToTexture(IEnumerable<Mechanism> prefabs, Vector2Int renderSize)
    {
      var mainCamera = Camera.main;

      var cameraPivot = GameObject.Find(CameraPivot).transform;
      var cameraObject = Instantiate(mainCamera.gameObject, cameraPivot);
      var cameraTransform = cameraObject.transform;
      DestroyImmediate(cameraObject.GetComponent<LeanCameraZoomSmooth>());
      cameraObject.name = "Render Camera";
      cameraObject.tag = "Untagged";

      var cameraComponent = cameraObject.GetComponent<Camera>();
      cameraComponent.backgroundColor = Color.clear;
      cameraComponent.clearFlags = CameraClearFlags.Color;
      cameraComponent.enabled = false;

      var positions = new[]
      {
        new Vector3(0.018f, yPosition, 26.1f),
        new Vector3(-0.88f, -0.46f, 29.1f),
        new Vector3(0.92f, 0.39f, 31.4f),
      };
      var rotations = new[]
      {
        new Vector3(17.849f, 137.523f, -15.676f),
        new Vector3(17.849f, 137.523f, -15.676f),
        new Vector3(17.849f, 137.523f, -15.676f),
      };
      var sizes = new[] {1.038291f, 0.4724042f, 0.3085397f};
      var gameEnvironment = GameObject.Find(GameEnvironment).transform;
      var mechanisms = new List<Mechanism>();

      var zip = prefabs
        .Zip(positions, (prefab, position) => (prefab, position))
        .Zip(sizes, (tuple, size) => (tuple.prefab, tuple.position, size))
        .Zip(rotations, (tuple, rotation) => (tuple.prefab, tuple.position, tuple.size, rotation));

      foreach (var (prefab, localPosition, size, localRotation) in zip)
      {
        var mechanismComponent = Component.Instantiate(prefab, gameEnvironment);
        var mechanismTransform = mechanismComponent.transform;

        mechanismTransform.SetParent(gameEnvironment, true);
        mechanismTransform.localScale = Vector3.one * size;

        mechanismTransform.SetParent(cameraTransform, true);
        mechanismTransform.localPosition = localPosition;
        mechanismTransform.localRotation = Quaternion.Euler(localRotation);

        mechanisms.Add(mechanismComponent);
      }

      var renderTexture = RenderToTexture(cameraComponent, renderSize);

      foreach (var mechanism in mechanisms)
      {
        DestroyImmediate(mechanism.gameObject);
      }

      DestroyImmediate(cameraObject);

      return renderTexture;
    }

    private static Texture2D RenderToTexture(Camera cameraComponent, Vector2Int renderSize)
    {
      var renderWidth = renderSize.x;
      var renderHeight = renderSize.y;
      var sourceRect = new Rect(0, 0, renderWidth, renderHeight);

      // Instantiate textures.
      var renderTexture = RenderTexture.GetTemporary(renderWidth, renderHeight, 0);
      var texture2D = new Texture2D(renderWidth, renderHeight, TextureFormat.RGBA32, false);

      // Store previous render settings.
      var activeRenderTexture = RenderTexture.active;
      var cameraTargetTexture = cameraComponent.targetTexture;

      // Render to camera's targetTexture.
      cameraComponent.targetTexture = renderTexture;
      cameraComponent.Render();

      // Read pixels from RenderTexture.active.
      RenderTexture.active = renderTexture;
      texture2D.ReadPixels(sourceRect, 0, 0);
      texture2D.Apply();

      // Restore previous render settings.
      cameraComponent.targetTexture = cameraTargetTexture;
      RenderTexture.active = activeRenderTexture;

      // Release temporary RenderTexture.
      RenderTexture.ReleaseTemporary(renderTexture);

      return texture2D;
    }
  }
}