using System.Collections.Generic;
using System.Linq;
using _Project.Scripts.MonoBehaviours.Game;
using UnityEditor;
using UnityEngine;

namespace _Project.Scripts.Tools.Editor
{
  [CanEditMultipleObjects]
  [CustomEditor(typeof(GamePartIcon))]
  public class GamePartIconEditor : UnityEditor.Editor
  {
    private const string UIPath = "UI/Game/Part";
    
    private RectTransform _uiSlot;
    private Vector3 _minScale;
    private Vector3 _maxScale;

    private Vector3 _cachedEulerRotation;
    
    public override void OnInspectorGUI()
    {
      base.OnInspectorGUI();
      
      GamePartIcon gamePartIcon = (GamePartIcon) target;
      
      /*if (!Application.isPlaying) 
        return;*/

      var previousScale = gamePartIcon.WeightedScaleInsideBox;
      gamePartIcon.WeightedScaleInsideBox = EditorGUILayout.Slider("WeightedScaleInsideBox",gamePartIcon.WeightedScaleInsideBox, 0, 1);
      gamePartIcon.transform.localScale = Vector3.Lerp(_minScale, _maxScale, gamePartIcon.WeightedScaleInsideBox);
      
      if(!Mathf.Approximately(previousScale, gamePartIcon.WeightedScaleInsideBox))
        EditorUtility.SetDirty((GamePartIcon) target);

      if (!Application.isPlaying) 
        return;
      
      if (GUILayout.Button("Rotate As In Game"))
        Rotate(gamePartIcon);
    }

    private void OnEnable()
    {
      GamePartIcon gamePartIcon = AssignUIFrame();
      
      var meshFilter = gamePartIcon.GetComponent<MeshFilter>();
      
      Bounds bounds;
      
      bounds = meshFilter != null ? meshFilter.sharedMesh.bounds : gamePartIcon.GetComponentInChildren<SkinnedMeshRenderer>().bounds;

      Vector2 sizeDelta = _uiSlot.sizeDelta;
      var values = new List<float>()
      {
        sizeDelta.x / bounds.size.x,
        sizeDelta.y / bounds.size.y
      };

      float min = values.Min();

      _minScale = Vector3.zero;
      _maxScale = min * Vector3.one;
      
      if(!gamePartIcon.setProgrammaticaly)
        gamePartIcon.WeightedScaleInsideBox = gamePartIcon.transform.localScale.x / _maxScale.x;
      else
        gamePartIcon.transform.localScale = Vector3.Lerp(_minScale, _maxScale, gamePartIcon.WeightedScaleInsideBox);
    }
    
    // Объект и иконка должны быть одноименнными
    private void Rotate(GamePartIcon gamePartIcon)
    {
      if (!Application.isPlaying)
        return;
      
      GameObject gamePart = GameObject.Find(gamePartIcon.name.Replace("_icon", ""));
      
      Transform cachedParent = gamePart.transform.parent;
      
      gamePart.transform.SetParent(FindObjectOfType<Canvas>().transform);
      gamePartIcon.transform.rotation = gamePart.transform.localRotation;
      gamePart.transform.SetParent(cachedParent);

      EditorUtility.SetDirty(gamePartIcon);
    }

    private void OnDisable()
    {
      if (target != null)
        EditorUtility.SetDirty((GamePartIcon) target);
    }

    private GamePartIcon AssignUIFrame()
    {
      _uiSlot = Resources.Load<GameObject>(UIPath).GetComponent<RectTransform>();
      GamePartIcon gamePartIcon = (GamePartIcon) target;
      gamePartIcon.Rect = _uiSlot;
      return gamePartIcon;
    }

    [DrawGizmo(GizmoType.Active | GizmoType.InSelectionHierarchy | GizmoType.Pickable)]
    public static void RenderCustomGizmo(GamePartIcon gamePart, GizmoType gizmoType)
    {
      if (gamePart.Rect == null)
        return;

      Gizmos.color = Color.red;
      Gizmos.DrawWireCube(gamePart.Rect.transform.position, gamePart.Rect.sizeDelta * Vector3.one);
      Gizmos.DrawWireCube(new Vector2(gamePart.Rect.rect.xMax - 8.2f - 16, gamePart.Rect.rect.yMax - 9.6f - 16), 32 * Vector3.one);
    }
  }
}