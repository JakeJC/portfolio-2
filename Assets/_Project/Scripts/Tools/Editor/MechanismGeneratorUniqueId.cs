using System.Collections.Generic;
using System.Linq;
using _Project.Scripts.MonoBehaviours.Game;
using Sirenix.OdinInspector;
using UnityEditor;
using UnityEngine;

namespace _Project.Scripts.Tools.Editor
{
  [CreateAssetMenu(fileName = "Mechanisms Unique Ids Generator", menuName = "Generators/Create Mechanisms Unique Ids Generator", order = 0)]
  public class MechanismGeneratorUniqueId : ScriptableObject
  {
    [Button]
    public void GenerateGUIDsForAllMechanisms()
    {
      List<GameObject> objects = GetListOfGameObjectsWithComponent<Mechanism>().ToList();

      foreach (var obj in objects) 
        FillGUID(obj);

      AssetDatabase.SaveAssets();
    }

    private void FillGUID(GameObject gameObject)
    {
      var mechanism = gameObject.GetComponent<Mechanism>();
      
      if(mechanism == null || mechanism.Id != "")
        return;

      mechanism.Id = GUID.Generate().ToString();
      PrefabUtility.SavePrefabAsset(gameObject);
    }

    private static List<GameObject> GetListOfGameObjectsWithComponent<T>() where T : Component
    {
      List<string> paths = AssetDatabase
        .FindAssets("t:Prefab")
        .Select(AssetDatabase.GUIDToAssetPath).ToList();

      List<GameObject> components = paths
        .Select(AssetDatabase.LoadAssetAtPath<GameObject>)
        .Where(p => p.GetComponentInChildren<T>())
        .ToList();

      return components;
    }
  }
}