using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace _Project.Scripts.Tools
{
  public static class VectorExtentions
  {
    public static Vector3 SetX(this Vector3 vector3, float x)
    {
      vector3.x = x;
      return vector3;
    }
    
    public static Vector3 SetY(this Vector3 vector3, float y)
    {
      vector3.y = y;
      return vector3;
    }
    
    public static Vector3 SetZ(this Vector3 vector3, float z)
    {
      vector3.z = z;
      return vector3;
    }

    public static float MaxValue(this Vector3 vector3)
    {
      List<float> values = new List<float>()
      {      
        vector3.x,
        vector3.y,
        vector3.z
      };
      
      return values.Max();
    } 
    
    public static float MinValue(this Vector3 vector3)
    {
      List<float> values = new List<float>()
      {      
        vector3.x,
        vector3.y,
        vector3.z
      };
      
      return values.Min();
    }
    
    public static float MaxValue(this Vector2 vector2)
    {
      List<float> values = new List<float>()
      {      
        vector2.x,
        vector2.y
      };
      
      return values.Max();
    } 
    
    public static float MinValue(this Vector2 vector2)
    {
      List<float> values = new List<float>()
      {      
        vector2.x,
        vector2.y
      };
      
      return values.Min();
    }
  }
}