using _Project.Scripts.MonoBehaviours.Visual;
using Lean.Touch;
using LeanTouch.Examples.Scripts;
using UnityEngine;

namespace _Project.Scripts.Tools
{
  public class PreparingForRecording : MonoBehaviour
  {
    public bool ShowGUI;

    private void OnGUI()
    {
      if (ShowGUI)
      {
        if (GUI.Button(new Rect(300, 130, 200, 50), "Preparing For Recording"))
          Prepare();
      }
    }

    private void Prepare()
    {
      if (Application.isPlaying == false)
        return;

      PreparingCanvses();
      PreparingCam();
      PreparingEnvironment();
    }

    private void PreparingCanvses()
    {
      Canvas[] canvases = FindObjectsOfType<Canvas>();

      foreach (Canvas canvas in canvases)
      {
        CanvasGroup canvasGroup = canvas.GetComponent<CanvasGroup>();
        canvasGroup = canvasGroup != null ? canvasGroup : canvas.gameObject.AddComponent<CanvasGroup>();
        canvasGroup.alpha = 0;
      }
    }

    private void PreparingCam()
    {
      Camera cam = Camera.main;
      cam.transform.parent.gameObject.GetComponent<LeanPitchYawSmooth>().enabled = false;
      cam.transform.parent.gameObject.GetComponent<LeanCameraMoveSmooth>().enabled = false;
      cam.gameObject.GetComponent<LeanCameraZoomSmooth>().enabled = false;
      cam.backgroundColor = new Color(0, 0, 0, 0);
    }

    private void PreparingEnvironment() =>
      FindObjectOfType<EnvironmentColorProvider>()?.gameObject.SetActive(false);
  }
}