using System.Text;
using _Project.Scripts.Data.Scriptables;
using _Project.Scripts.MonoBehaviours.Game;
using _Project.Scripts.MonoBehaviours.Game.Interfaces;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;

namespace _Project.Scripts.Tools
{
  public class GamePartsCounter : MonoBehaviour
  {
    private AsyncOperationHandle _asyncOperationHandle;
    private const string MechanismConfigsPath = "_Mechanism Configs";

    [Button]
    public async void CountMessage()
    {
      MechanismData[] mechanismData = Resources.LoadAll<MechanismData>(MechanismConfigsPath);

      StringBuilder stringBuilder = new StringBuilder();
      
      foreach (MechanismData data in mechanismData)
      {
        _asyncOperationHandle = Addressables.LoadAssetAsync<GameObject>(data.AssetAddress);
        await _asyncOperationHandle.Task;
        
        GameObject result = _asyncOperationHandle.Result as GameObject;

        if (result != null)
        {
          IMechanism mechanism = result.GetComponentInChildren<IMechanism>();
          GamePart[] gameParts = mechanism.GetMechanismParts();

          stringBuilder.AppendLine($"{mechanism.GetGO().name}");
        }
        
        

        Addressables.Release(_asyncOperationHandle);
      }

      Debug.Log(stringBuilder.ToString());
    }
  }
}