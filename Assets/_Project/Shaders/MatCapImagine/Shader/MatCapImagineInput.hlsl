//--------------------------------------
// Includes
#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"

CBUFFER_START(UnityPerMaterial)

// -------------------------------------
// Main
float4 _BaseMap_ST;

half4 _BaseColorR;
half4 _BaseColorG;
half4 _BaseColorB;
half4 _SpecialColor;

// -------------------------------------
// Rim
half4 _RimColorR;
half4 _RimColorG;
half4 _RimColorB;

// -------------------------------------
// _Reflection
samplerCUBE _Cube;
half _ReflectionMaskPower;

// -------------------------------------
// Noise
half _ShadingNoise;
CBUFFER_END

// -------------------------------------
// Globals
uniform half _GLOBALSphereRadius;
uniform half3 _GLOBALSpherePosition;
uniform half _GLOBALSphereFade;
uniform half _GLOBALSphereShadowFade;
uniform half4 _GLOBALClipColor1;
uniform half4 _GLOBALClipColor2;
uniform half4 _GLOBALClipColor3;
uniform half4 _GLOBALClipShadowColor;
uniform half _GLOBALExtrudeAnimationSpeed;
uniform half _GLOBALExtrudePower;

// Backlight
uniform float _MinDarkness;
uniform int _BackLightsActiveAmount;
uniform half4 _BackLights[12]; // TODO: check array size
uniform half4 _BackLightsColors[12]; // TODO: check array size
// (BacklightSetter.cs) BacklightAmount = N // TODO check: array size

// -------------------------------------
// Textures
TEXTURE2D(_BaseMap);
SAMPLER(sampler_BaseMap);

TEXTURE2D(_RGBmask);
SAMPLER(sampler_RGBmask);

TEXTURE2D(_ReflectionMask);
SAMPLER(sampler_ReflectionMask);

TEXTURE2D(_SpecialMask);
SAMPLER(sampler_SpecialMask);

TEXTURE2D(_MatCap);
SAMPLER(sampler_MatCap);
