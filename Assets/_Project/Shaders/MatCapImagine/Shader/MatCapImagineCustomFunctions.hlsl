half4 GetMaskedColor(half4 rgbMask, half4 ColorR, half4 ColorG, half4 ColorB)
{
    return (ColorR * rgbMask.r) + (ColorG * rgbMask.g) + (ColorB * rgbMask.b);
}

half4 GetMaskedValue(half4 rgbMask, half ValueR, half ValueG, half ValueB)
{
    return (ValueR * rgbMask.r) + (ValueG * rgbMask.g) + (ValueB * rgbMask.b);
}

half4 GetColorSpaceDouble()
{
    return half4(4.59479380, 4.59479380, 4.59479380, 2.0) / 2;
}

half GetGrayScale(half3 target)
{
    return dot(target, half3(0.3, 0.59, 0.11));
}

half3 GetRim(half3 viewDirectionWS, half3 normalWS, half3 rimColor, half rimAmount)
{
    half3 rimDot = 1 - dot(normalize(viewDirectionWS), normalize(normalWS));
    half rimIntensity = smoothstep(1 - rimAmount - 0.21, 1 - rimAmount + 0.21, rimDot);
    return rimIntensity * rimColor;
}

float Noise(float2 uv)
{
    return frac(sin(dot(uv, half2(12.9898, 78.233))) * 43758.5453) * 2.0 - 1.0;
}

float2 NoiseUV(float2 uv, float2 ps, half2 dir, float power)
{
    half2 offset = power * half2(ps.x * Noise(uv), ps.y * Noise(uv.yx));

    uv -= dir * 0.5;
    uv += dir;

    return uv + offset;
}

half3 SetBacklightTotalFrag(half3 color, float3 positionWS, int backlightsAmount, half minDarkness, out half lightFactor)
{
    half sphereFade = 0.9; // TODO: можно вынести в параметризацию
    half overGlow = 4; // TODO: можно вынести в параметризацию

    half distBackLight = 0;
    half3 blendedClr = half3(1, 1, 1);

    for (int i = 0; i < backlightsAmount; i++) // TODO: check size 
    {
        const half sphereRadius = _BackLights[i].w;

        half distBuffer = distance(_BackLights[i], positionWS);
        distBuffer = 1 - saturate((distBuffer - (sphereRadius - 0.3)) / sphereFade);
        distBuffer *= _BackLightsColors[i].a;

        blendedClr = lerp(blendedClr, _BackLightsColors[i] * overGlow, distBuffer);
        distBackLight = max(distBackLight, distBuffer);
    }

    lightFactor = distBackLight;
    
    color *= clamp(distBackLight, minDarkness, 1);
    return lerp(color.rgb, color.rgb * blendedClr, distBackLight);
}

half4 SetBacklightPartVert(float3 positionWS, int backlightsAmount)
{
    half sphereFade = 0.9; // TODO: можно вынести в параметризацию
    half overGlow = 4; // TODO: можно вынести в параметризацию

    half distBackLight = 0;
    half3 blendedClr = half3(1, 1, 1);

    for (int i = 0; i < backlightsAmount; i++) // TODO: check size 
    {
        const half sphereRadius = _BackLights[i].w;

        half distBuffer = distance(_BackLights[i], positionWS);
        distBuffer = 1 - saturate((distBuffer - (sphereRadius - 0.3)) / sphereFade);
        distBuffer *= _BackLightsColors[i].a;

        blendedClr = lerp(blendedClr, _BackLightsColors[i] * overGlow, distBuffer);
        distBackLight = max(distBackLight, distBuffer);
    }

    return half4(blendedClr, distBackLight);
}

half3 SetBacklightPartFrag(half3 color, half4 vertResult, half minDarkness)
{
    color *= clamp(vertResult.a, minDarkness, 1);
    return lerp(color.rgb, color.rgb * vertResult.rgb, vertResult.a);
}
