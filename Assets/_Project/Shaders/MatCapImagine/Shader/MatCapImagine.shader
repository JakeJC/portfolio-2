Shader "Universal Render Pipeline/Custom/MatCapImagine"
{
    Properties
    {
        // -------------------------------------
        // Editmode props
        [HideInInspector] _QueueOffset("Queue offset", Float) = 0.0

        // -------------------------------------
        // Main
        _RGBmask("Albedo", 2D) = "white" {}
        [MainTexture] _BaseMap("Albedo", 2D) = "white" {}

        [MainColor] _BaseColorR("Base Color R", Color) = (1,1,1,1)
        [MainColor] _BaseColorG("Base Color G", Color) = (1,1,1,1)
        [MainColor] _BaseColorB("Base Color B", Color) = (1,1,1,1)

        [Toggle(SPECIALCOLOR_ON)] _UseSpecialColor("Use Special Color", Int) = 0.0
        [Toggle(EMISSION_ON)] _UseEmission("Use Emission", Int) = 0
        _SpecialMask("Special Mask", 2D) = "white" {}
        _SpecialColor("Special Color", Color) = (1,1,1,1)

        // -------------------------------------
        // Rim
        _RimColorR("Rim Color R", Color) = (0.5, 0.5, 0.5, 1)
        _RimColorG("Rim Color G", Color) = (0.5, 0.5, 0.5, 1)
        _RimColorB("Rim Color B", Color) = (0.5, 0.5, 0.5, 1)

        // -------------------------------------
        // Reflection
        _Cube("Reflection Cubemap", Cube) = "_Skybox"
        _ReflectionMask("Reflection Mask", 2D) = "white" {}
        _ReflectionMaskPower("Reflection Mask Power", Range(0.0, 1.0)) = 0.3

        // -------------------------------------
        // SphereClip
        [Toggle(SPHERECLIP_ON)] _UseSphereClip("Use sphere clip", Int) = 0
        [Toggle(SPHERECLIPSHADOW_ON)] _UseSphereClipShadow("Use sphere clip", Int) = 0
        [Toggle(EXTRUDE_ON)] _UseExtrude("Use Extrude", Int) = 0

        // -------------------------------------
        // MatCap
        _MatCap ("MatCap", 2D) = "white" {}

        // -------------------------------------
        // Noise
        _ShadingNoise("ShadingNoise", Range(0.0, 300.0)) = 20

        // -------------------------------------
        // Backlight
        [Toggle(BACKLIGHT_ON)] _UseBacklight("Use Backlight", Int) = 0
    }
    SubShader
    {
        Tags
        {
            "RenderType" = "Opaque" "IgnoreProjector" = "True" "RenderPipeline" = "UniversalPipeline" "ShaderModel"="4.5"
        }
        LOD 100

        ZWrite On
        Cull Back

        Pass
        {
            Name "Unlit MatCap"

            HLSLPROGRAM
            #pragma exclude_renderers gles gles3 glcore geometry
            #pragma enable_d3d11_debug_symbols
            #pragma target 4.5 geometry

            // -------------------------------------
            // Custom Keywords
            #pragma shader_feature SPHERECLIP_ON
            #pragma shader_feature SPHERECLIPSHADOW_ON
            #pragma shader_feature SPECIALCOLOR_ON
            #pragma shader_feature EXTRUDE_ON
            #pragma shader_feature BACKLIGHT_ON
            #pragma shader_feature EMISSION_ON

            //--------------------------------------
            // GPU Instancing Keywords
            #pragma multi_compile_instancing

            //--------------------------------------
            // Functions
            #pragma geometry GeometryFunc
            #pragma vertex VertexFunc
            #pragma fragment FragmentFunc

            //--------------------------------------
            // Includes
            #include "MatCapImagineInput.hlsl"
            #include "MatCapImagineForwardPass.hlsl"
            ENDHLSL
        }
    }

    SubShader
    {
        Tags
        {
            "RenderType" = "Opaque" "IgnoreProjector" = "True" "RenderPipeline" = "UniversalPipeline" "ShaderModel"="4.5"
        }
        LOD 100

        ZWrite On
        Cull Back

        Pass
        {
            Name "Unlit MatCap"

            HLSLPROGRAM
            #pragma exclude_renderers gles gles3 glcore
            #pragma enable_d3d11_debug_symbols
            #pragma target 4.5

            // -------------------------------------
            // Custom Keywords
            #pragma shader_feature SPHERECLIP_ON
            #pragma shader_feature SPHERECLIPSHADOW_ON
            #pragma shader_feature SPECIALCOLOR_ON
            #pragma shader_feature EXTRUDE_ON
            #pragma shader_feature BACKLIGHT_ON
            #pragma shader_feature EMISSION_ON

            //--------------------------------------
            // GPU Instancing Keywords
            #pragma multi_compile_instancing

            //--------------------------------------
            // Functions
            /*#pragma geometry GeometryFunc */
            #pragma vertex VertexFunc
            #pragma fragment FragmentFunc

            //--------------------------------------
            // Includes
            #include "MatCapImagineInput.hlsl"
            #include "MatCapImagineForwardPass.hlsl"
            ENDHLSL
        }
    }

    SubShader
    {
        Tags
        {
            "RenderType" = "Opaque" "IgnoreProjector" = "True" "RenderPipeline" = "UniversalPipeline" "ShaderModel"="2.0"
        }
        LOD 100

        ZWrite On
        Cull Back

        Pass
        {
            Name "Unlit MatCap"

            HLSLPROGRAM
            #pragma only_renderers gles gles3 glcore d3d11
            #pragma enable_d3d11_debug_symbols
            #pragma target 2.0

            // -------------------------------------
            // Custom Keywords
            #pragma shader_feature SPHERECLIP_ON
            #pragma shader_feature SPHERECLIPSHADOW_ON
            #pragma shader_feature SPECIALCOLOR_ON
            #pragma shader_feature EXTRUDE_ON
            #pragma shader_feature BACKLIGHT_ON
            #pragma shader_feature EMISSION_ON

            //--------------------------------------
            // Functions
            /*#pragma geometry GeometryFunc*/
            #pragma vertex VertexFunc
            #pragma fragment FragmentFunc

            //--------------------------------------
            // Includes
            #include "MatCapImagineInput.hlsl"
            #include "MatCapImagineForwardPass.hlsl"
            ENDHLSL
        }
    }
    FallBack "Hidden/Universal Render Pipeline/FallbackError"
    CustomEditor "_Project.Shaders.MatCapImagine.Editor.MatCapImagineGUI"
}