const half3 l = half3(0.3, 0.59, 0.11);
 
float SinglePinLight(float s, float d)
{
    return (2.0*s - 1.0 > d) ? 2.0*s - 1.0 : (s < 0.5 * d) ? 2.0*s : d;
}
 
float SingleVividLight(float s, float d)
{
    return (s < 0.5) ? 1.0 - (1.0 - d) / (2.0 * s) : d / (2.0 * (1.0 - s));
}
 
float SingleHardLight(float s, float d)
{
    return (s < 0.5) ? 2.0*s*d : 1.0 - 2.0*(1.0 - s)*(1.0 - d);
}
 
float SingleSoftLight(float s, float d)
{
    return (s < 0.5) ? d - (1.0 - 2.0*s)*d*(1.0 - d) 
                : (d < 0.25) ? d + (2.0*s - 1.0)*d*((16.0*d - 12.0)*d + 3.0) 
                : d + (2.0*s - 1.0) * (sqrt(d) - d);
}
 
float SingleOverlay( float s, float d )
{
    return (d < 0.5) ? 2.0*s*d : 1.0 - 2.0*(1.0 - s)*(1.0 - d);
}
 
half3 RGBtoHSV(half3 c)
{
    half4 K = half4(0.0, -1.0 / 3.0, 2.0 / 3.0, -1.0);
    half4 p = lerp(half4(c.bg, K.wz), half4(c.gb, K.xy), step(c.b, c.g));
    half4 q = lerp(half4(p.xyw, c.r), half4(c.r, p.yzx), step(p.x, c.r));
    
    float d = q.x - min(q.w, q.y);
    float e = 1.0e-10;
    return half3(abs(q.z + (q.w - q.y) / (6.0 * d + e)), d / (q.x + e), q.x);
}
 
half3 HSVtoRGB(half3 c)
{
    half4 K = half4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
    half3 p = abs(frac(c.xxx + K.xyz) * 6.0 - K.www);
    return c.z * lerp(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y);
}
 
half3 ColorBurn(half3 s, half3 d)
{
    return 1.0 - (1.0 - d) / s;
}
 
half3 LinearBurn(half3 s, half3 d )
{
    return s + d - 1.0;
}
 
half3 DarkerColor(half3 s, half3 d)
{
    return (s.x + s.y + s.z < d.x + d.y + d.z) ? s : d;
}
 
half3 Lighten(half3 s, half3 d)
{
    return max(s, d);
}
 
half3 Screen(half3 s, half3 d)
{
    return s + d - s * d;
}
 
half3 ColorDodge(half3 s, half3 d)
{
    return d / (1.0 - s);
}
 
half3 LinearDodge(half3 s, half3 d)
{
    return s + d;
}
 
half3 LighterColor(half3 s, half3 d)
{
    return (s.x + s.y + s.z > d.x + d.y + d.z) ? s : d;
}
 
half3 Overlay(half3 s, half3 d)
{
    half3 c;
    c.x = SingleOverlay(s.x, d.x);
    c.y = SingleOverlay(s.y, d.y);
    c.z = SingleOverlay(s.z, d.z);
    return c;
}
 
half3 SoftLight(half3 s, half3 d)
{
    half3 c;
    c.x = SingleSoftLight(s.x, d.x);
    c.y = SingleSoftLight(s.y, d.y);
    c.z = SingleSoftLight(s.z, d.z);
    return c;
}
 
half3 HardLight(half3 s, half3 d)
{
    half3 c;
    c.x = SingleHardLight(s.x, d.x);
    c.y = SingleHardLight(s.y, d.y);
    c.z = SingleHardLight(s.z, d.z);
    return c;
}
 
half3 VividLight(half3 s, half3 d)
{
    half3 c;
    c.x = SingleVividLight(s.x, d.x);
    c.y = SingleVividLight(s.y, d.y);
    c.z = SingleVividLight(s.z, d.z);
    return c;
}
 
half3 LinearLight(half3 s, half3 d)
{
    return 2.0*s + d - 1.0;
}
 
half3 PinLight(half3 s, half3 d)
{
    half3 c;
    c.x = SinglePinLight(s.x, d.x);
    c.y = SinglePinLight(s.y, d.y);
    c.z = SinglePinLight(s.z, d.z);
    return c;
}
 
half3 HardMix(half3 s, half3 d)
{
    return floor(s+d);
}
 
half3 Difference(half3 s, half3 d)
{
    return abs(d-s);
}
 
half3 Exclusion(half3 s, half3 d)
{
    return s + d - 2.0*s*d;
}
 
half3 Subtract(half3 s, half3 d)
{
    return s-d;
}
 
half3 Divide(half3 s, half3 d)
{
    return s/d;
}
 
half3 Add(half3 s, half3 d)
{
    return s+d;
}
 
half3 Hue(half3 s, half3 d)
{
    d = RGBtoHSV(d);
    d.x = RGBtoHSV(s).x;
    return HSVtoRGB(d);
}
 
half3 Color(half3 s, half3 d)
{
    s = RGBtoHSV(s);
    s.z = RGBtoHSV(d).z;
    return HSVtoRGB(s);
}
 
half3 Saturation(half3 s, half3 d)
{
    d = RGBtoHSV(d);
    d.y = RGBtoHSV(s).y;
    return HSVtoRGB(d);
}
 
half3 Luminosity(half3 s, half3 d)
{
    float dLum = dot(d, l);
    float sLum = dot(s, l);
    float lum = sLum - dLum;
    half3 c = d + lum;
    float minC = min(min(c.x, c.y), c.z);
    float maxC = max(max(c.x, c.y), c.z);
    if(minC < 0.0) return sLum + ((c - sLum) * sLum) / (sLum - minC);
    else if(maxC > 1.0) return sLum + ((c - sLum) * (1.0 - sLum)) / (maxC - sLum);
    else return c;
}
 