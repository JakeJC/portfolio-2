//--------------------------------------
// Includes
#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"
#include "MatCapImagineCustomFunctions.hlsl"
#include "PhotoshopBlendsFunctions.hlsl"

///////////////////////////////////////////////////////////////////////////////
//                                 Structs                                   //
///////////////////////////////////////////////////////////////////////////////
struct Attributes // CHANGE data types
{
    float4 positionOS : POSITION;
    float3 normalOS : NORMAL;
    half4 tangentOS : TANGENT;
    #if(BACKLIGHT_ON)
    half4 vertexColor : COLOR;
    #endif
    half2 texcoord : TEXCOORD0;

    UNITY_VERTEX_INPUT_INSTANCE_ID
};

struct Varyings // CHANGE data types
{
    float4 positionCS : SV_POSITION;
    float2 uv : TEXCOORD0;
    float3 positionWS : TEXCOORD1;
    half3 normalWS : TEXCOORD2;
    half3 viewDirWS : TEXCOORD3;
    float2 cap : TEXCOORD4;
    float2 positionOS : TEXCOORD5;

    #if(BACKLIGHT_ON)
    /*half4 backlightData : TEXCOORD6;*/
    half4 vertexColor : TEXCOORD7;
    #endif

    UNITY_VERTEX_INPUT_INSTANCE_ID
};

struct FragData
{
    float3 positionWS;
    half3 normalWS;
    half3 viewDirectionWS;
};

void InitializeFragData(Varyings input, out FragData fragData)
{
    fragData = (FragData)0;

    fragData.positionWS = input.positionWS;

    half3 viewDirWS = SafeNormalize(input.viewDirWS);
    fragData.normalWS = input.normalWS;
    fragData.normalWS = NormalizeNormalPerPixel(fragData.normalWS);
    fragData.viewDirectionWS = viewDirWS;
}

///////////////////////////////////////////////////////////////////////////////
//                  Vertex Geometry Fragment                                 //
///////////////////////////////////////////////////////////////////////////////

Varyings VertexFunc(Attributes input)
{
    Varyings output = (Varyings)0;

    UNITY_SETUP_INSTANCE_ID(input);
    UNITY_TRANSFER_INSTANCE_ID(input, output);

    VertexPositionInputs vertexInput = GetVertexPositionInputs(input.positionOS.xyz);
    VertexNormalInputs normalInput = GetVertexNormalInputs(input.normalOS, input.tangentOS);

    output.positionCS = vertexInput.positionCS;
    output.uv = TRANSFORM_TEX(input.texcoord, _BaseMap);
    output.normalWS = normalInput.normalWS;
    output.positionWS = vertexInput.positionWS;
    output.viewDirWS = GetWorldSpaceViewDir(vertexInput.positionWS);
    output.positionOS = input.positionOS;

    // -------------------------------------
    // Backlight
    #if(BACKLIGHT_ON)
    /*output.backlightData = SetBacklightPartVert(output.positionWS, _BackLightsActiveAmount);*/
    output.vertexColor = input.vertexColor;
    #endif

    // -------------------------------------
    // MatCap
    half3 e = normalize(mul(UNITY_MATRIX_MV, input.positionOS));
    half3 n = normalize(mul(UNITY_MATRIX_MV, half4(input.normalOS, 0.)));

    half3 r = reflect(e, n);
    half m = 2. * sqrt(
        pow(r.x, 2.) +
        pow(r.y, 2.) +
        pow(r.z + 1., 2.)
    );
    float2 capCoord;
    capCoord = r.xy / m + 0.5;
    output.cap = capCoord;

    return output;
}

//Geom
inline float4 UnityObjectToClipPos(in float3 pos)
{
    #if defined(STEREO_CUBEMAP_RENDER_ON)
    return UnityObjectToClipPosODS(pos);
    #else
    // More efficient than computing M*VP matrix product
    return mul(UNITY_MATRIX_VP, mul(unity_ObjectToWorld, float4(pos, 1.0)));
    #endif
}


float gold_noise(in float2 uv, in float seed)
{
    return frac(tan(distance(uv * 1.61803398874989484820459, uv) * seed) * uv.x);
}

float parabola(float x, float k)
{
    return pow(4.0 * x * (1.0 - x), k);
}

[maxvertexcount(15)]
void GeometryFunc(triangle Varyings input[3], inout TriangleStream<Varyings> outputStream)
{
    Varyings output = (Varyings)0;
    Varyings top[3];

    float3 dir[3];

    [unroll(3)]
    for (int k = 0; k < 3; ++k)
    {
        dir[k] = input[(k + 1) % 3].positionWS - input[k].positionWS;
    }

    float3 extrudeDir = normalize(cross(dir[0], -dir[2]));

    float extrudeAmount = /*_ExtrudeSize **/ (sin(
        (gold_noise(input[0].uv, 810) * 2 + _Time.y * _GLOBALExtrudeAnimationSpeed) * PI * 2) + 1);

    // Extrude face
    [unroll(3)]
    for (int i = 0; i < 3; ++i)
    {
        #if(EXTRUDE_ON)
        // Sphere
        half dist = distance(_GLOBALSpherePosition, input[i].positionWS);
        half sphere = 1 - saturate((dist - _GLOBALSphereRadius) / _GLOBALSphereFade);
        half glowRing = 1 - sphere;
        glowRing = parabola(glowRing, 3.0);
        half extrudeCalculatedPower = _GLOBALExtrudePower * glowRing;
        //
        #else
        half extrudeCalculatedPower = 0;
        #endif

        top[i] = input[i];
        top[i].positionWS = top[i].positionWS + extrudeDir * (extrudeCalculatedPower * extrudeAmount);
        top[i].positionCS = TransformWorldToHClip(top[i].positionWS);

        // URP shadow coordinate
        /*VertexPositionInputs vertexInput = (VertexPositionInputs)0;
        vertexInput.positionWS = top[i].positionWS;*/

        outputStream.Append(top[i]);
    }

    outputStream.RestartStrip();

    // Construct sides
    [unroll(3)]
    for (int j = 0; j < 3; ++j)
    {
        half3 normalWS = normalize(cross(dir[j], extrudeDir));

        output = top[(j + 1) % 3];
        output.normalWS = normalWS;
        outputStream.Append(output);
        output = top[j];
        output.normalWS = normalWS;
        outputStream.Append(output);
        output = input[(j + 1) % 3];
        output.normalWS = normalWS;
        outputStream.Append(output);
        output = input[j];
        output.normalWS = normalWS;
        outputStream.Append(output);

        outputStream.RestartStrip();
    }
}

half4 FragmentFunc(Varyings input) : SV_Target
{
    UNITY_SETUP_INSTANCE_ID(input);
    UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(input);

    half4 color = half4(1, 1, 1, 1);

    // -------------------------------------
    // Sphere Clip
    #if(SPHERECLIP_ON)
    half dist = distance(_GLOBALSpherePosition, input.positionWS);
    half sphere = /*1 - */ saturate((dist - _GLOBALSphereRadius) / _GLOBALSphereFade);

    half glowRing = 1 - sphere;

    clip(sphere - 0.1);
    #endif

    #if(SPHERECLIPSHADOW_ON)
    half distShadow = distance(_GLOBALSpherePosition, input.positionWS);
    half sphereShadow = 1 - saturate((distShadow - (_GLOBALSphereRadius - 0.3)) / _GLOBALSphereShadowFade);
    // TODO: в переменную

    half shadowRing = 1 - sphereShadow;
    #endif

    // -------------------------------------
    // Masks
    half4 rgbMask = SAMPLE_TEXTURE2D(_RGBmask, sampler_RGBmask, input.uv);
    half4 specialMask = SAMPLE_TEXTURE2D(_SpecialMask, sampler_SpecialMask, input.uv);

    // -------------------------------------
    // Data
    FragData fragData;
    InitializeFragData(input, fragData);

    // -------------------------------------
    // Base
    half4 baseTexture = SAMPLE_TEXTURE2D(_BaseMap, sampler_BaseMap, input.uv);
    half4 baseColor = GetMaskedColor(rgbMask, _BaseColorR, _BaseColorG, _BaseColorB);

    #if(SPECIALCOLOR_ON/* && !BACKLIGHT_ON*/)
    baseColor = lerp(baseColor, _SpecialColor, specialMask);
    #endif

    color = baseTexture * baseColor;

    // -------------------------------------
    // Noise
    float2 iResolution = float2(_ScreenParams.x, _ScreenParams.y);
    float2 ps = 1.0 / iResolution.xy;
    float2 noisedUV = NoiseUV(input.cap, ps, float2(0, ps.y), _ShadingNoise);

    // -------------------------------------
    // MatCap
    half4 matCap = SAMPLE_TEXTURE2D(_MatCap, sampler_MatCap, noisedUV) * GetColorSpaceDouble();
    color *= matCap;

    // -------------------------------------
    // Reflection
    half4 cubeReflection = texCUBE(_Cube, reflect(input.viewDirWS, normalize(input.normalWS)));
    half4 metallMask = SAMPLE_TEXTURE2D(_ReflectionMask, sampler_ReflectionMask, input.uv);

    half3 metallReflection = lerp(color, SoftLight(color, cubeReflection.rgb), rgbMask.b);
    metallReflection = lerp(metallReflection, Divide(metallReflection, metallMask),
                            _ReflectionMaskPower * GetGrayScale(metallReflection) * rgbMask.b);

    color.rgb = metallReflection;

    // -------------------------------------
    // Rim
    half4 rimColor = GetMaskedColor(rgbMask, _RimColorR, _RimColorG, _RimColorB);
    half3 rim = GetRim(fragData.viewDirectionWS, fragData.normalWS, rimColor, rimColor.a);
    color.rgb = max(color.rgb, color.rgb + rim);

    // -------------------------------------
    // Backlight
    half lightFactor = 1;
    #if(BACKLIGHT_ON)
    color.rgb = SetBacklightTotalFrag(color.rgb, input.positionWS, _BackLightsActiveAmount, clamp(_MinDarkness, 1 - GetGrayScale(input.vertexColor), 1) /*_MinDarkness*/, lightFactor); 
    /*color.rgb = SetBacklightPartFrag(color.rgb, input.backlightData, _MinDarkness);*/
    #endif
    /*#if(SPECIALCOLOR_ON && BACKLIGHT_ON)
    color = lerp(color, _SpecialColor, clamp(specialMask, 0, lightFactor));
    #endif*/

    // -------------------------------------
    // Emission
    #if(EMISSION_ON)
    color.rgb = lerp(color.rgb, _SpecialColor, specialMask);
    #endif

    // -------------------------------------
    // Sphere Clip
    #if(SPHERECLIP_ON)
    half _Middle = 0.3; // TODO: в переменную

    half4 glowColor = lerp(_GLOBALClipColor1, _GLOBALClipColor2, glowRing / _Middle) * step(glowRing, _Middle);
    glowColor += lerp(_GLOBALClipColor2, _GLOBALClipColor3, (glowRing - _Middle) / (1 - _Middle)) * step(
        _Middle, glowRing);

    color.rgb = lerp(color, glowColor.rgb, glowRing);
    #endif

    #if(SPHERECLIPSHADOW_ON)
    color.rgb = lerp(color, Overlay(color.rgb, _GLOBALClipShadowColor.rgb), shadowRing);
    #endif

    return color;
}
