using UnityEditor;
using UnityEngine;

namespace _Project.Shaders.MatCapImagine.Editor
{
  public enum SurfaceType
  {
    Opaque,
    Transparent
  }

  public enum BlendMode
  {
    Alpha,
    Premultiply,
    Additive,
    Multiply
  }

  public enum RenderFace
  {
    Front = 2,
    Back = 1,
    Both = 0
  }

  public static class ShaderGUIHelper
  {
    public static void DrawTexture(MaterialProperty property, MaterialEditor editor, string title, MaterialProperty tint = null)
    {
      if (property != null)
      {
        if (tint == null)
          editor.TexturePropertySingleLine(new GUIContent(title), property);
        else
          editor.TexturePropertySingleLine(new GUIContent(title), property, tint);
      }
    }

    public static void DrawToggle(MaterialProperty property, Material material, string keyword, string title, out bool outValue, bool inverse = false)
    {
      outValue = false;

      if (property != null)
      {
        bool value = (property.floatValue != 0.0f);
        EditorGUI.BeginChangeCheck();
        value = EditorGUILayout.Toggle(title, value);
        outValue = value;

        if (EditorGUI.EndChangeCheck())
        {
          property.floatValue = value ? 1.0f : 0.0f;

          if (!inverse)
          {
            if (value)
              material.EnableKeyword(keyword);
            else
              material.DisableKeyword(keyword);
          }
          else
          {
            if (value)
              material.DisableKeyword(keyword);
            else
              material.EnableKeyword(keyword);
          }
        }
      }
    }

    public static void DrawShaderProperty(MaterialProperty property, MaterialEditor editor, string title = "")
    {
      if (property != null)
        editor.ShaderProperty(property, title == "" ? property.displayName : title);
    }

    public static void SwitchTextureKeyword(string propertyName, Material material, string keyword)
    {
      if (material.HasProperty(propertyName))
      {
        if (material.GetTexture(propertyName))
          material.EnableKeyword(keyword);
        else
          material.DisableKeyword(keyword);
      }
    }
  }
}