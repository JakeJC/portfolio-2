using UnityEditor;
using UnityEngine;

namespace _Project.Shaders.MatCapImagine.Editor
{
  public class MatCapImagineGUI : ShaderGUI
  {
    private Material _material;
    private MaterialEditor _editor;
    private MaterialProperty[] _properties;

    public override void OnGUI(MaterialEditor materialEditor, MaterialProperty[] properties)
    {
      _editor = materialEditor;
      _properties = properties;
      _material = materialEditor.target as Material;

      if (_material != null) 
        _material.enableInstancing = true;

      DrawTextures();
      GUILayout.Space(10);
      DrawRim();
      GUILayout.Space(10);
      DrawReflection();
      /*GUILayout.Space(10);
      DrawSphereClip();*/
      /*GUILayout.Space(10);
      Extrude();*/
      GUILayout.Space(10);
      Backlight();
      GUILayout.Space(10);
    }

    private void DrawTextures()
    {
      if (EditorGUILayout.BeginFoldoutHeaderGroup(true, "[Base]"))
      {
        GUILayout.Space(5);
        GUILayout.Label("Marble / Wood / Metall");
        ShaderGUIHelper.DrawTexture(FindProperty("_RGBmask", _properties), _editor, "RGB Mask");
        
        GUILayout.Space(5);
        ShaderGUIHelper.DrawTexture(FindProperty("_MatCap", _properties), _editor, "Mat Cap");
        ShaderGUIHelper.DrawShaderProperty(FindProperty("_ShadingNoise", _properties), _editor);
        
        GUILayout.Space(5);
        ShaderGUIHelper.DrawTexture(FindProperty("_BaseMap", _properties), _editor, "Base Map");

        GUILayout.Space(5);
        ShaderGUIHelper.DrawShaderProperty(FindProperty("_BaseColorR", _properties), _editor);
        ShaderGUIHelper.DrawShaderProperty(FindProperty("_BaseColorG", _properties), _editor);
        ShaderGUIHelper.DrawShaderProperty(FindProperty("_BaseColorB", _properties), _editor);
        
        GUILayout.Space(5);
        ShaderGUIHelper.DrawToggle(FindProperty("_UseSpecialColor", _properties), _material, "SPECIALCOLOR_ON", "Use Special Color", out bool use);
        ShaderGUIHelper.DrawToggle(FindProperty("_UseEmission", _properties), _material, "EMISSION_ON", "Use Emission", out bool useEmission);

        if(use || useEmission)
          ShaderGUIHelper.DrawTexture(FindProperty("_SpecialMask", _properties), _editor, "Special Mask", FindProperty("_SpecialColor", _properties));
      }

      EditorGUILayout.EndFoldoutHeaderGroup();
    }

    private void DrawRim()
    {
      if (EditorGUILayout.BeginFoldoutHeaderGroup(true, "[Rim]"))
      {
        GUILayout.Space(5);
        ShaderGUIHelper.DrawShaderProperty(FindProperty("_RimColorR", _properties), _editor);
        ShaderGUIHelper.DrawShaderProperty(FindProperty("_RimColorG", _properties), _editor);
        ShaderGUIHelper.DrawShaderProperty(FindProperty("_RimColorB", _properties), _editor);
      }

      EditorGUILayout.EndFoldoutHeaderGroup();
    }

    private void DrawReflection()
    {
      if (EditorGUILayout.BeginFoldoutHeaderGroup(true, "[Reflection B]"))
      {
        GUILayout.Space(5);
        ShaderGUIHelper.DrawTexture(FindProperty("_Cube", _properties), _editor, "Reflection Cubemap");
        ShaderGUIHelper.DrawTexture(FindProperty("_ReflectionMask", _properties), _editor, "Reflection Mask");
        ShaderGUIHelper.DrawShaderProperty(FindProperty("_ReflectionMaskPower", _properties), _editor);
      }

      EditorGUILayout.EndFoldoutHeaderGroup();
    }

    private void DrawSphereClip()
    {
      if (EditorGUILayout.BeginFoldoutHeaderGroup(true, "[Sphere Clip]"))
      {
        GUILayout.Space(5);

        ShaderGUIHelper.DrawToggle(FindProperty("_UseSphereClip", _properties), _material, "SPHERECLIP_ON", "Enabled", out bool use);
        ShaderGUIHelper.DrawToggle(FindProperty("_UseSphereClipShadow", _properties), _material, "SPHERECLIPSHADOW_ON", "Shadow", out bool useShadow);
      }

      EditorGUILayout.EndFoldoutHeaderGroup();
    }

    private void Extrude()
    {
      if (EditorGUILayout.BeginFoldoutHeaderGroup(true, "[Extrude]")) 
        ShaderGUIHelper.DrawToggle(FindProperty("_UseExtrude", _properties), _material, "EXTRUDE_ON", "Enabled", out bool use);

      EditorGUILayout.EndFoldoutHeaderGroup();
    }
    
    private void Backlight()
    {
      if (EditorGUILayout.BeginFoldoutHeaderGroup(true, "[Backlight]"))
      {
        GUILayout.Space(5);
        ShaderGUIHelper.DrawToggle(FindProperty("_UseBacklight", _properties), _material, "BACKLIGHT_ON", "Enabled", out bool use);
      }

      EditorGUILayout.EndFoldoutHeaderGroup();
    }
  }
}