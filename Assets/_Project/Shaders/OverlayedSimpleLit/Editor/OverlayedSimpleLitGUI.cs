using _Project.Shaders.Editor;
using UnityEditor;
using UnityEngine;

namespace _Project.Shaders.OverlayedSimpleLit.Editor
{
  public class OverlayedSimpleLitGUI : ShaderGUI
  {
    private Material _material;
    private MaterialEditor _editor;
    private MaterialProperty[] _properties;

    public override void OnGUI(MaterialEditor materialEditor, MaterialProperty[] properties)
    {
      _editor = materialEditor;
      _properties = properties;
      _material = materialEditor.target as Material;
      
      DrawTextures();
      GUILayout.Space(10);
    }

    private void DrawTextures()
    {
      if (EditorGUILayout.BeginFoldoutHeaderGroup(true, "[Base]"))
      {
        GUILayout.Space(5);
        ShaderGUIHelper.DrawTexture(FindProperty("_BaseMap", _properties), _editor, "Base Map", FindProperty("_BaseColor", _properties));
        ShaderGUIHelper.DrawTexture(FindProperty("_OverlayedMask", _properties), _editor, "Overlayed Mask", FindProperty("_OverlayedMaskColor", _properties));
      }

      EditorGUILayout.EndFoldoutHeaderGroup();
    }
  }
}