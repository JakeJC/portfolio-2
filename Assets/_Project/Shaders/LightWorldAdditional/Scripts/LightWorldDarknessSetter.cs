using UnityEngine;

namespace _Project.Shaders.LightWorldAdditional.Scripts
{
  [ExecuteInEditMode]
  public class LightWorldDarknessSetter : MonoBehaviour
  {
    private static readonly int MinDarknessId = Shader.PropertyToID("_MinDarkness");

    public static bool IsSpawn;
    
    [SerializeField] [Range(0, 1)] private float minDarkness = 0.1f;
    [SerializeField] [Range(0, 1)] private float minDarknessSpawn = 0.3f;

    private void Update() => 
      Shader.SetGlobalFloat(MinDarknessId, IsSpawn ? minDarknessSpawn : minDarkness);

    [ContextMenu("Switch Spawn Status")]
    private void SwitchSpawnStatus() => 
      IsSpawn = !IsSpawn;
  }
}