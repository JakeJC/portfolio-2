using _Project.Scripts.Services.MaterialService.Data;
using UnityEngine;

namespace _Project.Shaders.LightWorldAdditional.Utility
{
  [CreateAssetMenu(fileName = "Transfer Colors", menuName = "Transfer Colors", order = 0)]
  public class TransferColors : ScriptableObject
  {
    private static readonly int GlowMainColor = Shader.PropertyToID("_MainColor");
    private static readonly int GlowSecondColor = Shader.PropertyToID("_SecondColor");
    private static readonly int DissolveOuterEdgeColor = Shader.PropertyToID("_OuterEdgeColor");
    private static readonly int DissolveInnerEdgeColor = Shader.PropertyToID("_InnerEdgeColor");
    private static readonly int DissolveGlowColor = Shader.PropertyToID("_DissolveGlowColor");
    private static readonly int DissolveMap = Shader.PropertyToID("_DissolveMap");

    public bool SetToSpecial;
    
    [Header("To")]
    public ColorSchemeData[] ColorSchemeDatas;
    
    [Header("From")]
    public Material[] GlowMaterials;
    public Material[] DissolveMaterials;

    [ContextMenu("Transfer")]
    public void Transfer()
    {
      for (int i = 0; i < ColorSchemeDatas.Length; i++)
      {
        ColorSchemeDatas[i].ColorData.LightWorldColorData.GlowColor.MainColor = GlowMaterials[i].GetColor(GlowMainColor);
        ColorSchemeDatas[i].ColorData.LightWorldColorData.GlowColor.SecondColor = GlowMaterials[i].GetColor(GlowSecondColor);

        ColorSchemeDatas[i].ColorData.LightWorldColorData.DissolveColor.Texture = DissolveMaterials[i].GetTexture(DissolveMap);
        ColorSchemeDatas[i].ColorData.LightWorldColorData.DissolveColor.OuterEdgeColor = DissolveMaterials[i].GetColor(DissolveOuterEdgeColor);
        ColorSchemeDatas[i].ColorData.LightWorldColorData.DissolveColor.InnerEdgeColor = DissolveMaterials[i].GetColor(DissolveInnerEdgeColor);
        ColorSchemeDatas[i].ColorData.LightWorldColorData.DissolveColor.GlowColor = DissolveMaterials[i].GetColor(DissolveGlowColor);
        
        ColorSchemeDatas[i].SetDirty(); 
      }

      if (SetToSpecial)
      {
        for (int i = 0; i < ColorSchemeDatas.Length; i++)
        {
          ColorSchemeDatas[i].ColorData.SpecialLightWorldColorData.GlowColor.MainColor = GlowMaterials[i].GetColor(GlowMainColor);
          ColorSchemeDatas[i].ColorData.SpecialLightWorldColorData.GlowColor.SecondColor = GlowMaterials[i].GetColor(GlowSecondColor);

          ColorSchemeDatas[i].ColorData.SpecialLightWorldColorData.DissolveColor.Texture = DissolveMaterials[i].GetTexture(DissolveMap);
          ColorSchemeDatas[i].ColorData.SpecialLightWorldColorData.DissolveColor.OuterEdgeColor = DissolveMaterials[i].GetColor(DissolveOuterEdgeColor);
          ColorSchemeDatas[i].ColorData.SpecialLightWorldColorData.DissolveColor.InnerEdgeColor = DissolveMaterials[i].GetColor(DissolveInnerEdgeColor);
          ColorSchemeDatas[i].ColorData.SpecialLightWorldColorData.DissolveColor.GlowColor = DissolveMaterials[i].GetColor(DissolveGlowColor);
        
          ColorSchemeDatas[i].SetDirty(); 
        }
      }
    }
  }
}