using _Project.Shaders.LightWorldAdditional.Controller.Scripts.Datas;
using UnityEngine;

namespace _Project.Shaders.LightWorldAdditional.Controller.Scripts
{
  [System.Serializable]
  public class LightWorldElement
  {
    [HideInInspector] public string Name;

    public GeneralData GeneralData;
    public LightData LightData;
    public GlowData GlowData;
    public DissolveData DissolveData;
  }
}