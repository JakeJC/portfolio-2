using UnityEngine;

namespace _Project.Shaders.LightWorldAdditional.Controller.Scripts.Builder
{
  public class GrabbingData : MonoBehaviour
  {
    public GameObject MainObject;
    public GameObject AnimationExtension;
    public GameObject DissolveExtension;

    private void OnValidate() => 
      MainObject = gameObject;
  }
}