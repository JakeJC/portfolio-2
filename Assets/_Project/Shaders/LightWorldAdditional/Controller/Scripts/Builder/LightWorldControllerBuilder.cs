using System.Collections.Generic;
using _Project.Shaders.LightWorldAdditional.Controller.Scripts.Datas;
using UnityEngine;
using Zenject;

namespace _Project.Shaders.LightWorldAdditional.Controller.Scripts.Builder
{
  [RequireComponent(typeof(ZenAutoInjecter))]
  public class LightWorldControllerBuilder : MonoBehaviour
  {
    private const string FakeGlowPath = "Fake Glow";
    private const string FakeLightPath = "Fake Light";

    private void OnValidate()
    {
      ZenAutoInjecter zenAutoInjecter = GetComponent<ZenAutoInjecter>();
      zenAutoInjecter.ContainerSource = ZenAutoInjecter.ContainerSources.SceneContext;
    }

    public void Build()
    {
      GrabbingData[] grabbingDatas = GetComponentsInChildren<GrabbingData>();

      List<LightWorldElement> lightWorldElements = new List<LightWorldElement>();

      foreach (GrabbingData grabbingData in grabbingDatas)
      {
        LightWorldElement lightWorldElement = new LightWorldElement
        {
          GeneralData = new GeneralData(),
          LightData = new LightData(),
          GlowData = new GlowData(),
          DissolveData = new DissolveData()
        };

        lightWorldElement.GeneralData.TargetParent = grabbingData.MainObject.transform;
        if (grabbingData.AnimationExtension)
          lightWorldElement.LightData.AnimationKeeper = grabbingData.AnimationExtension.transform;
        if (grabbingData.DissolveExtension)
          lightWorldElement.DissolveData.Renderer = grabbingData.DissolveExtension.GetComponent<Renderer>();
        else
          lightWorldElement.GlowData.StayGlow = true;

        GameObject fakeGlowObject = Instantiate(Resources.Load<GameObject>(FakeGlowPath), lightWorldElement.GeneralData.TargetParent);
        fakeGlowObject.name = "Fake Glow Object";
        lightWorldElement.GlowData.Renderer = fakeGlowObject.GetComponent<Renderer>();

        GameObject fakeLightObject = Instantiate(Resources.Load<GameObject>(FakeLightPath), lightWorldElement.GeneralData.TargetParent);
        fakeLightObject.name = "Fake Light Object";
        lightWorldElement.LightData.Transform = fakeLightObject.transform;

        lightWorldElements.Add(lightWorldElement);
      }

      LightWorldController lightWorldController = gameObject.AddComponent<LightWorldController>();
      lightWorldController.SetElements(lightWorldElements.ToArray());

      DestroyImmediate(this);
    }
  }
}