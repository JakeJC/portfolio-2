using UnityEditor;
using UnityEngine;

namespace _Project.Shaders.LightWorldAdditional.Controller.Scripts.Builder.Editor
{
  #if UNITY_EDITOR
  [CustomEditor(typeof(LightWorldControllerBuilder))]
  public class LightWorldControllerBuilderEditor : UnityEditor.Editor
  {
    public override void OnInspectorGUI()
    {
      LightWorldControllerBuilder script = target as LightWorldControllerBuilder;

      if (GUILayout.Button("Build"))
        script.Build();
    }
  }
  #endif
}