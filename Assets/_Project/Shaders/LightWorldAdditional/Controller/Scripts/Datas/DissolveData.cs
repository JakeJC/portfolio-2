using UnityEngine;

namespace _Project.Shaders.LightWorldAdditional.Controller.Scripts.Datas
{
  [System.Serializable]
  public class DissolveData
  {
    public Renderer Renderer; 
    public Material Material => Renderer == null ? null : Renderer.material;

    [Range(0, 2)] public float LightRadius = 0.2f;
  }
}