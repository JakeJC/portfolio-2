using UnityEngine;

namespace _Project.Shaders.LightWorldAdditional.Controller.Scripts.Datas
{
  [System.Serializable]
  public class GlowData
  {
    public Renderer Renderer;
    public Material Material => Renderer.material;

    [Range(0, 2)] public float LightRadius = 0.2f;

    [Tooltip("Позволяет не выключать Glow Object при смене скина на Dissolve")]
    public bool StayGlow;
  }
}