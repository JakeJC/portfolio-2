using UnityEngine;

namespace _Project.Shaders.LightWorldAdditional.Controller.Scripts.Datas
{
  [System.Serializable]
  public class LightData
  {
    public Transform Transform;
    public Color Color = Color.cyan;
    public Transform AnimationKeeper;
  }
}