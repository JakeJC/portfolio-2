using UnityEngine;

namespace _Project.Shaders.LightWorldAdditional.Controller.Scripts.Datas
{
  [System.Serializable]
  public class GeneralData
  {
    [Tooltip("Не позволяет менять цвет через ColorScheme")]
    public bool SpecialColorScheme;
    
    public Transform TargetParent;
  }
}