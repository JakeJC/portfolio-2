using System;
using _Project.Scripts.MonoBehaviours.Game;
using _Project.Scripts.Services.MaterialService.Data;
using _Project.Scripts.Services.MaterialService.Interfaces;
using _Project.Shaders.LightWorldAdditional.Scripts;
using UnityEngine;
using Zenject;

namespace _Project.Shaders.LightWorldAdditional.Controller.Scripts
{
  public class LightWorldController : MonoBehaviour, IColorProvider
  {
    public enum Axis
    {
      x,
      y,
      z
    }

    private const int BacklightAmountMax = 12; // TODO: дополнительно менять в MatCapImagineInput
    private static readonly int BackLightsID = Shader.PropertyToID("_BackLights");
    private static readonly int BackLightsColorID = Shader.PropertyToID("_BackLightsColors");
    private static readonly int GlowPowerID = Shader.PropertyToID("_GlowPower");
    private static readonly int BackLightsActiveAmountID = Shader.PropertyToID("_BackLightsActiveAmount");
    private static readonly int DissolveAmountID = Shader.PropertyToID("_DissolveAmount");
    private static readonly int GlowMainColor = Shader.PropertyToID("_MainColor");
    private static readonly int GlowSecondColor = Shader.PropertyToID("_SecondColor");
    private static readonly int DissolveOuterEdgeColor = Shader.PropertyToID("_OuterEdgeColor");
    private static readonly int DissolveInnerEdgeColor = Shader.PropertyToID("_InnerEdgeColor");
    private static readonly int DissolveGlowColor = Shader.PropertyToID("_DissolveGlowColor");
    private static readonly int DissolveMap = Shader.PropertyToID("_DissolveMap");

    public bool IsDemo;
    [Range(0, 1)] public float LightMaxValue = 1.0f; 
    
    public Axis ExtractAnimationAxis = Axis.z;
    public Animator MechanismAnimator;
    public AnimationCurve DissolveChangingCurve;

    [SerializeField] private LightWorldElement[] Elements;

    private bool _glowIsActive;

    private IMaterialService _materialService;

    private bool _manualInitialize;
    
    private void OnValidate()
    {
      if (MechanismAnimator == null)
        MechanismAnimator = GetComponentInChildren<Animator>();

      if (Elements == null)
        return;

      if (Elements.Length > BacklightAmountMax)
        Debug.LogError($"Amount of Fake Lights must be less than {BacklightAmountMax}");

      foreach (LightWorldElement element in Elements)
      {
        if (element.LightData != null && element.LightData.Transform != null)
          element.Name = element.LightData.Transform.gameObject.name;
        else
          element.Name = "Element";
      }
    }

    private void Awake()
    {
      if (_manualInitialize)
        return;

      CreateMaterialInstances();
    }

    private void Start()
    {  
      if (_manualInitialize)
        return;

      RenameGameObjects();
      
      if(IsDemo)
        SwitchToGlow();

      OnStartSpawn();
      GetComponent<Mechanism>().OnLaunchAddListener(OnLaunch);
    }

    private void OnDestroy() => 
      GetComponent<Mechanism>().OnLaunchRemoveListener(OnLaunch);

    private void Update() =>
      SetData();

    [Inject]
    public void Construct(IMaterialService materialService) => 
      _materialService = materialService;

    public void ForceInitialize()
    {
      _manualInitialize = true;
      Awake();
      Start();
    }

    public void SetElements(LightWorldElement[] elements) =>
      Elements = elements;

    public void SetColor(ColorData colorData, bool dependsOnMaterial)
    {
      foreach (LightWorldElement element in Elements)
      {
        if (element.GeneralData.SpecialColorScheme)
          Set(element, colorData.SpecialLightWorldColorData);
        else
          Set(element, colorData.LightWorldColorData);
      }

      if (_materialService.Material.GetAllMaterialsScheme()[_materialService.Material.GetCurrentSchemeIndex()].IsDissolve)
        SwitchToDissolve();
      else
        SwitchToGlow();

      void Set(LightWorldElement element, LightWorldColorData lightWorldColorData)
      {
        element.LightData.Color = lightWorldColorData.FakeLightColor;

        if (element.GlowData.Renderer && element.GlowData.Material)
        {
          element.GlowData.Material.SetColor(GlowMainColor, lightWorldColorData.GlowColor.MainColor);
          element.GlowData.Material.SetColor(GlowSecondColor, lightWorldColorData.GlowColor.SecondColor);
        }

        if (element.DissolveData.Renderer && element.DissolveData.Material)
        {
          element.DissolveData.Material.SetColor(DissolveOuterEdgeColor, lightWorldColorData.DissolveColor.OuterEdgeColor);
          element.DissolveData.Material.SetColor(DissolveInnerEdgeColor, lightWorldColorData.DissolveColor.InnerEdgeColor);
          element.DissolveData.Material.SetColor(DissolveGlowColor, lightWorldColorData.DissolveColor.GlowColor);
          element.DissolveData.Material.SetTexture(DissolveMap, lightWorldColorData.DissolveColor.Texture);
        }
      }
    }

    public void SwitchToGlow()
    {
      foreach (LightWorldElement element in Elements)
      {
        if (element.GlowData.Renderer != null)
          element.GlowData.Renderer.enabled = true;

        if (element.DissolveData.Renderer != null)
          element.DissolveData.Renderer.enabled = false;
      }

      _glowIsActive = true;
    }

    public void SwitchToDissolve()
    {
      foreach (LightWorldElement element in Elements)
      {
        if (element.GlowData.StayGlow == false)
          if (element.GlowData.Renderer != null)
            element.GlowData.Renderer.enabled = false;

        if (element.DissolveData.Renderer != null)
          element.DissolveData.Renderer.enabled = true;
      }

      _glowIsActive = false;
    }

    private void SetActiveDissolveObjects(bool value)
    {
      foreach (LightWorldElement element in Elements)
      {
        if (element.DissolveData.Renderer != null)
          element.DissolveData.Renderer.gameObject.SetActive(value);
      }
    }

    private void CreateMaterialInstances()
    {
      foreach (LightWorldElement element in Elements)
      {
        if (element.GlowData.Renderer)
          element.GlowData.Renderer.material = new Material(element.GlowData.Renderer.material);
        if (element.DissolveData.Renderer)
          element.DissolveData.Renderer.material = new Material(element.DissolveData.Renderer.material);
      }
    }

    [ContextMenu("Set Parents")]
    private void SetParents()
    {
      foreach (LightWorldElement element in Elements)
      {
        element.LightData.Transform.parent = element.GeneralData.TargetParent;
        element.GlowData.Renderer.gameObject.transform.parent = element.GeneralData.TargetParent;
      }
    }

    private void RenameGameObjects()
    {
      foreach (LightWorldElement element in Elements)
      {
        if (element.DissolveData.Renderer)
          element.DissolveData.Renderer.gameObject.name = $"{element.GeneralData.TargetParent.gameObject.name} Dissolve";

        if (element.LightData.AnimationKeeper)
          element.LightData.AnimationKeeper.gameObject.name = $"Light Animation Keeper";
      }
    }

    private void SetData()
    {
      if (Elements == null)
        return;

      Vector4[] points = new Vector4[BacklightAmountMax];
      Vector4[] colors = new Vector4[BacklightAmountMax];

      for (int i = 0; i < BacklightAmountMax; i++)
      {
        if (i < Elements.Length)
        {
          if (Elements[i].LightData.Transform == null)
            continue;

          SetLightPoints(i, ref points);
          SetLightColors(i, ref colors);

          if (_glowIsActive)
            SetFakeGlowPower(i, false);
          else
          {
            SetFakeGlowPower(i, true);
            SetDissolveAmount(i);
          }
        }
      }

      SetDataToShader(points, colors);
    }

    private void SetLightPoints(int i, ref Vector4[] points)
    {
      Vector3 lightPosition = Elements[i].LightData.Transform.position;
      float lightRadius = _glowIsActive ? Elements[i].GlowData.LightRadius : Elements[i].DissolveData.LightRadius;
      points[i] = new Vector4(lightPosition.x, lightPosition.y, lightPosition.z, lightRadius);
    }

    private void SetLightColors(int i, ref Vector4[] colors)
    {
      if (MechanismAnimator && Application.isPlaying)
      {
        if (Elements[i].LightData.AnimationKeeper == null)
          Elements[i].LightData.Color.a = 1;
        else
          switch (ExtractAnimationAxis)
          {
            case Axis.x:
              Elements[i].LightData.Color.a = Mathf.Clamp01(Elements[i].LightData.AnimationKeeper.localScale.x);
              break;
            case Axis.y:
              Elements[i].LightData.Color.a = Mathf.Clamp01(Elements[i].LightData.AnimationKeeper.localScale.y);
              break;
            case Axis.z:
              Elements[i].LightData.Color.a = Mathf.Clamp01(Elements[i].LightData.AnimationKeeper.localScale.z);
              break;
          }
        
        if(IsDemo)
          Elements[i].LightData.Color.a = LightMaxValue;
        
        if(LightWorldDarknessSetter.IsSpawn)
          Elements[i].LightData.Color.a = 0;

        Elements[i].LightData.Color.a = Mathf.Clamp( Elements[i].LightData.Color.a, 0, LightMaxValue);
      }

      colors[i] = Elements[i].LightData.Color;
    }

    private void SetFakeGlowPower(int i, bool stayOnly)
    {
      if (stayOnly)
        if (!Elements[i].GlowData.StayGlow)
          return;

      if (Elements[i].GlowData.Material != null)
        Elements[i].GlowData.Material.SetFloat(GlowPowerID, Elements[i].LightData.Color.a);
    }

    private void SetDissolveAmount(int i)
    {
      if (Elements[i].DissolveData.Material != null)
        Elements[i].DissolveData.Material.SetFloat(DissolveAmountID, DissolveChangingCurve.Evaluate(Elements[i].LightData.Color.a));
    }

    private void SetDataToShader(Vector4[] points, Vector4[] colors)
    {
      Shader.SetGlobalInt(BackLightsActiveAmountID, Mathf.Clamp(Elements.Length, 0, BacklightAmountMax));
      Shader.SetGlobalVectorArray(BackLightsID, points);
      Shader.SetGlobalVectorArray(BackLightsColorID, colors);
    }

    private void OnStartSpawn()
    {
      LightWorldDarknessSetter.IsSpawn = true;
      SetActiveDissolveObjects(false);
    }

    private void OnLaunch()
    {
      LightWorldDarknessSetter.IsSpawn = false;
      SetActiveDissolveObjects(true);
    }
  }
}