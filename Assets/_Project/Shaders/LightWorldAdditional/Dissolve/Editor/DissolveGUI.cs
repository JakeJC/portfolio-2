using UnityEditor;
using UnityEngine;

namespace _Project.Shaders.LightWorldAdditional.Dissolve.Editor
{
  public class DissolveGUI : ShaderGUI
  {
    private Material _material;
    private MaterialEditor _editor;
    private MaterialProperty[] _properties;

    public override void OnGUI(MaterialEditor materialEditor, MaterialProperty[] properties)
    {
      _editor = materialEditor;
      _properties = properties;
      _material = materialEditor.target as Material;
      
      if (_material != null) 
        _material.enableInstancing = true;

      DrawURPSettings();
      GUILayout.Space(10);
      Dissolve();
    }

    private void Dissolve()
    {
      if (EditorGUILayout.BeginFoldoutHeaderGroup(true, "[Dissolve]"))
      {
        ShaderGUIHelper.DrawTexture(FindProperty("_DissolveMap", _properties), _editor, "Dissolve Map");
        ShaderGUIHelper.DrawTexture(FindProperty("_DirectionMap", _properties), _editor, "Direction Map");
        ShaderGUIHelper.DrawTexture(FindProperty("_DissolveMask", _properties), _editor, "Dissolve Mask");
        ShaderGUIHelper.DrawShaderProperty(FindProperty("_DissolveAmount", _properties), _editor);
        ShaderGUIHelper.DrawShaderProperty(FindProperty("_DissolveDelay", _properties), _editor);
        
        GUILayout.Space(3);
        ShaderGUIHelper.DrawShaderProperty(FindProperty("_OuterEdgeColor", _properties), _editor);
        ShaderGUIHelper.DrawShaderProperty(FindProperty("_InnerEdgeColor", _properties), _editor);
        ShaderGUIHelper.DrawShaderProperty(FindProperty("_EdgeThickness", _properties), _editor);
        ShaderGUIHelper.DrawShaderProperty(FindProperty("_DissolveRampUp", _properties), _editor);
        
        GUILayout.Space(3);
        ShaderGUIHelper.DrawToggle(FindProperty("_DissolveGlow", _properties), _material, "_DISSOLVEGLOW_ON", "EnabledDissolveGlow", out bool use);

        if (use)
        {
          ShaderGUIHelper.DrawShaderProperty(FindProperty("_DissolveGlowColor", _properties), _editor);
          ShaderGUIHelper.DrawShaderProperty(FindProperty("_DissolveGlowIntensity", _properties), _editor);
        }
      }

      EditorGUILayout.EndFoldoutHeaderGroup();
    }

    private void DrawURPSettings()
    {
      bool alphaClip = false;
      if (_material.HasProperty("_AlphaClip"))
      {
        alphaClip = _material.GetFloat("_AlphaClip") >= 0.5;
      }

      if (alphaClip)
      {
        _material.EnableKeyword("_ALPHATEST_ON");
      }
      else
      {
        _material.DisableKeyword("_ALPHATEST_ON");
      }

      if (_material.HasProperty("_Surface"))
      {
        EditorGUI.BeginChangeCheck();
        var surfaceProp = FindProperty("_Surface", _properties);
        EditorGUI.showMixedValue = surfaceProp.hasMixedValue;
        var surfaceType = (SurfaceType)surfaceProp.floatValue;

        surfaceType = (SurfaceType)EditorGUILayout.EnumPopup("Surface Type", surfaceType);
        if (EditorGUI.EndChangeCheck())
        {
          _editor.RegisterPropertyChangeUndo("Surface Type");
          surfaceProp.floatValue = (float)surfaceType;
        }

        if (surfaceType == SurfaceType.Opaque)
        {
          if (alphaClip)
          {
            _material.renderQueue = (int)UnityEngine.Rendering.RenderQueue.AlphaTest;
            _material.SetOverrideTag("RenderType", "TransparentCutout");
          }
          else
          {
            _material.renderQueue = (int)UnityEngine.Rendering.RenderQueue.Geometry;
            _material.SetOverrideTag("RenderType", "Opaque");
          }

          _material.renderQueue +=
            _material.HasProperty("_QueueOffset") ? (int)_material.GetFloat("_QueueOffset") : 0;
          _material.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.One);
          _material.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.Zero);
          _material.SetInt("_ZWrite", 1);
          _material.DisableKeyword("_ALPHAPREMULTIPLY_ON");
          _material.SetShaderPassEnabled("ShadowCaster", true);
        }
        else // Transparent
        {
          BlendMode blendMode = (BlendMode)_material.GetFloat("_Blend");

          // Specific Transparent Mode Settings
          switch (blendMode)
          {
            case BlendMode.Alpha:
              _material.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.SrcAlpha);
              _material.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
              _material.DisableKeyword("_ALPHAPREMULTIPLY_ON");
              break;
            case BlendMode.Premultiply:
              _material.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.One);
              _material.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
              _material.EnableKeyword("_ALPHAPREMULTIPLY_ON");
              break;
            case BlendMode.Additive:
              _material.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.SrcAlpha);
              _material.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.One);
              _material.DisableKeyword("_ALPHAPREMULTIPLY_ON");
              break;
            case BlendMode.Multiply:
              _material.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.DstColor);
              _material.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.Zero);
              _material.DisableKeyword("_ALPHAPREMULTIPLY_ON");
              _material.EnableKeyword("_ALPHAMODULATE_ON");
              break;
          }

          // General Transparent Material Settings
          _material.SetOverrideTag("RenderType", "Transparent");
          _material.SetInt("_ZWrite", 0);
          _material.renderQueue = (int)UnityEngine.Rendering.RenderQueue.Transparent;
          _material.renderQueue +=
            _material.HasProperty("_QueueOffset") ? (int)_material.GetFloat("_QueueOffset") : 0;
          _material.SetShaderPassEnabled("ShadowCaster", false);
        }

        // DR: draw popup.
        if (surfaceType == SurfaceType.Transparent && _material.HasProperty("_Blend"))
        {
          EditorGUI.BeginChangeCheck();
          var blendModeProp = FindProperty("_Blend", _properties);
          EditorGUI.showMixedValue = blendModeProp.hasMixedValue;
          var blendMode = (BlendMode)blendModeProp.floatValue;
          blendMode = (BlendMode)EditorGUILayout.EnumPopup("Blend Mode", blendMode);
          if (EditorGUI.EndChangeCheck())
          {
            _editor.RegisterPropertyChangeUndo("Blend Mode");
            blendModeProp.floatValue = (float)blendMode;
          }
        }
      }

      // DR: draw popup.
      if (_material.HasProperty("_Cull"))
      {
        EditorGUILayout.Separator();
        EditorGUI.BeginChangeCheck();
        var cullingProp = FindProperty("_Cull", _properties);
        EditorGUI.showMixedValue = cullingProp.hasMixedValue;
        var culling = (RenderFace)cullingProp.floatValue;
        culling = (RenderFace)EditorGUILayout.EnumPopup("Render Faces", culling);
        if (EditorGUI.EndChangeCheck())
        {
          _editor.RegisterPropertyChangeUndo("Render Faces");
          cullingProp.floatValue = (float)culling;
          _material.doubleSidedGI = (RenderFace)cullingProp.floatValue != RenderFace.Front;
        }
      }

      /*if (_material.HasProperty("_AlphaClip"))
      {
        EditorGUILayout.Separator();
        EditorGUI.BeginChangeCheck();
        var alphaClipProp = FindProperty("_AlphaClip", _properties);
        EditorGUI.showMixedValue = alphaClipProp.hasMixedValue;
        var alphaClipEnabled = EditorGUILayout.Toggle("Alpha Clipping", alphaClipProp.floatValue == 1);
        if (EditorGUI.EndChangeCheck())
          alphaClipProp.floatValue = alphaClipEnabled ? 1 : 0;
        EditorGUI.showMixedValue = false;

        if (alphaClipProp.floatValue == 1 && _material.HasProperty("_Cutoff"))
        {
          var alphaCutoffProp = FindProperty("_Cutoff", _properties);
          _editor.ShaderProperty(alphaCutoffProp, "Threshold", 1);
        }
      }*/
    }
  }
}