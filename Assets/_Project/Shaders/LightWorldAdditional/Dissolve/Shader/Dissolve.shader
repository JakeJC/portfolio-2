Shader "Universal Render Pipeline/Custom/Dissolve"
{
    Properties
    {
        // -------------------------------------
        // Main
        [MainColor] _MainColor("Main Color", Color) = (1,1,1,1)
        [MainTexture] _BaseMap("Albedo", 2D) = "white" {}

        // -------------------------------------
        // Dissolve Settings
        // General
        _DissolveMap ("Dissolve Map", 2D) = "white" {}
        _TilingX("X", Float) = 1.0
        _TilingY("Y", Float) = 1.0
        _DirectionMap ("Direction Map", 2D) = "white" {}
        _DissolveMask ("Dissolve Mask", 2D) = "white" {}
        _DissolveAmount ("Dissolve Amount", Range(0, 1.0)) = 0.5
        _DissolveDelay ("Dissolve Delay", Range(0, 1.0)) = 0.2

        // Edge color
        _OuterEdgeColor ("Outer Edge Color", Color) = (1,0,0,1)
        _InnerEdgeColor ("Inner Edge Color", Color) = (1,1,0,1)
        _EdgeThickness ("Edge Thickness", Range(0.0, 1.0)) = 0.1
        _DissolveRampUp ("Color Factor", Range(0.0, 3.0)) = 1

        // Glow
        [Toggle(_DISSOLVEGLOW_ON)] _DissolveGlow ("Dissolve Glow", Int) = 1
        _DissolveGlowColor ("Glow Color", Color) = (1,0.5,0,1)
        _DissolveGlowIntensity ("Glow Intensity", Range(0.0, 1.0)) = 0.5

        // -------------------------------------
        // Hidden properties - Generic
        [HideInInspector] _Surface("__surface", Float) = 0.0
        [HideInInspector] _Blend("__mode", Float) = 0.0
        [HideInInspector] _AlphaClip("__clip", Float) = 0.0
        [HideInInspector] _BlendOp("__blendop", Float) = 0.0
        [HideInInspector] _SrcBlend("__src", Float) = 1.0
        [HideInInspector] _DstBlend("__dst", Float) = 0.0
        [HideInInspector] _ZWrite("__zw", Float) = 1.0
        [HideInInspector] _Cull("__cull", Float) = 2.0

        // -------------------------------------
        // SphereClip
        [Toggle(SPHERECLIP_ON)] _UseSphereClip("Use sphere clip", Int) = 0
        [Toggle(SPHERECLIPSHADOW_ON)] _UseSphereClipShadow("Use sphere clip", Int) = 0
    }
    SubShader
    {
        Tags
        {
            "RenderType" = "Opaque" "IgnoreProjector" = "True" "PreviewType" = "Plane" "PerformanceChecks" = "False" "RenderPipeline" = "UniversalPipeline"
        }
        LOD 100

        BlendOp[_BlendOp]
        Blend[_SrcBlend][_DstBlend]
        ZWrite[_ZWrite]
        Cull[_Cull]
        ColorMask RGB

        Pass
        {
            Name "Unlit MatCap"

            HLSLPROGRAM
            #pragma exclude_renderers gles gles3 glcore
            #pragma enable_d3d11_debug_symbols
            #pragma target 2.0

            // -------------------------------------
            // Custom Keywords
            #pragma shader_feature _DISSOLVEGLOW_ON

            #pragma shader_feature SPHERECLIP_ON
            #pragma shader_feature SPHERECLIPSHADOW_ON

            //--------------------------------------
            // GPU Instancing Keywords
            #pragma multi_compile_instancing

            //--------------------------------------
            // Functions
            #pragma vertex VertexFunc
            #pragma fragment FragmentFunc

            //--------------------------------------
            // Includes
            #include "DissolveInput.hlsl"
            #include "DissolveForwardPass.hlsl"
            ENDHLSL
        }
    }

    FallBack "Hidden/Universal Render Pipeline/FallbackError"
    CustomEditor "_Project.Shaders.LightWorldAdditional.Dissolve.Editor.DissolveGUI"
}