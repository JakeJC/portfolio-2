#ifndef UNIVERSAL_CUSTOM_DISSOLVE_INPUT_INCLUDED
#define UNIVERSAL_CUSTOM_DISSOLVE_INPUT_INCLUDED

//--------------------------------------
// Includes
#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"

CBUFFER_START(UnityPerMaterial)

half4 _MainColor;
float4 _BaseMap_ST;

half _DissolveGlowIntensity;
half4 _DissolveGlowColor;
half _DissolveRampUp;
half _DissolveDelay;
sampler2D _DissolveMap;
sampler2D _DissolveMask;
sampler2D _DirectionMap;
half _DissolveAmount;

half3 _OuterEdgeColor;
half3 _InnerEdgeColor;
half _EdgeThickness;

float _TilingX;
float _TilingY;

CBUFFER_END


// -------------------------------------
// Globals
uniform half _GLOBALSphereRadius;
uniform half3 _GLOBALSpherePosition;
uniform half _GLOBALSphereFade;

TEXTURE2D(_BaseMap);
SAMPLER(sampler_BaseMap);
#endif