//--------------------------------------
// Includes
#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"
#include "DissolveCoreFunctions.hlsl"

///////////////////////////////////////////////////////////////////////////////
//                                 Structs                                   //
///////////////////////////////////////////////////////////////////////////////
struct Attributes // CHANGE data types
{
    float4 positionOS : POSITION;
    float3 normalOS : NORMAL;
    half4 tangentOS : TANGENT;
    float2 texcoord : TEXCOORD0;

    UNITY_VERTEX_INPUT_INSTANCE_ID
};

struct Varyings // CHANGE data types
{
    float4 positionCS : SV_POSITION;
    float2 positionWS : TEXCOORD0;
    half3 normalWS : TEXCOORD1;
    half3 viewDirWS : TEXCOORD2;
    float2 texcoord: TEXCOORD3;

    UNITY_VERTEX_INPUT_INSTANCE_ID
};

///////////////////////////////////////////////////////////////////////////////
//                  Vertex Geometry Fragment                                 //
///////////////////////////////////////////////////////////////////////////////

Varyings VertexFunc(Attributes input)
{
    Varyings output = (Varyings)0;

    UNITY_SETUP_INSTANCE_ID(input);
    UNITY_TRANSFER_INSTANCE_ID(input, output);

    VertexPositionInputs vertexInput = GetVertexPositionInputs(input.positionOS.xyz);
    VertexNormalInputs normalInput = GetVertexNormalInputs(input.normalOS, input.tangentOS);

    output.positionCS = vertexInput.positionCS;
    output.positionWS = vertexInput.positionWS;
    output.normalWS = normalInput.normalWS;
    output.viewDirWS = GetWorldSpaceViewDir(vertexInput.positionWS);
    output.texcoord = TRANSFORM_TEX(input.texcoord, _BaseMap);

    return output;
}

half4 FragmentFunc(Varyings input) : SV_Target
{
    UNITY_SETUP_INSTANCE_ID(input);

    half oneMinusReflectivity = 0.5;
    half4 color = _MainColor;
    color.rgb = Dissolve(color, input.texcoord, oneMinusReflectivity);
    color.rgb += DissolveEmission(input.texcoord);

    // -------------------------------------
    // Sphere Clip
    #if(SPHERECLIP_ON) // TODO: из-за специфики рематериала (Imagine Shader Only) лучше сделать просто lerp между двумя цветами


    half dist = distance(_GLOBALSpherePosition, input.positionWS);
    half sphere = /*1 - */ saturate((dist - _GLOBALSphereRadius) / _GLOBALSphereFade);

    clip(sphere - 0.1);
    #endif

    color.a = GetGrayScale(color.rgb);

    return color;
}
