Shader "Universal Render Pipeline/Custom/Fake Glow Effect"
{
    Properties
    {
        // -------------------------------------
        // Main
        _MainColor("Main Color", Color) = (1,1,1,1)
        _SecondColor("Second Color", Color) = (1,1,1,1)
        _RimAmount("Rim Amount", Range(0,1)) = 0.0
        _RimFactorAlpha("Rim Factor Alpha", Range(0.01 ,7)) = 0.0
        _GlowPower("Glow Power", Range(0 , 1)) = 1.0

        // -------------------------------------
        // Hidden properties - Generic
        [HideInInspector] _Surface("__surface", Float) = 0.0
        [HideInInspector] _Blend("__mode", Float) = 0.0
        [HideInInspector] _AlphaClip("__clip", Float) = 0.0
        [HideInInspector] _BlendOp("__blendop", Float) = 0.0
        [HideInInspector] _SrcBlend("__src", Float) = 1.0
        [HideInInspector] _DstBlend("__dst", Float) = 0.0
        [HideInInspector] _ZWrite("__zw", Float) = 1.0
        [HideInInspector] _Cull("__cull", Float) = 2.0
        
        // SphereClip
        [Toggle(SPHERECLIP_ON)] _UseSphereClip("Use sphere clip", Int) = 0
        [Toggle(SPHERECLIPSHADOW_ON)] _UseSphereClipShadow("Use sphere clip", Int) = 0
    }
    SubShader
    {
        Tags
        {
            "RenderType" = "Opaque" "IgnoreProjector" = "True" "PreviewType" = "Plane" "PerformanceChecks" = "False" "RenderPipeline" = "UniversalPipeline"
        }
        LOD 100

        BlendOp[_BlendOp]
        Blend[_SrcBlend][_DstBlend]
        ZWrite[_ZWrite]
        Cull[_Cull]
        ColorMask RGB

        Pass
        {
            Name "Unlit MatCap"

            HLSLPROGRAM
            #pragma exclude_renderers gles gles3 glcore
            #pragma enable_d3d11_debug_symbols
            #pragma target 2.0

            // -------------------------------------
            // Custom Keywords
            #pragma shader_feature SPHERECLIP_ON
            #pragma shader_feature SPHERECLIPSHADOW_ON
            
            //--------------------------------------
            // GPU Instancing Keywords
            #pragma multi_compile_instancing

            //--------------------------------------
            // Functions
            #pragma vertex VertexFunc
            #pragma fragment FragmentFunc

            //--------------------------------------
            // Includes
            #include "FakeGlowEffectInput.hlsl"
            #include "FakeGlowEffectForwardPass.hlsl"
            ENDHLSL
        }
    }

    FallBack "Hidden/Universal Render Pipeline/FallbackError"
    CustomEditor "_Project.Shaders.LightWorldAdditional.FakeGlow.Editor.FakeGlowEffectGUI"
}