//--------------------------------------
// Includes
#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"

///////////////////////////////////////////////////////////////////////////////
//                                 Structs                                   //
///////////////////////////////////////////////////////////////////////////////
struct Attributes // CHANGE data types
{
    float4 positionOS : POSITION;
    float3 normalOS : NORMAL;
    half4 tangentOS : TANGENT;
    half2 texcoord : TEXCOORD0;

    UNITY_VERTEX_INPUT_INSTANCE_ID
};

struct Varyings // CHANGE data types
{
    float4 positionCS : SV_POSITION;
    float2 positionWS : TEXCOORD0;
    half3 normalWS : TEXCOORD1;
    half3 viewDirWS : TEXCOORD2;
    float2 texcoord: TEXCOORD3;

    UNITY_VERTEX_INPUT_INSTANCE_ID
};


///////////////////////////////////////////////////////////////////////////////
//                  Vertex Geometry Fragment                                 //
///////////////////////////////////////////////////////////////////////////////

half GetGrayScale(half3 target)
{
    return dot(target, half3(0.3, 0.59, 0.11));
}

half3 GetRimIntensity(half3 viewDirectionWS, half3 normalWS, half rimAmount)
{
    half3 rimDot = 1 - dot(normalize(viewDirectionWS), normalize(normalWS));
    return smoothstep(1 - rimAmount - 0.21, 1 - rimAmount + 0.21, rimDot);
}

Varyings VertexFunc(Attributes input)
{
    Varyings output = (Varyings)0;

    UNITY_SETUP_INSTANCE_ID(input);
    UNITY_TRANSFER_INSTANCE_ID(input, output);

    VertexPositionInputs vertexInput = GetVertexPositionInputs(input.positionOS.xyz);
    VertexNormalInputs normalInput = GetVertexNormalInputs(input.normalOS, input.tangentOS);

    output.positionCS = vertexInput.positionCS;
    output.positionWS = vertexInput.positionWS;
    output.texcoord = input.texcoord;
    output.normalWS = normalInput.normalWS;
    output.viewDirWS = GetWorldSpaceViewDir(vertexInput.positionWS);

    return output;
}

half4 FragmentFunc(Varyings input) : SV_Target
{
    UNITY_SETUP_INSTANCE_ID(input);

    // -------------------------------------
    // Sphere Clip
    #if(SPHERECLIP_ON) // TODO: из-за специфики рематериала (Imagine Shader Only) лучше сделать просто lerp между двумя цветами
    half dist = distance(_GLOBALSpherePosition, input.positionWS);
    half sphere = /*1 - */ saturate((dist - _GLOBALSphereRadius) / _GLOBALSphereFade);

    clip(sphere - 0.1);
    #endif
    
    float rimIntensity = GetRimIntensity(input.viewDirWS, input.normalWS, _RimAmount + (1 - _GlowPower));
    
    half4 color = lerp(_MainColor, _SecondColor, rimIntensity);
    color.a = lerp(1, 0, pow(rimIntensity, _RimFactorAlpha * (1 - _RimAmount))) * _GlowPower;

    return color;
}
