//--------------------------------------
// Includes
#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"

CBUFFER_START(UnityPerMaterial)

half4 _MainColor;
half4 _SecondColor;
float _RimAmount;
float _RimFactorAlpha;
float _GlowPower;

CBUFFER_END

// -------------------------------------
// Globals
uniform half _GLOBALSphereRadius;
uniform half3 _GLOBALSpherePosition;
uniform half _GLOBALSphereFade;
