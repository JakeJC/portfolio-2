using System.IO;
using Sirenix.OdinInspector;
using TMPro;
using UnityEditor;
using UnityEngine;


[CreateAssetMenu(fileName = "TMPConverter", menuName = "Create TMPConverter/TMPConverter", order = 0)]
public class TMPConverter : ScriptableObject
{
  [SerializeField] private Object _targetDirectory;
  [SerializeField] private Font _font;

  [Button]
  public void Convert() =>
    CreateTextMeshProAsset(_font, AssetDatabase.GetAssetPath(_targetDirectory));

  private void CreateTextMeshProAsset(Font baseFont, string path)
  {
    TMP_FontAsset tmpFontAsset = TMP_FontAsset.CreateFontAsset(baseFont);
    string assetFonPath = Path.Combine(path, baseFont.name + ".asset");
    AssetDatabase.CreateAsset(tmpFontAsset, assetFonPath);
  }
}